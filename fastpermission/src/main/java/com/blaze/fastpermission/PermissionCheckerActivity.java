package com.blaze.fastpermission;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import rebus.permissionutils.AskagainCallback;
import rebus.permissionutils.FullCallback;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;

public class PermissionCheckerActivity extends AppCompatActivity implements FullCallback {
    private static int enterAnim = android.R.anim.fade_in;
    private static int exitAnim = android.R.anim.fade_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission_checker);
        int[] ids = getIntent().getIntArrayExtra("permissions");
        PermissionEnum[] permissions = new PermissionEnum[ids.length];
        for (int i = 0; i < ids.length; i++) {
            permissions[i] = PermissionEnum.values()[ids[i]];
        }
        PermissionManager.with(this)
                .permission(permissions)
                .askagain(true)
                .askagainCallback(this::showDialog)
                .callback((permissionsGranted, permissionsDenied, permissionsDeniedForever, permissionsAsked) -> {
                    setResult(RESULT_OK);
                    finish();
                    overridePendingTransition(0, exitAnim);
                })
                .ask();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.handleResult(requestCode, permissions, grantResults);
    }

    @Override
    public void result(ArrayList<PermissionEnum> permissionsGranted, ArrayList<PermissionEnum> permissionsDenied, ArrayList<PermissionEnum> permissionsDeniedForever, ArrayList<PermissionEnum> permissionsAsked) {

    }

    private void showDialog(final AskagainCallback.UserResponse response) {
        new AlertDialog.Builder(this)
                .setTitle("Permission needed")
                .setMessage("This app really need to use this permission, you wont to authorize it?")
                .setPositiveButton("OK", (dialogInterface, i) -> response.result(true))
                .setNegativeButton("NOT NOW", (dialogInterface, i) -> response.result(false))
                .show();
    }
}

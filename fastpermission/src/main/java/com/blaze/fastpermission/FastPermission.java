package com.blaze.fastpermission;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionUtils;

/**
 * Created by slava on 31.10.16.
 */

public class FastPermission {
    private static int enterAnim = android.R.anim.fade_in;
    private static int exitAnim = android.R.anim.fade_out;

    public static void request(AppCompatActivity activity, int requestCode,
                               OnPermissionGrantedListener listener,
                               PermissionEnum... permissions) {
        request(activity, listener,
                (intent)-> activity.startActivityForResult(intent, requestCode), permissions);
                activity.overridePendingTransition(enterAnim, exitAnim);

    }

    public static void request(Fragment fragment, int requestCode,
                               OnPermissionGrantedListener listener,
                               PermissionEnum... permissions) {
        request(fragment.getActivity(), listener,
                (intent)-> fragment.startActivityForResult(intent, requestCode), permissions);
    }

    public static void request(Context context, OnPermissionGrantedListener listener,
                               PermissionEnum... permissions) {
        request(context, listener, (intent)->{
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            ((AppCompatActivity) context).overridePendingTransition(enterAnim, exitAnim);
        }, permissions);
    }

    private static void request(Context context, OnPermissionGrantedListener listener, ActivityLauncher launcher, PermissionEnum... permissions) {
        if(PermissionUtils.isGranted(context, permissions)) {
            if(listener != null) listener.onPermissionGranted();
        } else {
            Intent intent = new Intent(context, PermissionCheckerActivity.class);
            int[] permissionsIDs = new int[permissions.length];
            for (int i = 0; i < permissionsIDs.length; i++) {
                permissionsIDs[i] = permissions[i].ordinal();
            }
            intent.putExtra("permissions", permissionsIDs);
            launcher.launchActivity(intent);
        }
    }

    public static boolean isGranted(Context context, PermissionEnum... permissions) {
        return PermissionUtils.isGranted(context, permissions);
    }

    public interface OnPermissionGrantedListener {
        void onPermissionGranted();
    }

    private interface ActivityLauncher {
        void launchActivity(Intent intent);
    }
}

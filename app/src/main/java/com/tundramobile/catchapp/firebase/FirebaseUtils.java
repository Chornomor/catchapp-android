package com.tundramobile.catchapp.firebase;

import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Sviatoslav Zaitsev on 27.04.17.
 */

public class FirebaseUtils {
    private static final String TAG = FirebaseUtils.class.getSimpleName();
    public static final String MAIL_DOMAIN = "@catchapp.com";

    public static void authorize(String phoneNumber, OnSuccessListener successListener, OnFailureListener failureListener) {
        FirebaseAuth fbInstance = FirebaseAuth.getInstance();
        if(fbInstance.getCurrentUser() != null) {
            successListener.onSuccess((AuthResult) fbInstance::getCurrentUser);
            return;
        }

        String fbEmail    = phoneNumber.trim().replace("+", "") + MAIL_DOMAIN;
        String fbPassword = phoneNumber;
        FirebaseAuth.getInstance().signInWithEmailAndPassword(fbEmail, fbPassword)
                .addOnSuccessListener(authResult -> {
                    // Sign in success, update UI with the signed-in fbUser's information
                    Log.d(TAG, "signInWithEmail:success");
                    successListener.onSuccess(authResult);
                })
                .addOnFailureListener(signInException -> {
                    Log.w(TAG, "signInWithEmail:failure. Trying signUp", signInException);
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(fbEmail, fbPassword)
                            .addOnSuccessListener(authResult -> {
                                // Sign up success, update UI with the signed-in fbUser's information
                                Log.d(TAG, "signUpWithEmail:success");
                                successListener.onSuccess(authResult);
                            })
                            .addOnFailureListener(signUpException -> {
                                // If sign up fails, display a message to the fbUser.
                                Log.w(TAG, "signUpWithEmail:failure", signUpException);
                                failureListener.onFailure(signUpException);
                            });
                });
    }

    public static void signOut() {
        FirebaseAuth.getInstance().signOut();
    }
}

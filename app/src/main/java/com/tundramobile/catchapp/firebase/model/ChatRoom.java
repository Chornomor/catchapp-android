package com.tundramobile.catchapp.firebase.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */
@IgnoreExtraProperties
public class ChatRoom implements Serializable {
    private List<Long> ids;
    private String lastMessageText;
    private Double lastMessageTimestamp;
    private Long ownerId;

    public ChatRoom() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public ChatRoom(List<Long> ids, Long ownerID) {
        this.ids = ids;
        this.ownerId = ownerID;
    }

    public List<Long> getIds() {
        return ids;
    }

    public String getLastMessageText() {
        return lastMessageText;
    }

    public Double getLastMessageTimestamp() {
        return lastMessageTimestamp;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setLastMessageText(String lastMessageText) {
        this.lastMessageText = lastMessageText;
    }

    public void setLastMessageTimestamp(Double lastMessageTimestamp) {
        this.lastMessageTimestamp = lastMessageTimestamp;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ChatRoom) {
            return ((ChatRoom) obj).getIds().equals(this.getIds())
                    && ((ChatRoom) obj).ownerId.equals(ownerId);
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("ChatRoom: \nOwnerID: %s\nParticipants: %s", ownerId, ids);
    }
}
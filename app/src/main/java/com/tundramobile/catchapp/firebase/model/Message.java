package com.tundramobile.catchapp.firebase.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

@IgnoreExtraProperties
public class Message {
    private String chatKey;
    private Long senderId;
    private String senderName;
    private String text;
    private Double timestamp;

    public Message() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public String getChatKey() {
        return chatKey;
    }

    public Long getSenderId() {
        return senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getText() {
        return text;
    }

    public Double getTimestamp() {
        return timestamp;
    }

    public void setChatKey(String chatKey) {
        this.chatKey = chatKey;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }
}

package com.tundramobile.catchapp.firebase;

import com.google.firebase.database.GenericTypeIndicator;
import com.tundramobile.catchapp.firebase.model.ChatRoom;
import com.tundramobile.catchapp.firebase.model.Message;

import java.util.List;
import java.util.Map;

/**
 * Created by Sviatoslav Zaitsev on 27.04.17.
 */

public class Types {

    public static
    GenericTypeIndicator<Map<String, List<String>>> chatRoomsIdsMap
            = new GenericTypeIndicator<Map<String, List<String>>>() {};

    public static
    GenericTypeIndicator<ChatRoom> chatRoom
            = new GenericTypeIndicator<ChatRoom>() { };

    public static
    GenericTypeIndicator<Message> message
            = new GenericTypeIndicator<Message>() { };

    public static
    GenericTypeIndicator<List<String>> stringsList
            = new GenericTypeIndicator<List<String>>() {};
}

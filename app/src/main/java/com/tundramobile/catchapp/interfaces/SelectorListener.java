package com.tundramobile.catchapp.interfaces;

public interface SelectorListener {
    void onItemSelected(Object item, int position, boolean isAdding);
}

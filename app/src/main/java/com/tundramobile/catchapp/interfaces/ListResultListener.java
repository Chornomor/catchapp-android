package com.tundramobile.catchapp.interfaces;

import java.util.List;

public interface ListResultListener<T> {
    void onResultReady(int type, boolean success, List<T> catchUps);
}

package com.tundramobile.catchapp.interfaces;

public interface LoadCallback<T> {
    void onLoadFinished(T something);
}

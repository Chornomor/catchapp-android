package com.tundramobile.catchapp.interfaces;

/**
 * Created by alexchern on 20.04.17.
 */

public interface OnEventReadyToCreateListener {
    void onEventReadyToCreate(boolean isReady);
}

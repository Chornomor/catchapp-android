package com.tundramobile.catchapp.interfaces;

import android.support.annotation.DrawableRes;

import com.tundramobile.catchapp.entity.FabOption;

import java.util.ArrayList;

public interface FabHandler {
    void showFab();

    void setupFabOptions(ArrayList<FabOption> fabOptions, @DrawableRes int fabIcon, boolean applyBlur);

    void hideFab();

    boolean isFabOpen();

    void hideFabIfNotRequired();
}

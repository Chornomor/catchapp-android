package com.tundramobile.catchapp.interfaces;

import com.tundramobile.catchapp.entity.TabData;

public interface TabDataI {
    void setTabData(TabData tabData);
    TabData getTabData();
}

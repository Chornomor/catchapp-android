package com.tundramobile.catchapp.interfaces;

/**
 * Created by eugenetroyanskii on 12.04.17.
 */

public interface ProgressDialogHandler {

    void showProgressDialog();

    void cancelProgressDialog();
}

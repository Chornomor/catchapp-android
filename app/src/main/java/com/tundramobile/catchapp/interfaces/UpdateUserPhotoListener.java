package com.tundramobile.catchapp.interfaces;

import com.tundramobile.catchapp.entity.User;

/**
 * Created by eugenetroyanskii on 25.04.17.
 */

public interface UpdateUserPhotoListener {

    void onUserUpdated(User updatedUser, String error);
}

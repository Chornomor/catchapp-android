package com.tundramobile.catchapp.interfaces;

import com.tundramobile.catchapp.entity.Event;
import com.tundramobile.catchapp.entity.User;

public interface EventDetailsClickListener {
    void onMessageClick();

    void onShareClick();

    void onActionButtonClick(boolean isDelete);

    void onTimeSlotClick(Event event, boolean isEditable);

    void onEditUserClick(User user);

    void onWebSiteClick(String url);

    void onPhoneClick(String phone);
}

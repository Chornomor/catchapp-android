package com.tundramobile.catchapp.interfaces;

public interface RecyclerItemClickListener<T> {
    void onItemClick(int position, T model);
}

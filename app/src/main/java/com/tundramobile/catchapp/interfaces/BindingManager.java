package com.tundramobile.catchapp.interfaces;

public interface BindingManager<T> {
    void bindContent(T model);
}

package com.tundramobile.catchapp.interfaces;

import java.util.HashMap;

/**
 * Created by admin on 02.04.2017.
 */

public interface FastScrollRecyclerViewInterface {
    public HashMap<String, Integer> getMapIndex();
}

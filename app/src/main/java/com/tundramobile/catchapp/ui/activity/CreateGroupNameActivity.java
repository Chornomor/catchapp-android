package com.tundramobile.catchapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.request.CreateGroupBody;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eugenetroyanskii on 09.04.17.
 */

public class CreateGroupNameActivity extends BaseActivity implements ContactManager.OnCreateGroupListener{

    public final static String GROUP_CONTACT_IDS = "group_ids";

    @BindView(R.id.create_group_name) TextView groupName;
    @BindView(R.id.create_group_save) TextView btnSave;
    @BindView(R.id.toolbar) AppToolbar toolbar;

    private CreateGroupBody createBody;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_name);
        ButterKnife.bind(this);
        toolbar.switchToContactsSelectingMode(getString(R.string.add_contact_create_group), (v -> onBackPressed()));
        Intent intent = getIntent();
        int[] idsArray = intent.getIntArrayExtra(GROUP_CONTACT_IDS);
        initCreateGroupData(idsArray);
        setUpSaveButton();
    }


    private void setUpSaveButton() {
        groupName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0) {
                    btnSave.setVisibility(View.VISIBLE);
                } else {
                    btnSave.setVisibility(View.GONE);
                }
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void initCreateGroupData(int[] idsArray) {
        ArrayList<Integer> groupIds = new ArrayList<>();
        createBody = new CreateGroupBody();
        for (int anIdsArray : idsArray) {
            groupIds.add(anIdsArray);
        }
        createBody.setMembers(groupIds);
    }

    @OnClick(R.id.create_group_save)
    public void onSave() {
        String currentGroupName = groupName.getText().toString().trim();
        if(!currentGroupName.isEmpty()) {
            Utils.closeKeyboard(this);
            showProgressDialog();
            createBody.setName(currentGroupName);
            ContactManager.getInstance(this).createGroup(this, createBody);
        } else {
            TopPopUpManager.showTopPopUp(this, "Enter group name", !TopPopUpManager.isError);
        }
    }


    @Override
    public void onGroupCreated(Contact group) {
        cancelProgressDialog();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}

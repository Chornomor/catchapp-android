package com.tundramobile.catchapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.ui.viewmodel.PermissionItem;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import rebus.permissionutils.PermissionEnum;

/**
 * Created by Sviatoslav Zaitsev on 05.04.17.
 */

public class PermissionsAdapter extends RecyclerView.Adapter<PermissionsAdapter.ViewHolder> {

    private RecyclerItemClickListener<PermissionEnum[]> listener;
    private PermissionItem[] items;

    public PermissionsAdapter(PermissionItem[] items, RecyclerItemClickListener<PermissionEnum[]> listener) {
        this.items = items;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.enabled_indicator)
        ImageView enabledIndicator;
        @BindView(R.id.enable_btn)
        Button enableBtn;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.permission_list_item_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PermissionItem item = items[position];

        boolean isGranted = FastPermission.isGranted(holder.itemView.getContext(),
                item.getPermissions());

        holder.image.setImageResource(item.getImage());
        holder.title.setText(item.getTitle());

        holder.enabledIndicator.setVisibility(isGranted ? View.VISIBLE : View.GONE);
        holder.enableBtn.setVisibility(!isGranted ? View.VISIBLE : View.GONE);

        holder.enableBtn.setOnClickListener(v -> {
            if(listener != null) listener.onItemClick(holder.getAdapterPosition(), item.getPermissions());
        });
    }


    @Override
    public int getItemCount() {
        return items == null ? 0 : items.length;
    }
}

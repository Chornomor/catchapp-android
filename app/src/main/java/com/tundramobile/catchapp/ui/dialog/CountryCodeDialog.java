package com.tundramobile.catchapp.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Country;
import com.tundramobile.catchapp.ui.adapter.CountryCodeAdapter;
import com.tundramobile.catchapp.ui.view.fastScroll.FastScrollRecyclerViewItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by alexchern on 31.03.17.
 */

public class CountryCodeDialog extends Dialog {

    @BindView(R.id.country_code_list) RecyclerView mRecyclerView;
    @BindView(R.id.back_button) ImageView backButton;

    private CountryCodeAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private CountryCodeAdapter.OnCountryItemClickListener listener;
    private View v;
    private Context context;


    public CountryCodeDialog(@NonNull Context context, CountryCodeAdapter.OnCountryItemClickListener listener) {
        super(context, R.style.CountryCodeDialog);
        this.context = context;
        this.listener = listener;
        fillViewForRooms();
    }


    private void fillViewForRooms() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.dialog_country_codes, null);
        ButterKnife.bind(this, v);


        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<Country> myDataset = (ArrayList<Country>) Country.getLibraryMasterCountriesEnglish();

        Collections.sort(myDataset);
        HashMap<String, Integer> mapIndex = calculateIndexesForName(myDataset);

        mAdapter = new CountryCodeAdapter(myDataset, mapIndex);
        mAdapter.setOnCountryItemClick(listener);
        mRecyclerView.setAdapter(mAdapter);
        FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(context);
        mRecyclerView.addItemDecoration(decoration);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        initDialog();
    }


    private HashMap<String, Integer> calculateIndexesForName(ArrayList<Country> items) {
        HashMap<String, Integer> mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < items.size(); i++) {
            String name = items.get(i).getName();
            String index = name.substring(0, 1);
            index = index.toUpperCase();

            if (!mapIndex.containsKey(index)) {
                mapIndex.put(index, i);
            }
        }
        return mapIndex;
    }


    public void initDialog() {
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);
        setContentView(v);
    }


    @OnClick(R.id.back_button)
    public void BackOnClick() {
        dismiss();
    }
}

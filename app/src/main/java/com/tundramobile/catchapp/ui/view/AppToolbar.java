package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tundramobile.catchapp.R;

/**
 * Created by Sviatoslav Zaitsev on 10.04.17.
 */

public class AppToolbar extends Toolbar {

    private View divider;
    private View logoView;
    private View profileBtn;
    private ImageButton backBtn;
    private View addGroupBtn;
    private TextView addBtn;
    private TextView calendarView;
    private TextView titleView;


    public AppToolbar(Context context) {
        super(context);
        init(context, null);
    }


    public AppToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    public AppToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.app_toolbar, this);

        divider = findViewById(R.id.divider);
        logoView = findViewById(R.id.logo);
        calendarView = (TextView) findViewById(R.id.calendar);
        titleView = (TextView) findViewById(R.id.title);
        profileBtn = findViewById(R.id.profileBtn);
        addGroupBtn = findViewById(R.id.addGroupBtn);
        backBtn = (ImageButton) findViewById(R.id.backBtn);
        addBtn = (TextView) findViewById(R.id.addBtn);
    }


    // TODO: 12.04.17 Refactor modes
    public void switchToDefaultMode() {
        divider.setVisibility(VISIBLE);

        logoView.setVisibility(VISIBLE);
        profileBtn.setVisibility(VISIBLE);

        backBtn.setVisibility(INVISIBLE);
        setBackClickListener(null);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(null);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarClickListener(null);

        titleView.setVisibility(INVISIBLE);
        setTitle("");
    }

    public void switchToDefaultMode(String title) {
        divider.setVisibility(VISIBLE);

        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(VISIBLE);

        backBtn.setVisibility(INVISIBLE);
        setBackClickListener(null);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(null);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarClickListener(null);

        titleView.setVisibility(VISIBLE);
        setTitle(title);
    }

    public void switchToDefaultMode(String title, OnClickListener listener) {
        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(INVISIBLE);

        backBtn.setVisibility(VISIBLE);
        setBackClickListener(listener);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(listener);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarClickListener(null);

        setTitle(title);
        titleView.setVisibility(VISIBLE);
    }


    public void switchToCalendarMode(String title, OnClickListener calendarClickListener) {
        divider.setVisibility(VISIBLE);

        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(VISIBLE);

        backBtn.setVisibility(INVISIBLE);
        setBackClickListener(null);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(null);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        titleView.setVisibility(INVISIBLE);
        setTitle("");

        calendarView.setVisibility(VISIBLE);
        setCalendarTitle(title);
        setCalendarClickListener(calendarClickListener);
    }

    public void showBackBtn(OnClickListener listener){
        profileBtn.setVisibility(INVISIBLE);
        backBtn.setVisibility(VISIBLE);
        backBtn.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
        setBackClickListener(listener);
    }


    public void switchToContactsMode() {
        divider.setVisibility(VISIBLE);

        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(VISIBLE);

        backBtn.setVisibility(INVISIBLE);
        setBackClickListener(null);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(null);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarClickListener(null);

        setTitle(getResources().getString(R.string.contacts));
        titleView.setVisibility(VISIBLE);
    }


    public void switchToContactsSelectingMode(String title, OnClickListener listener) {
        switchToContactsSelectingMode(listener);
        setTitle(title);
    }


    public void switchToContactsSelectingMode(OnClickListener listener) {
        divider.setVisibility(VISIBLE);

        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(INVISIBLE);

        backBtn.setVisibility(VISIBLE);
        backBtn.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
        setBackClickListener(listener);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(null);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarTitle("");
        setCalendarClickListener(null);

        setTitle(getResources().getString(R.string.contacts));
        titleView.setVisibility(VISIBLE);
    }


    public void switchToGroupsMode(OnClickListener listener) {
        switchToContactsMode();

        addGroupBtn.setVisibility(VISIBLE);
        setOnAddGroupClickListener(listener);
    }


    public void switchToGroupListMode(String groupName, OnClickListener listener) {
        divider.setVisibility(VISIBLE);

        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(INVISIBLE);

        backBtn.setVisibility(VISIBLE);
        backBtn.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
        setBackClickListener(listener);

        addBtn.setVisibility(VISIBLE);
        setAddClickListener(listener);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarClickListener(null);

        setTitle(groupName);
        titleView.setVisibility(VISIBLE);
    }


    public void switchToEventDetailsMode(OnClickListener backButtonClickListener) {

        divider.setVisibility(INVISIBLE);

        logoView.setVisibility(INVISIBLE);
        profileBtn.setVisibility(INVISIBLE);

        backBtn.setVisibility(VISIBLE);
        backBtn.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));
        setBackClickListener(backButtonClickListener);

        addBtn.setVisibility(INVISIBLE);
        setAddClickListener(null);

        addGroupBtn.setVisibility(INVISIBLE);
        setOnAddGroupClickListener(null);

        calendarView.setVisibility(INVISIBLE);
        setCalendarClickListener(null);

        setTitle("");
        titleView.setVisibility(INVISIBLE);
    }


    public void setCalendarTitle(String title) {
        calendarView.setText(title);
    }


    public void setTitle(String title) {
        titleView.setText(title);
    }


    public void setCalendarClickListener(OnClickListener listener) {
        calendarView.setOnClickListener(listener);
    }


    public void setOnAddGroupClickListener(OnClickListener listener) {
        addGroupBtn.setOnClickListener(listener);
    }


    public void setAddClickListener(OnClickListener listener) {
        addBtn.setOnClickListener(listener);
    }


    public void setBackClickListener(OnClickListener listener) {
        backBtn.setOnClickListener(listener);
    }


    public void setOnProfileClickListener(OnClickListener listener) {
        profileBtn.setOnClickListener(listener);
    }
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.adapter.ScheduleAdapter;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ScheduleHolder extends RecyclerView.ViewHolder {
    private static final int NOT_TRANSPARENT = 255;
    private static final int SEMI_TRANSPARENT = 150;
    @BindView(R.id.tv_title)
    public TextView tvTitle;
    @BindView(R.id.tv_description)
    public TextView tvDesc;
    @BindView(R.id.tv_time)
    public TextView tvTime;
    @BindView(R.id.btn_delete)
    public ImageView btnDelete;
    @BindView(R.id.dragpin)
    public View dragpin;
    @BindView(R.id.layout)
    public ViewGroup layout;

    public ScheduleItem item;

    boolean isExternal = false;

    public int position;

    ScheduleAdapter.OnOpenItemClick onOpenItemClick;

    public ScheduleHolder(Context context) {
        super(LayoutInflater.from(context).inflate(R.layout.item_schedule,null));
        ButterKnife.bind(this,itemView);
    }


    public void bind(ScheduleItem item){
        this.item = item;

        setBackgroundColor(itemView.getContext(),item.backgroundColor);
        tvTitle.setText(item.title);
        tvTitle.setAllCaps(true);
        tvDesc.setText(item.desctiption);
        tvTime.setText(item.getTimePeriodString());
    }

    public void setBackgroundColor(Context context,int color){
        Drawable mDrawable = context.getResources().getDrawable(R.drawable.bg_schedule);
        mDrawable.setColorFilter(new
                PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
        itemView.setBackground(mDrawable);
    }

    public void setOpenOnClick(ScheduleAdapter.OnOpenItemClick listener){
        if(isExternal)
            return;
        onOpenItemClick = listener;
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOpenItemClick.openScheduleItem(item);
            }
        });
    }

    public void setSemiTransparent(boolean isSemiTransparent){
        int alpha = isSemiTransparent ? SEMI_TRANSPARENT : NOT_TRANSPARENT;
        itemView.getBackground().setAlpha(alpha);
//        setTextColorAlpha(tvTitle,alpha);
//        setTextColorAlpha(tvDesc,alpha);
//        setTextColorAlpha(tvTime,alpha);
        btnDelete.setImageAlpha(alpha);
    }
    public void setDragpinVisible(boolean isVisible){
        dragpin.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    void setTextColorAlpha(TextView textView, int alpha){
        textView.setTextColor(textView.getTextColors().withAlpha(alpha));
    }

    public void setDeleteBtnVisibility(boolean isVisible){
        btnDelete.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setExternal(boolean external) {
        isExternal = external;
    }

    public void setTitle(String title){
        tvTitle.setText(title);
    }
    public void setDescription(String description){
        tvDesc.setText(description);
    }
}

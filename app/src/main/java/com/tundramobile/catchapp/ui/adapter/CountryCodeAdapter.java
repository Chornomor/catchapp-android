package com.tundramobile.catchapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Country;
import com.tundramobile.catchapp.interfaces.FastScrollRecyclerViewInterface;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 02.04.2017.
 */

public class CountryCodeAdapter extends RecyclerView.Adapter<CountryCodeAdapter.ViewHolder> implements FastScrollRecyclerViewInterface
        , View.OnClickListener {

    private ArrayList<Country> mDataset;
    private HashMap<String, Integer> mMapIndex;
    private OnCountryItemClickListener onCountryItemListener;


    public CountryCodeAdapter(ArrayList<Country> myDataset, HashMap<String, Integer> mapIndex) {
        mDataset = myDataset;
        mMapIndex = mapIndex;
    }


    public void setOnCountryItemClick(OnCountryItemClickListener onCountryItemClick) {
        this.onCountryItemListener = onCountryItemClick;
    }


    @Override
    public CountryCodeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country_code, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Country country = mDataset.get(position);

        if (country.getPhoneCode().isEmpty()) {
            holder.header.setVisibility(View.VISIBLE);
            holder.countryLayout.setVisibility(View.GONE);
            holder.header.setText(country.getName());
            return;
        }

        holder.header.setVisibility(View.GONE);
        holder.countryLayout.setVisibility(View.VISIBLE);

        holder.countryLayout.setTag(position);
        holder.countryLayout.setOnClickListener(this);

        holder.countryName.setText(country.getName());
        holder.countryValue.setText(country.getPhoneCode());
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public HashMap<String, Integer> getMapIndex() {
        return this.mMapIndex;
    }


    @Override
    public void onClick(View view) {
        int position = Integer.parseInt(view.getTag().toString());
        onCountryItemListener.onCountryItemClick(mDataset.get(position));
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.country_code_header) TextView header;
        @BindView(R.id.country_code_name) TextView countryName;
        @BindView(R.id.country_code_value) TextView countryValue;
        @BindView(R.id.ll_country_code) LinearLayout countryLayout;


        private ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    public interface OnCountryItemClickListener {
        void onCountryItemClick(Country country);
    }
}
package com.tundramobile.catchapp.ui.view.pinCode;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.tundramobile.catchapp.ui.view.FontEditText;

import java.util.ArrayList;

/**
 * Created by alexchern on 03.04.17.
 */

public class PinNumberTextWatcher implements TextWatcher {

    private final EditText currentEditTextFieldId;
    private ArrayList<FontEditText> arrayList;
    private EditText nextEditTextFieldId;
    private EditText editTextFieldToChange;


    public PinNumberTextWatcher(EditText currentEditTextFieldId, ArrayList<FontEditText> arrayList) {
        this.currentEditTextFieldId = currentEditTextFieldId;
        this.arrayList = arrayList;
    }


    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        this.currentEditTextFieldId.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }


    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (this.currentEditTextFieldId.getText().length() == 1) {

            int nextEditText = (currentEditTextFieldId.getId() + 1 > arrayList.size() - 1 ? arrayList.size() - 1 : currentEditTextFieldId.getId() + 1);
            int editTextToChangeId = (currentEditTextFieldId.getId() - 1 < 0 ? nextEditText : currentEditTextFieldId.getId() - 1);

            editTextFieldToChange = arrayList.get(editTextToChangeId);
            editTextFieldToChange.requestFocus();

            nextEditTextFieldId = arrayList.get(nextEditText);
            nextEditTextFieldId.requestFocus();

            if (this.currentEditTextFieldId.getId() == this.nextEditTextFieldId.getId()) {
                ((InputMethodManager) this.currentEditTextFieldId.getContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(this.currentEditTextFieldId.getWindowToken(), 0);
            }
        }
    }


    public void afterTextChanged(Editable s) {

    }
}
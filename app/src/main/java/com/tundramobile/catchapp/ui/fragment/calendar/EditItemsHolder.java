package com.tundramobile.catchapp.ui.fragment.calendar;

import com.tundramobile.catchapp.ui.view.dateEditor.SlotRect;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by andreybofanov on 21.04.17.
 */

public class EditItemsHolder implements Serializable {
    public long confirmCatchUpId = -1;
    public long slotId = -1;
    public ArrayList<SlotRect> items = new ArrayList<>();
}

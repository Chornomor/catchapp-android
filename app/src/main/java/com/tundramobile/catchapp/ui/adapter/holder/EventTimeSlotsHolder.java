package com.tundramobile.catchapp.ui.adapter.holder;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.Event;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.other.CustomTypefaceSpan;
import com.tundramobile.catchapp.other.TimeUtils;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.EventSlotHeaderView;
import com.tundramobile.catchapp.ui.view.EventSlotItemView;

import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EventTimeSlotsHolder extends BaseEventHolder {

    @BindView(R.id.topic) TextView topic;
    @BindView(R.id.hint) TextView hintTextView;
    @BindView(R.id.slotItemsContainer) LinearLayout slotItemsContainer;

    @BindString(R.string.hint_text) String hintText;
    @BindString(R.string.hint) String hintWord;
    @BindString(R.string.montserrat_bold) String montserrat_bold;
    @BindString(R.string.montserrat_light) String montserrat_light;

    @BindDimen(R.dimen.padding_big) int slotMargin;
    @BindDimen(R.dimen.view_time_slot_header_margin_top) int headerMargin;

    private boolean isEditable;
    private String currentHeaderText = "";


    public EventTimeSlotsHolder(View itemView, EventDetailsClickListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
    }


    @Override
    public void bindContent(CatchUp catchUp, int type) {
        super.bindContent(catchUp, type);
        this.isEditable = catchUp.isMine(MyUserManager.getCurrentUser(itemView.getContext()).getId());

        setupTopicText();
        setupHint();
        setupTimeSlots();
    }


    private void setupTopicText() {
        topic.setText(R.string.when);

//        if (something) {
//            topic.setText(R.string.when);
//        } else {
//            topic.setText(R.string.select_time_below);
//        }
    }


    private void setupHint() {
        if (!isEditable) {
            SpannableStringBuilder builder = new SpannableStringBuilder(hintText);
            builder.setSpan(new CustomTypefaceSpan("", Utils.getTypeface(itemView.getContext(), montserrat_bold)),
                    0, hintWord.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(new CustomTypefaceSpan("", Utils.getTypeface(itemView.getContext(), montserrat_light)),
                    hintWord.length() + 1, hintText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            hintTextView.setText(builder);
        } else {
            hintTextView.setVisibility(View.GONE);
        }
    }


    private void setupTimeSlots() {
        slotItemsContainer.removeAllViews();
        currentHeaderText = "";
        for (Event event : catchUp.getEvents()) {
            String headerText = TimeUtils.getTimeFromTimestamp(event.getFrom());
            if (!currentHeaderText.equals(headerText)) {
                currentHeaderText = headerText;
                slotItemsContainer.addView(createHeader(headerText));
            }
            slotItemsContainer.addView(createTimeSlot(event));
        }
    }


    private View createHeader(String time) {
        EventSlotHeaderView view = new EventSlotHeaderView(itemView.getContext());
        view.setText(time);
        return view;
    }


    private View createTimeSlot(Event event) {
        EventSlotItemView view = new EventSlotItemView(itemView.getContext());
        view.setClickListener(listener);
        view.setupWith(event);
        view.setEditable(isEditable);
        return view;
    }


}

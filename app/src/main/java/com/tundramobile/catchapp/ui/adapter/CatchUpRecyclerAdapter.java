package com.tundramobile.catchapp.ui.adapter;

import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.other.TimeUtils;
import com.tundramobile.catchapp.ui.adapter.holder.CatchUpViewHolder;
import com.tundramobile.catchapp.ui.adapter.holder.DateItemHolder;

import java.util.ArrayList;
import java.util.List;

public class CatchUpRecyclerAdapter extends RecyclerView.Adapter<CatchUpViewHolder>
        implements StickyRecyclerHeadersAdapter {

    private RecyclerItemClickListener<CatchUp> listener;
    private List<CatchUp> items;
    private int color;
    private long headerId = 1;
    private String headerText = "";


    public CatchUpRecyclerAdapter(@ColorInt int color,
                                  @NonNull RecyclerItemClickListener<CatchUp> listener) {
        items = new ArrayList<>();
        this.color = color;
        this.listener = listener;
    }


    @Override
    public CatchUpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CatchUpViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.holder_catchup, parent, false), color, listener);
    }


    @Override
    public void onBindViewHolder(CatchUpViewHolder holder, int position) {
        holder.bindContent(items.get(position));
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    private String getHeaderText() {
        return headerText;
    }


    private void setHeaderText(String headerText) {
        this.headerText = headerText;
    }


    @Override
    public long getHeaderId(int position) {
        try {
            String currentHeaderText = TimeUtils.getTimeFromTimestamp(items.get(position).getEvents().get(0).getFrom());
            if (!currentHeaderText.equals(getHeaderText())) {
                setHeaderText(currentHeaderText);
                headerId++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return headerId;
    }


    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new DateItemHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.holder_time_header, parent, false));
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((DateItemHolder) holder).bindContent(getHeaderText());
    }


    @Override
    public int getItemCount() {
        return items == null || items.isEmpty() ? 0 : items.size();
    }


    public void setItems(List<CatchUp> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    public void addItems(List<CatchUp> items) {
        if (items != null && !items.isEmpty()) {
            int idleCount = getItemCount();
            this.items.addAll(items);
            notifyItemRangeInserted(idleCount, getItemCount());
        }
    }


    public ArrayList<CatchUp> getItems() {
        return (ArrayList<CatchUp>) items;
    }
}

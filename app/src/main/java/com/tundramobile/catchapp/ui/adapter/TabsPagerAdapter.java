package com.tundramobile.catchapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import java.util.ArrayList;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<BaseFragment> fragments;
    private FragmentManager fm;

    public TabsPagerAdapter(FragmentManager fm,
                            @NonNull ArrayList<BaseFragment> fragments) {
        super(fm);
        this.fm = fm;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public ArrayList<BaseFragment> getFragments() {
        return fragments;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    public Fragment getActiveFragment(ViewPager viewPager, int position) {
        String name = makeFragmentName(viewPager.getId(), position);
        return fm.findFragmentByTag(name);
    }



    private static String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.interfaces.BindingManager;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.ImageLoader;
import com.tundramobile.catchapp.other.Regex;
import com.tundramobile.catchapp.ui.view.CollageImageView;
import com.tundramobile.catchapp.ui.view.FontTextView;
import com.tundramobile.catchapp.ui.viewmodel.ChatRoomViewModel;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class ChatRoomHolder extends RecyclerView.ViewHolder implements BindingManager<ChatRoomViewModel> {

    @BindView(R.id.photo) CollageImageView image;
    @BindView(R.id.name) FontTextView name;
    @BindView(R.id.when) FontTextView when;
    @BindView(R.id.message) FontTextView message;

    @BindDimen(R.dimen.catchup_holder_avatar_size) int avatarSize;

    private ChatRoomViewModel roomViewModel;
    private Context context;


    public ChatRoomHolder(View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }


    @Override
    public void bindContent(ChatRoomViewModel model) {
        roomViewModel = model;

        setName();
        setAvatar();
        when.setText(model.getWhen());
        message.setText(model.getMsg());
    }


    private void setAvatar() {
        image.setCollageImages(null);

        String url = roomViewModel.getImage();
        if (!TextUtils.isEmpty(url)) {
            ImageLoader.newInstance(image, avatarSize, false).loadImage(roomViewModel.getImage());
        } else {
            image.setImageResource(R.drawable.rect_gray_common);
//            initials.setText(Utils.getNameInitials(name.getText().toString()));
//            initials.setVisibility(View.VISIBLE);
        }
    }


    private void setName() {
        String userName = roomViewModel.getName();
//        if (Regex.isTextPhoneNumber(userName)) {
//            String tempUserName = ContactManager.getInstance(context).getUserContactNameOnDevice(userName);
//            if (tempUserName != null) {
////                contact.setName(tempUserName);
//                userName = tempUserName;
//            }
//        }
        name.setText(userName);
    }
}

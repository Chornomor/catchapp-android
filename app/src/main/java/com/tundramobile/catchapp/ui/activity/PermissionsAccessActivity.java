package com.tundramobile.catchapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.ui.viewmodel.PermissionItem;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.adapter.PermissionsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rebus.permissionutils.PermissionEnum;

/**
 * Created by Sviatoslav Zaitsev on 05.04.17.
 */

public class PermissionsAccessActivity extends BaseActivity {
    public static final int REQUEST_PERMISSIONS = 101;
    @BindView(R.id.list) RecyclerView listView;
    @BindView(R.id.perm_action) Button actionBtn;

    private final PermissionItem[] permissionsArray = {
            new PermissionItem(R.drawable.permission_calendar,      R.string.permissions_access_calendar, PermissionEnum.READ_CALENDAR),
            new PermissionItem(R.drawable.permission_contacts,      R.string.permissions_access_contacts, PermissionEnum.READ_CONTACTS),
            new PermissionItem(R.drawable.permission_location,      R.string.permissions_access_location, PermissionEnum.ACCESS_COARSE_LOCATION, PermissionEnum.ACCESS_FINE_LOCATION),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_permissions_access);
        ButterKnife.bind(this);

        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.setAdapter(new PermissionsAdapter(permissionsArray, new RecyclerItemClickListener<PermissionEnum[]>() {
            @Override
            public void onItemClick(int position, PermissionEnum[] model) {
                FastPermission.request(PermissionsAccessActivity.this,REQUEST_PERMISSIONS,
                        () -> {} , model);
            }
        }));
    }

    @Override
    protected void onStart() {
        super.onStart();
        listView.getAdapter().notifyDataSetChanged();
    }

    @OnClick(R.id.perm_action)
    public void onActionBtnClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_PERMISSIONS && resultCode == RESULT_OK) {
            actionBtn.setText(R.string.permissions_access_done);
            listView.getAdapter().notifyDataSetChanged();
        }
    }
}

/*
 * Copyright (C) 2015 Google Inc. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.tundramobile.catchapp.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.SimplePlace;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter that handles Autocomplete requests from the Places Geo Data API.
 * {@link AutocompletePrediction} results from the API are frozen and stored directly in this
 * adapter. (See {@link AutocompletePrediction#freeze()}.)
 * <p>
 * Note that this adapter requires a valid {@link com.google.android.gms.common.api.GoogleApiClient}.
 * The API client must be maintained in the encapsulating Activity, including all lifecycle and
 * connection states. The API client must be connected with the {@link Places#GEO_DATA_API} API.
 */
public class PlaceAutocompleteAdapter
        extends RecyclerView.Adapter<PlaceAutocompleteAdapter.ViewHolder>
        implements Filterable {

    private static final String TAG = PlaceAutocompleteAdapter.class.getSimpleName();
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);

    /**
     * Note: change this description
     * Current results returned by this adapter.
     */
    private Context mContext;

    /**
     * Uses to notify when item was clicked
     */
    private OnCustomLocationClickListener mOnCustomLocationClickListener;

    /**
     * Uses to notify when custom location was clicked
     */
    private RecyclerItemClickListener<SimplePlace> mOnItemClickListener;

    /**
     * Uses to notify when item count changed
     */
    private OnItemsCountChangedListener mOnItemsCountChangedListener;

    /**
     * Current results returned by this adapter.
     */
    private ArrayList<AutocompletePrediction> mResultList;
    /**
     * Current results returned by this adapter.
     */
    private String customLocation;

    /**
     * Handles autocomplete requests.
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * The bounds used for Places Geo Data autocomplete API requests.
     */
    private LatLngBounds mBounds;

    /**
     * The autocomplete filter used to restrict queries to a specific set of place types.
     */
    private AutocompleteFilter mPlaceFilter;

    /**
     * Selected item position.
     */
    private int mSelectedItemPosition = -1;

    /**
     * Initializes with a resource for text rows and autocomplete query bounds.
     *
     * @see android.widget.ArrayAdapter#ArrayAdapter(android.content.Context, int)
     */
    public PlaceAutocompleteAdapter(Context context, GoogleApiClient googleApiClient,
            LatLngBounds bounds, AutocompleteFilter filter) {
        mContext = context;
        mGoogleApiClient = googleApiClient;
        mBounds = bounds;
        mPlaceFilter = filter;
    }

    /**
     * Sets the saved places.
     */
    public void setCustomLocation(String locationName) {
        this.customLocation = locationName;
        notifyDataSetChanged();
        notifyItemsCountChanged();
    }

    /****/
    private void notifyItemsCountChanged() {
        if(mOnItemsCountChangedListener != null) {
            mOnItemsCountChangedListener.onItemsCountChanged(getCustomItemCount());
        }
    }

    /**
     * Sets the bounds for all subsequent queries.
     */
    public void setBounds(LatLngBounds bounds) {
        mBounds = bounds;
    }

    /**
     * Sets click listener for items in list.
     */
    public void setOnItemClickListener(RecyclerItemClickListener<SimplePlace> listener) {
        mOnItemClickListener = listener;
    }

    /**
     * Sets listener for items count changes.
     */
    public void setOnItemsCountChangedListener(OnItemsCountChangedListener listener) {
        mOnItemsCountChangedListener = listener;
    }

    /**
     * Sets listener for custom item.
     */
    public void setOnCustomLocationClickListener(OnCustomLocationClickListener listener) {
        mOnCustomLocationClickListener = listener;
    }

    /****/
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).
        inflate(R.layout.location_list_item_layout, parent, false));
    }

    /**
     * Returns the number of results received in the last autocomplete query and saved results.
     */
    @Override
    public int getItemCount() {
        return (getCustomItemCount())
               + (mResultList == null ? 0 : mResultList.size());
    }

    /**
     * Returns the number of saved places.
     */
    public int getCustomItemCount() {
        return (customLocation == null || customLocation.isEmpty() ? 0 : 1);
    }

    /**
     * Returns an item from the last autocomplete query.
     */
    public AutocompletePrediction getItem(int position) {
        return mResultList.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setSelected(mSelectedItemPosition == position);

        if(position < getCustomItemCount()) {
            bindSavedPlaces(holder, position);
        } else {
            bindAutocompletePredictions(holder, position - getCustomItemCount());
        }
    }

    private void bindSavedPlaces(ViewHolder holder, int position) {
        holder.pinIcon.setVisibility(View.GONE);

        holder.row1.setText(customLocation);
        holder.row2.setText(R.string.custom_location);

        holder.itemView.setOnClickListener(v -> {
            selectItem(position);
            if(mOnCustomLocationClickListener != null) {
                    mOnCustomLocationClickListener.onCustomLocationClicked(customLocation);
                }
            }
        );
    }

    private void bindAutocompletePredictions(ViewHolder holder, int position) {
        // Sets the primary and secondary text for a row.
        // Note that getPrimaryText() and getSecondaryText() return a CharSequence that may contain
        // styling based on the given CharacterStyle.

        AutocompletePrediction item = getItem(position);

        holder.pinIcon.setVisibility(View.VISIBLE);

        holder.row1.setText(item.getPrimaryText(STYLE_BOLD));
        holder.row2.setText(item.getSecondaryText(STYLE_BOLD));

        holder.itemView.setOnClickListener(v -> {
            selectItem(position);
            onItemClicked(position, item);
        });
    }

    private void selectItem(int position) {
        if(mSelectedItemPosition != -1) {
            notifyItemChanged(mSelectedItemPosition);
        }
        mSelectedItemPosition = position;
//        notifyDataSetChanged();
        notifyItemChanged(mSelectedItemPosition);
    }

    public void resetSelection() {
//        mSelectedItemPosition = -1;
//        notifyDataSetChanged();
    }

    /**
     * Submits an place query to the Places Geo Data Autocomplete API.
     * Creates SimplePlace object
     * Sets position and place object to callback
     */
    private void onItemClicked(final int position, AutocompletePrediction item) {
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, item.getPlaceId()).then(new ResultTransform<PlaceBuffer, Result>() {
            @Nullable
            @Override
            public PendingResult<Result> onSuccess(@NonNull PlaceBuffer places) {
                if(places.get(0) != null) {
                    SimplePlace clickedPlace = new SimplePlace(
                            item.getPrimaryText(STYLE_BOLD).toString(),
                            item.getSecondaryText(STYLE_BOLD).toString(),
                            places.get(0));

                    places.release();

                    ((AppCompatActivity) mContext).runOnUiThread(() ->
                            mOnItemClickListener.onItemClick(position, clickedPlace));
                }
                return null;
            }
        });
    }

    /**
     * Returns the filter for the current set of autocomplete results.
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();

                // We need a separate list to store the results, since
                // this is run asynchronously.
                ArrayList<AutocompletePrediction> filterData = new ArrayList<>();

                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    filterData = getAutocomplete(constraint);
                }

                results.values = filterData;
                if (filterData != null) {
                    results.count = filterData.size();
                } else {
                    results.count = 0;
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    mResultList = (ArrayList<AutocompletePrediction>) results.values;
                } else {
                    // The API did not return any results, invalidate the data set.
                    mResultList = null;
                }
                notifyDataSetChanged();
                notifyItemsCountChanged();
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                // Override this method to display a readable result in the AutocompleteTextView
                // when clicked.
                if (resultValue instanceof AutocompletePrediction) {
                    return ((AutocompletePrediction) resultValue).getFullText(null);
                } else {
                    return super.convertResultToString(resultValue);
                }
            }
        };
    }

    /**
     * Submits an autocomplete query to the Places Geo Data Autocomplete API.
     * Results are returned as frozen AutocompletePrediction objects, ready to be cached.
     * objects to store the Place ID and description that the API returns.
     * Returns an empty list if no results were found.
     * Returns null if the API client is not available or the query did not complete
     * successfully.
     * This method MUST be called off the main UI thread, as it will block until data is returned
     * from the API, which may include a network request.
     *
     * @param constraint Autocomplete query string
     * @return Results from the autocomplete API or null if the query was not successful.
     * @see Places#GEO_DATA_API#getAutocomplete(CharSequence)
     * @see AutocompletePrediction#freeze()
     */
    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Starting autocomplete query for: " + constraint);

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                Log.e(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Freeze the results immutable representation that can be stored safely.
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        Log.e(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pin_icon) View pinIcon;
        @BindView(android.R.id.text1) TextView row1;
        @BindView(android.R.id.text2) TextView row2;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface OnItemsCountChangedListener {
        void onItemsCountChanged(int count);
    }

    public interface OnCustomLocationClickListener {
        void onCustomLocationClicked(String locationName);
    }
}
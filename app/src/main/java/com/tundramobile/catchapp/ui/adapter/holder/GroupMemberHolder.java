package com.tundramobile.catchapp.ui.adapter.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.interfaces.BindingManager;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.ImageLoader;
import com.tundramobile.catchapp.other.Regex;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.CollageImageView;
import com.tundramobile.catchapp.ui.view.FontTextView;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupMemberHolder extends RecyclerView.ViewHolder implements BindingManager<Contact> {

    @BindView(R.id.avatar) CollageImageView avatar;
    @BindView(R.id.initials) TextView initials;
    @BindView(R.id.name) FontTextView name;

    @BindDimen(R.dimen.catchup_holder_avatar_size) int avatarSize;

    private Contact contact;
    private Context context;


    public GroupMemberHolder(View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }


    @Override
    public void bindContent(Contact model) {
        contact = model;

        setName();
        setAvatar();
    }


    private void setAvatar() {
        avatar.setCollageImages(null);
        initials.setVisibility(View.INVISIBLE);

        String url = contact.getProfilePic();
        if (!TextUtils.isEmpty(url)) {
            ImageLoader.newInstance(avatar, avatarSize, false).loadImage(contact);
        } else {
            avatar.setImageResource(R.drawable.rect_gray_common);
            initials.setText(Utils.getNameInitials(name.getText().toString()));
            initials.setVisibility(View.VISIBLE);
        }
    }


    private void setName() {
        String userName = contact.getName();
        if (Regex.isTextPhoneNumber(userName)) {
            String tempUserName = ContactManager.getInstance(context).getUserContactNameOnDevice(userName);
            if (tempUserName != null) {
                contact.setName(tempUserName);
                userName = tempUserName;
            }
        }
        name.setText(userName);
    }
}

package com.tundramobile.catchapp.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.event.GetContactsEvent;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.PagerManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.other.Regex;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.ContactsTabLayout;
import com.tundramobile.catchapp.ui.view.NonSwipeViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;

import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.SELECTED_CONTACTS;

/**
 * Created by eugenetroyanskii on 18.04.17.
 */

public class SelectEventMembersFragment extends BaseFragment implements ContactManager.OnObtainContactListener,
        TabLayout.OnTabSelectedListener {

    public static final int SINGLE_MEMBER_TYPE = 0;
    public static final int MULTI_MEMBER_TYPE = 1;
    public static final int GROUP_MEMBERS_TYPE = 2;
    public static final int MULTI_CATCHAPP_MEMBER_TYPE = 3;

    @BindView(R.id.contactsTabLayout) ContactsTabLayout contactsTabLayout;
    @BindView(R.id.viewPager) NonSwipeViewPager viewPager;
    @BindView(R.id.create_group_select_btn) TextView addContactsBtn;

    private Unbinder unbinder;
    private ContactManager contactManager;
    private TabsPagerAdapter adapter;
    private AppToolbar toolbar;
    private Set<Contact> selectedContacts = new HashSet<>();
    private int selectionType;
    private int selectedTab;
    private int requestCount = 0;


    public static SelectEventMembersFragment newInstance(int selectType) {
        Bundle args = new Bundle();
        args.putInt(TYPE, selectType);
        SelectEventMembersFragment fragment = new SelectEventMembersFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_CONTACTS);
        if (bundleFromNavigator != null) {
            ArrayList<Contact> contacts = bundleFromNavigator.getParcelableArrayList(SELECTED_CONTACTS);
            selectedContacts.addAll(contacts);
        }
        if (savedInstanceState != null) {
            selectionType = savedInstanceState.getInt(TYPE);
        } else if (getArguments() != null) {
            selectionType = getArguments().getInt(TYPE);
        }
        contactManager = ContactManager.getInstance(getContext());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_select_event_members, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.e("LOG", "onResume");
        if (adapter != null) {
            onGetContacts(new GetContactsEvent(ContactManager.GROUPS));
        }
    }


    private void initLayout() {
        adapter = PagerManager.getSelectingContactsPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        contactsTabLayout.setupWithViewPager(viewPager);
        contactsTabLayout.addOnTabSelectedListener(this);
        if (selectionType == GROUP_MEMBERS_TYPE) {
            contactsTabLayout.selectTab(GROUP_MEMBERS_TYPE);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e("LOG", "onStart");
        EventBus.getDefault().register(this);
        FastPermission.request(this, PermissionEnum.READ_CONTACTS.ordinal(), null,
                PermissionEnum.READ_CONTACTS);
    }


    @Override
    public void onStop() {
        super.onStop();
    }


    private void startLoading() {
        if (requestCount < 1) {
            showProgressDialog();
        }
        requestCount++;
    }


    private void stopLoading() {
        requestCount--;
        if (requestCount < 1) {
            cancelProgressDialog();
            fillSelectedContacts();
        }
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onGetContacts(GetContactsEvent event) {
        startLoading();
        switch (event.getType()) {
            case ContactManager.CONTACTS:
                if (FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS)) {
                    contactManager.getPhoneContacts(getContext(), true, this);
                } else {
                    cancelProgressDialog();
                }
                break;
            case ContactManager.RECENT:
                contactManager.getRecent(true, this);
                break;
            case ContactManager.GROUPS:
                contactManager.getGroups(this);
                break;
        }
    }


    @Override
    public void onObtainContacts(List<Contact> contacts, String error) {
        if (error == null) {
            setContacts(ContactManager.CONTACTS, (ArrayList<Contact>) contacts);
        } else {
            TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
        }
        stopLoading();
    }


    @Override
    public void onObtainRecentContacts(List<Contact> recentContactList) {
        if (!recentContactList.isEmpty()) {
            setContacts(ContactManager.RECENT, (ArrayList<Contact>) recentContactList);
        }
        stopLoading();
    }


    @Override
    public void onObtainContactGroups(List<Contact> groupList) {
        if (!groupList.isEmpty()) {
            setContacts(ContactManager.GROUPS, (ArrayList<Contact>) groupList);
        }
        stopLoading();
    }


    private void setContacts(int type, ArrayList<Contact> contacts) {
        if (viewPager != null) {
            SelectContactListFragment f = ((SelectContactListFragment) adapter.getActiveFragment(viewPager, type));
            if (f == null) {
                f = (SelectContactListFragment) adapter.getItem(type);
            }
            if (f != null) {
                f.setContacts(contacts);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PermissionEnum.READ_CONTACTS.ordinal() && resultCode == Activity.RESULT_OK) {
            if (FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS)) {
                onGetContacts(new GetContactsEvent(ContactManager.CONTACTS));
            } else {
                cancelProgressDialog();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Utils.closeKeyboard(getActivity());
        selectedTab = tab.getPosition();
        Log.e("Log.e", "fillSelectedContacts");
    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }


    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if (toolbar == null) return;
        this.toolbar = toolbar;
        setToolbarMode();
    }


    private void setToolbarMode() {
        toolbar.switchToContactsSelectingMode(getResources().getString(R.string.select_contact_who), v -> getActivity().onBackPressed());
    }


    private void fillSelectedContacts() {
        Log.e("Log.e", "fillSelectedContacts");
        for (Contact contact : selectedContacts) {
            ((SelectContactListFragment) adapter.getActiveFragment(viewPager, selectedTab)).selectItemByContact(contact);
        }
        updateAddButton();
    }


    private boolean isValidMemberForSelect(Contact contact, int position, boolean isAdded) {
        switch (selectionType) {
            case SINGLE_MEMBER_TYPE:
                if ((selectedContacts.size() > 0 && isAdded) || contact.isGroupContact()) {
                    TopPopUpManager.showTopPopUp(getContext(), getString(R.string.select_contact_one_member), !TopPopUpManager.isError);
                    ((SelectContactListFragment) adapter.getActiveFragment(viewPager, selectedTab)).selectItemByPosition(position, contact);
                    return false;
                }
                break;
            case MULTI_MEMBER_TYPE:
                break;
            case MULTI_CATCHAPP_MEMBER_TYPE:
                if ((selectedContacts.size() > 0 && isAdded) || contact.isGroupContact()) {
                    TopPopUpManager.showTopPopUp(getContext(), getString(R.string.select_contact_one_member), !TopPopUpManager.isError);
                    ((SelectContactListFragment) adapter.getActiveFragment(viewPager, selectedTab)).selectItemByPosition(position, contact);
                    return false;
                }
                break;
            case GROUP_MEMBERS_TYPE:
                if (!contact.isGroupContact()) {
                    TopPopUpManager.showTopPopUp(getActivity(), getString(R.string.select_contact_group_members), !TopPopUpManager.isError);
                    ((SelectContactListFragment) adapter.getActiveFragment(viewPager, selectedTab)).selectItemByPosition(position, contact);
                    return false;
                }
                break;
        }
        return true;
    }


    public void onItemClicked(Contact contact, int position, boolean isAdded) {
        if (isValidMemberForSelect(contact, position, isAdded)) {
            updateContactIdList(contact, isAdded);
            updateAddButton();
        }
    }


    private void updateContactIdList(Contact contact, boolean isAdding) {
        if (contact.isGroupContact()) {
            if (isAdding) {
                for (Contact tempContact : contact.getMembers()) {
                    initContactName(tempContact);
                    selectedContacts.add(tempContact);
                }
            } else {
                for (Contact tempContact : contact.getMembers()) {
                    selectedContacts.remove(tempContact);
                }
            }
        } else {
            if (isAdding) {
                selectedContacts.add(contact);
            } else {
                selectedContacts.remove(contact);
            }
        }
    }


    private void initContactName(Contact contact) {
        String userName = contact.getName();
        if (Regex.isTextPhoneNumber(userName)) {
            String tempUserName = ContactManager.getInstance(getActivity()).getUserContactNameOnDevice(userName);
            if (tempUserName != null) {
                contact.setName(tempUserName);
            }
        }
    }


    private void updateAddButton() {
        if (addContactsBtn.getVisibility() != View.VISIBLE) {
            addContactsBtn.setVisibility(View.VISIBLE);
        }
        if (selectedContacts.size() < 1) {
            addContactsBtn.setVisibility(View.INVISIBLE);
        }
        addContactsBtn.setText(selectedContacts.size() + " " + getResources().getString(R.string.select_contact_selected));
    }


    @OnClick(R.id.create_group_select_btn)
    public void onSelectedContactsClicked() {
        Bundle bundle = new Bundle();
        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.addAll(selectedContacts);
        bundle.putParcelableArrayList(SELECTED_CONTACTS, contacts);
        Navigator.with(getActivity()).putData(SELECTED_CONTACTS, bundle);
        getActivity().onBackPressed();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(TYPE, selectionType);
        super.onSaveInstanceState(outState);
    }
}

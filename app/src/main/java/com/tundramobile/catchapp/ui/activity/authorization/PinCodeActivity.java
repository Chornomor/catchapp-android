package com.tundramobile.catchapp.ui.activity.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;

import com.pushwoosh.PushManager;
import com.tundramobile.catchapp.entity.request.RequestUpdateUser;
import com.tundramobile.catchapp.entity.response.UpdateUserResponse;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.entity.response.Notification;
import com.tundramobile.catchapp.ui.activity.BaseActivity;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.request.RequestOTPBody;
import com.tundramobile.catchapp.entity.request.RespondOTP;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.LoginRequestOTP;
import com.tundramobile.catchapp.entity.response.LoginRespondOTP;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.other.Dialogs;
import com.tundramobile.catchapp.firebase.FirebaseUtils;
import com.tundramobile.catchapp.other.Regex;
import com.tundramobile.catchapp.ui.activity.BaseActivity;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.activity.PermissionsAccessActivity;
import com.tundramobile.catchapp.ui.view.pinCode.PinCodeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class PinCodeActivity extends BaseActivity {

    private final static int CURRENT_USER_POSITION = 0;

    @BindView(R.id.pin) PinCodeLayout pinCode;
    @BindView(R.id.auth_fab_next) FloatingActionButton nextBtn;

    private MyUserManager userManager;
    private User userModel;
    private String pin;
    private User currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_pin_code);
        ButterKnife.bind(this);
        setupKeyboardDoneButton();
        userManager = MyUserManager.getInstance();
        userModel = MyUserManager.getCurrentUser(this);
        pinCode.setIsPinCompleteListener(onPinCompleted);
    }


    @OnClick(R.id.auth_resend_pin)
    public void onRepeatClick() {
        showProgressDialog();
        userManager.requestOTP(new RequestOTPBody(userModel.getMobile()), resendPhoneCallback);
    }


    private void setupKeyboardDoneButton() {
        pinCode.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                OnNextClick();
            }
            return false;
        });
    }


    @OnClick(R.id.auth_fab_next)
    public void OnNextClick() {
        if (!pinCode.isPinCodeEmpty()) {
            pin = pinCode.getPinCode();
            showProgressDialog();
            userManager.respondOTP(new RespondOTP(userModel.getMobile(), pin), pinCallback);
        } else {
            TopPopUpManager.showTopPopUp(this, getString(R.string.pin_codes_incorrect), !TopPopUpManager.isError);
        }
    }


    @OnClick(R.id.btn_back)
    public void OnBackClick() {
        finish();
    }

    private void authWithFirebase(User user) {
        FirebaseUtils.authorize(
                user.getMobile(),
                successResult -> startNextActivity(user),
                exception -> Dialogs.showFailureDialog(this, exception.getLocalizedMessage())
        );
    }




    private void startNextActivity(User user) {
        Class targetActivity = Regex.isEmailValid(user.getHomeEmail())
                ? MainActivity.class : EmailActivity.class;

        Intent intent = new Intent(PinCodeActivity.this, targetActivity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    HandledCallback<LoginRespondOTP> pinCallback = new HandledCallback<LoginRespondOTP>() {
        @Override
        public void onSuccess(Call<LoginRespondOTP> call, Response<LoginRespondOTP> response) {
            userManager.setCurrentUserToken(PinCodeActivity.this, response.body().getToken());
            userManager.fetchUser(response.body(), fetchUserCallback);
        }


        @Override
        public void onFail(Call<LoginRespondOTP> call, Response<LoginRespondOTP> response) {
            TopPopUpManager.showTopPopUp(PinCodeActivity.this, response.body().getError(), TopPopUpManager.isError);
            cancelProgressDialog();
        }


        @Override
        public void onProcessFailed(Call<LoginRespondOTP> call, Throwable t, String error) {
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(PinCodeActivity.this, error, TopPopUpManager.isError);
        }
    };


    HandledCallback<ListResponseModel<User>> fetchUserCallback = new HandledCallback<ListResponseModel<User>>() {
        @Override
        public void onSuccess(Call<ListResponseModel<User>> call, Response<ListResponseModel<User>> response) {
            cancelProgressDialog();
            currentUser = response.body().getResult().get(CURRENT_USER_POSITION);
            currentUser.setDeviceToken(PushManager.getPushToken(PinCodeActivity.this));
            MyUserManager.getInstance().updateUser(new RequestUpdateUser(
                    MyUserManager.getInstance().getCurrentUserToken(PinCodeActivity.this),
                    currentUser), updateUserCallback);
        }


        @Override
        public void onFail(Call<ListResponseModel<User>> call, Response<ListResponseModel<User>> response) {

        }


        @Override
        public void onProcessFailed(Call<ListResponseModel<User>> call, Throwable t, String error) {

        }
    };


    HandledCallback<LoginRequestOTP> resendPhoneCallback = new HandledCallback<LoginRequestOTP>() {
        @Override
        public void onSuccess(Call<LoginRequestOTP> call, Response<LoginRequestOTP> response) {
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(PinCodeActivity.this, getString(R.string.auth_2_pin_resend), !TopPopUpManager.isError);
        }


        @Override
        public void onFail(Call call, Response response) {
            cancelProgressDialog();
        }


        @Override
        public void onProcessFailed(Call call, Throwable t, String error) {
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(PinCodeActivity.this, error, TopPopUpManager.isError);
        }
    };


    HandledCallback<UpdateUserResponse> updateUserCallback = new HandledCallback<UpdateUserResponse>() {
        @Override
        public void onSuccess(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            cancelProgressDialog();
            if (response.body().getSuccess() == (currentUser.getId())) {
                MyUserManager.setCurrentUser(PinCodeActivity.this, currentUser);
                authWithFirebase(currentUser);
            }
        }


        @Override
        public void onFail(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            cancelProgressDialog();
        }


        @Override
        public void onProcessFailed(Call<UpdateUserResponse> call, Throwable t, String error) {
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(PinCodeActivity.this, getString(R.string.unhandled_error_message), TopPopUpManager.isError);
        }
    };


    private OnPinCompleted onPinCompleted = new OnPinCompleted() {
        @Override
        public void isCompleted(boolean isCompleted) {
            if (isCompleted) {
                nextBtn.setVisibility(View.VISIBLE);
            } else {
                nextBtn.setVisibility(View.GONE);
            }
        }
    };


    public interface OnPinCompleted {

        void isCompleted(boolean isCompleted);
    }
}

package com.tundramobile.catchapp.ui.fragment.date_picker;

import android.view.LayoutInflater;
import android.view.View;

import java.util.Date;

/**
 * Created by andreybofanov on 25.04.17.
 */

public interface DateFormatProvider {
    View inflateEmptyView(LayoutInflater inflater);
    View inflateView(LayoutInflater inflater);
    View formatView(View view,Date date);
}

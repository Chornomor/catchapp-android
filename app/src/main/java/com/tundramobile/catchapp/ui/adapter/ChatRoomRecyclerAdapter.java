package com.tundramobile.catchapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.filter.ChatRoomFilter;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.ui.adapter.holder.ChatRoomHolder;
import com.tundramobile.catchapp.ui.viewmodel.ChatRoomViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class ChatRoomRecyclerAdapter extends RecyclerView.Adapter<ChatRoomHolder> implements
        Filterable {

    private List<ChatRoomViewModel> items = new ArrayList<>();
    private ChatRoomFilter filter;

    private RecyclerItemClickListener<ChatRoomViewModel> onItemClickListener;


    public ChatRoomRecyclerAdapter(List<ChatRoomViewModel> items) {
        this.items = items;
        Collections.sort(this.items, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    }


    @Override
    public ChatRoomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatRoomHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.chat_room_list_item_layout, parent, false), parent.getContext());
    }


    @Override
    public void onBindViewHolder(ChatRoomHolder holder, int position) {
        holder.bindContent(items.get(position));
        holder.itemView.setOnClickListener(v -> {
            if(onItemClickListener != null) {
                onItemClickListener.onItemClick(holder.getAdapterPosition(), items.get(position));
            }
        });
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ChatRoomFilter(this, items);
        }
        if (filter.isItemsEmpty()) filter.setItems(items);
        return filter;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addItems(List<ChatRoomViewModel> items) {
        if (items != null && !items.isEmpty()) {
            int idleCount = getItemCount();
            this.items.addAll(items);
            notifyItemRangeInserted(idleCount, getItemCount());
        }
    }


    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItem(int position) {
        notifyItemChanged(position);
    }


    public void setItems(List<ChatRoomViewModel> items) {
        this.items = items;
        Collections.sort(this.items, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(RecyclerItemClickListener<ChatRoomViewModel> listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return items == null || items.isEmpty() ? 0 : items.size();
    }


    public ChatRoomViewModel getItem(int position) {
        return position > items.size() ? null : items.get(position);
    }

    public void addItem(ChatRoomViewModel model) {
        items.add(model);
        notifyItemInserted(items.size() - 1);
    }
}

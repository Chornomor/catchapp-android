package com.tundramobile.catchapp.ui.adapter.holder;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.ui.view.WorkingTimeView;
import com.tundramobile.catchapp.ui.viewmodel.PlaceDetailsItem;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventPlaceDetailsHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback,
        View.OnClickListener {

    @BindView(R.id.progressBar) View progressBar;
    @BindView(R.id.mainContent) ViewGroup mainContent;
    @BindView(R.id.placeName) TextView placeName;
    @BindView(R.id.placeType) TextView placeType;
    @BindView(R.id.ratingText) TextView ratingText;
    @BindView(R.id.ratingBarRed) RatingBar ratingBarRed;
    @BindView(R.id.ratingBarGreen) RatingBar ratingBarGreen;
    @BindView(R.id.ratingBarBlue) RatingBar ratingBarBlue;
    @BindView(R.id.placeDetails) TextView placeDetails;
    @BindView(R.id.addressRow1) TextView addressRow1;
    @BindView(R.id.addressRow2) TextView addressRow2;
    @BindView(R.id.workingTimeContainer) LinearLayout workingTimeContainer;
    @BindView(R.id.webSite) TextView webSite;
    @BindView(R.id.phoneNumber) TextView phoneNumber;
    @BindView(R.id.mapview) MapView mapView;
    @BindView(R.id.location_layout) ViewGroup locationLayout;

    private EventDetailsClickListener listener;
    private GoogleMap googleMap;
    private PlaceDetailsItem item;
    private int type;


    public EventPlaceDetailsHolder(View itemView, EventDetailsClickListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
        initMap();
    }


    private void initMap() {
        mapView.onCreate(null);
        mapView.onResume();
        mapView.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(itemView.getContext().getApplicationContext());
        this.googleMap = googleMap;
        locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double latitude = item.getLocation().latitude;
                double longitude = item.getLocation().longitude;
                String name = "(" + item.getName() + ")";
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f" + name,
                        latitude, longitude, latitude, longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                mainContent.getContext().startActivity(intent);
            }
        });
        setMapLocation();
    }


    public void bindContent(PlaceDetailsItem item, int type) {
        this.item = item;
        this.type = type;

        if (item != null) {
            setPlaceName();
            setType();
            setupRatingBar();
            setDetails();
            setLocationText();
            setMapLocation();
            fillWorkingTime();
            setupWebsite();
            setPhoneNumber();

            progressBar.setVisibility(View.GONE);
            mainContent.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    private void setPlaceName() {
        placeName.setText(item.getName());
    }


    private void setupRatingBar() {
        ratingText.setText(String.valueOf(item.getRating()));

        switch (type) {
            case CatchUpManager.CONFIRMED:
                ratingBarGreen.setVisibility(View.VISIBLE);
                ratingBarGreen.setRating(item.getRating());
                break;
            case CatchUpManager.INCOMING:
                ratingBarRed.setVisibility(View.VISIBLE);
                ratingBarRed.setRating(item.getRating());
                break;
            case CatchUpManager.OUTGOING:
                ratingBarBlue.setVisibility(View.VISIBLE);
                ratingBarBlue.setRating(item.getRating());
                break;
        }
    }


    private void setLocationText() {
        addressRow1.setText(item.getAddress1());
        addressRow2.setText(item.getAddress2());
    }


    private void setMapLocation() {
        if (item != null && googleMap != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(item.getLocation(), 15));
        }
    }


    private void fillWorkingTime() {
        if (item.getWorkTimeItems() != null && !item.getWorkTimeItems().isEmpty()) {
            for (int i = 0; i < item.getWorkTimeItems().size(); i++) {
                int position = i == item.getWorkTimeItems().size() - 1 ? 0 : i + 1;
                WorkingTimeView workingTimeView = new WorkingTimeView(itemView.getContext());
                workingTimeView.setup(item.getWorkTimeItems().get(position), i == 0);
                workingTimeContainer.addView(workingTimeView);
            }
        }
    }


    private void setupWebsite() {
//        webSite.setText(item.getWebSite().replaceAll("^(http|https)://", "").replaceAll("/", ""));
        webSite.setText(item.getWebSite());
        webSite.setOnClickListener(this);
    }


    private void setPhoneNumber() {
        phoneNumber.setText(item.getPhone());
        phoneNumber.setOnClickListener(this);
    }


    private void setType() {
        if (!TextUtils.isEmpty(item.getType())) {
            placeType.setText(item.getType());
        } else {
            placeType.setVisibility(View.GONE);
        }
    }


    private void setDetails() {
        if (TextUtils.isEmpty(item.getDetails())) {
            placeDetails.setVisibility(View.GONE);
        } else {
            placeDetails.setText(item.getDetails());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.webSite:
                if (listener != null) listener.onWebSiteClick(item.getWebSite());
                break;
            case R.id.phoneNumber:
                if (listener != null) listener.onPhoneClick(item.getPhone());
                break;
        }
    }


    public void clearMap() {
        if (googleMap != null) {
            googleMap.clear();
            if (mapView != null) {
                mapView.onStop();
                mapView.onDestroy();
            }
        }
    }
}

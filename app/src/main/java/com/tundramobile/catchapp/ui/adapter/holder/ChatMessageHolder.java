package com.tundramobile.catchapp.ui.adapter.holder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.firebase.model.Message;
import com.tundramobile.catchapp.interfaces.BindingManager;
import com.tundramobile.catchapp.other.TimeUtils;
import com.tundramobile.catchapp.ui.viewmodel.ChatRoomViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class ChatMessageHolder extends RecyclerView.ViewHolder implements BindingManager<Message> {

//    @BindView(R.id.photo) CollageImageView image;
//    @BindView(R.id.name) FontTextView name;
//    @BindView(R.id.when) FontTextView when;
//    @BindView(R.id.message) FontTextView message;
//
//    @BindDimen(R.dimen.catchup_holder_avatar_size) int avatarSize;

    @BindView(R.id.message) TextView message;
    @BindView(R.id.when) TextView  when;

    private ChatRoomViewModel roomViewModel;
    private Context context;

    private long ownerID;

    public ChatMessageHolder(View itemView, Context context, long ownerID) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
        this.ownerID = ownerID;
    }

//    public static ChatMessageHolder create() {
//        return new
//    }


    @Override
    public void bindContent(Message model) {
        if(ownerID == model.getSenderId()) {
            message.setBackgroundResource(R.drawable.chat_owner_msg_bg);
            message.setTextColor(ContextCompat.getColor(context, R.color.white));
            ((LinearLayout) itemView).setGravity(Gravity.END);
        } else {
            message.setBackgroundResource(R.drawable.chat_msg_bg);
            message.setTextColor(ContextCompat.getColor(context, R.color.black));
            ((LinearLayout) itemView).setGravity(Gravity.START);
        }
        message.setText(model.getText());
        when.setText(TimeUtils.getRelativeTime(model.getTimestamp()));
//        roomViewModel = model;
//
//        when.setText(model.getWhen());
//        message.setText(model.getMsg());


    }
}

package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.OpeningHours;
import com.tundramobile.catchapp.entity.Schedule;

import java.text.DateFormatSymbols;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorkingTimeView extends LinearLayout {

    @BindView(R.id.clockImage) View clockImage;
    @BindView(R.id.dayOfWeek) TextView dayOfWeek;
    @BindView(R.id.timeSchedule) TextView timeSchedule;

    private boolean showClock;
    private int day;
    private String time;

    private String[] days;


    public WorkingTimeView(Context context) {
        super(context);
        init(context);
    }


    public WorkingTimeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public WorkingTimeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public WorkingTimeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_working_time, this, true);
        ButterKnife.bind(this);

        days = context.getResources().getStringArray(R.array.days);
    }


    public void setShowClock(boolean showClock) {
        this.showClock = showClock;
        clockImage.setVisibility(showClock ? VISIBLE : INVISIBLE);
    }


    public void setDayOfWeek(int day) {
        this.day = day;
        dayOfWeek.setText(days[day]);
    }


    public void setTimeSchedule(String time) {
        this.time = time;
        timeSchedule.setText(time);
    }


    public void setup(OpeningHours workTimeItem, boolean isFirst) {
        setShowClock(isFirst);
        setDayOfWeek(workTimeItem.getOpen().getDay());
        String schedule = getFormattedTime(workTimeItem.getOpen()) + "-" + getFormattedTime(workTimeItem.getClose());
        setTimeSchedule(schedule);
    }


    private String getFormattedTime(@Nullable Schedule schedule) {
        if (schedule == null || TextUtils.isEmpty(schedule.getTime())) return "";
        return new StringBuilder(schedule.getTime()).insert(schedule.getTime().length() - 2, ":").toString();
    }
}

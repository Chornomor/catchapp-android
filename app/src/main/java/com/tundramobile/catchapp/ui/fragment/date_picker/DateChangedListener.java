package com.tundramobile.catchapp.ui.fragment.date_picker;

import java.util.Date;

/**
 * Created by andreybofanov on 25.04.17.
 */

public interface DateChangedListener {
    void onMonthChanged(int year,int month);
    void onDateSelected(Date date);
}

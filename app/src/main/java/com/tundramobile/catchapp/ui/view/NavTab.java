package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.TabData;

import butterknife.BindString;
import butterknife.BindView;

public class NavTab extends BaseTab {

    @BindView(R.id.image) ImageView image;
    @BindView(R.id.title) FontTextView title;

    @BindString(R.string.montserrat_light) String montserratLight;
    @BindString(R.string.montserrat_semi_bold) String montserratSemiBold;


    public NavTab(Context context, TabData tabData) {
        super(context, tabData);
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.tab;
    }


    @Override
    protected void setupViews(TabData tabData, boolean isSelected) {
        image.setImageResource(isSelected ? tabData.getImageSelected() : tabData.getImageDefault());
        title.setText(tabData.getTitle());
        title.setCustomFont(isSelected ? montserratSemiBold : montserratLight);
        title.setTextColor(ContextCompat.getColor(getContext(),
                isSelected ? tabData.getSelectedColor() : tabData.getDefaultColor()));
    }

}

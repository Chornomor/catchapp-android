package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.interfaces.TabDataI;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;

import butterknife.ButterKnife;

public class ContactsTabLayout extends TabLayout implements TabLayout.OnTabSelectedListener {

    private static final String TAG = ContactsTabLayout.class.getSimpleName();
    private static final String SELECTED_POSITION = "SELECTED_POSITION";

    private NonSwipeViewPager mViewPager;


    public ContactsTabLayout(Context context) {
        super(context);
        ButterKnife.bind(this);
    }


    public ContactsTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        ButterKnife.bind(this);
    }


    public ContactsTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ButterKnife.bind(this);
    }


    @Override
    public void setupWithViewPager(ViewPager viewPager) {
        super.setupWithViewPager(viewPager);
        mViewPager = (NonSwipeViewPager) viewPager;
        initTabs();
        addOnTabSelectedListener(this);
        highlightTab(viewPager.getCurrentItem());
    }


    public void initTabs() {
        for (int i = 0; i < getTabCount(); i++) {
            Tab tab = getTabAt(i);
            Utils.logd(TAG, "initTabs: tab = " + tab);
            if (tab != null) {
                TabData tabData = ((TabDataI) ((TabsPagerAdapter) mViewPager.getAdapter())
                        .getFragments().get(i)).getTabData();
                tab.setCustomView(new ContactTab(getContext(), tabData));
            }
        }
    }


    public void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            onTabSelected(getTabAt(savedInstanceState.getInt(SELECTED_POSITION)));
        }
    }


    public Bundle saveState(Bundle outState) {
        outState.putInt(SELECTED_POSITION, getSelectedTabPosition());
        return outState;
    }


    public void highlightTab(int position) {
        Tab t = getTabAt(position);
        if (t != null) onTabSelected(t);
    }


    public void selectTab(int position) {
        Tab t = getTabAt(position);
        if (t != null) t.select();
    }


    private View setTabUI(boolean isActive, View v) {
        Utils.logd(TAG, "setTabUI: isActive = " + isActive + ", view = " + (v));
        if (v != null)
            ((ContactTab) v).setSelected(isActive);
        return v;
    }


    @Override
    public void onTabSelected(Tab tab) {
        Utils.logd(TAG, "onTabSelected: " + tab.getPosition());
        for (int i = 0; i < getTabCount(); i++) {
            Tab t = getTabAt(i);
            if (t != null) setTabUI(tab.getPosition() == i, t.getCustomView());
        }
    }


    @Override
    public void onTabUnselected(Tab tab) {

    }


    @Override
    public void onTabReselected(Tab tab) {

    }

}
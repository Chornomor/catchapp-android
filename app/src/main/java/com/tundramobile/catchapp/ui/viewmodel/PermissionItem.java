package com.tundramobile.catchapp.ui.viewmodel;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import rebus.permissionutils.PermissionEnum;

/**
 * Created by Sviatoslav Zaitsev on 06.04.17.
 */

public class PermissionItem {
    private int image;
    private int title;
    private PermissionEnum[] permissions;


    public PermissionItem(@DrawableRes int image, @StringRes int title, PermissionEnum... permissions) {
        this.image = image;
        this.title = title;
        this.permissions = permissions;
    }

    public int getImage() {
        return image;
    }

    public int getTitle() {
        return title;
    }

    public PermissionEnum[] getPermissions() {
        return permissions;
    }
}

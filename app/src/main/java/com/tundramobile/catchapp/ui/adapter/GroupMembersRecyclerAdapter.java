package com.tundramobile.catchapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.ui.adapter.holder.GroupMemberHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GroupMembersRecyclerAdapter extends RecyclerView.Adapter<GroupMemberHolder> {

    private List<Contact> items;


    public GroupMembersRecyclerAdapter(List<Contact> items) {
        this.items = items;
        Collections.sort(this.items, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    }


    @Override
    public GroupMemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupMemberHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.holder_group_member, parent, false), parent.getContext());
    }


    @Override
    public void onBindViewHolder(GroupMemberHolder holder, int position) {
        holder.bindContent(items.get(position));
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addItems(List<Contact> items) {
        if (items != null && !items.isEmpty()) {
            int idleCount = getItemCount();
            this.items.addAll(items);
            notifyItemRangeInserted(idleCount, getItemCount());
        }
    }


    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }


    public void setItems(List<Contact> items) {
        this.items = items;
        Collections.sort(this.items, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return items == null || items.isEmpty() ? 0 : items.size();
    }


    public ArrayList<Contact> getItems() {
        return (ArrayList<Contact>) items;
    }
}

package com.tundramobile.catchapp.ui.viewmodel;

public class WorkTimeItem {

    private boolean isFirst;
    private int day;
    private String time;


    public WorkTimeItem() {
    }


    public WorkTimeItem(boolean isFirst, int day, String time) {
        this.isFirst = isFirst;
        this.day = day;
        this.time = time;
    }


    public boolean isFirst() {
        return isFirst;
    }


    public void setFirst(boolean first) {
        isFirst = first;
    }


    public int getDay() {
        return day;
    }


    public void setDay(int day) {
        this.day = day;
    }


    public String getTime() {
        return time;
    }


    public void setTime(String time) {
        this.time = time;
    }
}

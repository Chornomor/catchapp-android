package com.tundramobile.catchapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.SimplePlace;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.request.CreateCatchAppBody;
import com.tundramobile.catchapp.entity.response.CreateCatchUpResponse;
import com.tundramobile.catchapp.event.EventCreatedEvent;
import com.tundramobile.catchapp.interfaces.OnEventReadyToCreateListener;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.GPSUtil;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.NewEventWhenAdapter;
import com.tundramobile.catchapp.ui.adapter.NewEventWhoAdapter;
import com.tundramobile.catchapp.ui.fragment.calendar.EditCalendarFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class NewEventFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener
        , GoogleApiClient.ConnectionCallbacks, NewEventWhenAdapter.OnAddWhenItemClickListener, OnEventReadyToCreateListener {

    public final static int ONE_PERSON = 0;
    public final static int SEVERAL_PEOPLE = 1;
    public final static int GROUP = 2;
    public final static String EVENT_MODE = "current_mode";
    public static final String SELECTED_CONTACTS = "SELECTED_CONTACTS";
    public final static String SCHEDULE_ITEMS = "SCHEDULE_ITEMS";
    public final static String SELECTED_PLACE = "SELECTED_PLACE";
    public final static String EVENT_CREATED = "EVENT_CREATED";

    private static final String TITLE_TEXT = "TITLE_TEXT";
    private static final String MESSAGE_TEXT = "MESSAGE_TEXT";

    @BindView(R.id.event_icon) ImageView eventIcon;
    @BindView(R.id.event_title) TextView eventTitle;
    @BindView(R.id.btn_create_new_event) TextView btnCreateNewEvent;

    @BindView(R.id.tv_who_title) TextView whoTitle;
    @BindView(R.id.tv_who_hint) TextView whoHint;
    @BindView(R.id.rv_new_event_who) RecyclerView rvEventWho;

    @BindView(R.id.when_layout) View whenLayout;
    @BindView(R.id.tv_when_hint) View whenHint;
    @BindView(R.id.tv_when_title) TextView whenTitle;
    @BindView(R.id.when_right_arrow) View whenRightArrow;
    @BindView(R.id.rv_new_event_when) RecyclerView rvEventWhen;

    @BindView(R.id.tv_where_hint) TextView whereHint;
    @BindView(R.id.tv_where_title) TextView whereTitle;
    @BindView(R.id.where_place_image) ImageView placeImage;
    @BindView(R.id.where_place_name) TextView placeName;
    @BindView(R.id.where_place_address) TextView placeAddress;
    @BindView(R.id.where_right_arrow) View whereRightArrow;
    @BindView(R.id.place_layout) FrameLayout placeLayout;

    @BindView(R.id.tv_new_event_title_name) TextView tvEventTitleName;
    @BindView(R.id.et_new_event_title) EditText etEventTitleName;


    @BindView(R.id.tv_new_event_title_message_1) TextView tvEventMessage1;
    @BindView(R.id.tv_new_event_title_message_2) TextView tvEventMessage2;
    @BindView(R.id.et_new_event_message) EditText etEventMessage;

    private NewEventWhoAdapter whoAdapter;
    private NewEventWhenAdapter whenAdapter;
    private AppToolbar toolbar;

    private ArrayList<Contact> contacts = new ArrayList<>();
    private ArrayList<ScheduleItem> scheduleItems = new ArrayList<>();
    private SimplePlace selectedPlace;
    private CreateCatchAppBody catchAppBody;

    private String titleText = "";
    private String messageText = "";

    private int eventMode = 1;

    TextWatcher titleWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }


        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }


        @Override
        public void afterTextChanged(Editable editable) {
            catchAppBody.setEventName(etEventTitleName.getText().toString());
        }
    };


    public static NewEventFragment newInstance(int eventMode) {
        NewEventFragment fragment = new NewEventFragment();
        Bundle args = new Bundle();
        args.putInt(EVENT_MODE, eventMode);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                eventMode = getArguments().getInt(EVENT_MODE);
            }
        } else {
            eventMode = savedInstanceState.getInt(EVENT_MODE);
            contacts = savedInstanceState.getParcelableArrayList(SELECTED_CONTACTS);
            scheduleItems = savedInstanceState.getParcelableArrayList(SCHEDULE_ITEMS);
            selectedPlace = savedInstanceState.getParcelable(SELECTED_PLACE);
            titleText = savedInstanceState.getString(TITLE_TEXT);
            messageText = savedInstanceState.getString(MESSAGE_TEXT);
        }
        getNavigatorData();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_event, container, false);
        ButterKnife.bind(this, v);
        catchAppBody = new CreateCatchAppBody(this);
        initEventMode();
        initEventTitleName();
        initEventMessage();
        initWhoRecycleView();
        initWhenRecycleView();
        initWhereView();

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    private void getNavigatorData() {
        getNavContactsData();
        getNavScheduleData();
        getNavPlaceData();
    }


    private void getNavContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_CONTACTS);
        if (bundleFromNavigator != null) {
            contacts.clear();
            contacts.addAll(bundleFromNavigator.getParcelableArrayList(SELECTED_CONTACTS));
        }
    }


    private void getNavScheduleData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SCHEDULE_ITEMS);
        if (bundleFromNavigator != null) {
            scheduleItems.clear();
            scheduleItems.addAll(bundleFromNavigator.getParcelableArrayList(SCHEDULE_ITEMS));
        }
    }


    private void getNavPlaceData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_PLACE);
        if (bundleFromNavigator != null) {
            selectedPlace = new Gson().fromJson(bundleFromNavigator.getString(SELECTED_PLACE), SimplePlace.class);
        }
    }


    private void initEventTitleName() {
        etEventTitleName.addTextChangedListener(titleWatcher);
        etEventTitleName.setOnEditorActionListener((textView, i, keyEvent) -> {
                    if (i == EditorInfo.IME_ACTION_DONE) {
                        hideKeyboard();
                        etEventTitleName.clearFocus();
                    }
                    return false;
                }
        );

        etEventTitleName.setOnFocusChangeListener((view, b) -> {
            setTitleColor(b);
        });

        etEventTitleName.setText(titleText);
        setTitleColor(false);
    }


    public void setTitleColor(boolean hasFocus) {
        int titleColor = 0;
        if (hasFocus) {
            titleColor = getContext().getResources().getColor(R.color.purple);
        } else {
            if (etEventTitleName.getText().toString().isEmpty()) {
                titleColor = getContext().getResources().getColor(R.color.title_black);
            } else {
                titleColor = getContext().getResources().getColor(R.color.common_text_color);
            }
        }
        tvEventTitleName.setTextColor(titleColor);
    }


    private void initEventMessage() {
        etEventMessage.setOnEditorActionListener((textView, i, keyEvent) -> {
                    if (i == EditorInfo.IME_ACTION_DONE) {
                        hideKeyboard();
                        etEventMessage.clearFocus();
                    }
                    return false;
                }
        );
        etEventMessage.setOnFocusChangeListener((view, b) -> {
            setMessageColor(b);
        });

        etEventMessage.setText(messageText);
        setMessageColor(false);
    }


    public void setMessageColor(boolean hasFocus) {
        int titleColor = 0;
        if (hasFocus) {
            titleColor = getContext().getResources().getColor(R.color.purple);
        } else {
            if (etEventMessage.getText().toString().isEmpty()) {
                titleColor = getContext().getResources().getColor(R.color.title_black);
            } else {
                titleColor = getContext().getResources().getColor(R.color.common_text_color);
            }
        }
        tvEventMessage1.setTextColor(titleColor);
        tvEventMessage2.setTextColor(titleColor);
    }


    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }


    private void initWhoRecycleView() {
        if (!contacts.isEmpty()) {
            rvEventWho.setVisibility(View.VISIBLE);
            whoTitle.setTextColor(getResources().getColor(R.color.common_text_color));
            whoHint.setVisibility(View.GONE);

            whoAdapter = new NewEventWhoAdapter((position, model) -> onWhoClick());
            whoAdapter.setItems(contacts);
            setInvitedUsers();
            StaggeredGridLayoutManager gridManager = new StaggeredGridLayoutManager(4, 1);
            gridManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
            rvEventWho.setLayoutManager(gridManager);
            rvEventWho.setNestedScrollingEnabled(false);
            rvEventWho.setAdapter(whoAdapter);
        } else {
            rvEventWho.setVisibility(View.GONE);
            whoTitle.setTextColor(getResources().getColor(R.color.title_black));
            whoHint.setVisibility(View.VISIBLE);
        }
    }


    private void setInvitedUsers() {
        List<Integer> users = new ArrayList<>();
        for (Contact contact : contacts) {
            users.add(contact.getUid());
        }
        catchAppBody.setInvitedUsers(users);
    }


    private void initWhenRecycleView() {
        if (!scheduleItems.isEmpty()) {
            whenHint.setVisibility(View.GONE);
            whenTitle.setTextColor(getResources().getColor(R.color.common_text_color));
            whenLayout.setVisibility(View.GONE);
            whenRightArrow.setVisibility(View.GONE);
            rvEventWhen.setVisibility(View.VISIBLE);

            if (whenAdapter == null) {
                whenAdapter = new NewEventWhenAdapter(this);
                whenAdapter.setItems(scheduleItems);
                LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
                rvEventWhen.setLayoutManager(linearManager);
                rvEventWhen.setNestedScrollingEnabled(false);
                rvEventWhen.setAdapter(whenAdapter);
            }
            whenAdapter.setItems(scheduleItems);
            setTimes();
        } else {
            whenHint.setVisibility(View.VISIBLE);
            whenTitle.setTextColor(getResources().getColor(R.color.title_black));
            whenLayout.setVisibility(View.VISIBLE);
            whenRightArrow.setVisibility(View.VISIBLE);
            rvEventWhen.setVisibility(View.GONE);
        }
    }


    private void setTimes() {
        List<List<Long>> times = new ArrayList<>();
        for (ScheduleItem scheduleItem : scheduleItems) {
            List<Long> time = new ArrayList<>();
            time.add(scheduleItem.getDateFromLong());
            time.add(scheduleItem.getDateToLong());
            times.add(time);
        }
        catchAppBody.setTimes(times);
    }


    private void removeWhen(int position) {
        scheduleItems.remove(position);
        if (scheduleItems.isEmpty()) {
            initWhenRecycleView();
            return;
        }
        whenAdapter.setItems(scheduleItems);
    }


    private void initWhereView() {
        if (selectedPlace != null) {
            whereTitle.setTextColor(getResources().getColor(R.color.common_text_color));
            if (selectedPlace.getLocationId() == 0) {
                whereHint.setVisibility(View.GONE);
                whereRightArrow.setVisibility(View.GONE);
                placeLayout.setVisibility(View.VISIBLE);

                placeName.setText(selectedPlace.getName());
                placeAddress.setText(selectedPlace.getSecondaryText());
                GPSUtil.requestPlacePhoto(((MainActivity) getActivity()).getGoogleApiClient()
                        , selectedPlace.getId(), placeImage, 0);
            } else {
                whereHint.setVisibility(View.VISIBLE);
                whereRightArrow.setVisibility(View.VISIBLE);
                placeLayout.setVisibility(View.GONE);
                whereHint.setText(selectedPlace.getName());
                whereHint.setTextColor(getResources().getColor(R.color.title_black));
            }
            catchAppBody.setLocation(selectedPlace);
        } else {
            whereHint.setVisibility(View.VISIBLE);
            whereTitle.setTextColor(getResources().getColor(R.color.title_black));
            whereRightArrow.setVisibility(View.VISIBLE);
            placeLayout.setVisibility(View.GONE);
        }
    }


    private void initEventMode() {
        switch (eventMode) {
            case ONE_PERSON:
                eventIcon.setImageDrawable(getActivity().getDrawable(R.drawable.new_event_one_person));
                eventTitle.setText(getString(R.string.invite_one_person));
                catchAppBody.setFundamentalType(false);
                break;
            case SEVERAL_PEOPLE:
                eventIcon.setImageDrawable(getActivity().getDrawable(R.drawable.new_event_several_people));
                eventTitle.setText(getString(R.string.invite_several_people));
                catchAppBody.setFundamentalType(false);
                break;
            case GROUP:
                eventIcon.setImageDrawable(getActivity().getDrawable(R.drawable.new_event_group));
                eventTitle.setText(getString(R.string.invite_group));
                catchAppBody.setFundamentalType(true);
                break;
        }
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if (toolbar != null) {
            toolbar.setVisibility(View.GONE);
            this.toolbar = toolbar;
        }
    }


    @Override
    public void onStop() {
        toolbar.setVisibility(View.VISIBLE);
        super.onStop();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(EVENT_MODE, eventMode);
        outState.putParcelableArrayList(SELECTED_CONTACTS, contacts);
        outState.putParcelableArrayList(SCHEDULE_ITEMS, scheduleItems);
        outState.putParcelable(SELECTED_PLACE, selectedPlace);
        outState.putString(TITLE_TEXT, etEventTitleName.getText().toString());
        outState.putString(MESSAGE_TEXT, etEventMessage.getText().toString());
        super.onSaveInstanceState(outState);
    }


    @OnClick(R.id.btn_back)
    public void onBackClick() {
        hideKeyboard();
        getActivity().onBackPressed();
    }


    @OnClick(R.id.btn_create_new_event)
    public void onCreateEvent() {
        hideKeyboard();
        catchAppBody.setMessage(etEventMessage.getText().toString());
        ((ProgressDialogHandler) getContext()).showProgressDialog();
        CatchUpManager.getInstance(getActivity()).createCatchUp(MyUserManager.getInstance().getCurrentUserToken(getActivity())
                , MyUserManager.getCurrentUser(getActivity()).getId(), catchAppBody, createCatchUpCallback);
    }


    @OnClick(R.id.who_layout)
    public void onWhoClick() {
        hideKeyboard();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SELECTED_CONTACTS, contacts);
        Navigator.with(getActivity()).putData(SELECTED_CONTACTS, bundle);
        showFragment(SelectEventMembersFragment.newInstance(eventMode));
    }


    @OnClick(R.id.when_layout)
    public void onWhenClick() {
        hideKeyboard();
        int selectionType = eventMode == GROUP ? EditCalendarFragment.SINGLE : EditCalendarFragment.MULTI;
        EditCalendarFragment f = EditCalendarFragment.makeSelect(getContext(), selectionType, scheduleItems);
        showFragment(f);
    }


    @OnClick(R.id.where_layout)
    public void onWhereClick() {
        hideKeyboard();
        User user = MyUserManager.getCurrentUser(getContext());
        showFragment(LocationSearchFragment.newInstance(MyUserManager.getInstance().getCurrentUserToken(getContext()), user.getId()));
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }


    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onAddWhenItemClick() {
        onWhenClick();
    }


    @Override
    public void onWhenRemoved(int position) {
        removeWhen(position);
    }


    @Override
    public void onEventReadyToCreate(boolean isReady) {
        if (isReady) {
            btnCreateNewEvent.setVisibility(View.VISIBLE);
        } else {
            btnCreateNewEvent.setVisibility(View.INVISIBLE);
        }
    }


    HandledCallback<CreateCatchUpResponse> createCatchUpCallback = new HandledCallback<CreateCatchUpResponse>() {
        @Override
        public void onSuccess(Call<CreateCatchUpResponse> call, Response<CreateCatchUpResponse> response) {
            EventBus.getDefault().post(new EventCreatedEvent(getString(R.string.event_created_title), getString(R.string.event_created_message)));
            ((ProgressDialogHandler) getContext()).cancelProgressDialog();
            ((MainActivity) getActivity()).reselectCurrent();
        }


        @Override
        public void onFail(Call<CreateCatchUpResponse> call, Response<CreateCatchUpResponse> response) {
            ((ProgressDialogHandler) getContext()).cancelProgressDialog();
            TopPopUpManager.showTopPopUp(getActivity(), "Internal server error", TopPopUpManager.isError);
        }


        @Override
        public void onProcessFailed(Call<CreateCatchUpResponse> call, Throwable t, String error) {
            ((ProgressDialogHandler) getContext()).cancelProgressDialog();
            TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
        }
    };
}

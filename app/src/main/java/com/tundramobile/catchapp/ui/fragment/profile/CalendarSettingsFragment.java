package com.tundramobile.catchapp.ui.fragment.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.manager.SharedPrefManager;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by eugenetroyanskii on 24.04.17.
 */

public class CalendarSettingsFragment extends BaseFragment {

    @BindView(R.id.profile_schedule_view_switch) Switch scheduleView;
    @BindView(R.id.profile_hour_view_switch) Switch hoursView;

    private Unbinder unbinder;
    private SharedPrefManager sharedPrefManager;

    public static CalendarSettingsFragment newInstance() {
        return new CalendarSettingsFragment();
    }


    public CalendarSettingsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_calendar, container, false);
        unbinder = ButterKnife.bind(this, v);
        sharedPrefManager = SharedPrefManager.getInstance(getActivity());
        setupSwitches();
        return v;
    }


    private void setupSwitches() {
        scheduleView.setChecked(sharedPrefManager.isScheduleViewEnabled());
        hoursView.setChecked(sharedPrefManager.isHoursViewEnabled());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.profile_back_btn)
    public void onBack() {
        onBackPressed();
    }


    @OnClick(R.id.profile_save_btn)
    public void onSave() {
        sharedPrefManager.saveScheduleView(scheduleView.isChecked());
        sharedPrefManager.saveHoursView(hoursView.isChecked());
        DateUtil.updateTimeFormat(getContext());
        onBackPressed();
    }


    @OnClick(R.id.profile_schedule_view_switch)
    public void onScheduleSwitch() {
    }


    @OnClick(R.id.profile_hour_view_switch)
    public void onHourViewSwitch() {

    }
}

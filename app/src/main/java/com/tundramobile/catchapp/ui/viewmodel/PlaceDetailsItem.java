package com.tundramobile.catchapp.ui.viewmodel;

import com.google.android.gms.maps.model.LatLng;
import com.tundramobile.catchapp.entity.GooglePlace;
import com.tundramobile.catchapp.entity.OpeningHours;

import java.util.ArrayList;

public class PlaceDetailsItem {

    private String name;
    private String type;
    private float rating;
    private int reviewsCount;
    private String details;
    private String address1;
    private String address2;
    private LatLng location;
    private ArrayList<OpeningHours> workTimeItems;
    private ArrayList<String> formattedOpeningHours;
    private String webSite;
    private String phone;

    private GooglePlace place;


    public PlaceDetailsItem(GooglePlace place) {
        this.place = place;

        this.name = place.getName();
        try {
            this.type = place.getTypes().get(0);
        } catch (Exception ignored) {
            this.type = "";
        }
        this.rating = place.getRating();
//        this.details = place.
        if(!place.getAddress().isEmpty()) {
            int i = place.getAddress().indexOf(",");
            if(i == -1){
                i = place.getAddress().length();
            }
            this.address1 = place.getAddress().substring(0, i);
            if(address1.length() + 2 < place.getAddress().length()) {
                this.address2 = place.getAddress().substring(address1.length() + 2, place.getAddress().length());
            }
        }
        this.location = place.getLatLng();
        this.workTimeItems = place.getOpeningHours();
        this.webSite = place.getUrlString();
        this.phone = place.getPhoneNumber();
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public float getRating() {
        return rating;
    }


    public void setRating(float rating) {
        this.rating = rating;
    }


    public int getReviewsCount() {
        return reviewsCount;
    }


    public void setReviewsCount(int reviewsCount) {
        this.reviewsCount = reviewsCount;
    }


    public String getDetails() {
        return details;
    }


    public void setDetails(String details) {
        this.details = details;
    }


    public String getAddress1() {
        return address1;
    }


    public void setAddress1(String address1) {
        this.address1 = address1;
    }


    public String getAddress2() {
        return address2;
    }


    public void setAddress2(String address2) {
        this.address2 = address2;
    }


    public LatLng getLocation() {
        return location;
    }


    public void setLocation(LatLng location) {
        this.location = location;
    }


    public ArrayList<OpeningHours> getWorkTimeItems() {
        return workTimeItems;
    }


    public void setWorkTimeItems(ArrayList<OpeningHours> workTimeItems) {
        this.workTimeItems = workTimeItems;
    }


    public String getWebSite() {
        return webSite;
    }


    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }


    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public GooglePlace getPlace() {
        return place;
    }


    public ArrayList<String> getFormattedOpeningHours() {
        return formattedOpeningHours;
    }


    public void setFormattedOpeningHours(ArrayList<String> formattedOpeningHours) {
        this.formattedOpeningHours = formattedOpeningHours;
    }
}

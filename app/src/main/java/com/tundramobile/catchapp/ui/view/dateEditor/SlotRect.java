package com.tundramobile.catchapp.ui.view.dateEditor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.ui.adapter.holder.ScheduleHolder;
import com.tundramobile.catchapp.ui.fragment.calendar.EditCalendarFragment;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;


public class SlotRect implements Comparable<SlotRect>,Serializable {
    private static final int HOUR = 1000*60*60;
    private static final int MINUTE = 1000*60;
    public static final int STEP_MILIS = MINUTE*5;

    float timeStep;

    transient Rect bounds,deleteBounds;
    public Date start;
    public Date end;
    transient ScheduleHolder view;

    transient Context context;

    boolean isNew = true;

    ScheduleItem item;

    int maxBottom = 0;


    boolean deleteClickStarted = false;

    public SlotRect(Context context,Rect bounds, Date start) {
        this.bounds = bounds;
        this.start = start;
        this.context = context;
        end = heightToDate(start,end,bounds.height());
        deleteBounds = new Rect();
        deleteBounds.right = bounds.right;
        deleteBounds.top = bounds.top;
        int deleteSize = (int) context.getResources().getDimension(R.dimen.time_slot_delete_btn);
        deleteBounds.left = deleteBounds.right - deleteSize*2;
        deleteBounds.bottom = deleteBounds.top + deleteSize*2;
        init();
        calculateMaxBottom();
    }

    public SlotRect(Context context,ScheduleItem item){
        this.item = item;
        this.start = item.dateFrom;
        this.end = item.dateTo;
        this.context = context;
        init();
    }
    public SlotRect(Context context,Date from,Date to){
        this.start = from;
        this.end = to;
        this.context = context;
        init();
    }

    void init(){
        timeStep = context.getResources().getDimension(R.dimen.time_slot_height)/12;
    }

    void calculateMaxBottom(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        int day = calendar.get(Calendar.DAY_OF_YEAR);
        calendar.set(Calendar.DAY_OF_YEAR,day+1);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        maxBottom = bounds.top + datesToHeight(start,calendar.getTime());
        Log.d("DEBUG","maxBottom " + maxBottom);
    }


    public void measure(int left, int right, long timeStart){
        bounds = new Rect();
        bounds.top = (int) ((start.getTime() - timeStart)/STEP_MILIS * timeStep);
        bounds.left = left;
        bounds.bottom = (int)(bounds.top + (end.getTime() - start.getTime())/STEP_MILIS*timeStep);
        bounds.right= right;
        calculateMaxBottom();
    }


    public boolean shouldDraw(float top, float botttom){
        boolean shouldDraw = false;
        float bTop = bounds.top;
        float bBot = bounds.bottom;
        if((bTop > top && bTop < botttom)
                || (bBot > top && bBot < botttom)
                || (bTop < top && bBot > botttom))
        {
            shouldDraw = true;
        }

        return shouldDraw;
    }
    public void prepare(ScheduleHolder holder){
        if(view != null) {
            updateSlotType();
            return;
        }
        view = holder;
        if(item != null){
            view.bind(item);
            updateSlotType();
        } else {
            bind();
        }
    }


    ScheduleHolder getView(){
        return view;
    }
    void recycleView(){
        view = null;
    }

    void setBottom(float bottom){
        bounds.bottom = (int) bottom;
        if(bounds.height() <= timeStep*3){
            bounds.bottom = (int) (bounds.top + timeStep*3);
        } else if(bounds.bottom > maxBottom){
            bounds.bottom = maxBottom;
        }
        end = heightToDate(start,end,bounds.height());
        bind();
    }

    void bind(){
        if(view == null)
            return;
        long diff = (long) ((int)(bounds.height()/timeStep) * STEP_MILIS);
        String time = DateUtil.formatTime(start) +
                " - " + DateUtil.formatTime(end);
        view.tvTitle.setAllCaps(false);
        view.setTitle(time);
        int hours = (int) (diff/HOUR);
        diff -= hours*HOUR;
        int minutes = (int) (diff/MINUTE);
        String period = "";
        if(hours > 0){
            period = hours + " " +
                    context.getResources().getQuantityString(R.plurals.hour_plural,hours) + " ";
        }
        if(minutes > 0){
            period += minutes + " " +
                    context.getResources().getQuantityString(R.plurals.min_plural,minutes);
        }
        view.setBackgroundColor(view.itemView.getContext(),
                view.itemView.getResources().getColor(R.color.outgoing));
        view.setDescription(period);
        view.tvTime.setText(null);
        updateSlotType();
    }

    void updateSlotType(){
        int widthSpec = View.MeasureSpec.makeMeasureSpec(bounds.width(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(bounds.height(), View.MeasureSpec.EXACTLY);
        view.itemView.measure(widthSpec, heightSpec);
        view.itemView.layout(0,0,bounds.width(),bounds.height());
        view.setSemiTransparent(isNew);
        view.setDragpinVisible(isNew);
        view.setDeleteBtnVisibility(isNew);
    }

    void draw(Canvas canvas){
        canvas.save();
        canvas.translate(bounds.left,bounds.top);
        view.itemView.draw(canvas);
        canvas.restore();
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    int datesToHeight(Date a,Date b){
        long diff = b.getTime() - a.getTime();
        return (int) (diff/STEP_MILIS*timeStep);
    }
    Date heightToDate(Date from,Date to,int height){
        if(to == null){
            to = new Date();
        }
        int steps = (int) (height/timeStep);
        to.setTime((long) (from.getTime() + steps*STEP_MILIS));
        return to;
    }

    boolean checkClickDown(float x,float y){
        if(deleteBounds != null && deleteBounds.contains((int)x, (int) y)){
            deleteClickStarted = true;
            return true;
        }
        return false;
    }

    boolean wasClickDone(float x, float y){
        if(deleteBounds != null && deleteClickStarted && deleteBounds.contains((int)x, (int) y)){
            deleteClickStarted = false;
            return true;
        }
        return false;
    }

    void resetClickState(){
        deleteClickStarted = false;
    }

    @Override
    public int compareTo(@NonNull SlotRect o) {
        return bounds.top - o.bounds.top;
    }

}

package com.tundramobile.catchapp.ui.view.dateEditor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.ui.adapter.EditScheduleAdapter;
import com.tundramobile.catchapp.ui.adapter.holder.ScheduleHolder;
import com.tundramobile.catchapp.ui.fragment.calendar.EditCalendarFragment;
import com.tundramobile.catchapp.ui.fragment.calendar.EditItemsHolder;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;


public class ScheduleRecycler extends RecyclerView
        implements EditScheduleAdapter.OnTimeSlotSelected
{


    long currentScroll = 0;

    Deque<ScheduleHolder> viewsToRecycle = new LinkedList<>();
    ArrayList<SlotRect> viewsToDraw = new ArrayList<>();
    EditItemsHolder itemsHolder;
    ArrayList<SlotRect> existingItems = new ArrayList<>();

    EditScheduleAdapter adapter;

    SlotRect draggingSlot;
    boolean isClicking = false;

    boolean editable = true;

    boolean hasSize = false;

    int firstVisiblePosition = 0;

    LinearLayoutManager layoutManager;
    OnDateChangedListener dateListener;
    ItemsHelper itemListener;

    public ScheduleRecycler(Context context) {
        super(context);
        init();
    }

    public ScheduleRecycler(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScheduleRecycler(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void init(){
        layoutManager = new LinearLayoutManager(getContext());
        setLayoutManager(layoutManager);
        adapter = new EditScheduleAdapter(getContext(),this);
        setAdapter(adapter);
    }

    public void setItemListener(ItemsHelper itemListener) {
        this.itemListener = itemListener;
        if(itemListener != null) {
            editable = !itemListener.isConfirmMode();
        }
    }

    public void setExistingItems(List<ScheduleItem> scheduleItems){
        for(ScheduleItem item : scheduleItems){
            SlotRect slotRect = new SlotRect(getContext(),item);
            slotRect.setNew(false);
            existingItems.add(slotRect);
        }
        if(hasSize){
            measureExistingItems();
        }

    }

    public void scrollToDate(Date date){
        int scroll = adapter.getScrollForDate(date);
        scrollBy(0, (int) (scroll-currentScroll));
    }

   public void setItemsHolder(EditItemsHolder holder){
       this.itemsHolder = holder;
   }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        int firstFound = layoutManager.findFirstVisibleItemPosition();
        if(firstVisiblePosition != firstFound){
            firstVisiblePosition = firstFound;
            dateListener.onNewDate(adapter.getDate(firstVisiblePosition));
        }
        currentScroll += dy;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        checkSlotsToDraw();
        canvas.save();
        canvas.translate(0,-currentScroll);
        for(SlotRect slot : viewsToDraw){
           slot.draw(canvas);
        }
        canvas.restore();
    }

    void checkSlotsToDraw(){
        viewsToDraw.clear();
        addSlotsFrom(existingItems);
        if(itemsHolder != null) {
            addSlotsFrom(itemsHolder.items);
        }

    }
    void addSlotsFrom(ArrayList<SlotRect> items){
        float top = currentScroll;
        float bottom = currentScroll + getHeight();
        for(SlotRect slotRect : items){
            if(slotRect.shouldDraw(top,bottom)){
                viewsToDraw.add(slotRect);
                slotRect.prepare(getSlotView());
            } else if(slotRect.getView() != null) {
                releaseView(slotRect.getView());
                slotRect.recycleView();
            }
        }
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        if(onEvent(e)){
            return true;
        }
        return super.onInterceptTouchEvent(e);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if(onEvent(e)){
            return true;
        }
        return super.onTouchEvent(e);
    }

    boolean onEvent(MotionEvent e){
        if(!editable){
            return false;
        }
        float y = e.getY();
        float x = e.getX();
        switch (e.getAction()){
            case MotionEvent.ACTION_DOWN:
                for(SlotRect slot : itemsHolder.items){
                    if(slot.checkClickDown(x,currentScroll+y)){
                        isClicking = true;
                        return true;
                    }
                    if(Math.abs(slot.bounds.bottom - currentScroll - y) < 50
                            && x > slot.bounds.left && x < slot.bounds.right){
                        draggingSlot = slot;
                        return true;
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(isClicking){
                    return true;
                }
                if(draggingSlot != null) {
                    moveEvent(y);
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                draggingSlot = null;
                isClicking = false;
                SlotRect slotToDelete = null;
                for(SlotRect slotRect : itemsHolder.items){
                    if(slotRect.wasClickDone( x,currentScroll +y)){
                        slotToDelete = slotRect;
                    }
                    slotRect.resetClickState();
                }
                if(slotToDelete != null){
                    itemsHolder.items.remove(slotToDelete);
                    invalidate();
                    afterItemListChanged();
                    return true;
                }
                break;
        }
        return false;
    }

    void moveEvent(float y){
        float bottom = turnicate(currentScroll + y);
        draggingSlot.setBottom(bottom);
        invalidate();
    }

    float  turnicate(float y){
        int neighborPos = itemsHolder.items.indexOf(draggingSlot) + 1;
        if(neighborPos < itemsHolder.items.size()){
            SlotRect neighbor = itemsHolder.items.get(neighborPos);
            if( y > neighbor.bounds.top){
                return neighbor.bounds.top;
            }
        }
        return y;
    }

    @Override
    public void OnTimeSlotSelected(Rect rect,Date from,Date to) {
        if(!editable){
            return;
        }
        if(!canAdd()){
            return;
        }
        rect.offset(0, (int) currentScroll);
        SlotRect slot = new SlotRect(getContext(),rect,from);
        itemsHolder.items.add(slot);
        draggingSlot = slot;
        Collections.sort(itemsHolder.items);
        invalidate();
        afterItemListChanged();
    }

    ScheduleHolder getSlotView(){
        if(!viewsToRecycle.isEmpty())
            return viewsToRecycle.pollFirst();
        return new ScheduleHolder(getContext());
    }
    void releaseView(ScheduleHolder view){
        viewsToRecycle.add(view);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
       measureExistingItems();
        measureItems();
        hasSize = true;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void measureExistingItems(){
        int left = (int) getResources().getDimension(R.dimen.time_slot_time_width);
        for(SlotRect slot : existingItems){
            slot.measure(left,getRight(),adapter.getStartTime());
        }
    }

    public void measureItems(){
        int left = (int) getResources().getDimension(R.dimen.time_slot_time_width);
        for(SlotRect slot : itemsHolder.items){
            slot.measure(left,getRight(),adapter.getStartTime());
        }
    }

    public void setDateListener(OnDateChangedListener dateListener) {
        this.dateListener = dateListener;
    }



    public interface OnDateChangedListener{
        void onNewDate(Date date);
    }

    boolean canAdd(){
        if(itemListener.getType() != EditCalendarFragment.SINGLE || itemsHolder.items.isEmpty()){
            return true;
        }
        TopPopUpManager.showTopPopUp(getContext(), getContext().getString(R.string.cal_only_one), !TopPopUpManager.isError);
        return false;
    }

    void afterItemListChanged(){
        if(itemListener != null){
            itemListener.onItemsChanged();
        }
    }



    public interface ItemsHelper {
        void onItemsChanged();
        int getType();
        boolean isConfirmMode();
    }
}

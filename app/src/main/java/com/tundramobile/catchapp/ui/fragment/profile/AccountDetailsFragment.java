package com.tundramobile.catchapp.ui.fragment.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by eugenetroyanskii on 21.04.17.
 */

public class AccountDetailsFragment extends BaseFragment {

    @BindView(R.id.profile_email) TextView tvEmail;
    @BindView(R.id.profile_phone_number) TextView tvPhone;

    private String email, phone;
    private Unbinder unbinder;

    public static AccountDetailsFragment newInstance(String email, String phone) {
        AccountDetailsFragment fragment = new AccountDetailsFragment();
        fragment.email = email;
        fragment.phone = phone;
        return fragment;
    }


    public AccountDetailsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_account_details, container, false);
        unbinder = ButterKnife.bind(this, v);

        initLayout();
        return v;
    }


    private void initLayout() {
        tvEmail.setText(email);
        tvPhone.setText(phone);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.profile_back_btn)
    public void onBackClick() {
        getActivity().onBackPressed();
    }
}

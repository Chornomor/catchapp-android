package com.tundramobile.catchapp.ui.fragment.date_picker;

import android.content.Context;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.TimeSlot;
import com.tundramobile.catchapp.manager.TimeSlotsManager;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andreybofanov on 25.04.17.
 */

public class DateItemAdapter implements DateFormatProvider {
    Calendar calendar = (Calendar) Calendar.getInstance().clone();
    ArrayList<TimeSlot> timeSlots;
    Map<Long,Integer> itemsInDay = new HashMap<>();

    public DateItemAdapter() {
        timeSlots = TimeSlotsManager.getInstance().getAllTimeSlots();
        for (TimeSlot item : timeSlots){
            calendar.setTime(new Date(item.getFrom()*1000));
            resetTime(calendar);
            long id = calendar.getTimeInMillis();
            Integer count = itemsInDay.get(id);
            if(count == null){
                count = 1;
            } else {
                count++;
            }
            itemsInDay.put(id,count);
        }
    }

    void resetTime(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
    }

    @Override
    public View inflateEmptyView(LayoutInflater inflater) {
        return new View(inflater.getContext());
    }

    @Override
    public View inflateView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.item_day,null);
    }

    @Override
    public View formatView(View view, Date date) {
        calendar.setTime(date);
        TextView dayView = (TextView) view.findViewById(R.id.tv_day);
        dayView.setText(String.valueOf(calendar.get(Calendar.DATE)));
        resetTime(calendar);
        Integer count = itemsInDay.get(calendar.getTimeInMillis());
        if(count != null) {
            setupCount((LinearLayout) view.findViewById(R.id.count_indicators), count);
        }
        return view;
    }

    void setupCount(LinearLayout in, int count){
        in.removeAllViews();
        if(count > 5){
            count = 5;
        }
        int size = (int) in.getResources().getDimension(R.dimen.date_dot_size);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size,size);
        params.setMargins(0,0,size,0);
        in.setGravity(Gravity.CENTER);

        for(int i =0; i < count; i++){
            View v = new View(in.getContext());
            v.setBackgroundColor(in.getResources().getColor(R.color.colorPrimaryDark));
            in.addView(v,params);
        }
    }
}

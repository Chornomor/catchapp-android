package com.tundramobile.catchapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.Country;
import com.tundramobile.catchapp.interfaces.FastScrollRecyclerViewInterface;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 02.04.2017.
 */

public class NewEventWhenAdapter extends RecyclerView.Adapter<NewEventWhenAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<ScheduleItem> scheduleItems;
    private OnAddWhenItemClickListener onAddWhenItemClickListener;
    private Context context;


    public NewEventWhenAdapter(OnAddWhenItemClickListener onCountryItemClick) {
        this.onAddWhenItemClickListener = onCountryItemClick;
    }


    public void setItems(ArrayList<ScheduleItem> scheduleItems) {
        this.scheduleItems = scheduleItems;
        notifyDataSetChanged();
    }


    @Override
    public NewEventWhenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context)
                .inflate(R.layout.item_new_event_when, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (isNotAddButton(position)) {
            ScheduleItem scheduleItem = scheduleItems.get(position);
            holder.root.setBackground(context.getDrawable(R.drawable.bg_new_event_schedule_item));
            holder.title.setText(scheduleItem.getDateString());
            holder.time.setText(scheduleItem.getTimePeriodString());
            holder.delete.setTag(position);
            holder.delete.setOnClickListener(this);

            holder.time.setVisibility(View.VISIBLE);
            holder.delete.setVisibility(View.VISIBLE);
        } else {
            holder.root.setBackground(context.getDrawable(R.drawable.bg_add_when_schedule));
            holder.root.setOnClickListener(this);
            holder.title.setText(context.getString(R.string.add_another_time));

            holder.time.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
        }
    }


    private boolean isNotAddButton(int position) {
        return position < scheduleItems.size();
    }


    @Override
    public int getItemCount() {
        return scheduleItems.size() + 1;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_delete:
                int position = Integer.parseInt(view.getTag().toString());
                onAddWhenItemClickListener.onWhenRemoved(position);
                break;
            default:
                onAddWhenItemClickListener.onAddWhenItemClick();
                break;
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.root) LinearLayout root;
        @BindView(R.id.tv_title) TextView title;
        @BindView(R.id.tv_time) TextView time;
        @BindView(R.id.btn_delete) ImageView delete;


        private ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    public interface OnAddWhenItemClickListener {
        void onAddWhenItemClick();
        void onWhenRemoved(int position);
    }
}
package com.tundramobile.catchapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import butterknife.ButterKnife;

/**
 * Created by alexchern on 18.04.17.
 */

public class WhatEventSlidePageFragment extends BaseFragment {

    protected static final String PAGE_NUMBER = "PAGE_NUMBER";

    private int pageNumber;


    public static WhatEventSlidePageFragment getInstance(int pageNumber) {
        WhatEventSlidePageFragment fragment = new WhatEventSlidePageFragment();
        fragment.pageNumber = pageNumber;
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            pageNumber = savedInstanceState.getInt(PAGE_NUMBER);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView;
        switch (pageNumber) {
            case 1:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_what_event_1, container, false);
                break;
            case 2:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_what_event_2, container, false);
                break;
            case 3:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_what_event_3, container, false);
                break;
            default:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_what_event_1, container, false);
        }

        ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(PAGE_NUMBER, pageNumber);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {

    }
}

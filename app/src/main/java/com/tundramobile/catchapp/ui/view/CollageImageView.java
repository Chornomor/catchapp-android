package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.tundramobile.catchapp.R;

/**
 * Created by Sviatoslav Zaitsev on 03.04.17.
 */

public class CollageImageView extends android.support.v7.widget.AppCompatImageView {
    private static final int GRID_SIZE = 2;
    public static final int MAX_PHOTOS = GRID_SIZE * GRID_SIZE;

    private final byte PATTERNS[][] = {
            {0, 0, 0, 0},
            {0, 0, 1, 1},
            {0, 1, 2, 2},
            {0, 1, 2, 3},
    };

    private Bitmap[] bitmaps;
    private Rect[] imagesBounds;
    private byte[] currentPattern;

    private int size;
    private int cx;
    private int radius;
    private int partSize;
    private int dividerPadding;
    private int borderSize;

    private Paint backGroundPaint;


    public CollageImageView(Context context) {
        super(context);
    }


    public CollageImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    public CollageImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setCollageImages(Bitmap... bitmaps) {
        // TODO: 04.04.17 Implement assert to check bitmap.length > MAX_PHOTOS
        this.bitmaps = bitmaps;
        if (bitmaps != null && bitmaps.length > 0) {
            currentPattern = PATTERNS[bitmaps.length - 1];
            imagesBounds = new Rect[bitmaps.length];
        }
        reinit();
        invalidate();
    }


    public void setBorderSize(int size) {
        this.borderSize = size;
        invalidate();
    }


    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        if(drawable == null) {
            return;
        }
        setCollageImages(drawableToBitmap(drawable));
    }


    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        // We ask for the bounds if they have been set as they would be most
        // correct, then we check we are  > 0
        final int width = !drawable.getBounds().isEmpty() ?
                drawable.getBounds().width() : drawable.getIntrinsicWidth();

        final int height = !drawable.getBounds().isEmpty() ?
                drawable.getBounds().height() : drawable.getIntrinsicHeight();

        // Now we check we are > 0
        final Bitmap bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        reinit();
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (bitmaps == null || bitmaps.length == 0 || imagesBounds == null || size == 0) {
            super.dispatchDraw(canvas);
        } else {

            if (backGroundPaint == null) {
                backGroundPaint = new Paint();
                backGroundPaint.setColor(Color.WHITE);
            }

            canvas.drawCircle(cx, cx, size / 2, backGroundPaint);
            canvas.saveLayer(null, null, Canvas.ALL_SAVE_FLAG);
            drawBitmaps(canvas);
            drawClipPath(canvas);
            canvas.restore();
        }
    }


    private void drawBitmaps(Canvas canvas) {
        for (int i = 0; bitmaps != null && i < bitmaps.length; i++) {
            canvas.drawBitmap(bitmaps[i], null, imagesBounds[i], null);
        }
    }


    private void drawClipPath(Canvas canvas) {
        Paint clipPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        Bitmap clipBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ALPHA_8);
        Canvas clipCanvas = new Canvas(clipBitmap);

        clipCanvas.drawCircle(cx, cx, radius - borderSize, clipPaint);
        clipPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

        canvas.drawBitmap(clipBitmap, 0, 0, clipPaint);
    }


    // TODO: 04.04.17 Simplify
    private int getLeftBound(int pos) {
        for (int y = 0; y < GRID_SIZE; y++) {
            for (int x = 0; x < GRID_SIZE; x++) {
                if (isInCell(pos, x, y)) {
                    return x * partSize + (x != 0 ? dividerPadding / 2 : 0);
                }
            }
        }
        return 0;
    }


    private int getRightBound(int pos) {
        for (int x = GRID_SIZE - 1; x > -1; x--) {
            for (int y = 0; y < GRID_SIZE; y++) {
                if (isInCell(pos, x, y)) {
                    return (x + 1) * partSize - (x != GRID_SIZE - 1 ? dividerPadding / 2 : 0);
                }
            }
        }
        return 0;
    }


    private int getTopBound(int pos) {
        for (int y = 0; y < GRID_SIZE; y++) {
            for (int x = 0; x < GRID_SIZE; x++) {
                if (isInCell(pos, x, y)) {
                    return y * partSize + (y != 0 ? dividerPadding / 2 : 0);
                }
            }
        }
        return 0;
    }


    private int getBottomBound(int pos) {
        for (int y = GRID_SIZE - 1; y > -1; y--) {
            for (int x = 0; x < GRID_SIZE; x++) {
                if (isInCell(pos, x, y)) {
                    return (y + 1) * partSize - (y != GRID_SIZE - 1 ? dividerPadding / 2 : 0);
                }
            }
        }
        return 0;
    }


    private boolean isInCell(int pos, int x, int y) {
        return currentPattern[y * GRID_SIZE + x] == pos;
    }


    private void reinit() {
        size = getWidth();

        if (size == 0 || bitmaps == null || bitmaps.length == 0)
            return;

        dividerPadding = getResources().getDimensionPixelSize(R.dimen.collage_divider_size);
        // TODO: 04.04.17 Check w == h, else -> exception
        partSize = size / GRID_SIZE;
        radius = cx = size / 2;
        // Init bounds
        for (int i = 0; i < bitmaps.length; i++) {
            imagesBounds[i] = getPatternRect(i);
        }
        fixBitmapsToRatio();
    }


    private void fixBitmapsToRatio() {
        for (int i = 0; i < imagesBounds.length; i++) {
            if (imagesBounds[i].height() != imagesBounds[i].width()) {
                int newX = bitmaps[i].getWidth() / 2 - imagesBounds[i].width() / 2;
                newX = newX < 0 ? 0 : newX;
                int newY = bitmaps[i].getHeight() / 2 - imagesBounds[i].height() / 2;
                newY = newY < 0 ? 0 : newY;
                int newWidth = imagesBounds[i].width();
                newWidth = newX + newWidth > bitmaps[i].getWidth() ? bitmaps[i].getWidth() : newWidth;
                int newHeight = imagesBounds[i].height();
                newHeight = newY + newHeight > bitmaps[i].getHeight() ? bitmaps[i].getHeight() : newHeight;


                bitmaps[i] = Bitmap.createBitmap(
                        bitmaps[i],
                        newX,
                        newY,
                        newWidth,
                        newHeight
                );
            }
        }
    }


    private Rect getPatternRect(int pos) {
        int l, r, t, b;

        l = getLeftBound(pos);
        r = getRightBound(pos);
        t = getTopBound(pos);
        b = getBottomBound(pos);
        return new Rect(l, t, r, b);
    }
}

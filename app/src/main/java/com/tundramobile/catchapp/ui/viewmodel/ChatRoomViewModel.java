package com.tundramobile.catchapp.ui.viewmodel;

import com.tundramobile.catchapp.firebase.model.ChatRoom;
import com.tundramobile.catchapp.other.TimeUtils;

/**
 * Created by Sviatoslav Zaitsev on 24.04.17.
 */

public class ChatRoomViewModel {
    private String roomKey;
    private String image;
    private String name;
    private String msg;
    private String when;

    public ChatRoomViewModel(String roomKey, String image, String name, ChatRoom room) {
        this.roomKey = roomKey;
        this.image = image;
        this.name = name;
        this.msg = room.getLastMessageText();
        this.when = TimeUtils.getRelativeTime(room.getLastMessageTimestamp());
    }

    public String getRoomKey() {
        return roomKey;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getMsg() {
        return msg;
    }

    public String getWhen() {
        return when;
    }
}

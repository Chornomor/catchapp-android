package com.tundramobile.catchapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.FabOption;
import com.tundramobile.catchapp.entity.request.CreateGroupBody;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.ui.adapter.GroupMembersRecyclerAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.SELECTED_CONTACTS;

public class GroupMembersListFragment extends BaseFabFragment {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private static final String GROUP = "GROUP";

    private Unbinder unbinder;
    private GroupMembersRecyclerAdapter adapter;
    private int positionToDelete;
    private Contact group;


    public static GroupMembersListFragment newInstance(Contact group) {
        GroupMembersListFragment fragment = new GroupMembersListFragment();
        Bundle args = new Bundle();
        args.putParcelable(GROUP, group);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            group = savedInstanceState.getParcelable(GROUP);
        } else if (getArguments() != null) {
            group = getArguments().getParcelable(GROUP);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_group_members_list, container, false);
        unbinder = ButterKnife.bind(this, v);
        setUpRecyclerView();
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((ProgressDialogHandler) getContext()).showProgressDialog();
        ContactManager.getInstance(getContext()).getGroups(new ContactManager.OnObtainContactListener() {
            @Override
            public void onObtainContacts(List<Contact> contacts, String error) {

            }


            @Override
            public void onObtainRecentContacts(List<Contact> recentContactList) {

            }


            @Override
            public void onObtainContactGroups(List<Contact> groupList) {
                ((ProgressDialogHandler) getContext()).cancelProgressDialog();
                if (groupList != null && !groupList.isEmpty()) {
                    for (Contact g : groupList) {
                        if (group.getUid() == g.getUid()) {
                            group = g;
                            if (adapter != null) adapter.setItems(g.getMembers());
                            return;
                        }
                    }
                }
            }
        });
    }


    private void setUpRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);

        adapter = new GroupMembersRecyclerAdapter(group.getMembers());
        recyclerView.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    private ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }


        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            positionToDelete = viewHolder.getAdapterPosition();
            if (direction == ItemTouchHelper.LEFT) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.delete_user_message);
                builder.setPositiveButton("REMOVE", (dialog, which) -> {
                    ((ProgressDialogHandler) getContext()).showProgressDialog();
                    ContactManager.getInstance(getContext()).updateGroup(createBodyForEdit(), group.getUid(), (group1, error) -> {
                        if (getContext() == null) return;
                        ((ProgressDialogHandler) getContext()).cancelProgressDialog();
                        if (error != null) {
                            TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
                            restoreDeletedItem();
                        } else {
                            adapter.removeItem(positionToDelete);
                        }
                    });
                }).setNegativeButton("CANCEL", (dialog, which) -> {
                    restoreDeletedItem();
                });

                AlertDialog dialog = builder.create();
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }
    };


    private CreateGroupBody createBodyForEdit() {
        ArrayList<Integer> membersIds = new ArrayList<>();
        for (Contact member : group.getMembers()) {
            membersIds.add(member.getUid());
        }
        membersIds.remove(positionToDelete);

        CreateGroupBody body = new CreateGroupBody();
        body.setName(group.getName());
        body.setMembers(membersIds);
        return body;
    }


    private void restoreDeletedItem() {
        adapter.notifyItemRemoved(positionToDelete + 1);
        adapter.notifyItemRangeChanged(positionToDelete, adapter.getItemCount());
    }


    @Override
    protected ArrayList<FabOption> getFabOptions() {
        ArrayList<FabOption> fabOptions = new ArrayList<>(2);
        fabOptions.add(new FabOption(R.string.create_chat, R.drawable.ic_chat));
        fabOptions.add(new FabOption(R.string.create_invite, R.drawable.ic_calendar));
        return fabOptions;
    }


    @Override
    protected int getFabIcon() {
        return R.drawable.arrow_right;
    }


    @Override
    protected boolean isBlurRequired() {
        return true;
    }


    @Override
    protected boolean showFabAtStart() {
        return true;
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if (toolbar != null) {
            toolbar.switchToGroupListMode(group.getName(), v -> {
                switch (v.getId()) {
                    case R.id.addBtn:
                        showFragment(ContactsFragment.newInstanceSelecting(group));
                        break;
                    case R.id.backBtn:
                        getActivity().onBackPressed();
                        break;
                }
            });
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(GROUP, group);
        putSelectedContacts();
        super.onSaveInstanceState(outState);
    }


    private void putSelectedContacts() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SELECTED_CONTACTS + ContactManager.GROUPS, adapter.getItems());
        Navigator.with(getActivity()).putData(SELECTED_CONTACTS + ContactManager.GROUPS, bundle);
    }
}

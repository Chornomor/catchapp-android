package com.tundramobile.catchapp.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.theartofdev.edmodo.cropper.CropImage;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.interfaces.UpdateUserPhotoListener;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.other.GetPhotoUtils;
import com.tundramobile.catchapp.ui.fragment.profile.ProfileFragment;

import java.io.File;

import static com.tundramobile.catchapp.other.GetPhotoUtils.REQ_CAMERA;
import static com.tundramobile.catchapp.other.GetPhotoUtils.REQ_GALLERY;


/**
 * Created by eugenetroyanskii on 20.04.17.
 */

public class ProfileActivity extends BaseActivity {

    private ProfileFragment fragment;
    private GetPhotoUtils mGetPhotoUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        if (fragment == null) {
            fragment = ProfileFragment.newInstance();
        }
        setFragment(fragment);
        initPhotoPicker();
        if (savedInstanceState != null) {
            mGetPhotoUtil.loadInstance(savedInstanceState);
        }
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }


    public void setFragment(Fragment baseFragment) {
        try {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            if (getSupportFragmentManager().findFragmentById(R.id.fragmentContainer) == null) {
                ft.add(R.id.fragmentContainer, baseFragment);
            } else {
                ft.replace(R.id.fragmentContainer, baseFragment);
            }
            ft.addToBackStack(null);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initPhotoPicker() {
        mGetPhotoUtil = new GetPhotoUtils(this, new GetPhotoUtils.PhotoUtilListener() {

            @Override
            public void onImageDecoding() {
            }


            @Override
            public void onPicObtained(GetPhotoUtils.LoadedPhoto loadedPhoto) {
                CropImage.activity(loadedPhoto.uri).start(ProfileActivity.this);
            }
        });
        fragment.setPhotoPicker(mGetPhotoUtil);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                showProgressDialog();
                File photoFile = new File(result.getUri().getPath());
                MyUserManager.getInstance().uploadUserPhoto(MyUserManager.getInstance().getCurrentUserToken(this), photoFile, updateUserPhotoListener);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("DEBUG", error.toString());
            }
        }
        if (requestCode == REQ_GALLERY || requestCode == REQ_CAMERA) {
            mGetPhotoUtil.resolveResult(requestCode, resultCode, data);
        }
    }


    private UpdateUserPhotoListener updateUserPhotoListener = new UpdateUserPhotoListener() {
        @Override
        public void onUserUpdated(User updatedUser, String error) {
            if(isDestroyed() || isFinishing()){
                return;
            }
            if (error.isEmpty()) {
                MyUserManager.setCurrentUser(ProfileActivity.this, updatedUser);
                fragment.initLayout(updatedUser);
            } else {
                TopPopUpManager.showTopPopUp(ProfileActivity.this, error, TopPopUpManager.isError);
            }
            cancelProgressDialog();
        }
    };


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mGetPhotoUtil != null) {
            mGetPhotoUtil.saveInstance(outState);
        }
    }
}

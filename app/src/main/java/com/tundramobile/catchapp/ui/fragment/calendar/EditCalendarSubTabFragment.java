package com.tundramobile.catchapp.ui.fragment.calendar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.entity.TimeSlot;
import com.tundramobile.catchapp.interfaces.TabDataI;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TimeSlotsManager;
import com.tundramobile.catchapp.other.CalendarUtil;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.dateEditor.ScheduleRecycler;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EditCalendarSubTabFragment extends BaseFragment implements TabDataI {
    public TabData tabData;
    int type;
    @BindView(R.id.recycler)
    ScheduleRecycler recycler;
    @BindView(R.id.tv_day)
    TextView tvDay;
    EditItemsHolder holder;
    ScheduleRecycler.ItemsHelper itemsHelper;

    Date initialDate = null;

    public static EditCalendarSubTabFragment newConfirmedInstance(TabData tabData,EditItemsHolder holder
            , ScheduleRecycler.ItemsHelper itemsHelper) {
        return newInstance(tabData, CatchUpManager.CONFIRMED,holder,itemsHelper);
    }


    public static EditCalendarSubTabFragment newIncomingInstance(TabData tabData,EditItemsHolder holder
            , ScheduleRecycler.ItemsHelper itemsHelper) {
        return newInstance(tabData, CatchUpManager.INCOMING,holder,itemsHelper);
    }


    public static EditCalendarSubTabFragment newOutgoingInstance(TabData tabData,EditItemsHolder holder
    , ScheduleRecycler.ItemsHelper itemsHelper) {
        return newInstance(tabData, CatchUpManager.OUTGOING,holder,itemsHelper);
    }


    private static EditCalendarSubTabFragment newInstance(TabData tabData, int type, EditItemsHolder holder, ScheduleRecycler.ItemsHelper itemsHelper) {
        EditCalendarSubTabFragment fragment = new EditCalendarSubTabFragment();
        fragment.itemsHelper = itemsHelper;
        fragment.holder = holder;
        fragment.setTabData(tabData);
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putParcelable(TAB_DATA, tabData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
            tabData = getArguments().getParcelable(TAB_DATA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_date,container,false);
        ButterKnife.bind(this,v);
        tvDay.setText(DateUtil.formatHeader(new Date()));
        recycler.setDateListener(new ScheduleRecycler.OnDateChangedListener() {
            @Override
            public void onNewDate(Date date) {
                tvDay.setText(DateUtil.formatHeader(date));
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(holder != null  && itemsHelper != null) {
            update(holder,itemsHelper);
        }
        if(initialDate != null) {
            recycler.scrollToDate(initialDate);
            initialDate = null;
        }
    }

    public void update(EditItemsHolder holder, ScheduleRecycler.ItemsHelper itemsHelper){
        this.holder = holder;
        this.itemsHelper = itemsHelper;
        if(recycler == null){
            return;
        }
        ArrayList<TimeSlot> timeSlots;
        if(type == CatchUpManager.INCOMING){
            timeSlots = TimeSlotsManager.getInstance().getAllTimeSlots();
        } else {
            timeSlots = TimeSlotsManager.getInstance().getConfirmedTimeSlots();
        }
        ArrayList<ScheduleItem> scheduleItems = new ArrayList<>();
        long userId = MyUserManager.getCurrentUser(getContext()).getId();
        for(TimeSlot timeSlot : timeSlots){
            if(timeSlot.getSlotId() != holder.slotId) {
                scheduleItems.add(new ScheduleItem(userId, getContext(), timeSlot));
            }
        }
        scheduleItems.addAll(CalendarUtil.readCalendarEvent(getContext()));
        recycler.setItemListener(itemsHelper);
        recycler.setExistingItems(scheduleItems);
        recycler.setItemsHolder(holder);
        recycler.invalidate();
    }

    public void scrollToDate(Date date){
        if(recycler == null) {
            initialDate = date;
            return;
        }
        recycler.scrollToDate(date);
    }


    public TabData getTabData() {
        return tabData;
    }

    public void setTabData(TabData tabData) {
        this.tabData = tabData;
    }

    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
    }
}

package com.tundramobile.catchapp.ui.fragment.calendar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.interfaces.ListResultListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.PagerManager;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.ErrorHandler;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;
import com.tundramobile.catchapp.ui.fragment.date_picker.DateChangedListener;
import com.tundramobile.catchapp.ui.fragment.date_picker.DateItemAdapter;
import com.tundramobile.catchapp.ui.fragment.date_picker.DatePager;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.HomeTabLayout;
import com.tundramobile.catchapp.ui.view.NonSwipeViewPager;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnPageChange;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;

public class CalendarFragment extends BaseFragment implements DateChangedListener, ViewPager.OnPageChangeListener {

    public static final String ARG_SHOW_SAVE_BTN = "ARG_SHOW_SAVE_BTN";

    private static final String TAG = CalendarFragment.class.getSimpleName();

    @BindView(R.id.homeTabLayout) HomeTabLayout homeTabLayout;
    @BindView(R.id.viewPager) NonSwipeViewPager viewPager;
    @BindView(R.id.emptyListHolder) SwipeRefreshLayout emptyListHolder;
    @BindView(R.id.pager_swipe_refresh) SwipeRefreshLayout pageRefresher;
    @BindView(R.id.save_btn) Button saveBtn;
    @BindView(R.id.calendar_picker)
    DatePager datePager;

    private Unbinder unbinder;
    TabsPagerAdapter adapter;
    ErrorHandler errorHandler;

    private final boolean DEFAULT_SAVE_BTN_VISIBILITY = false;
    boolean isSaveBtnVisible = false;


    boolean refreshing = false;

    boolean gotIncoming = false;
    boolean gotOutgoing = false;
    boolean gotConfirmed = false;

    public CalendarFragment() {
    }


    public static CalendarFragment newInstance(Bundle args) {
        CalendarFragment fragment = new CalendarFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            isSaveBtnVisible = getArguments().getBoolean(ARG_SHOW_SAVE_BTN, DEFAULT_SAVE_BTN_VISIBILITY);
        }
        errorHandler = ErrorHandler.getInstance(getContext());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calendar, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();
        if(!FastPermission.isGranted(getContext(), PermissionEnum.READ_CALENDAR)){
            FastPermission.request(this, 8, new FastPermission.OnPermissionGrantedListener() {
                @Override
                public void onPermissionGranted() {
                    refresh();
                }
            },PermissionEnum.READ_CALENDAR);
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    TabsPagerAdapter getAdapter(){
        return PagerManager.getCalendarPagerAdapter(getChildFragmentManager());
    }

    private void initLayout() {
        datePager.setDateFormatProvider(new DateItemAdapter());
        datePager.setDateChangedListener(this);
        adapter = getAdapter();
        viewPager.setAdapter(null);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        homeTabLayout.setupWithViewPager(viewPager);
        emptyListHolder.setColorSchemeResources(R.color.purple);
        emptyListHolder.setOnRefreshListener(() -> {
            refresh();
        });
        pageRefresher.setColorSchemeResources(R.color.purple);
        pageRefresher.setOnRefreshListener(() -> {
            refresh();
        });
        saveBtn.setVisibility(isSaveBtnVisible ? View.VISIBLE : View.GONE);
    }



    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if(toolbar == null)
            return;

        Calendar calendar = Calendar.getInstance();
        String monthName = getCurrentMonthName(calendar);
        setDateToFragment(calendar.getTimeInMillis());

        toolbar.switchToCalendarMode(monthName, v -> {
            int visibility = datePager.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE;
            datePager.setVisibility(visibility);
//            datePager.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
//                @Override
//                public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(year,month,dayOfMonth);
//                    setDateToFragment(cal.getTimeInMillis());
//                }
//            });
        });
    }

    private String getCurrentMonthName(Calendar calendar) {
        return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
    }


    public void refresh(){
        if(refreshing)
            return;
        refreshing = true;
        CatchUpManager.getInstance(getContext()).getCatchUps(CatchUpManager.CONFIRMED, true, new ListResultListener<CatchUp>() {
            @Override
            public void onResultReady(int type, boolean success, List<CatchUp> catchUps) {
                proceed(type);
            }
        });
        CatchUpManager.getInstance(getContext()).getCatchUps(CatchUpManager.OUTGOING, true, new ListResultListener<CatchUp>() {
            @Override
            public void onResultReady(int type, boolean success, List<CatchUp> catchUps) {
                proceed(type);
            }
        });
        CatchUpManager.getInstance(getContext()).getCatchUps(CatchUpManager.INCOMING, true, new ListResultListener<CatchUp>() {
            @Override
            public void onResultReady(int type, boolean success, List<CatchUp> catchUps) {
                proceed(type);
            }
        });
    }

    private void proceed(int type){
        switch (type){
            case CatchUpManager.CONFIRMED:
                gotConfirmed = true;
                break;
            case  CatchUpManager.INCOMING:
                gotIncoming = true;
                break;
            case CatchUpManager.OUTGOING:
                gotOutgoing = true;
                break;
        }
        if(gotIncoming && gotOutgoing && gotConfirmed){
            if(pageRefresher != null) {
                pageRefresher.setRefreshing(false);
            }
            updateCurrentFragment();
            gotConfirmed = gotOutgoing = gotIncoming = false;
            refreshing = false;
        }
    }


    private void applyNewDate(AppToolbar toolbar, int year, int month, int dayOfMonth) {
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(year, month, dayOfMonth);
        String monthName = getCurrentMonthName(selectedDate);
        toolbar.setCalendarTitle(monthName);
        setDateToFragment(selectedDate.getTimeInMillis());
    }

    void setDateToFragment(long millis) {
        if (adapter == null || viewPager == null)
            return;
        Fragment curFragment = adapter.getActiveFragment(viewPager,viewPager.getCurrentItem());
        if(curFragment != null && curFragment instanceof CalendarSubTabFragment) {
            ((CalendarSubTabFragment) curFragment).setDate(new Date(millis));
        }
    }
    void updateCurrentFragment() {
        if (adapter == null || viewPager == null)
            return;
        Fragment curFragment = adapter.getActiveFragment(viewPager,viewPager.getCurrentItem());
        if(curFragment != null && curFragment instanceof CalendarSubTabFragment) {
            ((CalendarSubTabFragment) curFragment).update();
        }
    }

    @Override
    public void onMonthChanged(int year,int month) {
        Calendar calendar = new GregorianCalendar(year,month,1);
        ((MainActivity) getActivity()).getToolbar()
                .setCalendarTitle(getCurrentMonthName(calendar));
    }

    @Override
    public void onDateSelected(Date date) {
        setDateToFragment(date.getTime());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Calendar calendar = Calendar.getInstance();
        ((MainActivity) getActivity()).getToolbar()
                .setCalendarTitle(getCurrentMonthName(calendar));
        setDateToFragment(calendar.getTimeInMillis());
        datePager.resetPosition();
        refresh();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        enableDisableSwipeRefresh( state == ViewPager.SCROLL_STATE_IDLE );
    }

    void enableDisableSwipeRefresh(boolean enable) {
        if (pageRefresher != null) {
            pageRefresher.setEnabled(enable);
        }
    }
}

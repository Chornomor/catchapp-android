package com.tundramobile.catchapp.ui.fragment.calendar;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Event;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.response.BaseResponseModel;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.PagerManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;
import com.tundramobile.catchapp.ui.fragment.NewEventFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.dateEditor.ScheduleRecycler;
import com.tundramobile.catchapp.ui.view.dateEditor.SlotRect;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.Date;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class EditCalendarFragment extends CalendarFragment implements ScheduleRecycler.ItemsHelper {
    public static final String SELECTION_TYPE = "sel_type";
    public static final String CONFIRM_MODE = "confirm_mode";
    public static final String CONFIRM_CATCH_UP = "confirm_catchup";
    public static final String CONFIRM_SLOT = "confirm_slot";
    public static final String LIST = "list";
    public static final String ITEM = "item";
    public static final int SINGLE = 0;
    public static final int MULTI = 1;

    EditItemsHolder itemsHolder = new EditItemsHolder();

    int selectionType = MULTI;
    boolean isConfirmMode = false;
    long catchUpId = -1;
    long slotId = -1;
    long date = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            selectionType = getArguments().getInt(SELECTION_TYPE,MULTI);
            isConfirmMode = getArguments().getBoolean(CONFIRM_MODE,false);
            if(isConfirmMode) {
                catchUpId = getArguments().getLong(CONFIRM_CATCH_UP, -1);
                slotId = getArguments().getLong(CONFIRM_SLOT, -1);
                SlotRect slotRect = (SlotRect) getArguments().getSerializable(ITEM);
                itemsHolder.confirmCatchUpId = catchUpId;
                itemsHolder.slotId = slotId;
                itemsHolder.items.add(slotRect);
            } else {
                ArrayList<SlotRect> slotRects = (ArrayList<SlotRect>) getArguments().getSerializable(LIST);
                if(slotRects != null)
                itemsHolder.items.addAll(slotRects);
            }
        }
        if(!itemsHolder.items.isEmpty()){
            isSaveBtnVisible = true;
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isConfirmMode) {
            saveBtn.setText(R.string.cal_confirm);
            saveBtn.setBackgroundColor(getResources().getColor(R.color.confirm));
            date = itemsHolder.items.get(0).start.getTime();
        } else {
            pageRefresher.setEnabled(false);
        }
    }




    @Override
    TabsPagerAdapter getAdapter() {
        return PagerManager.getEditCalendarPagerAdapter(getChildFragmentManager(),itemsHolder,this);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAll();
        viewPager.post(new Runnable() {
            @Override
            public void run() {
                setDateToFragment(-1);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    void setDateToFragment(long millis) {
        if (adapter == null || viewPager == null)
            return;
        if(isConfirmMode){
            millis = date;
        }
        Fragment curFragment = adapter.getActiveFragment(viewPager,viewPager.getCurrentItem());
        if(curFragment != null && curFragment instanceof EditCalendarSubTabFragment) {
            ((EditCalendarSubTabFragment) curFragment).scrollToDate(new Date(millis));
        }
    }

    void updateCurrentFragment() {
        if (adapter == null || viewPager == null)
            return;
        Fragment curFragment = adapter.getActiveFragment(viewPager,viewPager.getCurrentItem());
        if(curFragment != null && curFragment instanceof EditCalendarSubTabFragment) {
            ((EditCalendarSubTabFragment) curFragment).update(itemsHolder,this);
        }
    }

    void updateAll(){
        if (adapter == null || viewPager == null)
            return;
        Fragment curFragment = adapter.getActiveFragment(viewPager,0);
        if(curFragment != null && curFragment instanceof EditCalendarSubTabFragment) {
            ((EditCalendarSubTabFragment) curFragment).update(itemsHolder,this);
        }
        curFragment = adapter.getActiveFragment(viewPager,1);
        if(curFragment != null && curFragment instanceof EditCalendarSubTabFragment) {
            ((EditCalendarSubTabFragment) curFragment).update(itemsHolder,this);
        }
    }


    public void set(Context context,ArrayList<ScheduleItem> items){
        for(ScheduleItem scheduleItem : items){
            itemsHolder.items.add(new SlotRect(context,scheduleItem.dateFrom,scheduleItem.dateTo));
        }
    }



        @Override
        public void onItemsChanged() {
            if(getActivity() == null || getActivity().isFinishing() || getActivity().isDestroyed()){
                return;
            }
            if(itemsHolder.items.isEmpty()){
                saveBtn.setVisibility(View.GONE);
            } else {
                saveBtn.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getType() {
            return selectionType;
        }

        @Override
        public boolean isConfirmMode() {
            return isConfirmMode;
        }

    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if(isConfirmMode) {
            return;
        }
        super.onToolbarChanged(toolbar);
        toolbar.showBackBtn(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }




    @OnClick(R.id.save_btn)
    public void onSaveBtnClicked(View v) {
        if(isConfirmMode){
            confirm();
            return;
        }
        ArrayList<ScheduleItem> result = new ArrayList<>();
        for(SlotRect slotRect : itemsHolder.items){
            result.add(new ScheduleItem(slotRect.start,slotRect.end));
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable(NewEventFragment.SCHEDULE_ITEMS,result);
        Navigator.with(getActivity()).putData(NewEventFragment.SCHEDULE_ITEMS,bundle);
        getActivity().onBackPressed();
    }

    void confirm(){
        showProgressDialog();
        User user = MyUserManager.getCurrentUser(getContext());
        RESTClient.getInstance().acceptCatchUpTimeSlot
                (MyUserManager.getInstance().getCurrentUserToken(getContext()), user.getId(),
                        (int)catchUpId, (int) slotId,
                        new HandledCallback<BaseResponseModel>() {
                            @Override
                            public void onSuccess(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                                cancelProgressDialog();
                                ((MainActivity)getActivity()).reselectCurrent();
                            }

                            @Override
                            public void onFail(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                                cancelProgressDialog();
                                TopPopUpManager.showTopPopUp(getActivity(), "Failed", TopPopUpManager.isError);
                            }

                            @Override
                            public void onProcessFailed(Call<BaseResponseModel> call, Throwable t, String error) {
                                cancelProgressDialog();
                                TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
                            }
                        });
    }

    public static EditCalendarFragment makeConfirm(Context context,long catchUpId, Event event){
        EditCalendarFragment f = new EditCalendarFragment();
        Bundle b = new Bundle();
        b.putBoolean(CONFIRM_MODE,true);
        b.putLong(CONFIRM_SLOT,event.getId());
        b.putLong(CONFIRM_CATCH_UP,catchUpId);
        b.putSerializable(ITEM,new SlotRect(context,
                new Date(event.getFrom()*1000),new Date(event.getTo()*1000)));
        f.setArguments(b);
        return f;
    }
    public static EditCalendarFragment makeSelect(Context context,int selectionType,
                                                  ArrayList<ScheduleItem> scheduleItems)
    {
        EditCalendarFragment f = new EditCalendarFragment();
        Bundle b = new Bundle();
        ArrayList<SlotRect> slotRects = new ArrayList<>();
        for(ScheduleItem scheduleItem : scheduleItems){
            slotRects.add(new SlotRect(context,scheduleItem.dateFrom,scheduleItem.dateTo));
        }
        b.putSerializable(LIST,slotRects);
        b.putInt(SELECTION_TYPE, selectionType);
        f.setArguments(b);
        return f;
    }
}

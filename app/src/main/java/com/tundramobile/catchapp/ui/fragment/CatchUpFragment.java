package com.tundramobile.catchapp.ui.fragment;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.event.GetCatchUpsEvent;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.interfaces.TabDataI;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.adapter.CatchUpRecyclerAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CatchUpFragment extends BaseFragment implements TabDataI, RecyclerItemClickListener<CatchUp> {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private static final String TAG = CatchUpFragment.class.getSimpleName();
    private static final String CATCH_UPS = "CATCH_UPS";
    private static final String UPDATE_LIST_ON_BACK = "UPDATE_LIST_ON_BACK";

    public TabData tabData;
    private Unbinder unbinder;
    private int type;
    private CatchUpRecyclerAdapter adapter;
    private StickyRecyclerHeadersDecoration headersDecor;
    private boolean updateListOnBack;


    public CatchUpFragment() {
    }


    public static CatchUpFragment newConfirmedInstance(TabData tabData) {
        return newInstance(tabData, CatchUpManager.CONFIRMED);
    }


    public static CatchUpFragment newIncomingInstance(TabData tabData) {
        return newInstance(tabData, CatchUpManager.INCOMING);
    }


    public static CatchUpFragment newOutgoingInstance(TabData tabData) {
        return newInstance(tabData, CatchUpManager.OUTGOING);
    }


    private static CatchUpFragment newInstance(TabData tabData, int type) {
        CatchUpFragment fragment = new CatchUpFragment();
        fragment.setTabData(tabData);
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            tabData = savedInstanceState.getParcelable(TAB_DATA);
            type = savedInstanceState.getInt(TYPE);
            updateListOnBack = savedInstanceState.getBoolean(UPDATE_LIST_ON_BACK);
        } else if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_catch_up, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();

        if (savedInstanceState != null) {
            setCatchUps(savedInstanceState.getParcelableArrayList(CATCH_UPS));
        } else {
            showLoading(true);
            EventBus.getDefault().post(new GetCatchUpsEvent(type));
        }
        return v;
    }


    private void initLayout() {
        setUpSwipeRefresh();
        setUpRecyclerView();
    }


    private void setUpSwipeRefresh() {
        swipeRefreshLayout.setColorSchemeResources(tabData.getSelectedColor());
        swipeRefreshLayout.setOnRefreshListener(
                () -> {
                    Utils.logd(TAG, "SwipeRefreshLayout.onRefresh");
                    EventBus.getDefault().post(new GetCatchUpsEvent(type));
                }
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        if(forceUpdate) {
            EventBus.getDefault().post(new GetCatchUpsEvent(type));
            forceUpdate = false;
        }
    }

    private void setUpRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(llm);

        adapter = new CatchUpRecyclerAdapter(tabData.getSelectedColor(), this);
        headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                headersDecor.invalidateHeaders();
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(headersDecor);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void setTabData(TabData tabData) {
        this.tabData = tabData;
    }


    @Override
    public TabData getTabData() {
        return tabData;
    }


    public boolean isEmpty() {
        return adapter == null || adapter.getItemCount() == 0;
    }


    public boolean isUpdateRequired() {
        boolean update = updateListOnBack;
        updateListOnBack = false;
        return update;
    }


    public void showLoading(boolean show) {
        if (swipeRefreshLayout != null) {
            if (show && !swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(true);
            else if (!show && swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        }
    }


    public void setCatchUps(List<CatchUp> items) {
        Utils.logd(TAG, "setCatchUps: type = " + type + ", size = " + items.size());
        showLoading(false);
        if (adapter != null) adapter.setItems(items);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(TAB_DATA, tabData);
        outState.putInt(TYPE, type);
        outState.putBoolean(UPDATE_LIST_ON_BACK, updateListOnBack);
        outState.putParcelableArrayList(CATCH_UPS, adapter.getItems());
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onItemClick(int position, CatchUp model) {
        updateListOnBack = true;
        showFragment(EventFragment.newInstance(model, type));
    }

}

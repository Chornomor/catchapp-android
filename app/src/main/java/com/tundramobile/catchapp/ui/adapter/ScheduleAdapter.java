package com.tundramobile.catchapp.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TimeSlotsManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.interfaces.DeleteRequestListener;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.ui.adapter.holder.ScheduleHolder;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleRow>
    implements StickyRecyclerHeadersAdapter<ScheduleAdapter.HeaderHolder>, View.OnClickListener
{
    public static final int MAX_COLUMNS = 8;

    private ArrayList<Date> items;
    private HashMap<Date,List<ScheduleItem>> timePeriods = new HashMap<>();

    Calendar calendar = Calendar.getInstance();

    StickyRecyclerHeadersDecoration decorator;

    ProgressDialogHandler progressDialogHandler;

    OnOpenItemClick onOpenItemClick;

    public ScheduleAdapter(){
        items = new ArrayList<>();
    }

    public ScheduleAdapter(List<ScheduleItem> items) {
        setItems(items);
    }

    public void setItems(List<ScheduleItem> items){
        this.items.clear();
        timePeriods.clear();
        countDuplicatedTime(items);
        Collections.sort(this.items);
    }

    public void setProgressDialogHandler(ProgressDialogHandler progressDialogHandler) {
        this.progressDialogHandler = progressDialogHandler;
    }

    public void setDecorator(StickyRecyclerHeadersDecoration decorator) {
        this.decorator = decorator;
    }

    private void countDuplicatedTime(List<ScheduleItem> scheduleItems){
        Set<Date> dates = new HashSet<>();
        List<ScheduleItem> schedules;
        for (ScheduleItem item : scheduleItems){
            schedules = timePeriods.get(item.dateFrom);
            if(schedules == null){
                schedules = new ArrayList<>();
            }
            dates.add(item.dateFrom);
            schedules.add(item);
            timePeriods.put(item.dateFrom,schedules);
        }
        items = new ArrayList<>(dates);
    }



    public int getPositionForDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int target = calendar.get(Calendar.DAY_OF_YEAR);
        for(Date item : items){
            calendar.setTime(item);
            if(calendar.get(Calendar.DAY_OF_YEAR) == target){
                return items.indexOf(item);
            }
        }
        return -1;
    }
    public void removeItem(ScheduleItem scheduleItem){
        List<ScheduleItem> scheduleItems = timePeriods.get(scheduleItem.dateFrom);
        scheduleItems.remove(scheduleItem);
        if(scheduleItems.isEmpty()){
            items.remove(scheduleItem.dateFrom);
        }
        TimeSlotsManager.getInstance().removeTimeSlot(scheduleItem.timeSlotId);
    }

    @Override
    public ScheduleRow onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ScheduleRow(parent);
    }

    @Override
    public void onBindViewHolder(ScheduleRow holder, int position) {
        List<ScheduleItem> list = timePeriods.get(items.get(position));
        holder.clear();
        for (ScheduleItem item : list){
            holder.addView(item,this,onOpenItemClick,position);
        }
    }

    @Override
    public long getHeaderId(int position) {
        calendar.setTime(items.get(position));
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new HeaderHolder(parent);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder holder, int position) {
        holder.bind(getHeaderId(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnOpenItemClick(OnOpenItemClick onOpenItemClick) {
        this.onOpenItemClick = onOpenItemClick;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_delete){
            ScheduleHolder holder = (ScheduleHolder) v.getTag();
            if(progressDialogHandler != null){
                progressDialogHandler.showProgressDialog();
            }
            int uid = MyUserManager.getCurrentUser(v.getContext()).getId();
            if(uid == holder.item.getCatchUp().getUser().getId()) {
                TimeSlotsManager.getInstance().deleteTimeSlot(MyUserManager.getInstance().getCurrentUserToken(v.getContext()),
                        holder.item.catchUpId, holder.item.timeSlotId, new DeleteListener(v.getContext(), holder.position, holder.item));
            } else {
                TimeSlotsManager.getInstance().cancelAttendanceForTimeSlot(MyUserManager.getInstance().getCurrentUserToken(v.getContext()),
                        (int)holder.item.catchUpId, (int) holder.item.timeSlotId, new DeleteListener(v.getContext(), holder.position, holder.item));
            }
        }
    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tvDate;
        int today;
        public HeaderHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_schedule_header,parent,false));
            ButterKnife.bind(this,itemView);
            today = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        }
        public void bind(long day){
            String dateLine = "";
            if(today == day){
                dateLine = itemView.getResources().getString(R.string.cal_today) + ", ";
            } else if(day-today == 1){
                dateLine = itemView.getResources().getString(R.string.cal_tomorrow) + ", ";
            }
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_YEAR, (int) day);
            dateLine += DateUtil.formatHeader(calendar.getTime());
            tvDate.setText(dateLine);
        }

    }

    public static class ScheduleRow extends RecyclerView.ViewHolder {
        LinearLayout layout;
        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
        public ScheduleRow(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_schedule_row,parent,false));
            layout = (LinearLayout) itemView;
            layout.setOrientation(LinearLayout.HORIZONTAL);
            params.weight = 1;
            int margin = (int) parent.getResources().getDimension(R.dimen.time_slot_margin);
            params.setMargins(0,margin,margin,margin);
        }
        public void clear(){
            layout.removeAllViews();
        }
        public void addView(ScheduleItem scheduleItem, View.OnClickListener listener
                , OnOpenItemClick onOpenItemClick,int position){
            ScheduleHolder holder = new ScheduleHolder(itemView.getContext());
            holder.bind(scheduleItem);
            holder.position = position;
            holder.btnDelete.setTag(holder);
            if(scheduleItem.isExternal()){
                holder.btnDelete.setVisibility(View.GONE);
            } else {
                holder.btnDelete.setVisibility(View.VISIBLE);
                holder.btnDelete.setOnClickListener(listener);
            }
            if(onOpenItemClick != null) {
                holder.setOpenOnClick(onOpenItemClick);
            }
           layout.addView(holder.itemView,params);
        }
    }
    private class DeleteListener implements DeleteRequestListener {
        int postition;
        Context context;
        ScheduleItem scheduleItem;

        public DeleteListener( Context context,int postition,ScheduleItem scheduleItem) {
            this.postition = postition;
            this.context = context;
            this.scheduleItem = scheduleItem;
        }

        @Override
        public void onSlotDeleteListener(boolean isSuccess, String error) {
            if(progressDialogHandler != null){
                progressDialogHandler.cancelProgressDialog();
            }
            if(isSuccess) {

                removeItem(scheduleItem);
                decorator.invalidateHeaders();
                notifyDataSetChanged();
            } else {
                if (context != null) {
                    TopPopUpManager.showTopPopUp(context, error, TopPopUpManager.isError);
                }
            }
        }
    }

    public interface OnOpenItemClick{
        void openScheduleItem(ScheduleItem item);
    }
}

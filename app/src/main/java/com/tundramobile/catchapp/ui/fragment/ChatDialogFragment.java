package com.tundramobile.catchapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.firebase.SingleChildEventListener;
import com.tundramobile.catchapp.firebase.Types;
import com.tundramobile.catchapp.firebase.model.ChatRoom;
import com.tundramobile.catchapp.firebase.model.Message;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.other.SimpleTextWatcher;
import com.tundramobile.catchapp.ui.adapter.ChatMessagesRecyclerAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class ChatDialogFragment extends BaseFragment {
    private static final String TAG = ChatDialogFragment.class.getSimpleName();

    private Unbinder unbinder;

    @BindView(R.id.recyclerView) RecyclerView listView;
    @BindView(R.id.message) EditText messageView;
    @BindView(R.id.send_btn) TextView sendButton;

    private ChatMessagesRecyclerAdapter adapter;

    private String title;
    private String roomKey;
    private long myID;
    private String myName = "";

    private boolean blockSending = true;

    public ChatDialogFragment() {
    }

    public static ChatDialogFragment newInstance(Bundle args) {
        return new ChatDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrieveData();
        myID = MyUserManager.getCurrentUser(getContext()).getId();
        myName = MyUserManager.getCurrentUser(getContext()).getFullName();
        adapter = new ChatMessagesRecyclerAdapter(myID);
    }

    private void retrieveData() {
        Bundle args = getArguments();
        if(args != null) {
            this.title = args.getString("title");
            this.roomKey = args.getString("roomKey");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat_dialog, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView.setLayoutManager(new LinearLayoutManager(getContext()));
        listView.setAdapter(adapter);

        messageView.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_SEND){
                sendMessage();
                return true;
            }
            return false;
        });

        messageView.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                blockSending = s.toString().trim().isEmpty();
                updateSendBtnState();
            }
        });
    }

    private void updateSendBtnState() {
        int color = blockSending ? ContextCompat.getColor(getContext(), R.color.gray) : ContextCompat.getColor(getContext(), R.color.colorAccent);
        sendButton.setTextColor(color);
        sendButton.setClickable(!blockSending);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadMessages();
    }

    private void loadMessages() {
        DatabaseReference roomMessagesRef = FirebaseDatabase.getInstance().getReference().child("message").child(roomKey);
        roomMessagesRef.orderByKey().addChildEventListener(new OnChildrenChangedListener());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        toolbar.switchToDefaultMode(title, (v) -> {
            hideKeyboard();
            getActivity().onBackPressed();
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(messageView.getWindowToken(), 0);
    }

    @OnClick(R.id.send_btn)
    public void onSendBtnClicked() {
        sendMessage();
    }

    private void sendMessage() {
        String senderName = myName;
        Long senderID = myID;
        String text = messageView.getText().toString().trim();
        messageView.setText("");
        Message messageModel = new Message();
        messageModel.setText(text);
        Double timestamp = (double) System.currentTimeMillis();
        timestamp /= 1000;
        messageModel.setTimestamp(timestamp);
        messageModel.setChatKey(roomKey);
        messageModel.setSenderId(senderID);
        messageModel.setSenderName(senderName);

        DatabaseReference roomMessagesRef = FirebaseDatabase.getInstance().getReference().child("message").child(roomKey);
        roomMessagesRef.push().setValue(messageModel).addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                Log.d(TAG, "sendMessage: " + task.toString());
            } else {
                Log.d(TAG, "sendMessage: " + "Failed");
            }
        });

        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReference().child("chat").child(roomKey);
        Double finalTimestamp = timestamp;
        roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ChatRoom room = dataSnapshot.getValue(Types.chatRoom);
                if(room != null) {
                    room.setLastMessageText(text);
                    room.setLastMessageTimestamp(finalTimestamp);

                    roomRef.setValue(room).addOnCompleteListener(task -> {
                        if(task.isSuccessful()) {
                            Log.d(TAG, "Update room onDataChange: " + task.toString());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


//    private void startChat() {
//        int myId = MyUserManager.getCurrentUser(getContext()).getId();
//        if(FirebaseAuth.getInstance().getCurrentUser() != null) {
//            int targetID = 19545;
//            DatabaseReference chatReference = FirebaseDatabase.getInstance().getReference().child("chat");
//            DatabaseReference chatParticipant = FirebaseDatabase.getInstance().getReference().child("chatParticipant");
//
//            // Check if chat already Exist
//            chatParticipant.orderByKey().equalTo(""+myId).addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    if(dataSnapshot.getValue() != null) {
//                        Log.d(TAG, "onDataChange: " + dataSnapshot.toString());
//                    } else {
//                        Log.d(TAG, "onDataChange: Chat not exist");
//
////                        createChat(ids, ownerId);
//                    }
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//
//        } else {
//            Log.w(TAG, "startChat: Unexpected error");
//        }
//    }

    private class OnChildrenChangedListener extends SingleChildEventListener {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Log.d(TAG, "onChildAdded: " + dataSnapshot.toString());
            Message newMessage = dataSnapshot.getValue(Types.message);
            if(newMessage != null) {
                adapter.addItem(newMessage);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    if (listView != null && adapter != null) {
                        listView.scrollToPosition(adapter.getItemCount() - 1);
                    }
                }, 50);
            }
        }
    }

    public static Fragment newInstance(String title, String roomKey) {
        Fragment fragment = new ChatDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("roomKey", roomKey);
        fragment.setArguments(args);
        return fragment;
    }
}

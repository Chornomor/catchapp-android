package com.tundramobile.catchapp.ui.viewmodel;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.TimeSlot;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.other.DateUtil;

import java.util.Calendar;
import java.util.Date;


public class ScheduleItem implements Comparable<ScheduleItem>, Parcelable {
    public long catchUpId;
    public long timeSlotId;

    public String title;
    public String desctiption;
    public Date dateFrom, dateTo;
    public int day;
    public int backgroundColor;
    CatchUp catchUp;

    boolean isExternal = false;


    public ScheduleItem(long userId, Context context, TimeSlot timeSlot) {
        title = timeSlot.getCatchUp().getName();
        dateFrom = new Date(timeSlot.getFrom() * 1000);
        dateTo = new Date(timeSlot.getTo() * 1000);
        setupDay();
        setupDescription(userId, context, timeSlot);
    }

    public ScheduleItem(Date from, Date to){
        dateFrom = from;
        dateTo = to;
    }
    public ScheduleItem(String title,Date from, Date to){
        this.title = title;
        dateFrom = from;
        dateTo = to;
    }

    protected ScheduleItem(Parcel in) {
        catchUpId = in.readLong();
        timeSlotId = in.readLong();
        title = in.readString();
        desctiption = in.readString();
        day = in.readInt();
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setExternal(boolean external) {
        isExternal = external;
    }

    public static final Creator<ScheduleItem> CREATOR = new Creator<ScheduleItem>() {
        @Override
        public ScheduleItem createFromParcel(Parcel in) {
            return new ScheduleItem(in);
        }


        @Override
        public ScheduleItem[] newArray(int size) {
            return new ScheduleItem[size];
        }
    };


    void setupDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFrom);
        day = calendar.get(Calendar.DAY_OF_YEAR);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        dateFrom = calendar.getTime();
    }


    public Long getDateFromLong() {
        return (dateFrom.getTime() / 1000);
    }


    public Long getDateToLong() {
        return (dateTo.getTime() / 1000);
    }


    void setupDescription(long userId, Context context, TimeSlot timeSlot) {
        CatchUp catchUp = timeSlot.getCatchUp();
        this.catchUp = catchUp;
        setupBackfroundColor(catchUp,context);
        catchUpId = catchUp.getId();
        timeSlotId = timeSlot.getSlotId();
        if (catchUp.getType().equals(CatchUp.TYPE_GROUP)) {
            if (catchUp.getUser().getId() == userId) {
                desctiption = context.getResources().getString(R.string.cal_your_group);
            } else {
                if (catchUp.getAttendingCount() == 1) {
                    desctiption = catchUp.getInitiatorName();
                } else {
                    desctiption = context.getResources().getString(R.string.cal_group, catchUp.getInitiatorName(),
                            catchUp.getAttendingCount() - 1);
                }
            }
        } else {
            if (catchUp.getAttendingCount() > 1 && catchUp.isMine(userId)) {
                desctiption = context.getResources().getString(R.string.cal_pending_1t1);
            } else if (catchUp.getUser().getId() == userId) {
                desctiption = catchUp.getInvited().get(0).getFullName();
            } else {
                desctiption = catchUp.getInitiatorName();
            }
        }
    }

    public void setToOutgoing(Context context){
        backgroundColor = context.getResources().getColor(R.color.outgoing);
    }

    public void setToExternal(){
        isExternal = true;
        backgroundColor = Color.GRAY;
    }

    public void setupBackfroundColor(CatchUp catchUp, Context context){
        if(isExternal){
            return;
        }
        int colorId = -1;
        switch (catchUp.getTabType()){
            case CatchUpManager.CONFIRMED:
                colorId = R.color.confirm;
                break;
            case CatchUpManager.INCOMING:
                colorId = R.color.incoming;
                break;
            case CatchUpManager.OUTGOING:
                colorId = R.color.outgoing;
                break;
        }
        if(colorId == -1){
            return;
        }
        backgroundColor = context.getResources().getColor(colorId);
    }


    public String getTimePeriodString() {
        return DateUtil.formatTime(dateFrom) + " - " + DateUtil.formatTime(dateTo);
    }


    public String getDateString() {
        return DateUtil.formatNewEventDate(dateFrom);
    }

    public CatchUp getCatchUp() {
        return catchUp;
    }

    @Override
    public int compareTo(@NonNull ScheduleItem o) {
        if (dateFrom == null && o.dateFrom == null)
            return 0;
        else if (dateFrom == null)
            return -1;
        return dateFrom.compareTo(o.dateFrom);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(catchUpId);
        parcel.writeLong(timeSlotId);
        parcel.writeString(title);
        parcel.writeString(desctiption);
        parcel.writeSerializable(dateFrom);
        parcel.writeSerializable(dateTo);
        parcel.writeInt(day);
    }
}

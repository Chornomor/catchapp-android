package com.tundramobile.catchapp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.blaze.fastpermission.FastPermission;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.firebase.FirebaseUtils;
import com.tundramobile.catchapp.firebase.SingleChildEventListener;
import com.tundramobile.catchapp.firebase.Types;
import com.tundramobile.catchapp.firebase.model.ChatRoom;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.other.AdapterItemTouchHelper;
import com.tundramobile.catchapp.other.ChatHelper;
import com.tundramobile.catchapp.other.Dialogs;
import com.tundramobile.catchapp.other.SimpleTextWatcher;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.ChatRoomRecyclerAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.viewmodel.ChatRoomViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;

import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.SELECTED_CONTACTS;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class ChatFragment extends BaseFragment {

    private static final String TAG = ChatFragment.class.getSimpleName();

    private Unbinder unbinder;

    @BindView(R.id.recyclerView) RecyclerView listView;
    @BindView(R.id.searchInput) EditText searchView;
    @BindView(R.id.chat_rooms_container) View chatRoomsContainerView;
    @BindView(R.id.chat_dialog_container) View chatDialogContainerView;

    private ChatRoomRecyclerAdapter adapter;

    private int roomsCount = 0;
    private int roomsDownloaded = 0;

    private int participantsCount = 0;
    private int participantsDownloaded = 0;

    private HashMap<String, ChatRoom> chatRooms = new HashMap<>();

    private Map<Long,Contact> userMap = new HashMap<>();


    Long currentUserId;

    private Long myID;

    private ChatRoom targetChatRoom;
    private String targetRoomKey;

    public ChatFragment() {
    }


    public static ChatFragment newInstance(Bundle args) {
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.setArguments(args);
        return chatFragment;
    }

    public static ChatFragment newInstance(Long ownerId, ArrayList<Long> ids) {
        ChatFragment chatFragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putLong("ownerId", ownerId);
        args.putSerializable("ids", ids);
        chatFragment.setArguments(args);
        return chatFragment;
    }

    public static ChatFragment newInstance(long ownerId, long... ids){
        ArrayList<Long> list = new ArrayList<>();
        for(long id : ids){
            list.add(id);
        }
        return newInstance(ownerId, list);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrieveTargetRoom();
        if(savedInstanceState != null) {
            chatRooms = (HashMap<String, ChatRoom>) savedInstanceState.getSerializable("rooms");
        }
        List<ChatRoomViewModel> items = new ArrayList<>();
        adapter = new ChatRoomRecyclerAdapter(items);
    }

    private void retrieveTargetRoom() {
        Bundle args = getArguments();
        if(args != null) {
            targetChatRoom = new ChatRoom(
                (ArrayList<Long>) args.getSerializable("ids"),
                args.getLong("ownerId")
            );
            if(targetChatRoom != null) {
                Log.d(TAG, "retrieveTargetRoom: " + targetChatRoom.toString());
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("rooms", chatRooms);
        super.onSaveInstanceState(outState);
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView.setLayoutManager(new LinearLayoutManager(getContext()));
        listView.setAdapter(adapter);

        setupItemRemoving();

        adapter.setOnItemClickListener((position, model) ->
                showFragment(ChatDialogFragment.newInstance(model.getName(), model.getRoomKey()))
        );


        searchView.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });

        if(targetChatRoom != null) {
            chatRoomsContainerView.setVisibility(View.GONE);
            chatDialogContainerView.setVisibility(View.VISIBLE);
        }
    }

    private void setupItemRemoving() {
        ItemTouchHelper.Callback touchCallback = new AdapterItemTouchHelper(getContext(), (context, adapterPosition) -> {
            String roomKeyToRemove = adapter.getItem(adapterPosition).getRoomKey();

            new AlertDialog.Builder(getContext())
                    .setMessage("Are you sure you want to delete this Chat?")
                    .setPositiveButton(R.string.delete, (dialog, which)
                            -> removeChatRoom(roomKeyToRemove, adapterPosition))
                    .setNegativeButton(android.R.string.cancel, (dialog, which)
                            -> adapter.updateItem(adapterPosition))
                    .create().show();
        });
        ItemTouchHelper touchHelper = new ItemTouchHelper(touchCallback);
        touchHelper.attachToRecyclerView(listView);
        AdapterItemTouchHelper.setUpAnimationDecoratorHelper(getContext(), listView);
    }

    private void removeChatRoom(String roomKey, int adapterPosition) {
        ChatHelper.removeRoom(roomKey, chatRooms.get(roomKey).getIds(),
                success -> {
                    adapter.removeItem(adapterPosition);
                    chatRooms.remove(roomKey);
                },
                exception -> {
                    adapter.removeItem(adapterPosition);
                    Dialogs.showFailureDialog(getContext(), exception.getLocalizedMessage());
                }
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        myID = (long) MyUserManager.getCurrentUser(getContext()).getId();
        currentUserId = new Long(MyUserManager.getCurrentUser(getContext()).getId());
        getNavContactsData();
        ((MainActivity)getActivity()).showProgressDialog();
        if(FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS)){
            authWithFirebase(MyUserManager.getCurrentUser(getContext()).getMobile());
        } else {
            FastPermission.request(this, 888, null,PermissionEnum.READ_CONTACTS);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 888){
            if(FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS)){
                authWithFirebase(MyUserManager.getCurrentUser(getContext()).getMobile());
            } else {
                ((MainActivity)getActivity()).cancelProgressDialog();
            }
        }
    }

    public void update(){
        authWithFirebase(MyUserManager.getCurrentUser(getContext()).getMobile());
    }

    private void getNavContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_CONTACTS);
        if (bundleFromNavigator != null) {
//            contacts.clear();
            List<Contact> contacts = new ArrayList<>();
            contacts.addAll(bundleFromNavigator.getParcelableArrayList(SELECTED_CONTACTS));
            Log.d(TAG, "getNavContactsData: " + contacts.toString());
            List<Long> ids = new ArrayList<>();
            for (Contact curContact : contacts) {
                ids.add((long) curContact.getUid());
            }
            ids.add(myID);
            Collections.sort(ids, Long::compareTo);

            targetRoomKey = findRoom(new ChatRoom(ids, myID));
            if(targetRoomKey != null) {
                showFragment(ChatDialogFragment.newInstance("Chat", targetRoomKey));
            } else {
                createChat(ids, myID);
            }
        }
    }

    private String findRoom(ChatRoom chatRoom) {
        for(String curRoomKey : chatRooms.keySet()) {
            if(chatRooms.get(curRoomKey).equals(chatRoom)) {
                return curRoomKey;
            }
        }
        return null;
    }

    private void createChat(List<Long> ids, Long ownerID) {
        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("chat");
        ChatRoom newChatRoom = new ChatRoom(ids, ownerID);
        DatabaseReference pushRef = chatRef.push();
        pushRef.setValue(newChatRoom).addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                Log.d(TAG, "createChat: Chat room created" + pushRef.getKey());
                updateParticipants(ids, pushRef.getKey());
            }
        });
    }

    private void updateParticipants(List<Long> ids, String chatRoomKey) {
        participantsCount = ids.size();
        participantsDownloaded = 0;
        DatabaseReference paticipantRef = FirebaseDatabase.getInstance().getReference().child("chatParticipant");
        for (Long curId : ids) {
            paticipantRef.child("" + curId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() == null) {
                        Log.d(TAG, "onDataChange: Need create participant ");
                        createParticipant(curId, chatRoomKey);
                    } else {
                        Log.d(TAG, "onDataChange: Need update participant " + dataSnapshot.toString());
                        List<String> data = dataSnapshot.getValue(Types.stringsList);
                        if(data != null) {
                            updateParticipant(dataSnapshot.getKey(), data, chatRoomKey);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void updateParticipant(String participantKey, List<String> roomsKeys, String chatRoomKey) {
        Log.d(TAG, "Update participant " + participantKey);
        roomsKeys.add(chatRoomKey);
        DatabaseReference paticipantRef = FirebaseDatabase.getInstance().getReference().child("chatParticipant").child(participantKey);
        paticipantRef.setValue(roomsKeys).addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                Log.d(TAG, "onComplete: Participant Created");
            }
            participantsDownloaded++;
            if(participantsDownloaded >= participantsCount) {
                participantsCount = 0;
                participantsDownloaded = 0;
                onChatRoomCreated(chatRoomKey);
            }
        });
    }

    private void createParticipant(Long curId, String chatRoomKey) {
        List<String> keys = new ArrayList<>();
        keys.add(chatRoomKey);

        Log.d(TAG, "Create participant " + curId);
        DatabaseReference paticipantRef = FirebaseDatabase.getInstance().getReference().child("chatParticipant");
        paticipantRef.child("" + curId).setValue(keys).addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                Log.d(TAG, "onComplete: Participant Created");
            }
            participantsDownloaded++;
            if(participantsDownloaded >= participantsCount) {
                participantsCount = 0;
                participantsDownloaded = 0;
                onChatRoomCreated(chatRoomKey);
            }
        });
//        GenericTypeIndicator<Map<String, List<String>>> typeIndicator = new GenericTypeIndicator<Map<String, List<String>>>() {};
//        Map<String, List<String>> data = new HashMap<>();
//        paticipantRef.push().setValue()


    }

    private void onChatRoomCreated(String chatRoomKey) {
        //                getParticipantChatRooms();
        // TODO: 27.04.17 get room name
        targetRoomKey = chatRoomKey;
        if(targetChatRoom != null) {
            Navigator
                    .with(getActivity())
                    .container(R.id.chat_dialog_container)
                    .replace(ChatDialogFragment.newInstance("Chat", targetRoomKey));
        } else {
            showFragment(ChatDialogFragment.newInstance("Chat", chatRoomKey));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void authWithFirebase(String phoneNumber) {
        FirebaseUtils.authorize(
                phoneNumber,
                successResult -> getParticipantChatRooms(),
                exception -> Dialogs.showFailureDialog(getContext(), exception.getLocalizedMessage())
        );
    }

    private void getParticipantChatRooms() {
        chatRooms = new HashMap<>();
        DatabaseReference chatParticipant = FirebaseDatabase.getInstance().getReference().child("chatParticipant");
        chatParticipant.orderByKey().equalTo("" + myID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    Log.d(TAG, "onDataChange: Participant " + dataSnapshot.getValue().toString());
                    getChatRooms(dataSnapshot);
                } else {
                    Log.d(TAG, "onDataChange: " + String.format("Participant %s not recognized", myID));
                    ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
//        chatParticipant.orderByKey().equalTo("" + myID).addChildEventListener(new OnChatRoomsChildsChangedListener());
        ///////////////////////////////////////////
//        chatRooms = new ArrayList<>();
//        DatabaseReference chatRoomsRef = FirebaseDatabase.getInstance().getReference().child("chat");
//        chatRoomsRef.orderByValue().startAt("ownerId", "" + myID).addChildEventListener(new OnChatRoomsChildsChangedListener());
    }

    private void getChatRooms(DataSnapshot dataSnapshot) {
        Map<String, List<String>> data = dataSnapshot.getValue(Types.chatRoomsIdsMap);

        Log.d(TAG, "getChatRooms: Count - " + dataSnapshot.getChildrenCount());
        Log.d(TAG, "getChatRooms: Data - " + data);

        DatabaseReference chatRoomsRef = FirebaseDatabase.getInstance().getReference().child("chat");
        List<String> ids = data.get("" + myID);
        Log.d(TAG, "getChatRooms: Data - " + ids);

        loadRooms(chatRoomsRef, ids);
    }

    private void loadRooms(DatabaseReference chatRoomsRef, List<String> ids) {
        roomsCount = ids.size();
        roomsDownloaded = 0;
        chatRooms = new HashMap<>();

        for (String roomID : ids) {
            chatRoomsRef.child(roomID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() != null) {
                        Log.d(TAG, "onDataChange: RoomDetails - " + dataSnapshot.toString());
                        Log.d(TAG, "onDataChange: RoomDetails - " + dataSnapshot.getValue().toString());
                        chatRooms.put(dataSnapshot.getKey(), dataSnapshot.getValue(Types.chatRoom));
                    }
                    roomsDownloaded++;
                    if(roomsDownloaded >= roomsCount) {
                        loadUsers();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void loadUsers(){
        ContactManager.getInstance(getContext()).getPhoneContacts(getContext(), true, new ContactManager.OnObtainContactListener() {
            @Override
            public void onObtainContacts(List<Contact> contacts, String error) {
                if(getActivity() != null) {
                    obtainUsers(contacts);
                    ((MainActivity) getActivity()).cancelProgressDialog();
                }
            }

            @Override
            public void onObtainRecentContacts(List<Contact> recentContactList) {

            }

            @Override
            public void onObtainContactGroups(List<Contact> groupList) {

            }
        });
    }

    private void obtainUsers(List<Contact> users){
        Set<Long> idsSet = new HashSet<>();
        for(String curRoomKey : chatRooms.keySet()){
            idsSet.addAll(chatRooms.get(curRoomKey).getIds());
        }
        idsSet.remove(currentUserId);
        for(Contact u : users){
            if(idsSet.contains(new Long(u.getUid()))) {
                userMap.put((long) u.getUid(), u);
            }
        }
        setupRooms();
    }

    private void setupRooms() {
        if(getContext() == null)
            return ;
        if(targetChatRoom != null) {
            targetRoomKey = findRoom(targetChatRoom);
            if(targetRoomKey != null) {
                Navigator
                        .with(getActivity())
                        .container(R.id.chat_dialog_container)
                        .replace(ChatDialogFragment.newInstance("Chat", targetRoomKey));
            } else {
                createChat(targetChatRoom.getIds(), targetChatRoom.getOwnerId());
            }
            return;
        }

        List<ChatRoomViewModel> items = new ArrayList<>();
        for(String curRoomKey : chatRooms.keySet()) {
            ChatRoomViewModel model = makeChatRoom(curRoomKey, chatRooms.get(curRoomKey));
            if(model != null) {
                items.add(model);
            }
        }
        adapter.setItems(items);
    }

    private ChatRoomViewModel makeChatRoom(String roomKey, ChatRoom chatRoom){
        ArrayList<Long> idsList = new ArrayList<>(chatRoom.getIds());
        idsList.remove(currentUserId);
        if(idsList.isEmpty()){
            return null;
        }
        if(idsList.size() == 1){
            Contact user = userMap.get(idsList.get(0));
            if(user == null){
                return new ChatRoomViewModel(roomKey, null,"Unknown",chatRoom);
            }
            return new ChatRoomViewModel(roomKey, user.getProfilePic(),user.getName(),chatRoom);
        } else {
            Contact user;
            if(chatRoom.getOwnerId().equals(currentUserId)){
                user = userMap.get(idsList.get(0));
                if(user == null){
                    user = findExistingUser(chatRoom.getIds());
                }
            } else {
                user = userMap.get(chatRoom.getOwnerId());
            }
            String name = user == null ? "Unknown " : user.getName();
            name +=  " " + getString(R.string.chat_room_more,idsList.size()-1);
            //TODO replace first image with multi image here
            return new ChatRoomViewModel(roomKey, user == null ? null : user.getProfilePic(),name,chatRoom);
        }

    }

    public Contact findExistingUser(List<Long> ids){
        for(Long id : ids){
            if(userMap.containsKey(id)){
                return userMap.get(id);
            }
        }
        return null;
    }


    private class OnChatRoomsChildsChangedListener extends SingleChildEventListener {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Log.d(TAG, "onChildAdded: " + dataSnapshot.toString());
            if(dataSnapshot.getValue() != null) {
                Log.d(TAG, "onDataChange: Participant " + dataSnapshot.getValue().toString());
                ChatRoom room = dataSnapshot.getValue(Types.chatRoom);
                if(room != null) {
//                    chatRooms.add(room);
//                    adapter.addItem(new ChatRoomViewModel(null, "Igor2", room));
                }
//                getChatRooms(dataSnapshot);
            } else {
                Log.d(TAG, "onDataChange: " + String.format("Participant %s not recognized", myID));
            }
//            GenericTypeIndicator<Message> mapType = new GenericTypeIndicator<Message>() { };
//            Message newMessage = dataSnapshot.getValue(mapType);
//            if(newMessage != null) {
//                adapter.addItem(newMessage);
//                getView().postDelayed(() -> listView.scrollToPosition(adapter.getItemCount() - 1), 50);
//            }
        }

    }

    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        super.onToolbarChanged(toolbar);
        toolbar.switchToGroupsMode(v -> {
            ArrayList<Contact> contacts = new ArrayList<>();
//            Bundle contactsBundle = new Bundle();
//            contactsBundle.putParcelableArrayList(SELECTED_CONTACTS, contacts);

            Bundle otherBundle = new Bundle();
            otherBundle.putBoolean("registered", true);

            Navigator
                    .with(getActivity())
                    .putData("registered", otherBundle);
//                    .putData(SELECTED_CONTACTS, contactsBundle);

            showFragment(SelectEventMembersFragment.newInstance(SelectEventMembersFragment.MULTI_MEMBER_TYPE));
        });

        toolbar.setTitle("Chat");
    }
}

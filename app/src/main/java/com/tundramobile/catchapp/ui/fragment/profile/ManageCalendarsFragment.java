package com.tundramobile.catchapp.ui.fragment.profile;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by eugenetroyanskii on 21.04.17.
 */

public class ManageCalendarsFragment extends BaseFragment {

    @BindView(R.id.manage_calendar_ical_state) ToggleButton ical;
    @BindView(R.id.manage_calendar_google_state) ToggleButton googleC;

    private Unbinder unbinder;


    public static ManageCalendarsFragment newInstance() {
        return new ManageCalendarsFragment();
    }


    public ManageCalendarsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manage_calendar, container, false);
        unbinder = ButterKnife.bind(this, v);
        setTypeface();
        return v;
    }


    /*we have to set typeface because it's ignored from style */
    private void setTypeface() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.montserrat_light));
        ical.setTypeface(typeface);
        googleC.setTypeface(typeface);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.manage_calendar_ical_state)
    public void onIcatClick() {
    }


    @OnClick(R.id.manage_calendar_google_state)
    public void onGoogleClick() {
    }


    @OnClick(R.id.profile_back_btn)
    public void onBack() {
        getActivity().onBackPressed();
    }
}

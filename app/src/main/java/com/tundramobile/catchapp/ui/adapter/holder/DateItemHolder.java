package com.tundramobile.catchapp.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tundramobile.catchapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alexei on 03.04.17.
 */

public class DateItemHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.divider) View divider;
    @BindView(R.id.text) TextView text;


    public DateItemHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public void bindContent(String date) {
        text.setText(date);
    }
}

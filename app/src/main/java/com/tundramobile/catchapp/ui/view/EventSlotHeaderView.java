package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.other.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventSlotHeaderView extends FrameLayout {

    @BindView(R.id.headerText) TextView headerText;


    public EventSlotHeaderView(@NonNull Context context) {
        super(context);
        init(context);
    }


    public EventSlotHeaderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public EventSlotHeaderView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public EventSlotHeaderView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_event_time_slot_header, this, true);
        ButterKnife.bind(this);
    }


    public void setText(String text) {
        headerText.setText(text);
    }
}

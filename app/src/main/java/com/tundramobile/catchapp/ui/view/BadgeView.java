package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.other.Utils;

public class BadgeView extends View {

    private static final String TAG = BadgeView.class.getSimpleName();
    private static final String INSTANCE_STATE = "saved_instance";
    private static final String INSTANCE_PADDING = "padding";
    private static final String INSTANCE_BADGE_TEXT = "badgeText";
    private static final String INSTANCE_BADGE_TEXT_SIZE = "badgeTextSize";
    private static final String INSTANCE_BADGE_TEXT_COLOR = "badgeTextColor";
    private static final String INSTANCE_BADGE_COLOR = "badgeColor";
    private static final String INSTANCE_BADGE_TEXT_STYLE = "badgeTextStyle";
    private static final String INSTANCE_BADGE_IS_SHADOWED = "isShadowed";

    public static final int STYLE_NORMAL = 0;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int CUSTOM = 3;

    private static Typeface normal = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
    private static Typeface bold = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
    private static Typeface italic = Typeface.create(Typeface.SANS_SERIF, Typeface.ITALIC);
    private static Typeface custom;

    private int padding;
    private String badgeText;
    private int badgeTextSize;
    private int badgeTextColor;
    private int badgeColor;
    private int badgeTextStyle;
    private boolean isShadowed;
    private int textShadowColor;

    private Rect textBounds;
    private Paint textPaint;

    private float badgeWidth;
    private float badgeHeight;


    public BadgeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }


    private void init(AttributeSet attrs) {

        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.BadgeView, 0, 0);
        try {
            setPadding(typedArray.getDimensionPixelSize(R.styleable.BadgeView_badgePadding, 0));
            setBadgeText(typedArray.getString(R.styleable.BadgeView_badgeText));
            setBadgeTextSize(typedArray.getDimensionPixelSize(R.styleable.BadgeView_badgeTextSize, 20));
            setBadgeTextColor(typedArray.getColor(R.styleable.BadgeView_badgeTextColor, ContextCompat.getColor(getContext(), R.color.white)));
            setBadgeColor(typedArray.getColor(R.styleable.BadgeView_badgeColor, ContextCompat.getColor(getContext(), R.color.green)));
            setBadgeTextStyle(typedArray.getInteger(R.styleable.BadgeView_badgeTextStyle, STYLE_NORMAL));
            setCustomFont(typedArray.getString(R.styleable.BadgeView_badgeCustomFont));
            setShadowed(typedArray.getBoolean(R.styleable.BadgeView_badgeTextShadowed, false));
        } finally {
            typedArray.recycle();
        }

        initPaints();
    }


    private void initPaints() {
        textShadowColor = ContextCompat.getColor(getContext(), R.color.gray);

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(badgeTextColor);
        textPaint.setTextSize(badgeTextSize);
        applyRewardTextStyleToPaint();

        textBounds = new Rect();
        textPaint.getTextBounds(badgeText, 0, badgeText.length(), textBounds);
    }


    public int getPadding() {
        return padding;
    }


    public void setPadding(int padding) {
        this.padding = padding;
        invalidate();
    }


    public String getBadgeText() {
        return badgeText;
    }


    public void setBadgeText(String badgeText) {
        if (TextUtils.isEmpty(badgeText)) badgeText = "0";
        this.badgeText = badgeText;
        requestLayout();
        invalidate();
    }


    public int getBadgeTextSize() {
        return badgeTextSize;
    }


    public void setBadgeTextSize(int badgeTextSize) {
        this.badgeTextSize = badgeTextSize;
        invalidate();
    }


    public int getBadgeTextColor() {
        return badgeTextColor;
    }


    public void setBadgeTextColor(int badgeTextColor) {
        this.badgeTextColor = badgeTextColor;
        if (textPaint != null)
            textPaint.setColor(badgeTextColor);
        invalidate();
    }


    public int getBadgeColor() {
        return badgeColor;
    }


    public void setBadgeColor(int badgeColor) {
        this.badgeColor = badgeColor;
        invalidate();
    }


    public boolean isShadowed() {
        return isShadowed;
    }


    public void setShadowed(boolean shadowed) {
        isShadowed = shadowed;
        invalidate();
    }


    public int getBadgeTextStyle() {
        return badgeTextStyle;
    }


    public void setBadgeTextStyle(int badgeTextStyle) {
        this.badgeTextStyle = badgeTextStyle;
        applyRewardTextStyleToPaint();
        invalidate();
    }


    public boolean setCustomFont(String asset) {
        return setCustomFont(getContext(), asset);
    }


    public boolean setCustomFont(Context ctx, String asset) {
        custom = Utils.getTypeface(ctx, asset);
        if (custom != null) {
            setBadgeTextStyle(CUSTOM);
//            Utils.logd(TAG, "setCustomFont: " + asset);
            return true;
        } else {
//            Utils.loge(TAG, "setCustomFont: Failed to set font");
            return false;
        }
    }


    private void applyRewardTextStyleToPaint() {
        if (textPaint == null) return;
        switch (badgeTextStyle) {
            case STYLE_NORMAL:
            default:
                textPaint.setTypeface(normal);
                break;
            case STYLE_BOLD:
                textPaint.setTypeface(bold);
                break;
            case STYLE_ITALIC:
                textPaint.setTypeface(italic);
                break;
            case CUSTOM:
                textPaint.setTypeface(custom);
                break;
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int specHeight = MeasureSpec.getSize(heightMeasureSpec);
        int specWidth = MeasureSpec.getSize(widthMeasureSpec);

        badgeHeight = textBounds.height() + (padding * 2);
//        badgeWidth = badgeText.length() <= 1 ? badgeHeight : textPaint.measureText(badgeText) + padding + (badgeHeight / 2);
        badgeWidth = badgeHeight;

        int width;
        int height;

        switch (MeasureSpec.getMode(heightMeasureSpec)) {

            // be exactly the given specHeight
            case MeasureSpec.EXACTLY:
                height = specHeight;
                width = specWidth;
                break;

            // be at most the given specHeight
            case MeasureSpec.AT_MOST:
                height = Math.min((int) badgeHeight, specHeight);
                width = Math.min((int) badgeWidth, specWidth);
                break;

            // be whatever size you want
            case MeasureSpec.UNSPECIFIED:
            default:
                height = specHeight;
                width = specWidth;
                break;
        }

        // must call this, otherwise the app will crash
        setMeasuredDimension(width, height);
    }


    RectF inset = new RectF();


    @Override
    protected void onDraw(Canvas canvas) {

        if (isShadowed) {
            textPaint.setShadowLayer(10.0f, 1.0f, 1.0f, textShadowColor);
        } else {
            textPaint.setShadowLayer(0, 0, 0, 0);
        }

        badgeHeight = textBounds.height() + (padding * 2);
//        badgeWidth = badgeText.length() <= 1 ? badgeHeight : textPaint.measureText(badgeText) + padding + (badgeHeight / 2);
        badgeWidth = badgeHeight;

        int halfHeight = getHeight() / 2;
        int halfWidth = getWidth() / 2;

        float radius = badgeHeight / 2;
        float[] rads = new float[]{radius, radius, radius, radius, radius, radius, radius, radius};

        inset.left = 0;
        inset.top = 0;
        inset.right = badgeWidth;
        inset.bottom = badgeHeight;

        RoundRectShape rect = new RoundRectShape(rads, inset, rads);
        ShapeDrawable mDrawable = new ShapeDrawable(rect);
        mDrawable.getPaint().setColor(badgeColor);
        mDrawable.setBounds((int) inset.left, (int) inset.top, (int) inset.right, (int) inset.bottom);
        mDrawable.draw(canvas);

        float x = halfWidth - textPaint.measureText(badgeText) / 2;
        float y = halfHeight - ((textPaint.descent() + textPaint.ascent()) / 2);

        canvas.drawText(badgeText, x, y, textPaint);

        super.onDraw(canvas);
    }


    @Override
    protected Parcelable onSaveInstanceState() {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(INSTANCE_STATE, super.onSaveInstanceState());
        bundle.putInt(INSTANCE_PADDING, getPadding());
        bundle.putString(INSTANCE_BADGE_TEXT, getBadgeText());
        bundle.putInt(INSTANCE_BADGE_TEXT_SIZE, getBadgeTextSize());
        bundle.putInt(INSTANCE_BADGE_TEXT_COLOR, getBadgeTextColor());
        bundle.putInt(INSTANCE_BADGE_COLOR, getBadgeColor());
        bundle.putInt(INSTANCE_BADGE_TEXT_STYLE, getBadgeTextStyle());
        bundle.putBoolean(INSTANCE_BADGE_IS_SHADOWED, isShadowed());
        return bundle;
    }


    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            final Bundle bundle = (Bundle) state;
            setPadding(bundle.getInt(INSTANCE_PADDING));
            setBadgeText(bundle.getString(INSTANCE_BADGE_TEXT));
            setBadgeTextSize(bundle.getInt(INSTANCE_BADGE_TEXT_SIZE));
            setBadgeTextColor(bundle.getInt(INSTANCE_BADGE_TEXT_COLOR));
            setBadgeColor(bundle.getInt(INSTANCE_BADGE_COLOR));
            setBadgeTextStyle(bundle.getInt(INSTANCE_BADGE_TEXT_STYLE));
            setShadowed(bundle.getBoolean(INSTANCE_BADGE_IS_SHADOWED));
            initPaints();
            super.onRestoreInstanceState(bundle.getParcelable(INSTANCE_STATE));
            return;
        }
        super.onRestoreInstanceState(state);
    }
}

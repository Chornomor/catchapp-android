package com.tundramobile.catchapp.ui.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.other.CustomTypefaceSpan;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.CollageImageView;
import com.tundramobile.catchapp.ui.viewmodel.NotificationItem;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sviatoslav Zaitsev on 21.04.17.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private Context context;
    private RecyclerItemClickListener<NotificationItem> listener;
    private List<NotificationItem> items;
    private Map<String,Object> viewedIds; //TODO refactor
    private SharedPreferences prefs;

    public NotificationsAdapter(Context context, RecyclerItemClickListener<NotificationItem> listener) {
        this.context = context;
        this.listener = listener;
        prefs = context.getSharedPreferences(NotificationsAdapter.class.getName(),Context.MODE_PRIVATE);
        viewedIds = (Map<String, Object>) prefs.getAll();

    }

    public void setItems(List<NotificationItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.photo) CollageImageView image;
        @BindView(R.id.message) TextView message;
        @BindView(R.id.when) TextView when;
        @BindView(R.id.initials) TextView initials;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationItem item = items.get(position);
        if(item.getPhoto() == null || item.getPhoto().isEmpty()){
            holder.initials.setVisibility(View.VISIBLE);
            holder.image.setImageResource(Utils.getRandomGreyShape());
            holder.initials.setText(Utils.getNameInitials(item.getUser()));
        } else {
            holder.image.setImageDrawable(null);
            holder.initials.setVisibility(View.INVISIBLE);
            Glide.with(context).load(item.getPhoto()).into(holder.image);
        }
        if(viewedIds.containsKey(item.getIdString())){
            holder.itemView.setBackground(null);
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.purple_light));
        }


        holder.message.setText(
                getSpannable(
                    item.getMessage(),
                    item.getUser(),
                    item.getCatchUp()
                )
        );

        holder.when.setText(item.getWhen());

        holder.itemView.setOnClickListener(v -> {
            if(listener != null) {
                selectItem(holder, item);
                listener.onItemClick(holder.getAdapterPosition(), item);
            }
        });
    }

    private void selectItem(ViewHolder holder, NotificationItem item) {
        viewedIds.put(item.getIdString(), "");
        notifyItemChanged(holder.getAdapterPosition());
        prefs.edit().putString(item.getIdString(), "").apply();
    }

    @NonNull
    private SpannableStringBuilder getSpannable(String message, String user, String catchUp) {

        Typeface userFont = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.montserrat_bold));
        Typeface catchUpFont = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.montserrat_bold));

        ForegroundColorSpan userColor = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.contact_black));
        ForegroundColorSpan catchUpColor = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.purple));

        int userEnd = message.indexOf(user) + user.length();
        int catchUpStart = message.lastIndexOf(catchUp);

        SpannableStringBuilder ss = new SpannableStringBuilder(message);

        if(userEnd != -1) {
            ss.setSpan(new CustomTypefaceSpan("", userFont), 0, userEnd, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ss.setSpan(userColor, 0, userEnd, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        if(catchUpStart != -1) {
            ss.setSpan(new CustomTypefaceSpan("", catchUpFont), catchUpStart, message.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            ss.setSpan(catchUpColor, catchUpStart, message.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        }

        return ss;
    }


    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}

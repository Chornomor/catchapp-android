package com.tundramobile.catchapp.ui.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tundramobile.catchapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eugenetroyanskii on 29.03.17.
 */

public class ScreenSlidePageFragment extends Fragment {

    private int pageNumber;

    public static ScreenSlidePageFragment getInstance(int pageNumber) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        fragment.pageNumber = pageNumber;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView;
        switch (pageNumber) {
            case 1:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_intro1, container, false);
                break;
            case 2:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_intro2, container, false);
                break;
            case 3:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_intro3, container, false);
                break;
            default:
                rootView = (ViewGroup) inflater.inflate(R.layout.layout_intro1, container, false);
        }

        ButterKnife.bind(this, rootView);
        return rootView;
    }
}

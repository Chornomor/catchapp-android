package com.tundramobile.catchapp.ui.activity.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Country;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.request.RequestOTPBody;
import com.tundramobile.catchapp.entity.response.LoginRequestOTP;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.other.Regex;
import com.tundramobile.catchapp.ui.activity.BaseActivity;
import com.tundramobile.catchapp.ui.adapter.CountryCodeAdapter;
import com.tundramobile.catchapp.ui.dialog.CountryCodeDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class NumberActivity extends BaseActivity implements CountryCodeAdapter.OnCountryItemClickListener {

    @BindView(R.id.country_code) TextView countryCode;
    @BindView(R.id.phone_number) EditText phoneNumber;
    @BindView(R.id.auth_fab_next) FloatingActionButton fabNext;

    private User userModel;
    private CountryCodeDialog codeDialog;
    private Country country;
    private String phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_number);
        ButterKnife.bind(this);
        initSimCountryCode();
        setupKeyboardDoneButton();
        userModel = MyUserManager.getCurrentUser(this);
    }


    private void initSimCountryCode() {
        country = Country.getCountryByCountryCode(this);
        fillCountryCode();
    }


    private void fillCountryCode() {
        String code = "+";
        if (country != null) {
            code = code + (country.getPhoneCode());
        }
        countryCode.setText(code);
    }


    private void setupKeyboardDoneButton() {
        phoneNumber.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                OnNextClick();
            }
            return false;
        });
    }


    @OnClick(R.id.auth_fab_next)
    public void OnNextClick() {
        if (Regex.validatePhoneNumber(phoneNumber.getText().toString(), country)) {
            showProgressDialog();
            String rawNumber = phoneNumber.getText().toString();
            if (rawNumber.startsWith("0")) {
                rawNumber = rawNumber.replaceFirst("0", "");
            }
            phone = countryCode.getText().toString().concat(rawNumber);
            MyUserManager.getInstance().requestOTP(new RequestOTPBody(phone), getPinCallback);

        } else {
            TopPopUpManager.showTopPopUp(this, getString(R.string.auth_1_invalid_phone), !TopPopUpManager.isError);
        }
    }


    @OnClick(R.id.country_code)
    public void OnCountryCodeClick() {
        codeDialog = new CountryCodeDialog(this, this);
        codeDialog.show();
    }


    @Override
    public void onCountryItemClick(Country country) {
        this.country = country;
        codeDialog.dismiss();
        fillCountryCode();
    }


    HandledCallback<LoginRequestOTP> getPinCallback = new HandledCallback<LoginRequestOTP>() {
        @Override
        public void onSuccess(Call<LoginRequestOTP> call, Response<LoginRequestOTP> response) {
            cancelProgressDialog();
            if (response.body().getSuccess().equals(phone.replace("+", ""))) {
                MyUserManager.getCurrentUser(NumberActivity.this).setMobile(phone);
                Intent intent = new Intent(NumberActivity.this, PinCodeActivity.class);
                startActivity(intent);
            }
        }


        @Override
        public void onFail(Call call, Response response) {
            cancelProgressDialog();
        }


        @Override
        public void onProcessFailed(Call call, Throwable t, String error) {
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(NumberActivity.this, error, TopPopUpManager.isError);
        }
    };
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.interfaces.BindingManager;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.manager.ImageLoader;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.CollageImageView;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactHolder extends RecyclerView.ViewHolder implements BindingManager<Contact> {

    private static final String TAG = ContactHolder.class.getSimpleName();

    @BindView(R.id.avatar) CollageImageView avatar;
    @BindView(R.id.initials) TextView initials;
    @BindView(R.id.emptyGroupText) View emptyGroupText;
    @BindView(R.id.emptyGroupImage) CollageImageView emptyGroupImage;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.phone) TextView phone;
    @BindView(R.id.chatFlag) View chatFlag;

    @BindDimen(R.dimen.catchup_holder_avatar_size) int avatarSize;

    @BindColor(R.color.purple_light) int selectedColor;

    private Contact contact;


    public ContactHolder(View itemView, RecyclerItemClickListener<Contact> listener) {
        super(itemView);
        initView(itemView, listener);
    }


    private void initView(View v, final RecyclerItemClickListener<Contact> listener) {
        ButterKnife.bind(this, v);
        itemView.setOnClickListener(v1 -> {
            if (listener != null) listener.onItemClick(getAdapterPosition(), contact);
        });
    }


    public void setSelected(boolean isSelected) {
        itemView.setBackgroundColor(isSelected ? selectedColor : Color.WHITE);
    }


    @Override
    public void bindContent(Contact contact) {
        this.contact = contact;

        setAvatar();
        setName();
        setPhone();
        setChatFlag();
    }


    private void setAvatar() {
        avatar.setCollageImages(null);
        initials.setVisibility(View.INVISIBLE);
        emptyGroupText.setVisibility(View.INVISIBLE);

        if (contact.getMembers() != null && !contact.getMembers().isEmpty()) {
            emptyGroupImage.setImageResource(Utils.getRandomGreyShape());
            emptyGroupText.setVisibility(View.VISIBLE);
            ImageLoader.newInstance(avatar, avatarSize).loadImages(contact.getMembers());
        } else {
            String url = contact.getProfilePic();
            if (!TextUtils.isEmpty(url)) {
                ImageLoader.newInstance(avatar, avatarSize, true).loadImage(contact);
            } else {
                avatar.setImageResource(Utils.getRandomGreyShape());
                initials.setText(Utils.getNameInitials(contact.getName()));
                initials.setVisibility(View.VISIBLE);
            }
        }
    }


    private void setName() {
        name.setText(contact.getName());
    }


    private void setPhone() {
        if (contact.isGroupContact()) {
            if (contact.getMembers() != null && !contact.getMembers().isEmpty()) {
                String members = phone.getResources().getQuantityString(R.plurals.members,
                        contact.getMembers().size(), contact.getMembers().size());
                phone.setText(members);
                phone.setVisibility(View.VISIBLE);
            } else {
                phone.setVisibility(View.GONE);
            }
        } else {
            if (TextUtils.isEmpty(contact.getMobile())) {
                phone.setVisibility(View.GONE);
            } else {
                phone.setText(contact.getMobile());
                phone.setVisibility(View.VISIBLE);
            }
        }
    }


    private void setChatFlag() {
        if (contact.getContactType() == Contact.REGISTERED_CONTACT_IN_SYSTEM) {
            chatFlag.setVisibility(View.VISIBLE);
        } else {
            chatFlag.setVisibility(View.GONE);
        }
    }


    public void bindContactForNewEventWho(Contact contact) {
        this.contact = contact;

        setAvatar();
        setName();
    }


    public void fillMoreCountForNewEventWho(int leftCount, Context context) {
        emptyGroupText.setVisibility(View.INVISIBLE);
        avatar.setCollageImages(null);
        avatar.setImageResource(Utils.getRandomGreyShape());
        initials.setText(String.valueOf("+" + leftCount));
        initials.setVisibility(View.VISIBLE);
        name.setText(context.getString(R.string.more));
    }


    public Contact getContact() {
        return contact;
    }
}

package com.tundramobile.catchapp.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.ui.adapter.holder.ContactHolder;

import java.util.ArrayList;

public class NewEventWhoAdapter extends RecyclerView.Adapter<ContactHolder> {

    private RecyclerItemClickListener<Contact> listener;
    private ArrayList<Contact> items;
    private Context context;


    public NewEventWhoAdapter(@NonNull RecyclerItemClickListener<Contact> listener) {
        this.listener = listener;
    }


    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ContactHolder(LayoutInflater.from(context).
                inflate(R.layout.item_new_event_who, parent, false), listener);
    }


    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        if (position < 3) {
            Contact contact = items.get(position);
            holder.bindContactForNewEventWho(contact);
        } else {
            holder.fillMoreCountForNewEventWho(items.size() - position, context);
        }
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public void setItems(ArrayList<Contact> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        int count = 0;
        if (items != null || !items.isEmpty()) {
            if (items.size() < 4) {
                count = items.size();
            } else {
                count = 4;
            }
        }
        return count;
    }


    public ArrayList<Contact> getContacts() {
        return items;
    }
}

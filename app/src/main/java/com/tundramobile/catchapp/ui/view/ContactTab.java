package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.TabData;

import butterknife.BindString;
import butterknife.BindView;

public class ContactTab extends BaseTab {

    @BindView(R.id.title) FontTextView title;

    @BindString(R.string.montserrat_light) String montserratLight;
    @BindString(R.string.montserrat_semi_bold) String montserratSemiBold;


    public ContactTab(Context context, TabData tabData) {
        super(context, tabData);
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.tab_contact;
    }


    @Override
    protected void setupViews(TabData tabData, boolean isSelected) {
        title.setText(tabData.getTitle());
        title.setCustomFont(isSelected ? montserratSemiBold : montserratLight);
        title.setTextColor(ContextCompat.getColor(getContext(),
                isSelected ? tabData.getSelectedColor() : tabData.getDefaultColor()));
    }
}

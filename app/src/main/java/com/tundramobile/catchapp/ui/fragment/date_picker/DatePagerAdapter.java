package com.tundramobile.catchapp.ui.fragment.date_picker;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by andreybofanov on 24.04.17.
 */

public class DatePagerAdapter extends PagerAdapter {
    Calendar calendar = Calendar.getInstance();
    Deque<MonthPage> views = new LinkedList<>();

    DateChangedListener dateChangedListener;
    DateFormatProvider dateFormatProvider;

    int initialSelection = -1;

    @Override
    public int getCount() {
        return 100;
    }

    public int convertPosition(int position){
        return (getCount()/2 * -1) + position;
    }

    public int getMonthForPosition(int position){
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH,convertPosition(position));
        return calendar.get(Calendar.MONTH);
    }
    public int getYearForPosition(int position){
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH,convertPosition(position));
        return calendar.get(Calendar.YEAR);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH,convertPosition(position));
        MonthPage v = getView(container.getContext());
        v.setDateFormatProvider(dateFormatProvider);
        v.setDateChangedListener(dateChangedListener);
        v.setup(calendar.get(Calendar.MONTH),calendar.get(Calendar.YEAR));
        if(position == initialSelection){
            v.setSelectedDay(calendar.get(Calendar.DATE));
        }
        container.addView(v,new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        v.setTag(position);
        return v;
    }


    void  setInitialSelection( int initialSelection ){
        this.initialSelection = initialSelection;
    }

    MonthPage getView(Context context){
        MonthPage convert = views.pollFirst();
        if(convert == null){
            convert = new MonthPage(context);
        }
        return convert;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        destroyItem((ViewGroup) container,position,object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public void setDateChangedListener(DateChangedListener dateChangedListener) {
        this.dateChangedListener = dateChangedListener;
    }

    public void setDateFormatProvider(DateFormatProvider dateFormatProvider) {
        this.dateFormatProvider = dateFormatProvider;
    }
}

package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Event;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.other.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventSlotItemView extends FrameLayout {

    @BindView(R.id.timeText) TextView time;
    @BindView(R.id.image) ImageView image;

    private Event event;
    private boolean isEditable;
    private EventDetailsClickListener listener;


    public EventSlotItemView(@NonNull Context context) {
        super(context);
        init(context);
    }


    public EventSlotItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public EventSlotItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public EventSlotItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_event_time_slot_item, this, true);
        ButterKnife.bind(this);
    }


    public void setEditable(boolean isEditable) {
        this.isEditable = isEditable;
        image.setImageResource(isEditable ? R.drawable.cross : R.drawable.arrow_right_black);
    }


    public void setupWith(Event event) {
        this.event = event;

        String from = DateUtil.formatTime(event.getFrom());
        String to = DateUtil.formatTime(event.getTo());
        String result = from + "-" + to;
        time.setText(result);
    }


    @OnClick(R.id.container)
    public void onClick() {
        if (listener != null) listener.onTimeSlotClick(event, isEditable);
    }


    public void setClickListener(EventDetailsClickListener listener) {
        this.listener = listener;
    }
}

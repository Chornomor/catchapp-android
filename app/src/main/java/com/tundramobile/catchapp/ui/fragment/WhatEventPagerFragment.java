package com.tundramobile.catchapp.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.EVENT_CREATED;
import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.EVENT_MODE;
import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.GROUP;
import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.ONE_PERSON;
import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.SELECTED_CONTACTS;
import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.SEVERAL_PEOPLE;

public class WhatEventPagerFragment extends BaseFragment {

    private final static int NUM_PAGES = 3;
    public final static String HAVE_CONTACTS = "HAVE_CONTACTS";
    public final static String HAVE_GROUP_CONTACTS = "HAVE_GROUP_CONTACTS";

    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.fab_done) FloatingActionButton fabNext;
    @BindView(R.id.indicator1) View indicator1;
    @BindView(R.id.indicator2) View indicator2;
    @BindView(R.id.indicator3) View indicator3;

    private WhatEventSlidePageAdapter whatEventPagerAdapter;
    private int currentPagerPosition = 0;

    private Set<Contact> contacts = new HashSet<>();
    private boolean haveContacts = false;
    private boolean haveGroupContacts = false;
    private int eventMode = -1;

    private int resultOk = 0;


    public static WhatEventPagerFragment newInstance() {
        WhatEventPagerFragment fragment = new WhatEventPagerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            contacts.addAll(savedInstanceState.getParcelableArrayList(SELECTED_CONTACTS));
            eventMode = savedInstanceState.getInt(EVENT_MODE);
        }
        getNavHaveContactsData();
        getNavHaveGroupContactsData();
        if (haveContacts) {
            getNavContactsData();
            getNavRecentContactsData();
        }
        if (haveGroupContacts) {
            getNavGroupContactsData();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_what_event_pager, container, false);
        ButterKnife.bind(this, v);
        selectIndicator(currentPagerPosition);
        setupPager();
        setupFab();
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    private void getNavHaveContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(HAVE_CONTACTS);
        if (bundleFromNavigator != null) {
            haveContacts = bundleFromNavigator.getBoolean(HAVE_CONTACTS, false);
        }
    }


    private void getNavHaveGroupContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(HAVE_GROUP_CONTACTS);
        if (bundleFromNavigator != null) {
            haveGroupContacts = bundleFromNavigator.getBoolean(HAVE_GROUP_CONTACTS, false);
        }
    }


    private void getNavContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_CONTACTS + ContactManager.CONTACTS);
        if (bundleFromNavigator != null) {
            contacts.clear();
            contacts.addAll(bundleFromNavigator.getParcelableArrayList(SELECTED_CONTACTS + ContactManager.CONTACTS));
        }
    }


    private void getNavRecentContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_CONTACTS + ContactManager.RECENT);
        if (bundleFromNavigator != null) {
            contacts.addAll(bundleFromNavigator.getParcelableArrayList(SELECTED_CONTACTS + ContactManager.RECENT));
        }
    }


    private void getNavGroupContactsData() {
        Bundle bundleFromNavigator = Navigator.with(getActivity()).popData(SELECTED_CONTACTS + ContactManager.GROUPS);
        if (bundleFromNavigator != null) {
            contacts.clear();
            contacts.addAll(bundleFromNavigator.getParcelableArrayList(SELECTED_CONTACTS + ContactManager.GROUPS));
        }
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if (toolbar != null) {
            toolbar.switchToDefaultMode(getString(R.string.what_event_title), v -> {
                if(getActivity() != null){
                    getActivity().onBackPressed();
                }
            });
        }
    }


    private void setupFab() {
        fabNext.setOnClickListener(v -> {
            if ((eventMode == SEVERAL_PEOPLE || eventMode == GROUP) && (currentPagerPosition == SEVERAL_PEOPLE || currentPagerPosition == GROUP)) {
                sendContactsAndOpenFragment();
            } else if ((eventMode == SEVERAL_PEOPLE || eventMode == GROUP) && (currentPagerPosition == ONE_PERSON)) {
                TopPopUpManager.showTopPopUp(getActivity(), "You choose multi select", !TopPopUpManager.isError);
            } else {
                sendContactsAndOpenFragment();
            }
        });
    }


    private void sendContactsAndOpenFragment() {
        if (!contacts.isEmpty()) {
            Bundle bundle = new Bundle();
            ArrayList<Contact> contacts = new ArrayList<>();
            contacts.addAll(this.contacts);
            bundle.putParcelableArrayList(SELECTED_CONTACTS, contacts);
            Navigator.with(getActivity()).putData(SELECTED_CONTACTS, bundle);
        }
        showFragment(NewEventFragment.newInstance(currentPagerPosition));
    }


    private void setupPager() {
        whatEventPagerAdapter = new WhatEventSlidePageAdapter(getChildFragmentManager());
        viewPager.setAdapter(whatEventPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }


            @Override
            public void onPageSelected(int position) {
                currentPagerPosition = position;
                selectIndicator(currentPagerPosition);
            }


            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        checkContacts();
    }


    private void checkContacts() {
        if (haveContacts) {
            if (contacts.size() == 1) {
                currentPagerPosition = 0;
            } else {
                if (contacts.size() > 1) {
                    currentPagerPosition = 1;
                }
            }
        } else {
            if (haveGroupContacts) {
                currentPagerPosition = 2;
            }
        }
        if (eventMode == -1) {
            eventMode = currentPagerPosition;
        }
        viewPager.setCurrentItem(currentPagerPosition);
    }


    private void selectIndicator(int position) {
        switch (position) {
            case 0:
                indicator1.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_selected));
                indicator2.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_default));
                indicator3.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_default));
                break;
            case 1:
                indicator1.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_default));
                indicator2.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_selected));
                indicator3.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_default));
                break;
            case 2:
                indicator1.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_default));
                indicator2.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_default));
                indicator3.setBackground(getActivity().getDrawable(R.drawable.tab_indicator_selected));
                break;
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.addAll(this.contacts);
        outState.putParcelableArrayList(SELECTED_CONTACTS, contacts);
        outState.putInt(EVENT_MODE, eventMode);
        super.onSaveInstanceState(outState);
    }


    private class WhatEventSlidePageAdapter extends FragmentStatePagerAdapter {

        private WhatEventSlidePageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return WhatEventSlidePageFragment.getInstance(position + 1);
        }


        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}

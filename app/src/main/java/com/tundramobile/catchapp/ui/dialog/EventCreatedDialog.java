package com.tundramobile.catchapp.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Country;
import com.tundramobile.catchapp.ui.adapter.CountryCodeAdapter;
import com.tundramobile.catchapp.ui.view.fastScroll.FastScrollRecyclerViewItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by alexchern on 31.03.17.
 */

public class EventCreatedDialog extends Dialog {

    @BindView(R.id.dialog_title) TextView tvTitle;
    @BindView(R.id.dialog_message) TextView tvMessage;


    public EventCreatedDialog(@NonNull Context context) {
        super(context);
        initView();
    }


    public void setDialogContent(String title, String message) {
        tvTitle.setText(title);
        tvMessage.setText(message);
    }


    private void initView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_event_created);
        ButterKnife.bind(this);
    }
}

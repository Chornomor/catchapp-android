package com.tundramobile.catchapp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.ui.activity.CreateGroupNameActivity;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by eugenetroyanskii on 10.04.17.
 */

public class CreateGroupFragment extends BaseFragment {

    @BindView(R.id.create_group_add_btn) TextView addBtn;

    private Unbinder unbinder;
    private Set<Integer> contactIdList = new HashSet<>();


    public CreateGroupFragment() {
    }


    public static CreateGroupFragment newInstance() {
        return new CreateGroupFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create_group, container, false);
        unbinder = ButterKnife.bind(this, v);
//        addFragment(ContactsFragment.newInstanceCreateGroup(), R.id.fragmentContainer);
        getChildFragmentManager().beginTransaction().add(R.id.fragmentContainer, ContactsFragment.newInstanceCreateGroup(), null).commit();
        return v;
    }


    @OnClick(R.id.create_group_add_btn)
    public void onAdd() {
        int[] idsArray = new int[contactIdList.size()];
        int counter = 0;
        for (Integer id : contactIdList) {
            idsArray[counter] = id;
            counter++;
        }
        Intent intent = new Intent(getActivity(), CreateGroupNameActivity.class);
        intent.putExtra(CreateGroupNameActivity.GROUP_CONTACT_IDS, idsArray);
        startActivity(intent);
    }


    private void updateContactIdList(Contact contact, boolean isAdding) {
        if (contact.isGroupContact()) {
            if (isAdding) {
                for (Contact tempContact : contact.getMembers()) {
                    contactIdList.add(tempContact.getUid());
                }
            } else {
                for (Contact tempContact : contact.getMembers()) {
                    contactIdList.remove(tempContact.getUid());
                }
            }
        } else {
            if (isAdding) {
                contactIdList.add(contact.getUid());
            } else {
                contactIdList.remove(contact.getUid());
            }
        }
    }


    private void updateAddButton() {
        if (addBtn.getVisibility() != View.VISIBLE) {
            addBtn.setVisibility(View.VISIBLE);
        }
        if (contactIdList.size() < 1) {
            addBtn.setVisibility(View.GONE);
        }
        addBtn.setText(getResources().getString(R.string.add_contact_add) + " " + contactIdList.size());
    }


    public void onItemClicked(Contact contact, boolean isAdding) {
        updateContactIdList(contact, isAdding);
        updateAddButton();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.interfaces.BindingManager;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;

public class BaseEventHolder extends RecyclerView.ViewHolder implements BindingManager<CatchUp> {

    protected EventDetailsClickListener listener;
    protected CatchUp catchUp;
    protected int type;


    public BaseEventHolder(View itemView, EventDetailsClickListener listener) {
        super(itemView);
        this.listener = listener;
    }


    @Override
    public void bindContent(CatchUp model) {
        this.catchUp = model;
    }

    public void bindContent(CatchUp catchUp, int type) {
        bindContent(catchUp);
        this.type = type;
    }
}

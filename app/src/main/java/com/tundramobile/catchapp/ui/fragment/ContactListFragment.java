package com.tundramobile.catchapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.entity.response.LoginRequestOTP;
import com.tundramobile.catchapp.event.GetContactsEvent;
import com.tundramobile.catchapp.interfaces.FabHandler;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.interfaces.SelectorListener;
import com.tundramobile.catchapp.interfaces.TabDataI;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.ui.adapter.ContactsRecyclerAdapter;
import com.tundramobile.catchapp.ui.adapter.holder.ContactHolder;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;

public class ContactListFragment extends BaseFragment implements TabDataI,
        RecyclerItemClickListener<Contact> {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.searchInput) EditText searchInput;

    private static final String CONTACTS = "CONTACTS";
    private static final String SELECTED_CONTACTS = "SELECTED_CONTACTS";
    private static final String SCREEN_TYPE = "SCREEN_TYPE";

    public TabData tabData;
    private Unbinder unbinder;
    private int type;
    private int screenType;
    private ContactsRecyclerAdapter adapter;
    private FabHandler fabHandler;
    private SelectorListener selectorListener;
    private int positionToDelete;
    private Contact contactToDelete;


    public ContactListFragment() {
    }


    public static ContactListFragment newContactsInstance(TabData tabData, int screenType) {
        return newInstance(tabData, screenType, ContactManager.CONTACTS);
    }


    public static ContactListFragment newRecentInstance(TabData tabData, int screenType) {
        return newInstance(tabData, screenType, ContactManager.RECENT);
    }


    public static ContactListFragment newGroupsInstance(TabData tabData) {
        return newInstance(tabData, -1, ContactManager.GROUPS);
    }


    private static ContactListFragment newInstance(TabData tabData, int screenType, int type) {
        ContactListFragment fragment = new ContactListFragment();
        fragment.setTabData(tabData);
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putInt(SCREEN_TYPE, screenType);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FabHandler) {
            fabHandler = (FabHandler) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FabHandler");
        }

        if (context instanceof SelectorListener) {
            selectorListener = (SelectorListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement SelectorListener");
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            tabData = savedInstanceState.getParcelable(TAB_DATA);
            type = savedInstanceState.getInt(TYPE);
            screenType = savedInstanceState.getInt(SCREEN_TYPE);
        } else if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
            screenType = getArguments().getInt(SCREEN_TYPE);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact_list, container, false);
        unbinder = ButterKnife.bind(this, v);
        setUpRecyclerView();
        initSearchField();
        return v;
    }


    private void initSearchField() {
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null) adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            setContacts(savedInstanceState.getParcelableArrayList(CONTACTS));
            setSelectedContacts(savedInstanceState.getParcelableArrayList(SELECTED_CONTACTS));
        } else {
            if (type != ContactManager.GROUPS) {
                EventBus.getDefault().post(new GetContactsEvent(type));
            }
        }
    }


    private void setUpRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);

        adapter = new ContactsRecyclerAdapter(this);
        recyclerView.setAdapter(adapter);

        if (screenType != ContactsFragment.TYPE_SELECTING_CONTACTS) {
            if (type == ContactManager.GROUPS) {
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
                itemTouchHelper.attachToRecyclerView(recyclerView);
            }
        }
    }


    private ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }


        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            positionToDelete = viewHolder.getAdapterPosition();

            if (direction == ItemTouchHelper.LEFT) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.delete_group_message);

                builder.setPositiveButton("REMOVE", (dialog, which) -> {
                    showProgressDialog();
                    contactToDelete = ((ContactHolder) viewHolder).getContact();
                    RESTClient.getInstance().deleteGroup(ContactManager.token,
                            contactToDelete.getUid(), deleteGroupCallback);
                }).setNegativeButton("CANCEL", (dialog, which) -> {
                    restoreDeletedItem();
                });

                AlertDialog dialog = builder.create();
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }
    };


    private void restoreDeletedItem() {
        adapter.notifyItemRemoved(positionToDelete + 1);
        adapter.notifyItemRangeChanged(positionToDelete, adapter.getItemCount());
        contactToDelete = null;
    }


    HandledCallback<LoginRequestOTP> deleteGroupCallback = new HandledCallback<LoginRequestOTP>() {
        @Override
        public void onSuccess(Call<LoginRequestOTP> call, Response<LoginRequestOTP> response) {
            adapter.removeItem(positionToDelete);
            contactToDelete = null;
            cancelProgressDialog();
        }


        @Override
        public void onFail(Call<LoginRequestOTP> call, Response<LoginRequestOTP> response) {
            restoreDeletedItem();
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(getActivity(), "Internal server error", TopPopUpManager.isError);
        }


        @Override
        public void onProcessFailed(Call<LoginRequestOTP> call, Throwable t, String error) {
            restoreDeletedItem();
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        fabHandler = null;
        selectorListener = null;
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        // do not override!!!!
    }


    @Override
    public void setTabData(TabData tabData) {
        this.tabData = tabData;
    }


    @Override
    public TabData getTabData() {
        return tabData;
    }


    public void setContacts(ArrayList<Contact> contacts) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (adapter != null) adapter.setItems(contacts);
            });
        }
    }


    public void setSelectedContacts(ArrayList<Contact> contacts) {
        if (contacts != null && !contacts.isEmpty()) {
            if (adapter != null) {
                adapter.setSelectedContacts(contacts);
                adapter.setSelectionActivated(true);
            }
            if (screenType == ContactsFragment.TYPE_DEFAULT_CONTACTS) {
                if (fabHandler != null) fabHandler.showFab();
            }
        }
    }


    @Override
    public void onItemClick(int position, Contact contact) {
        if (type == ContactManager.GROUPS) {
            if (!showFragment(GroupMembersListFragment.newInstance(contact))) {
                if (adapter != null && !adapter.isSelectionActivated()) {
                    adapter.setSelectionActivated(true);
                }
                if (selectorListener != null) {
                    selectorListener.onItemSelected(contact, position,
                            adapter.setSelected(getViewHolderByPosition(position), contact));
                }
            }
        } else {

            if (adapter != null && !adapter.isSelectionActivated()) {
                adapter.setSelectionActivated(true);
                if (screenType == ContactsFragment.TYPE_DEFAULT_CONTACTS) {
                    if (fabHandler != null) fabHandler.showFab();
                }
            }

            if (selectorListener != null) {
                selectorListener.onItemSelected(contact, position,
                        adapter.setSelected(getViewHolderByPosition(position), contact));
            }

            if (adapter != null && adapter.isSelectionActivated()) {
                if (adapter.getSelectedContacts().isEmpty()) {
                    adapter.setSelectionActivated(false);
                    if (fabHandler != null) fabHandler.hideFab();
                }
            }
        }
    }


    private ContactHolder getViewHolderByPosition(int position) {
        if (recyclerView != null) {
            return ((ContactHolder) recyclerView.findViewHolderForAdapterPosition(position));
        } else return null;
    }


    public boolean backPressed() {
        if (adapter != null && adapter.isSelectionActivated()) {
            adapter.setSelectionActivated(false);
            if (fabHandler != null) fabHandler.hideFab();
            return true;
        } else {
            return false;
        }
    }


    public boolean isSelectionActivated() {
        return adapter != null && adapter.isSelectionActivated();
    }


    private void clearFilter() {
        if (searchInput != null) searchInput.setText("");
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        clearFilter();

        outState.putParcelable(TAB_DATA, tabData);
        outState.putInt(TYPE, type);
        outState.putInt(SCREEN_TYPE, screenType);
        outState.putParcelableArrayList(CONTACTS, adapter.getContacts());
        outState.putParcelableArrayList(SELECTED_CONTACTS, adapter.getSelectedContacts());
        putSelectedContacts();
        super.onSaveInstanceState(outState);
    }

    private void putSelectedContacts() {
        if (type != ContactManager.GROUPS) {
            Bundle bundle = new Bundle();
            ArrayList<Contact> contacts = new ArrayList<>();
            contacts.addAll(adapter.getSelectedContacts());
            bundle.putParcelableArrayList(SELECTED_CONTACTS + type, contacts);
            Navigator.with(getActivity()).putData(SELECTED_CONTACTS + type, bundle);
        }
    }
}

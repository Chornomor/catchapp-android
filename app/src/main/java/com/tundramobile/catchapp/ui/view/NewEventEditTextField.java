package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alexchern on 24.04.17.
 */

public class NewEventEditTextField extends LinearLayout {

//    private static final String EDIT_TEXT = "EDIT_TEXT";
//    private static final String TITLE_COLOR = "TITLE_COLOR";
//
////    @BindView(R.id.tv_new_event_1) TextView textView1;
////    @BindView(R.id.tv_new_event_2) TextView textView2;
////    @BindView(R.id.et_new_event) EditText editText;
//
//    private View root;
//
//
    public NewEventEditTextField(Context context) {
        super(context);
    }
//
//
//    public NewEventEditTextField(Context context, @Nullable AttributeSet attrs) {
//        super(context, attrs);
//        initRoot(context, attrs);
//    }
//
//
//    public NewEventEditTextField(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        initRoot(context, attrs);
//    }
//
//
//    private void initRoot(Context context, AttributeSet attrs) {
//        root = LayoutInflater.from(getContext()).inflate(R.layout.new_event_message_field, null);
//        ButterKnife.bind(this, root);
//        initViews(context, attrs);
//        removeAllViews();
//        addView(root, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//    }
//
//
//    public String getText() {
//        return editText.getText().toString();
//    }
//
//
//    private void initViews(Context context, AttributeSet attrs) {
//        if (null == attrs) {
//            throw new IllegalArgumentException("Attributes should be provided to this view");
//        }
//        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NewEventEditTextField);
//        textView1.setText(typedArray.getString(R.styleable.NewEventEditTextField_newEventFieldTitle));
//        String subTitle = typedArray.getString(R.styleable.NewEventEditTextField_newEventFieldSubTitle);
//        textView2.setVisibility(subTitle == null ? View.GONE : View.VISIBLE);
//        textView2.setText(subTitle);
//        editText.setHint(typedArray.getString(R.styleable.NewEventEditTextField_newEventFieldHint));
//        editText.setOnFocusChangeListener((view, b) -> {
//            setTitleColor(b);
//        });
//
//        editText.setOnEditorActionListener((textView, i, keyEvent) -> {
//            if (i == EditorInfo.IME_ACTION_DONE) {
//                hideKeyboard();
//                editText.clearFocus();
//            }
//            return false;
//        });
//    }
//
//
//    public void setTitleColor(boolean hasFocus) {
//        int titleColor = 0;
//        if (hasFocus) {
//            titleColor = getContext().getResources().getColor(R.color.purple);
//        } else {
//            if (editText.getText().toString().isEmpty()) {
//                titleColor = getContext().getResources().getColor(R.color.title_black);
//            } else {
//                titleColor = getContext().getResources().getColor(R.color.common_text_color);
//            }
//        }
//        textView1.setTextColor(titleColor);
//        textView2.setTextColor(titleColor);
//    }
//
//
//    public void setText(String text) {
//        editText.setText(text);
//    }
//
//
//    private void hideKeyboard() {
//        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(root.getWindowToken(), 0);
//    }
//
//
//    public void addTextChangedListener(TextWatcher textWatcher) {
//        editText.addTextChangedListener(textWatcher);
//
//    }
//
//    public void removeTextChangeListener(TextWatcher textWatcher){
//        editText.removeTextChangedListener(textWatcher);
//    }

}

package com.tundramobile.catchapp.ui.fragment;

import android.support.v4.app.Fragment;

import com.tundramobile.catchapp.ui.activity.BaseActivity;
import com.tundramobile.catchapp.interfaces.FabHandler;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.activity.StackActivity;
import com.tundramobile.catchapp.ui.view.AppToolbar;

public class BaseFragment extends Fragment {

    protected static final String TYPE = "TYPE";
    protected static final String TAB_DATA = "TAB_DATA";

    boolean forceUpdate = false;

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            if (getActivity() instanceof MainActivity) {
                onToolbarChanged(((MainActivity) getActivity()).getToolbar());
            }
            if (getActivity() instanceof FabHandler) {
                ((FabHandler) getActivity()).hideFabIfNotRequired();
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        cancelProgressDialog();
    }


    protected void onToolbarChanged(AppToolbar toolbar) {
        if (toolbar != null) {
            toolbar.switchToDefaultMode();
        }
    }


    protected boolean showFragment(Fragment fragment) {
        if (getContext() instanceof StackActivity) {
            ((StackActivity) getContext()).showFragment(fragment);
            return true;
        }
        return false;
    }

    protected boolean showFragment(Fragment fragment,boolean addToBackStack) {
        if (getContext() instanceof StackActivity) {
            ((StackActivity) getContext()).showFragment(fragment,addToBackStack);
            return true;
        }
        return false;
    }
    protected void addFragment(Fragment fragment, int container) {
        if (getContext() instanceof BaseActivity) {
            ((BaseActivity) getContext()).addFragment(container, fragment, null);
        }
    }


    protected void showProgressDialog() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (getActivity() != null && getActivity() instanceof ProgressDialogHandler) {
                    ((ProgressDialogHandler) getActivity()).showProgressDialog();
                }
            });
        }
    }


    protected void cancelProgressDialog() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (getActivity() != null && getActivity() instanceof ProgressDialogHandler) {
                    ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
                }
            });
        }
    }


    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    public void forceUpdate(){
        forceUpdate = true;
    }
}

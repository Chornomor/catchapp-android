package com.tundramobile.catchapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.filter.ContactsFilter;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.ui.adapter.holder.ContactHolder;

import java.util.ArrayList;
import java.util.List;

public class ContactsRecyclerAdapter extends RecyclerView.Adapter<ContactHolder>
        implements Filterable {

    private RecyclerItemClickListener<Contact> listener;
    private ArrayList<Contact> items;
    private ContactsFilter filter;

    private boolean isSelectionActivated;
    private ArrayList<Contact> selected;


    public ContactsRecyclerAdapter(@NonNull RecyclerItemClickListener<Contact> listener) {
        this.listener = listener;
        selected = new ArrayList<>();
    }


    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.holder_contact, parent, false), listener);
    }


    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        Contact contact = items.get(position);
        holder.bindContent(contact);
        holder.setSelected(selected != null && selected.contains(contact));
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addItems(List<Contact> items) {
        if (items != null && !items.isEmpty()) {
            int idleCount = getItemCount();
            this.items.addAll(items);
            notifyItemRangeInserted(idleCount, getItemCount());
        }
    }


    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }


    public void setItems(ArrayList<Contact> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return items == null || items.isEmpty() ? 0 : items.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ContactsFilter(this, items);
        }
        if (filter.isItemsEmpty()) filter.setItems(items);
        return filter;
    }


    public boolean setSelected(@Nullable ContactHolder holder, Contact contact) {
        if (selected.contains(contact)) {
            selected.remove(contact);
            if (holder != null) {
                holder.setSelected(false);
            }
            return false;
        } else {
            selected.add(contact);
            if (holder != null) {
                holder.setSelected(true);
            }
            return true;
        }
    }


    public boolean isSelectionActivated() {
        return isSelectionActivated;
    }


    public void setSelectionActivated(boolean isActivated) {
        isSelectionActivated = isActivated;
        if (!isActivated) {
            selected.clear();
            notifyDataSetChanged();
        }
    }


    public void setSelectedContacts(ArrayList<Contact> selected) {
        this.selected = selected;
    }


    public ArrayList<Contact> getSelectedContacts() {
        return selected;
    }


    public ArrayList<Contact> getContacts() {
        return items;
    }
}

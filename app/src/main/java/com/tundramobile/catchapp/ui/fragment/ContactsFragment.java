package com.tundramobile.catchapp.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.FabOption;
import com.tundramobile.catchapp.entity.request.CreateGroupBody;
import com.tundramobile.catchapp.event.GetContactsEvent;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.PagerManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.activity.CreateGroupActivity;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.ContactsTabLayout;
import com.tundramobile.catchapp.ui.view.NonSwipeViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;

public class ContactsFragment extends BaseFabFragment implements ContactManager.OnObtainContactListener,
        TabLayout.OnTabSelectedListener {

    public static final String GROUP = "GROUP";

    public static final int TYPE_DEFAULT_CONTACTS = 0;
    public static final int TYPE_SELECTING_CONTACTS = 1;
    public static final int TYPE_CREATE_GROUP_CONTACTS = 2;

    @BindView(R.id.contactsTabLayout) ContactsTabLayout contactsTabLayout;
    @BindView(R.id.viewPager) NonSwipeViewPager viewPager;
    @BindView(R.id.addContactsBtn) TextView addContactsBtn;

    private Unbinder unbinder;
    private ContactManager contactManager;
    private TabsPagerAdapter adapter;
    private AppToolbar toolbar;
    private int type;
    private Contact group;
    private Set<Integer> contactIdList = new HashSet<>();


    public ContactsFragment() {
    }


    public static ContactsFragment newInstanceSelecting(@NonNull Contact group) {
        Bundle args = new Bundle();
        args.putInt(TYPE, TYPE_SELECTING_CONTACTS);
        args.putParcelable(GROUP, group);
        return newInstance(args);
    }


    public static ContactsFragment newInstanceCreateGroup() {
        Bundle args = new Bundle();
        args.putInt(TYPE, TYPE_CREATE_GROUP_CONTACTS);
        return newInstance(args);
    }


    public static ContactsFragment newInstance(@Nullable Bundle args) {
        ContactsFragment fragment = new ContactsFragment();
        if (args == null) {
            args = new Bundle();
            args.putInt(TYPE, TYPE_DEFAULT_CONTACTS);
        }
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactManager = ContactManager.getInstance(getContext());

        if (savedInstanceState != null) {
            type = savedInstanceState.getInt(TYPE);
            group = savedInstanceState.getParcelable(GROUP);
        } else if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
            group = getArguments().getParcelable(GROUP);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contacts, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null && (adapter.getCount() - 1) == ContactManager.GROUPS) {
            onGetContacts(new GetContactsEvent(ContactManager.GROUPS));
        }
    }


    private void initLayout() {
        adapter = PagerManager.getContactsPagerAdapter(getChildFragmentManager(), type);
        viewPager.setAdapter(adapter);
        contactsTabLayout.setupWithViewPager(viewPager);
        contactsTabLayout.addOnTabSelectedListener(this);
    }


    @Override
    protected ArrayList<FabOption> getFabOptions() {
        ArrayList<FabOption> fabOptions = new ArrayList<>(2);
        fabOptions.add(new FabOption(R.string.create_invite, R.drawable.ic_calendar));
        fabOptions.add(new FabOption(R.string.new_group, R.drawable.ic_groups));
        fabOptions.add(new FabOption(R.string.create_chat, R.drawable.ic_chat));
        return fabOptions;
    }


    @Override
    protected int getFabIcon() {
        return R.drawable.arrow_right;
    }


    @Override
    protected boolean isBlurRequired() {
        return true;
    }


    @Override
    protected boolean showFabAtStart() {
        return false;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        FastPermission.request(this, PermissionEnum.READ_CONTACTS.ordinal(), null,
                PermissionEnum.READ_CONTACTS);
        if (viewPager.getCurrentItem() == ContactManager.GROUPS) {
            hideFab();
        }
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onGetContacts(GetContactsEvent event) {
        switch (event.getType()) {
            case ContactManager.CONTACTS:
                if (FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS)) {
                    showProgressDialog();
                    contactManager.getPhoneContacts(getContext(),
                            type != TYPE_DEFAULT_CONTACTS, this);
                }
                break;
            case ContactManager.RECENT:
                contactManager.getRecent(type != TYPE_DEFAULT_CONTACTS, this);
                break;
            case ContactManager.GROUPS:
                contactManager.getGroups(this);
                break;
        }
    }


    @Override
    public void onObtainContacts(List<Contact> contacts, String error) {
        if (error == null) {
            setContacts(ContactManager.CONTACTS, (ArrayList<Contact>) contacts);
        } else {
            TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
        }
        // TODO: 12.04.17 Need refactoring!!! Hide progress bar when all 3 callbacks called
        cancelProgressDialog();
    }


    @Override
    public void onObtainRecentContacts(List<Contact> recentContactList) {
        if (!recentContactList.isEmpty()) {
            setContacts(ContactManager.RECENT, (ArrayList<Contact>) recentContactList);
        }
//        ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
    }


    @Override
    public void onObtainContactGroups(List<Contact> groupList) {
        if (!groupList.isEmpty()) {
            setContacts(ContactManager.GROUPS, (ArrayList<Contact>) groupList);
        }
//        ((ProgressDialogHandler) getActivity()).cancelProgressDialog();

    }


    private void setContacts(int type, ArrayList<Contact> contacts) {
        if (viewPager != null) {
            if (this.type == TYPE_SELECTING_CONTACTS) {
                ArrayList<Contact> toRemove = findEqualContacts(contacts);
                contacts.removeAll(toRemove);
            }

            try {
                ContactListFragment f = ((ContactListFragment) adapter.getActiveFragment(viewPager, type));
                if (f == null) {
                    f = (ContactListFragment) adapter.getItem(type);
                }
                if (f != null) f.setContacts(contacts);
            } catch (IndexOutOfBoundsException ignored) {
            }
        }
    }


    private ArrayList<Contact> findEqualContacts(ArrayList<Contact> contacts) {
        ArrayList<Contact> toRemove = new ArrayList<>();
        for (Contact existed : contacts) {
            if (group.getMembers().contains(existed)) {
                toRemove.add(existed);
            } else {
                for (Contact c : group.getMembers()) {
                    String mobile = existed.getMobile();
                    String name = c.getName();
                    if (mobile != null && name != null && mobile.equals(name)) {
                        toRemove.add(existed);
                    }
                }
            }
        }
        return toRemove;
    }


    public boolean backPressed() {
        if (contactIdList.isEmpty()) {
            return viewPager != null &&
                    ((ContactListFragment) adapter.getActiveFragment(viewPager, viewPager.getCurrentItem()))
                            .backPressed();
        } else {
            if (viewPager != null) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    ((ContactListFragment) adapter.getActiveFragment(viewPager, i)).backPressed();
                }
            }
            contactIdList.clear();
            updateAddButton();
            return true;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PermissionEnum.READ_CONTACTS.ordinal() && resultCode == Activity.RESULT_OK) {
            if (FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS)) {
                onGetContacts(new GetContactsEvent(ContactManager.CONTACTS));
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Utils.closeKeyboard(getActivity());
        setToolbarMode(tab.getPosition());

        switch (tab.getPosition()) {
            case ContactManager.CONTACTS:
            case ContactManager.RECENT:
                if (type == TYPE_DEFAULT_CONTACTS) {
                    if (isSelectionActivated(tab.getPosition())) {
                        showFab();
                    }
                }
                break;
            case ContactManager.GROUPS:
                hideFab();
                break;
        }
    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }


    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if (toolbar == null) return;
        this.toolbar = toolbar;
        setToolbarMode(viewPager.getCurrentItem());
    }


    private void setToolbarMode(int position) {
        switch (position) {
            case ContactManager.CONTACTS:
            case ContactManager.RECENT:
                if (toolbar != null) {
                    if (type == TYPE_SELECTING_CONTACTS) {
                        toolbar.switchToContactsSelectingMode(v -> getActivity().onBackPressed());
                    } else {
                        toolbar.switchToContactsMode();
                    }
                }
                break;
            case ContactManager.GROUPS:
                if (toolbar != null) toolbar.switchToGroupsMode(v ->
                        startActivity(CreateGroupActivity.createIntent(getContext())));
                break;
        }
    }


    private boolean isSelectionActivated(int position) {
        return viewPager != null &&
                (((ContactListFragment) adapter.getActiveFragment(viewPager, position))
                        .isSelectionActivated());
    }


    public boolean isSelectingContacts() {
        return type == TYPE_SELECTING_CONTACTS;
    }


    public void onItemClicked(Contact contact) {
        updateContactIdList(contact);
        updateAddButton();
    }


    private void updateContactIdList(Contact contact) {
        if (!contactIdList.contains(contact.getUid())) {
            contactIdList.add(contact.getUid());
        } else {
            contactIdList.remove(contact.getUid());
        }
    }


    private void updateAddButton() {
        if (addContactsBtn.getVisibility() != View.VISIBLE) {
            addContactsBtn.setVisibility(View.VISIBLE);
        }
        if (contactIdList.size() < 1) {
            addContactsBtn.setVisibility(View.INVISIBLE);
        }
        addContactsBtn.setText(getResources().getString(R.string.add_contact_add) + " " + contactIdList.size());
    }


    @OnClick(R.id.addContactsBtn)
    public void onAddContactsClicked() {

        CreateGroupBody body = new CreateGroupBody();
        body.setName(group.getName());
        body.setMembers(getNewContactsIds());

        ((ProgressDialogHandler) getContext()).showProgressDialog();
        ContactManager.getInstance(getContext()).updateGroup(body, group.getUid(), (group1, error) -> {
            if (getContext() == null) return;
            ((ProgressDialogHandler) getContext()).showProgressDialog();
            if (error != null) {
                TopPopUpManager.showTopPopUp(getActivity(), error, TopPopUpManager.isError);
            } else {
                if (getActivity() != null) {
                    // cancel selected items
                    getActivity().onBackPressed();
                    //get back to previous screen
                    getActivity().onBackPressed();
                }
            }
        });
    }


    private ArrayList<Integer> getNewContactsIds() {
        ArrayList<Integer> newContacts = new ArrayList<>();
        for (Contact contact : group.getMembers()) {
            newContacts.add(contact.getUid());
        }
        newContacts.addAll(contactIdList);
        return newContacts;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(TYPE, type);
        outState.putParcelable(GROUP, group);
        super.onSaveInstanceState(outState);
    }
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.view.View;
import android.widget.Button;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.MyUserManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventActionButtonHolder extends BaseEventHolder {

    @BindView(R.id.actionBtn) Button actionBtn;

    private boolean isDeletable;


    public EventActionButtonHolder(View itemView, EventDetailsClickListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
    }


    @Override
    public void bindContent(CatchUp catchUp, int type) {
        super.bindContent(catchUp, type);

        isDeletable = type != CatchUpManager.CONFIRMED;
        actionBtn.setText(isDeletable ? R.string.delete_event : R.string.cancel_event);
    }


    @OnClick(R.id.actionBtn)
    public void onActionClick() {
        if (listener != null) listener.onActionButtonClick(isDeletable);
    }
}

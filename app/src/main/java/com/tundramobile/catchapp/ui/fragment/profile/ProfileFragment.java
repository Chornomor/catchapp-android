package com.tundramobile.catchapp.ui.fragment.profile;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blaze.fastpermission.FastPermission;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.response.UpdateUserResponse;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.SharedPrefManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.firebase.FirebaseUtils;
import com.tundramobile.catchapp.other.GetPhotoUtils;
import com.tundramobile.catchapp.ui.activity.ProfileActivity;
import com.tundramobile.catchapp.ui.activity.authorization.NumberActivity;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by eugenetroyanskii on 21.04.17.
 */

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.user_photo) ImageView userPhoto;
    @BindView(R.id.profile_edit) TextView editAvatarBtn;
    @BindView(R.id.profile_name) TextView profileName;
    @BindView(R.id.add_photo_btn) TextView addPhoto;
    @BindView(R.id.photo_picker) LinearLayout photoPicker;

    private Unbinder unbinder;

    private User currentUser;
    private MyUserManager userManager;
    private GetPhotoUtils mGetPhotoUtil;


    public ProfileFragment() {
    }


    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        userManager = MyUserManager.getInstance();
        currentUser = userManager.getUser(getActivity());
        initLayout(currentUser);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void setPhotoPicker(GetPhotoUtils getPhotoUtil) {
        mGetPhotoUtil = getPhotoUtil;
    }


    public void initLayout(User currentUser) {
        profileName.setText(currentUser.getFirstName() + " " + currentUser.getLastName());
        if (currentUser.getProfilePic() != null && !currentUser.getProfilePic().isEmpty()) {
            AvatarTarget target = new AvatarTarget(userPhoto);
            Glide.with(getActivity()).load(currentUser.getProfilePic()).asBitmap().into(target);
        } else {
            userPhoto.setImageDrawable(getResources().getDrawable(R.drawable.profile_photo_plaeholder));
            hideEdit();
        }
    }


    private void showEdit() {
        editAvatarBtn.setVisibility(View.VISIBLE);
        addPhoto.setVisibility(View.GONE);
    }


    private void hideEdit() {
        if(editAvatarBtn != null && addPhoto != null) {
            editAvatarBtn.setVisibility(View.GONE);
            addPhoto.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.profile_close_btn)
    public void onCloseBtn() {
        getActivity().onBackPressed();
    }


    @OnClick(R.id.profile_account_details)
    public void onAccountDetails() {
        ((ProfileActivity) getActivity()).setFragment(AccountDetailsFragment.newInstance(currentUser.getHomeEmail(), currentUser.getMobile()));
    }


    @OnClick(R.id.profile_manage_calendar)
    public void onManageCalendar() {
        ((ProfileActivity) getActivity()).setFragment(ManageCalendarsFragment.newInstance());
    }


    @OnClick(R.id.profile_manage_permission)
    public void onManagePermissions() {
        ((ProfileActivity) getActivity()).setFragment(ManagePermissionsFragment.newInstance());
    }


    @OnClick(R.id.profile_calendar_settings)
    public void onCalendarSettings() {
        ((ProfileActivity) getActivity()).setFragment(CalendarSettingsFragment.newInstance());
    }


    @OnClick(R.id.profile_email_notification)
    public void onEmailNotification() {
        ((ProfileActivity) getActivity()).setFragment(NotificationSettingsFragment.newInstance());
    }


    @OnClick(R.id.profile_terms)
    public void onTermsOfService() {
    }


    @OnClick(R.id.profile_privacy)
    public void onPrivacy() {
    }


    @OnClick(R.id.add_photo_btn)
    public void onAddPhoto() {
        photoPicker.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.profile_edit)
    public void onEditPhoto() {
        photoPicker.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.library_photo)
    public void libraryPhoto() {
        if (!FastPermission.isGranted(getActivity(), PermissionEnum.READ_EXTERNAL_STORAGE)) {
            FastPermission.request(this, GetPhotoUtils.REQ_GALLERY, galleryPermissionListener, PermissionEnum.READ_EXTERNAL_STORAGE);
        } else {
            mGetPhotoUtil.takeFromGallery();
            cancelPhoto();
        }
    }


    @OnClick(R.id.camera_photo)
    public void cameraPhoto() {
        if (!FastPermission.isGranted(getActivity(), PermissionEnum.CAMERA) || !FastPermission.isGranted(getActivity(), PermissionEnum.READ_EXTERNAL_STORAGE)) {
            FastPermission.request(this, GetPhotoUtils.REQ_CAMERA, cameraPermissionListener, PermissionEnum.CAMERA, PermissionEnum.READ_EXTERNAL_STORAGE);
        } else {
            mGetPhotoUtil.sendPhotoIntent();
            cancelPhoto();
        }
    }


    @OnClick(R.id.cancel_photo)
    public void cancelPhoto() {
        photoPicker.setVisibility(View.GONE);
    }


    @OnClick(R.id.photo_picker_bg)
    public void onPhotoBg() {
        photoPicker.setVisibility(View.GONE);
    }


    @OnClick(R.id.profile_logout_button)
    public void onLogoutBtn() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm");
        builder.setMessage("Are you sure you want to log out?");
        builder.setPositiveButton(getResources().getString(R.string.profile_logout), (dialog, which) -> {
            showProgressDialog();
            logoutRequest();
        }).setNegativeButton(getResources().getString(R.string.profile_cancel), (dialog, which) -> {
            dialog.dismiss();
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void logoutRequest() {
        userManager.logout(userManager.getCurrentUserToken(getActivity()), new HandledCallback<UpdateUserResponse>() {
            @Override
            public void onSuccess(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
                FirebaseUtils.signOut();
                SharedPrefManager.getInstance(getActivity()).cleanStoredUserData();
                CatchUpManager.destroy();
                ContactManager.clear();
                cancelProgressDialog();
                Intent intent = new Intent(getActivity(), NumberActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }


            @Override
            public void onFail(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
                TopPopUpManager.showTopPopUp(getActivity(), "Woops something was wrong", !TopPopUpManager.isError);
                cancelProgressDialog();
            }


            @Override
            public void onProcessFailed(Call<UpdateUserResponse> call, Throwable t, String error) {
                cancelProgressDialog();
                TopPopUpManager.showTopPopUp(getActivity(), error, !TopPopUpManager.isError);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GetPhotoUtils.REQ_GALLERY:
                if (FastPermission.isGranted(getActivity(), PermissionEnum.READ_EXTERNAL_STORAGE)) {
                    mGetPhotoUtil.sendGallery();
                    cancelPhoto();
                }
                break;
            case GetPhotoUtils.REQ_CAMERA:
                if (FastPermission.isGranted(getActivity(), PermissionEnum.CAMERA) && FastPermission.isGranted(getActivity(), PermissionEnum.READ_EXTERNAL_STORAGE)) {
                    mGetPhotoUtil.sendPhotoIntent();
                    cancelPhoto();
                }
                break;
        }
    }


    private FastPermission.OnPermissionGrantedListener cameraPermissionListener = new FastPermission.OnPermissionGrantedListener() {
        @Override
        public void onPermissionGranted() {
            mGetPhotoUtil.sendPhotoIntent();
            cancelPhoto();
        }
    };


    private FastPermission.OnPermissionGrantedListener galleryPermissionListener = new FastPermission.OnPermissionGrantedListener() {
        @Override
        public void onPermissionGranted() {
            mGetPhotoUtil.takeFromGallery();
            cancelPhoto();
        }
    };


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mGetPhotoUtil != null) {
            mGetPhotoUtil.saveInstance(outState);
        }
    }


    private class AvatarTarget extends SimpleTarget<Bitmap> {
        ImageView tartget;


        private AvatarTarget(ImageView tartget) {
            this.tartget = tartget;
        }


        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            if(tartget != null && tartget.getContext() != null){
                tartget.setImageBitmap(resource);
                showEdit();
            }
        }


        @Override
        public void onLoadFailed(Exception e, Drawable errorDrawable) {
            super.onLoadFailed(e, errorDrawable);
            hideEdit();
        }
    }
}

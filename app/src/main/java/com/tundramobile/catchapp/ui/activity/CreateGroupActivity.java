package com.tundramobile.catchapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.FabOption;
import com.tundramobile.catchapp.interfaces.FabHandler;
import com.tundramobile.catchapp.interfaces.SelectorListener;
import com.tundramobile.catchapp.ui.fragment.CreateGroupFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eugenetroyanskii on 08.04.17.
 */

public class CreateGroupActivity extends BaseActivity implements SelectorListener, FabHandler {


    @BindView(R.id.toolbar) AppToolbar toolbar;


    public static Intent createIntent(Context context) {
        return new Intent(context, CreateGroupActivity.class);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
        toolbar.switchToContactsSelectingMode((v -> onBackPressed()));
        addFragment(R.id.fragmentContainer, CreateGroupFragment.newInstance(), null);
    }


    @Override
    public void onItemSelected(Object item, int position, boolean isAdding) {
        ((CreateGroupFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer)).
                onItemClicked((Contact) item, isAdding);
    }


    @Override
    public void showFab() {

    }


    @Override
    public void setupFabOptions(ArrayList<FabOption> fabOptions, @DrawableRes int fabIcon, boolean applyBlur) {

    }


    @Override
    public void hideFab() {

    }


    @Override
    public boolean isFabOpen() {
        return false;
    }


    @Override
    public void hideFabIfNotRequired() {

    }
}

package com.tundramobile.catchapp.ui.fragment.date_picker;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.fragment.calendar.CalendarFragment;

import org.w3c.dom.Text;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by andreybofanov on 24.04.17.
 */

public class DatePager extends LinearLayout implements ViewPager.OnPageChangeListener {
    @BindView(R.id.week_day_indicators)
    LinearLayout weekDayIndicators;
    @BindView(R.id.month_pager)
    ViewPager pager;
    DatePagerAdapter adapter;

    DateChangedListener dateChangedListener;
    DateFormatProvider dateFormatProvider;

    public DatePager(Context context) {
        super(context);
        init();
    }

    public DatePager(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DatePager(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DatePager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        setOrientation(VERTICAL);
        inflate(getContext(), R.layout.layout_calendar_picker,this);
        ButterKnife.bind(this);
        setupIndicators();
        setupPager();

    }



    void setupIndicators(){
        String[] days = getResources().getStringArray(R.array.days_short);
        LayoutParams params = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        weekDayIndicators.removeAllViews();

        boolean isSundayFirst = Calendar.getInstance().getFirstDayOfWeek() == Calendar.SUNDAY;
        for(int i = 0; i < days.length; i ++){
            String day = days[i];
            TextView textView = new TextView(getContext());
            textView.setText(day);
            textView.setGravity(Gravity.CENTER);
            if(isSundayFirst && i == days.length -1){
                weekDayIndicators.addView(textView,0,params);
            } else {
                weekDayIndicators.addView(textView, params);
            }
        }
    }

    void setupPager(){
        adapter = new DatePagerAdapter();
        pager.setAdapter(adapter);
        pager.setCurrentItem(adapter.getCount()/2);
        adapter.setInitialSelection(adapter.getCount()/2);
        pager.setOnPageChangeListener(this);
    }

    public void resetPosition(){
        pager.setCurrentItem(adapter.getCount()/2,true);
    }

    public void setDateChangedListener(DateChangedListener dateChangedListener) {
        this.dateChangedListener = dateChangedListener;
        if(adapter != null){
            adapter.setDateChangedListener(dateChangedListener);
        }
    }

    public void setDateFormatProvider(DateFormatProvider dateFormatProvider) {
        this.dateFormatProvider = dateFormatProvider;
        if(adapter != null){
            adapter.setDateFormatProvider(dateFormatProvider);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        ((MonthPage)findViewWithTag(position)).setSelectedDay(1);
        if(dateChangedListener != null && adapter != null){
            dateChangedListener.onMonthChanged(adapter.getYearForPosition(position),
                    adapter.getMonthForPosition(position));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.interfaces.BindingManager;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.ImageLoader;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.TimeUtils;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.CollageImageView;
import com.tundramobile.catchapp.ui.view.FontTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CatchUpViewHolder extends RecyclerView.ViewHolder implements BindingManager<CatchUp> {

    private static final String TAG = CatchUpViewHolder.class.getSimpleName();

    @BindView(R.id.item_event_user_avatar) CollageImageView avatar;
    @BindView(R.id.initials) TextView initials;
    @BindView(R.id.emptyGroupText) View emptyGroupText;
    @BindView(R.id.emptyGroupImage) CollageImageView emptyGroupImage;
    @BindView(R.id.item_event_user_name) FontTextView initiatorName;
    @BindView(R.id.item_event_goal) TextView catchUpName;
    @BindView(R.id.item_event_date) TextView startTime;
    @BindView(R.id.item_event_meeting_place) TextView location;
    @BindDimen(R.dimen.catchup_holder_avatar_size) int avatarSize;

    private CatchUp catchUp;
    Long uid;

    public CatchUpViewHolder(View itemView, @ColorInt int nameColor,
                             RecyclerItemClickListener<CatchUp> listener) {
        super(itemView);
        initView(itemView, nameColor, listener);
        uid =(long) MyUserManager.getCurrentUser(itemView.getContext()).getId();
    }


    private void initView(View v, int nameColor,
                          final RecyclerItemClickListener<CatchUp> listener) {
        ButterKnife.bind(this, v);
        initiatorName.setTextColor(ContextCompat.getColor(initiatorName.getContext(), nameColor));
        itemView.setOnClickListener(v1 -> {
            if (listener != null) listener.onItemClick(getAdapterPosition(), catchUp);
        });
    }


    @Override
    public void bindContent(CatchUp catchUp) {
        this.catchUp = catchUp;

        setAvatar();
        setName();
        setCatchUpName();
        setSlots();
        setLocation();
    }


    private void setAvatar() {
        avatar.setCollageImages(null);
        initials.setVisibility(View.INVISIBLE);
        emptyGroupText.setVisibility(View.INVISIBLE);

        if (catchUp.isGroup() && catchUp.getInvited() != null && !catchUp.getInvited().isEmpty()) {
            emptyGroupImage.setImageResource(R.drawable.rect_gray_common);
            emptyGroupText.setVisibility(View.VISIBLE);
            ImageLoader.newInstance(avatar, avatarSize).loadImages(catchUp.getInvited());
        } else {
            String url = getUser().getProfilePic();
            if (!TextUtils.isEmpty(url)) {
                ImageLoader.newInstance(avatar, avatarSize, false).loadImage(url);
            } else {
                avatar.setImageResource(R.drawable.rect_gray_common);
                initials.setText(Utils.getNameInitials(getUser().getFirstName(),
                        getUser().getLastName()));
                initials.setVisibility(View.VISIBLE);
            }
        }
    }

    User getUser(){
        if(catchUp.isMine(uid)){
            if(catchUp.getInvited() != null && !catchUp.getInvited().isEmpty()){
                return catchUp.getInvited().get(0);
            } else {
                return catchUp.getAttending().get(0);
            }
        } else {
            return catchUp.getUser();
        }
    }


    private void setName() {
        String name;
        List<User> otherUsers = getOtherUsers();
        if(catchUp.isMine(uid) || catchUp.isGroup()){
            if(otherUsers.size() > 1){
                name = initiatorName.getResources().getQuantityString(R.plurals.other,
                        otherUsers.size() -1, searchName(otherUsers),
                        otherUsers.size() -1 );
            } else {
                if(otherUsers == null || otherUsers.isEmpty()){
                    name = "";
                } else {
                    name = otherUsers.get(0).getFullName();
                }
            }
        } else {
            if(otherUsers.size() > 1){
                name = initiatorName.getResources().getQuantityString(R.plurals.other,
                        otherUsers.size() -1, catchUp.getInitiatorName(),
                        otherUsers.size() -1 );
            } else {
                name = catchUp.getInitiatorName();
            }
        }
        initiatorName.setText(name);
    }

    String searchName(List<User> list){
        for(User u : list){
            if(u.getFullName() != null && ! u.getFullName().isEmpty()){
                return u.getFullName();
            }
        }
        return "";
    }

    List<User> getOtherUsers(){
        List<User> other;
        if(catchUp.getTabType() == CatchUpManager.CONFIRMED){
            other = catchUp.getAttending();
        } else {
            other = catchUp.getInvited();
        }
        if(other == null || other.isEmpty()){
            return other;
        }
        User meInList = null;
        for(User u : other){
            if(u.getId() == uid){
                meInList = u;
                break;
            }
        }
        if(meInList != null){
            other.remove(meInList);
        }
        return other;

    }



    private void setCatchUpName() {
        catchUpName.setText(catchUp.getName());
    }


    private void setSlots() {
        int count = catchUp.getEvents().size();
        if (count > 0) {
            String timeSlots;
            if (count > 1) {
                timeSlots = startTime.getResources().getQuantityString(R.plurals.slots, count, count);
            } else {
                timeSlots = DateUtil.getTimeWithTimeFromTimestamp(catchUp.getEvents().get(0).getFrom());
            }
            startTime.setText(timeSlots);
            startTime.setVisibility(View.VISIBLE);
        } else {
            startTime.setVisibility(View.GONE);
        }
    }


    private void setLocation() {
        location.setText(catchUp.getLocationName());
    }
}

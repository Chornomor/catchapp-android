package com.tundramobile.catchapp.ui.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.tundramobile.catchapp.R;


/**
 * Created by eugenetroyanskii on 31.03.17.
 */

public class LoadingDialog {

    private ProgressDialog dialog;
    private Context mContext;


    public LoadingDialog(Context context) {
        mContext = context;
    }


    public void showProgressDialog() {
        if (dialog == null) {
            dialog = new ProgressDialog(mContext);
        }
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage(mContext.getResources().getString(R.string.loading_dialog_message));
        dialog.setIndeterminateDrawable(ContextCompat.getDrawable(mContext,
                R.drawable.progress_animation));
        dialog.show();
    }


    public void cancelProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog.cancel();
            dialog = null;
        }
    }
}
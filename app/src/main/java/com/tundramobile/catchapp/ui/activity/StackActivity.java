package com.tundramobile.catchapp.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;

import com.jetradar.multibackstack.BackStackActivity;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;
import com.tundramobile.catchapp.ui.fragment.calendar.CalendarFragment;
import com.tundramobile.catchapp.ui.fragment.ChatFragment;
import com.tundramobile.catchapp.ui.fragment.ContactsFragment;
import com.tundramobile.catchapp.ui.fragment.HomeFragment;
import com.tundramobile.catchapp.ui.fragment.NotificationsFragment;

public class StackActivity extends BackStackActivity {

    private static final String STATE_CURRENT_TAB_ID = "current_tab_id";
    protected static final int MAIN_TAB_ID = 0;

    private Fragment curFragment;
    private int curTabId;


    @NonNull
    protected Fragment rootTabFragment(int tabId, Bundle args) {
        switch (tabId) {
            case 0:
                return HomeFragment.newInstance(args);
            case 1:
                return CalendarFragment.newInstance(args);
            case 2:
                return ChatFragment.newInstance(args);
            case 3:
                return NotificationsFragment.newInstance(args);
            case 4:
                return ContactsFragment.newInstance(args);
            default:
                throw new IllegalArgumentException();
        }
    }


    @NonNull
    protected Fragment rootTabFragment(int tabId) {
        return rootTabFragment(tabId, null);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_TAB_ID, curTabId);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        curFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        curTabId = savedInstanceState.getInt(STATE_CURRENT_TAB_ID);
        selectTab(curTabId);
    }


    protected void selectTab(int position) {

    }


    @Override
    public void onBackPressed() {
        if (curFragment != null) {
            if (curFragment instanceof ContactsFragment) {
                if (((ContactsFragment) curFragment).backPressed())
                    return;
            }
        }

        Pair<Integer, Fragment> pair = popFragmentFromBackStack();
        if (pair != null) {
            backTo(pair.first, pair.second);
        } else {
            super.onBackPressed();
        }

    }


    public void showFragment(@NonNull Fragment fragment) {
        showFragment(fragment, true);
    }


    public void showFragment(@NonNull Fragment fragment, boolean addToBackStack) {
        if (curFragment != null && addToBackStack) {
            pushFragmentToBackStack(curTabId, curFragment);
        }
        replaceFragment(fragment);
    }


    private void backTo(int tabId, @NonNull Fragment fragment) {
        if (tabId != curTabId) {
            curTabId = tabId;
            selectTab(curTabId);
        }
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }


    protected void backToRoot() {
        if (isRootTabFragment(curFragment, curTabId)) {
            return;
        }
        resetBackStackToRoot(curTabId);
        Fragment rootFragment = popFragmentFromBackStack(curTabId);
        assert rootFragment != null;
        ((BaseFragment)rootFragment).forceUpdate();
        backTo(curTabId, rootFragment);
    }


    private boolean isRootTabFragment(@NonNull Fragment fragment, int tabId) {
        return fragment.getClass() == rootTabFragment(tabId).getClass();
    }


    private void replaceFragment(@NonNull Fragment fragment) {
        Navigator.with(this)
                .commitAllowingStateLoss()
                .replace(fragment);

        curFragment = fragment;
    }


    protected void tabSelected(int position) {
        if (curFragment != null) {
            pushFragmentToBackStack(curTabId, curFragment);
        }
        curTabId = position;
        Fragment fragment = popFragmentFromBackStack(curTabId);
        if (fragment == null) {
            fragment = rootTabFragment(curTabId);
        }
        replaceFragment(fragment);
    }


    protected Fragment getCurrentFragment() {
        return curFragment;
    }


    protected void onFragmentChanged(Fragment currentFragment) {

    }
}

package com.tundramobile.catchapp.ui.activity.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.request.RequestUpdateUser;
import com.tundramobile.catchapp.entity.response.UpdateUserResponse;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.ui.activity.BaseActivity;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.activity.PermissionsAccessActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class UserNameActivity extends BaseActivity {

    @BindView(R.id.user_first_name) EditText userFirstName;
    @BindView(R.id.user_last_name) EditText userLastName;

    private User currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_user_name);
        ButterKnife.bind(this);
        setupKeyboardDoneButton();
        currentUser = MyUserManager.getCurrentUser(this);
    }


    @OnClick(R.id.btn_back)
    public void OnBackClick() {
        finish();
    }


    private void setupKeyboardDoneButton() {
        userLastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    OnNextClick();
                }
                return false;
            }
        });
    }


    @OnClick(R.id.auth_fab_done)
    public void OnNextClick() {
        String firstName = userFirstName.getText().toString();
        String lastName = userLastName.getText().toString();
        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)) {
            showProgressDialog();
            currentUser.setFirstName(firstName);
            currentUser.setLastName(lastName);
            MyUserManager.getInstance().updateUser(new RequestUpdateUser(
                    MyUserManager.getInstance().getCurrentUserToken(UserNameActivity.this),
                    currentUser), updateUserCallback);
        } else {
            TopPopUpManager.showTopPopUp(this, getString(R.string.auth_4_fill_the_filds), !TopPopUpManager.isError);
        }
    }


    HandledCallback<UpdateUserResponse> updateUserCallback = new HandledCallback<UpdateUserResponse>() {
        @Override
        public void onSuccess(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            cancelProgressDialog();
            if (response.body().getSuccess() == (currentUser.getId())) {
                MyUserManager.setCurrentUser(UserNameActivity.this, currentUser);
                Intent intent;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    intent = new Intent(UserNameActivity.this, PermissionsAccessActivity.class);
                } else {
                    intent = new Intent(UserNameActivity.this, MainActivity.class);
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }


        @Override
        public void onFail(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            cancelProgressDialog();
        }


        @Override
        public void onProcessFailed(Call<UpdateUserResponse> call, Throwable t, String error) {
            cancelProgressDialog();
            TopPopUpManager.showTopPopUp(UserNameActivity.this, error, TopPopUpManager.isError);
        }
    };
}

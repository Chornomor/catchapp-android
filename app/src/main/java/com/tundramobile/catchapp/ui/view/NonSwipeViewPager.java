package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.tundramobile.catchapp.R;

public class NonSwipeViewPager extends ViewPager {

    private boolean isPagingEnabled = true;


    public NonSwipeViewPager(Context context) {
        super(context);
        init(context, null);
    }


    public NonSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.NonSwipeViewPager, 0, 0);
        try {
            setPagingEnabled(typedArray.getBoolean(R.styleable.NonSwipeViewPager_swipingEnabled,
                    true));
        } finally {
            typedArray.recycle();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }


    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }


    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        if(adapter != null)
            setOffscreenPageLimit(adapter.getCount());
    }
}

package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.manager.ImageLoader;
import com.tundramobile.catchapp.other.Utils;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AvatarView extends FrameLayout {

    @BindView(R.id.avatar) ImageView avatar;
    @BindView(R.id.initials) TextView initials;

    @BindDimen(R.dimen.event_preview_avatar_size) int avatarSize;

    private User user;


    public AvatarView(@NonNull Context context) {
        super(context);
        init(context);
    }


    public AvatarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public AvatarView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public AvatarView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.avatar_view, this, true);
        ButterKnife.bind(this);
    }


    public void setupWith(User user) {
        this.user = user;
        loadSingleAvatar();
    }


    private void loadSingleAvatar() {
        String url = user.getProfilePic();
        if (!TextUtils.isEmpty(url)) {
            ImageLoader.newInstance(avatar, avatarSize, false).loadImage(url);
        } else {
            avatar.setImageResource(R.drawable.rect_gray_common);
            initials.setText(Utils.getNameInitials(user.getFirstName(), user.getLastName()));
            initials.setVisibility(View.VISIBLE);
        }
    }
}

package com.tundramobile.catchapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.event.EventCreatedEvent;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.interfaces.SelectorListener;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.manager.PagerManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.ui.dialog.EventCreatedDialog;
import com.tundramobile.catchapp.ui.fragment.ChatFragment;
import com.tundramobile.catchapp.ui.fragment.ContactsFragment;
import com.tundramobile.catchapp.ui.fragment.GroupMembersListFragment;
import com.tundramobile.catchapp.ui.fragment.HomeFragment;
import com.tundramobile.catchapp.ui.fragment.SelectEventMembersFragment;
import com.tundramobile.catchapp.ui.fragment.WhatEventPagerFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.LoadingDialog;
import com.tundramobile.catchapp.ui.view.NavigationTabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.tundramobile.catchapp.ui.fragment.WhatEventPagerFragment.HAVE_CONTACTS;
import static com.tundramobile.catchapp.ui.fragment.WhatEventPagerFragment.HAVE_GROUP_CONTACTS;

public class MainActivity extends FabActivity implements NavigationTabLayout.TabSelectionListener,
        SelectorListener, ProgressDialogHandler {

    public static final String ARG_SELECTED_TAB_ID = "ARG_SELECTED_TAB_ID";
    public static final String ARG_BOTTOM_BAR_VISIBILITY = "ARG_BOTTOM_BAR_VISIBILITY";

    @BindView(R.id.navigationTabLayout) NavigationTabLayout bottomNavBar;
    @BindView(R.id.dividerBottom) View bottomDivider;
    @BindView(R.id.toolbar) AppToolbar toolbar;

    private static final String TAG = MainActivity.class.getSimpleName();

    private final boolean DEFAULT_BOTTOM_BAR_VISIBILITY = true;

    private boolean isBottomBarVisible = DEFAULT_BOTTOM_BAR_VISIBILITY;
    private int startTabId = MAIN_TAB_ID;
    private boolean isSetup = true;

    private Set<Integer> contactIdList = new HashSet<>();
    private Set<Contact> contactsList = new HashSet<>();

    private LoadingDialog loadingDialog;

    private GoogleApiClient googleApiClient;
    private EventCreatedDialog eventCreatedDialog;


    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupGoogleApiClient();
        retrieveIntentData();
        setupBottomBar();
        if (eventCreatedDialog == null) {
            eventCreatedDialog = new EventCreatedDialog(this);
        }

        if (savedInstanceState == null) {
            bottomNavBar.selectTab(startTabId);
            showFragment(rootTabFragment(startTabId, getIntent().getExtras()));
            bottomNavBar.setVisibility(isBottomBarVisible ? View.VISIBLE : View.GONE);
            bottomDivider.setVisibility(isBottomBarVisible ? View.VISIBLE : View.GONE);
        }
        checkDeepLink();
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    private void checkDeepLink() {
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        if (TextUtils.equals(action, Intent.ACTION_VIEW)) {
            openUrl(data);
        }
    }


    private void openUrl(Uri uri) {
        String promoId = uri.getQueryParameter("id");
        Log.e(TAG, uri.toString());
        Log.e(TAG, promoId);
    }


    private void setupGoogleApiClient() {
        // TODO: 24.04.17 Implement checking for google services
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, connectionResult -> {
                    // TODO: 19.04.17 Implement this
                })
                .build();
    }


    private void retrieveIntentData() {
        Bundle args = getIntent().getExtras();
        if (args != null) {
            startTabId = args.getInt(ARG_SELECTED_TAB_ID, MAIN_TAB_ID);
            isBottomBarVisible = args.getBoolean(ARG_BOTTOM_BAR_VISIBILITY, DEFAULT_BOTTOM_BAR_VISIBILITY);
        }
    }


    private void setupBottomBar() {
        bottomNavBar.setup(PagerManager.getHomePageTabData());
        bottomNavBar.setTabSelectionListener(this);
    }


    @Override
    public void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);
        }
        loadingDialog.showProgressDialog();
    }


    @Override
    public void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }


    @Override
    public void onBackPressed() {
        clearSelectedContactIds();
        super.onBackPressed();
    }


    @OnClick(R.id.profileBtn)
    void onProfileClick() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }


    @Override
    protected void selectTab(int position) {
        super.selectTab(position);
        bottomNavBar.highlightTab(position);
    }


    public AppToolbar getToolbar() {
        return toolbar;
    }


    @OnClick(R.id.mainFab)
    public void onFabClicked() {
        super.onFabClicked();
        if (getCurrentFragment() instanceof HomeFragment) {
            showFragment(WhatEventPagerFragment.newInstance());
        }
    }


    @OnClick({R.id.fabOption1, R.id.fabOption2, R.id.fabOption3})
    public void onFabOptionClicked(View v) {
        switch (v.getId()) {
            case R.id.fabOption1:
                if (getCurrentFragment() instanceof ContactsFragment) {
                    setIsHaveSelectedContacts();
                    animateFAB();
                    showFragment(WhatEventPagerFragment.newInstance());
                }
                break;
            case R.id.fabOption2:
                if (getCurrentFragment() instanceof ContactsFragment) {
                    openCreateGroupNameActivity(contactIdList);
                }
                if (getCurrentFragment() instanceof GroupMembersListFragment) {
                    setIsHaveGroupContacts();
                    animateFAB();
                    showFragment(WhatEventPagerFragment.newInstance());
                }
                break;
            case R.id.fabOption3:
                if (getCurrentFragment() instanceof ContactsFragment) {
                    ArrayList<Long> ids = new ArrayList<>();
                    for(Contact curContact : contactsList) {
                        if(curContact.getContactType() == Contact.REGISTERED_CONTACT_IN_SYSTEM) {
                            ids.add((long) curContact.getUid());
                        } else {
                            TopPopUpManager.showTopPopUp(this, getString(R.string.error_chat_creation_not_registered), true);
                            return;
                        }
                    }
                    long owner = (long) MyUserManager.getCurrentUser(this).getId();
                    ids.add(owner);
                    Collections.sort(ids, Long::compareTo);
                    animateFAB();

                    showFragment(ChatFragment.newInstance(owner, ids));
                }
                break;
        }
    }


    private void setIsHaveSelectedContacts() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(HAVE_CONTACTS, true);
        Navigator.with(this).putData(HAVE_CONTACTS, bundle);
    }


    private void setIsHaveGroupContacts() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(HAVE_GROUP_CONTACTS, true);
        Navigator.with(this).putData(HAVE_GROUP_CONTACTS, bundle);
    }


    private void openCreateGroupNameActivity(Set<Integer> contactIdList) {
        int[] idsArray = new int[contactIdList.size()];
        int counter = 0;
        for (Integer id : contactIdList) {
            idsArray[counter] = id;
            counter++;
        }
        Intent intent = new Intent(this, CreateGroupNameActivity.class);
        intent.putExtra(CreateGroupNameActivity.GROUP_CONTACT_IDS, idsArray);
        animateFAB();
        startActivity(intent);
    }


    private void clearSelectedContactIds() {
        if (!isFabOpen() && getCurrentFragment() instanceof ContactsFragment) {
            contactIdList.clear();
        }
    }


    @Override
    public void onItemSelected(Object item, int position, boolean isAdded) {
        if (getCurrentFragment() instanceof ContactsFragment) {
            if (((ContactsFragment) getCurrentFragment()).isSelectingContacts()) {
                ((ContactsFragment) getCurrentFragment()).onItemClicked((Contact) item);
            } else {
                Contact contact = (Contact) item;
                if (isAdded) {
                    contactIdList.add(contact.getUid());
                    contactsList.add(contact);
                } else {
                    contactIdList.remove(contact.getUid());
                    contactsList.remove(contact);
                }

            }
        }
        if (getCurrentFragment() instanceof SelectEventMembersFragment) {
            ((SelectEventMembersFragment) getCurrentFragment()).onItemClicked((Contact) item, position, isAdded);
        } else {
            // TODO: 10.04.17 handle items selection
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showEventDialogPopUp(EventCreatedEvent event) {
        eventCreatedDialog.setDialogContent(event.getTitle(), event.getMessage());
        eventCreatedDialog.show();
    }


    @Override
    public void onTabSelected(int position) {
        tabSelected(position);
    }


    @Override
    public void onTabReselected(int position) {
        if (isSetup) {
            isSetup = false;
            return;
        }
        backToRoot();
    }


    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }


    public void reselectCurrent() {
        bottomNavBar.selectTab(bottomNavBar.getSelectedTabPosition());
    }
}

package com.tundramobile.catchapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.CatchUpDetail;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.Notification;
import com.tundramobile.catchapp.interfaces.ListResultListener;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.NotificationsAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.viewmodel.NotificationItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;

public class NotificationsFragment extends BaseFragment {

    private static final String TAG = NotificationsFragment.class.getSimpleName();

    @BindView(R.id.list) RecyclerView list;
    @BindView(R.id.notifications_refresher)
    SwipeRefreshLayout refresher;

    private Unbinder unbinder;

    private NotificationsAdapter adapter;
    private List<NotificationItem> notifications;

    public NotificationsFragment() {
    }


    public static NotificationsFragment newInstance(Bundle args) {
        return new NotificationsFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new NotificationsAdapter(getContext(), new RecyclerItemClickListener<NotificationItem>() {
            @Override
            public void onItemClick(int position, NotificationItem model) {
                int type = model.getCatchupType();
                if(type == -1){
                    return;
                }
                ((MainActivity)getActivity()).showProgressDialog();
                    CatchUpManager.getInstance(getContext())
                            .getCatchUps(type, true, new OnGetCatchUp(model.catchUpId));
            }
        });
    }





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notifications, container, false);
        unbinder = ButterKnife.bind(this, v);
        refresher.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        return v;
    }

    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        if(toolbar == null)
            return;
        toolbar.switchToDefaultMode(getString(R.string.title_notification));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupList();
        updateNotifications();
    }

    private void setupList() {
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(adapter);
    }

    private void updateNotifications() {
        ((ProgressDialogHandler) getActivity()).showProgressDialog();
        RESTClient.getInstance().getNotifications(MyUserManager.getInstance().getCurrentUserToken(getContext()), new NotificationsRequestCallback());
    }
    private void refresh() {
        RESTClient.getInstance().getNotifications(MyUserManager.getInstance().getCurrentUserToken(getContext()), new NotificationsRequestCallback());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void createViewModels(List<Notification> result) {
        notifications = new ArrayList<>();
        for(int i = 0; result != null && i < result.size(); i++) {
            if(result.get(i).getCatchUp() != null) {
                notifications.add(new NotificationItem(result.get(i)));
                adapter.setItems(notifications);
            }
        }
    }

    private class NotificationsRequestCallback extends HandledCallback<ListResponseModel<Notification>> {
        @Override
        public void onSuccess(Call<ListResponseModel<Notification>> call, Response<ListResponseModel<Notification>> response) {
            if(refresher != null) {
                refresher.setRefreshing(false);
            }
            if(getActivity() != null) {
                ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
            }
            createViewModels(response.body().getResult());
        }

        @Override
        public void onFail(Call<ListResponseModel<Notification>> call, Response<ListResponseModel<Notification>> response) {
            if(refresher != null) {
                refresher.setRefreshing(false);
            }
            if(getActivity() != null) {
                ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
            }
        }

        @Override
        public void onProcessFailed(Call<ListResponseModel<Notification>> call, Throwable t, String error) {
            if(refresher != null) {
                refresher.setRefreshing(false);
            }
            if(getActivity() != null) {
                ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
            }
        }
    }

    private class OnGetCatchUp implements ListResultListener<CatchUp>{
        long id;

        public OnGetCatchUp(long id) {
            this.id = id;
        }

        @Override
        public void onResultReady(int type, boolean success, List<CatchUp> catchUps) {
            if(catchUps == null){
                if(getContext() != null) {
                    TopPopUpManager.showTopPopUp(getContext(),getString(R.string.notification_catchup_not_found),true);
                }
                ((MainActivity)getActivity()).cancelProgressDialog();
                return;
            }
            CatchUp catchUp = null;
            for(CatchUp cat : catchUps){
                if(cat.getId() == id){
                    catchUp = cat;
                    break;
                }
            }
            if(catchUp == null){
                if(type == CatchUpManager.INCOMING && getContext() != null){
                    CatchUpManager.getInstance(getContext())
                            .getCatchUps(CatchUpManager.CONFIRMED,true,new OnGetCatchUp(id));
                    return;
                }
                TopPopUpManager.showTopPopUp(getContext(),getString(R.string.notification_catchup_not_found),true);
                ((MainActivity)getActivity()).cancelProgressDialog();
                return;
            }
            ((MainActivity)getActivity()).cancelProgressDialog();
            EventFragment fragment = EventFragment.newInstance(catchUp,type);
            showFragment(fragment);
        }
    }


}

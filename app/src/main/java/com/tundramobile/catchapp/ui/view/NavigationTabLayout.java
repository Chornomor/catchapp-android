package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;

import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.other.Utils;

import java.util.List;

public class NavigationTabLayout extends TabLayout implements TabLayout.OnTabSelectedListener {

    private static final String TAG = NavigationTabLayout.class.getSimpleName();
    private List<TabData> tabData;
    private TabSelectionListener tabSelectionListener;
    private int highlightPosition = -1;


    public NavigationTabLayout(Context context) {
        super(context);
        init();
    }


    public NavigationTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public NavigationTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        addOnTabSelectedListener(this);
    }


    public void setup(List<TabData> tabData) {
        this.tabData = tabData;
        initTabs(tabData.size());
    }


    private void initTabs(int tabsCount) {
        for (int i = 0; i < tabsCount; i++) {
            TabLayout.Tab tab = newTab();
            tab.setCustomView(new NavTab(getContext(), tabData.get(i)));
            addTab(tab);
        }
    }


    private View setTabUI(boolean isActive, View v) {
        Utils.logd(TAG, "setTabUI: isActive = " + isActive + ", view = " + (v));
        if (v != null)
            ((NavTab) v).setSelected(isActive);
        return v;
    }


    @Override
    public void onTabSelected(Tab tab) {
        Utils.logd(TAG, "onTabSelected: " + tab.getPosition());
        for (int i = 0; i < getTabCount(); i++) {
            Tab t = getTabAt(i);
            if (t != null) setTabUI(tab.getPosition() == i, t.getCustomView());
        }

        if (highlightPosition == tab.getPosition()) {
            highlightPosition = -1;
        } else {
            if (tabSelectionListener != null) tabSelectionListener.onTabSelected(tab.getPosition());
        }
    }


    @Override
    public void onTabUnselected(Tab tab) {

    }


    @Override
    public void onTabReselected(Tab tab) {
        if (tabSelectionListener != null) tabSelectionListener.onTabReselected(tab.getPosition());
    }


    public void highlightTab(int position) {
        highlightPosition = position;
        selectTab(position);
    }


    public void selectTab(int position) {
        Tab t = getTabAt(position);
        if (t != null) t.select();
    }


    public void setTabSelectionListener(TabSelectionListener tabSelectionListener) {
        this.tabSelectionListener = tabSelectionListener;
    }


    public interface TabSelectionListener {
        void onTabSelected(int position);

        void onTabReselected(int position);
    }
}

package com.tundramobile.catchapp.ui.view.pinCode;

import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

/**
 * Created by alexchern on 03.04.17.
 */

public class PinNumberDeleteKeyListener implements View.OnKeyListener {


    private final EditText pinBox;


    public PinNumberDeleteKeyListener(EditText pinBox) {
        this.pinBox = pinBox;
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {

            if (event.getAction() != KeyEvent.ACTION_DOWN)
                return true;

            EditText pinbox = (EditText) v;

            Integer textlength = pinbox.getText().length();
            if (textlength < 1) {
                this.pinBox.requestFocus();
                this.pinBox.setText("");
            }
        }

        return false;
    }
}
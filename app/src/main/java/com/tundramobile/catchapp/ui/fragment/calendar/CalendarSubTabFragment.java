package com.tundramobile.catchapp.ui.fragment.calendar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.blaze.fastpermission.FastPermission;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.entity.TimeSlot;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.interfaces.TabDataI;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TimeSlotsManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.other.CalendarUtil;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.ScheduleAdapter;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;
import com.tundramobile.catchapp.ui.fragment.EventFragment;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import java.util.Date;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;

public class CalendarSubTabFragment extends BaseFragment implements TabDataI, RecyclerItemClickListener<CatchUp> {

    private static final String TAG = CalendarSubTabFragment.class.getSimpleName();
    private static final String TYPE = "TYPE";
    private static final String TAB_DATA = "TAB_DATA";

    public TabData tabData;
    private Unbinder unbinder;
    private int type;

    @BindView(R.id.calendar_recycler)
    RecyclerView recyclerView;

    ScheduleAdapter adapter;
    StickyRecyclerHeadersDecoration decorator;

    public CalendarSubTabFragment() {
    }


    public static CalendarSubTabFragment newConfirmedInstance(TabData tabData) {
        return newInstance(tabData, CatchUpManager.CONFIRMED);
    }


    public static CalendarSubTabFragment newIncomingInstance(TabData tabData) {
        return newInstance(tabData, CatchUpManager.INCOMING);
    }


    public static CalendarSubTabFragment newOutgoingInstance(TabData tabData) {
        return newInstance(tabData, CatchUpManager.OUTGOING);
    }


    private static CalendarSubTabFragment newInstance(TabData tabData, int type) {
        CalendarSubTabFragment fragment = new CalendarSubTabFragment();
        fragment.setTabData(tabData);
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putParcelable(TAB_DATA, tabData);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
            tabData = getArguments().getParcelable(TAB_DATA);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calendar_sub_tab, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();
        return v;
    }


    private void initLayout() {
        adapter = new ScheduleAdapter();
        adapter.setOnOpenItemClick(new ScheduleAdapter.OnOpenItemClick() {
            @Override
            public void openScheduleItem(ScheduleItem item) {
                if(item.isExternal()){
                    return;
                }
                showFragment(EventFragment.newInstance(item.getCatchUp(), item.getCatchUp().getTabType()));
            }
        });
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        decorator = new StickyRecyclerHeadersDecoration(adapter);
        recyclerView.addItemDecoration(decorator);
        recyclerView.setAdapter(adapter);
        adapter.setDecorator(decorator);
        adapter.setProgressDialogHandler((MainActivity)getActivity());
    }


    @Override
    public void onResume() {
        super.onResume();
        update();
    }
    public void update(){
        if(adapter == null){
            return;
        }
        ArrayList<TimeSlot> timeSlots;
        if(type == CatchUpManager.INCOMING){
            timeSlots = TimeSlotsManager.getInstance().getAllTimeSlots();
        } else {
            timeSlots = TimeSlotsManager.getInstance().getConfirmedTimeSlots();
        }
        ArrayList<ScheduleItem> scheduleItems = new ArrayList<>();
        long userId = MyUserManager.getCurrentUser(getContext()).getId();
        for(TimeSlot timeSlot : timeSlots){
            scheduleItems.add(new ScheduleItem(userId,getContext(),timeSlot));
        }
        if(FastPermission.isGranted(getContext(), PermissionEnum.READ_CALENDAR)) {
            scheduleItems.addAll(CalendarUtil.readCalendarEvent(getContext()));
        }
        adapter.setItems(scheduleItems);
        adapter.notifyDataSetChanged();
    }

    void scrollToDate(Date date){
        if(adapter == null) {
            return;
        }
        int position = adapter.getPositionForDate(date);
        if(position == -1){
            return;
        }
        recyclerView.scrollToPosition(position);
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void setTabData(TabData tabData) {
        this.tabData = tabData;
    }


    @Override
    public TabData getTabData() {
        return tabData;
    }


    @Override
    public void onItemClick(int position, CatchUp model) {
        Toast.makeText(getContext(),
                "position: " + position + ", " + model.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        // Note: Not required to change toolbar mode for calendar subtabs
    }

    public void setDate(Date date) {
        scrollToDate(date);
    }
}

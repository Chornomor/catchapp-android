package com.tundramobile.catchapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.firebase.model.Message;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.ui.adapter.holder.ChatMessageHolder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class ChatMessagesRecyclerAdapter extends RecyclerView.Adapter<ChatMessageHolder> {

    private LinkedList<Message> items = new LinkedList<>();

    private RecyclerItemClickListener<Message> onItemClickListener;

    private long ownerID;

    public ChatMessagesRecyclerAdapter(long ownerID) {
        this.ownerID = ownerID;
    }


    @Override
    public ChatMessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatMessageHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_message_item_layout, parent, false),
                parent.getContext(),
                ownerID);
    }

    @Override
    public void onBindViewHolder(ChatMessageHolder holder, int position) {
        holder.bindContent(items.get(position));
        holder.itemView.setOnClickListener(v -> {
            if(onItemClickListener != null) {
                onItemClickListener.onItemClick(holder.getAdapterPosition(), items.get(position));
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addItems(List<Message> items) {
        if (items != null && !items.isEmpty()) {
            int idleCount = getItemCount();
            this.items.addAll(items);
            notifyItemRangeInserted(idleCount, getItemCount());
        }
    }


    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void setItems(LinkedList<Message> items) {
        this.items = items;
        Collections.sort(this.items, (o1, o2) -> o1.getTimestamp().compareTo(o2.getTimestamp()));
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(RecyclerItemClickListener<Message> listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return items == null || items.isEmpty() ? 0 : items.size();
    }

    public void addItem(Message value) {
        this.items.add(value);
        notifyItemInserted(this.getItemCount() - 1);
    }


//    public ArrayList<ChatRoomViewModel> getItems() {
//        return (ArrayList<Message>) items;
//    }
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.TimeUtils;
import com.tundramobile.catchapp.ui.view.FontButton;
import com.tundramobile.catchapp.ui.view.FontTextView;
import com.tundramobile.catchapp.ui.viewmodel.PlaceDetailsItem;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventMainInfoHolder extends BaseEventHolder {

    @BindView(R.id.initiatorText) FontTextView initiatorText;
    @BindView(R.id.titleText) FontTextView titleText;
    @BindView(R.id.timeSlotsText) FontTextView timeSlotsText;
    @BindView(R.id.locationText) FontTextView locationText;
    @BindView(R.id.detailsText) FontTextView detailsText;
    @BindView(R.id.messageBtn) FontButton messageBtn;
    @BindView(R.id.shareBtn) FontButton shareBtn;

    @BindColor(R.color.red) int red;
    @BindColor(R.color.green) int green;
    @BindColor(R.color.blue) int blue;

    @BindString(R.string.your_group_event) String yourGroupEventText;
    @BindString(R.string.your_pending_1_1_invites) String pendingInvitesText;

    private boolean initiatedByMe;


    public EventMainInfoHolder(View itemView, EventDetailsClickListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
    }


    @OnClick(R.id.messageBtn)
    public void onMessageClick() {
        if (listener != null) listener.onMessageClick();
    }


    @OnClick(R.id.shareBtn)
    public void onShareClick() {
        if (listener != null) listener.onShareClick();
    }


    public void bindContent(CatchUp catchUp, int type, @Nullable PlaceDetailsItem item) {
        bindContent(catchUp, type);

        initiatedByMe = catchUp.isMine(MyUserManager.getCurrentUser(itemView.getContext()).getId());

        setupByType();
        setName();
        setCatchUpName();
        setSlots();
        setLocation(item);
        setDetailsText();

//        if (catchUp.isOneToOneSingleUser() || catchUp.isGroup()) {
//            shareBtn.setVisibility(View.VISIBLE);
//        }
    }


    private void setupByType() {
        switch (type) {
            case CatchUpManager.CONFIRMED:
                initiatorText.setTextColor(green);
                messageBtn.setBackgroundResource(R.drawable.green_btn_bg);
                break;
            case CatchUpManager.INCOMING:
                initiatorText.setTextColor(red);
                messageBtn.setBackgroundResource(R.drawable.red_btn_bg);
                break;
            case CatchUpManager.OUTGOING:
                initiatorText.setTextColor(blue);
                messageBtn.setBackgroundResource(R.drawable.blue_btn_bg);
                break;
        }
    }


    private void setName() {
        String name;
        if (catchUp.isOneToOne()) {
            if (catchUp.isOneToOneSingleUser()) {
                if (initiatedByMe) {
                    name = catchUp.getInvitedUser().getFullName();
                } else {
                    name = catchUp.getInitiatorName();
                }
            } else {
                if (catchUp.isMine(MyUserManager.getCurrentUser(itemView.getContext()).getId())) {
                    name = pendingInvitesText;
                } else {
                    name = initiatorText.getResources().getQuantityString(R.plurals.other,
                            catchUp.getInvited().size(), catchUp.getInitiatorName(),
                            catchUp.getInvited().size());
                }
            }
        } else {
            if (catchUp.isMine(MyUserManager.getCurrentUser(itemView.getContext()).getId())) {
                name = yourGroupEventText;
            } else {
                name = catchUp.getInitiatorName();
            }
        }
        initiatorText.setText(name);
    }


    private void setCatchUpName() {
        titleText.setText(catchUp.getName());
    }


    private void setSlots() {
        String timeSlots;
        if (catchUp.isOneToOne() && type != CatchUpManager.CONFIRMED) {
            int count = catchUp.getEvents().size();
            timeSlots = timeSlotsText.getResources().getQuantityString(R.plurals.time_available,
                    count, count);
        } else {
            timeSlots = DateUtil.getTimeWithTimeFromTimestamp(catchUp.getEvents().get(0).getFrom());
        }

        timeSlotsText.setText(timeSlots);
    }


    private void setLocation(@Nullable PlaceDetailsItem item) {
        if (item != null && !TextUtils.isEmpty(item.getName())) {
            String place = "@" + item.getName();
            locationText.setText(place);
            locationText.setVisibility(View.VISIBLE);
        } else {
            if (catchUp.getLocationName() != null && !TextUtils.isEmpty(catchUp.getLocationName())) {
                String place = "@" + catchUp.getLocationName();
                locationText.setText(place);
                locationText.setVisibility(View.VISIBLE);
            }
        }
    }


    private void setDetailsText() {
        String details = catchUp.getInitiatorMessage();
        if (TextUtils.isEmpty(details)) {
            detailsText.setVisibility(View.GONE);
        } else {
            detailsText.setText(details);
            detailsText.setVisibility(View.VISIBLE);
        }
    }
}

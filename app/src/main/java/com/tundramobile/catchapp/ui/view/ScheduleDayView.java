package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by andreybofanov on 11.04.17.
 */

public class ScheduleDayView extends RelativeLayout {

    public ScheduleDayView(Context context) {
        super(context);
        init();
    }

    public ScheduleDayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScheduleDayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ScheduleDayView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){

    }
}

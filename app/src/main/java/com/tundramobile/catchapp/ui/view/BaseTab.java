package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.tundramobile.catchapp.entity.TabData;

import butterknife.ButterKnife;

public abstract class BaseTab extends FrameLayout {

    private TabData mTabData;
    private boolean isSelected;


    public BaseTab(Context context) {
        super(context);
    }


    public BaseTab(Context context, TabData tabData) {
        super(context);
        setData(tabData);
        setContentView();
        setupViews(tabData, isSelected);
    }


    @LayoutRes
    protected abstract int getLayoutRes();


    private void setContentView() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(getLayoutRes(), this, true);
        ButterKnife.bind(this);
    }


    public void setData(TabData tabData) {
        this.mTabData = tabData;
    }


    public TabData getData(){
        return mTabData;
    }

    public void updateData(TabData tabData) {
        setData(tabData);
        setupViews(tabData, isSelected);
    }


    @Override
    public boolean isSelected() {
        return isSelected;
    }


    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        setupViews(mTabData, isSelected);
    }


    protected void setupViews(TabData tabData, boolean isSelected) {

    }

    protected void refreshViews(){
        setupViews(mTabData, isSelected);
    }
}

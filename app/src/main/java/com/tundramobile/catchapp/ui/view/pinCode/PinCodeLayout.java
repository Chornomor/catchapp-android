package com.tundramobile.catchapp.ui.view.pinCode;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.ui.activity.authorization.PinCodeActivity;
import com.tundramobile.catchapp.ui.view.FontEditText;

import java.util.ArrayList;

/**
 * Created by alexchern on 03.04.17.
 */

public class PinCodeLayout extends LinearLayout {

    private final static int DEFAULT_NUMBER_COUNT = 4;
    ArrayList<FontEditText> editTextList = new ArrayList<>();
    private boolean isTextChanging = false;

    PinCodeActivity.OnPinCompleted onPinCompleteListener;

    private int numberPaddingStart;
    private int numberPaddingTop;
    private int numberPaddingEnd;
    private int numberPaddingBottom;


    public PinCodeLayout(Context context) {
        super(context);
        init(context, null);
    }


    public PinCodeLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    public PinCodeLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PinCodeLayout);
        int pinNumberCount = a.getInteger(R.styleable.PinCodeLayout_pinNumberCount, DEFAULT_NUMBER_COUNT);

        initNumberPadding(a);

        if (pinNumberCount <= 0) {
            pinNumberCount = DEFAULT_NUMBER_COUNT;
        }

        for (int i = 0; i < pinNumberCount; i++) {
            final FontEditText editText = new FontEditText(context, attrs);
            editText.setId(i);
            editText.setWidth(100);
            editText.setGravity(Gravity.CENTER_HORIZONTAL);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }


                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!isTextChanging && s.length() > 1) {
                        isTextChanging = true;
                        if (editText.getSelectionStart() == 1) {
                            editText.setText(String.valueOf(s.charAt(0)));
                        } else {
                            editText.setText(String.valueOf(s.charAt(1)));
                        }
                    }
                }


                @Override
                public void afterTextChanged(Editable s) {
                    isTextChanging = false;
                    onPinCompleteListener.isCompleted(getPinCode().length() == 4);
                }
            });
            editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/montserrat_bold.otf");
            editText.setTypeface(typeface);
            editTextList.add(editText);
            addView(editText);

            editText.setBackground(context.getDrawable(R.drawable.number_underline));
            LinearLayout.LayoutParams layoutParams = (LayoutParams) editText.getLayoutParams();
            layoutParams.setMargins(numberPaddingStart, numberPaddingTop, numberPaddingEnd, numberPaddingBottom);
        }

        fillTextChangerd();
        fillOnKey();
    }


    private void fillTextChangerd() {
        for (EditText editText : editTextList) {
            editText.addTextChangedListener(new PinNumberTextWatcher(editText, editTextList));
        }
    }


    private void fillOnKey() {
        for (int i = 1; i < editTextList.size(); i++) {
            editTextList.get(i).setOnKeyListener(new PinNumberDeleteKeyListener(editTextList.get(i - 1)));
        }
    }


    private void initNumberPadding(TypedArray a) {
        int numberPadding = a.getInteger(R.styleable.PinCodeLayout_numberPadding, 0);
        numberPaddingStart = a.getInteger(R.styleable.PinCodeLayout_numberPaddingStart, 0);
        numberPaddingTop = a.getInteger(R.styleable.PinCodeLayout_numberPaddingTop, 0);
        numberPaddingEnd = a.getInteger(R.styleable.PinCodeLayout_numberPaddingEnd, 30);
        numberPaddingBottom = a.getInteger(R.styleable.PinCodeLayout_numberPaddingBottom, 0);

        if (numberPadding != 0 && numberPaddingStart == 0 && numberPaddingTop == 0 && numberPaddingEnd == 0 && numberPaddingBottom == 0) {
            numberPaddingStart = numberPadding;
            numberPaddingTop = numberPadding;
            numberPaddingEnd = numberPadding;
            numberPaddingBottom = numberPadding;
        }
    }


    public boolean isPinCodeEmpty() {
        boolean isEmpty = false;
        for (EditText editText : editTextList) {
            if (editText.getText().toString().isEmpty()) {
                return true;
            }
        }
        return isEmpty;
    }


    public String getPinCode() {
        String pinCode = "";
        for (EditText editText : editTextList) {
            pinCode += editText.getText().toString();
        }
        return pinCode;
    }


    public void setIsPinCompleteListener(PinCodeActivity.OnPinCompleted listener) {
        onPinCompleteListener = listener;
    }


    public void setOnEditorActionListener(TextView.OnEditorActionListener listener) {
        for (EditText et : editTextList) {
            et.setOnEditorActionListener(listener);
        }
    }
}

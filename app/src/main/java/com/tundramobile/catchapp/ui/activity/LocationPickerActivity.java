package com.tundramobile.catchapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.SimplePlace;
import com.tundramobile.catchapp.ui.fragment.LocationSearchFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sviatoslav Zaitsev on 08.04.17.
 */

public class LocationPickerActivity extends BaseActivity  {
    public static String ARG_PLACE          = "ARG_PLACE";
    public static String ARG_TOKEN          = "ARG_TOKEN";
    public static String ARG_USER_ID        = "ARG_USER_ID";
    public static String PLACE_BUNDLE       = "PLACE_BUNDLE";

    @BindView(R.id.toolbar) AppToolbar toolbar;

    private String token = "";
    private int userId = -1;

    public static Intent createIntent(Context context) {
        return new Intent(context, LocationPickerActivity.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_picker);
        ButterKnife.bind(this);
        retrieveIntentData();
        toolbar.switchToContactsSelectingMode(getString(R.string.location_picker_title), (v -> onBackPressed()));
        LocationSearchFragment fragment = LocationSearchFragment.newInstance(token, userId);
        fragment.setOnPlaceSavedListener(new OnPlaceSelectedListener());
        addFragment(R.id.fragmentContainer, fragment, null);
    }

    private class OnPlaceSelectedListener implements LocationSearchFragment.OnSavePlaceListener {

        @Override
        public void onSavePlace(SimplePlace place) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(ARG_PLACE, new Gson().toJson(place));
            setResult(RESULT_OK, resultIntent);
        }
    }

    private void retrieveIntentData() {
        Bundle args = getIntent().getExtras();
        if(args != null) {
            token = args.getString(ARG_TOKEN);
            userId = args.getInt(ARG_USER_ID);
        }
    }
}
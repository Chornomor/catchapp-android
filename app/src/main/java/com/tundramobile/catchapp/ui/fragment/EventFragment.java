package com.tundramobile.catchapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.CatchUpDetail;
import com.tundramobile.catchapp.entity.Event;
import com.tundramobile.catchapp.entity.GooglePlace;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.ImageLoader;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.TimeSlotsManager;
import com.tundramobile.catchapp.net.interfaces.DeleteRequestListener;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.ErrorHandler;
import com.tundramobile.catchapp.other.GPSUtil;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.EventDetailsRecyclerAdapter;
import com.tundramobile.catchapp.ui.fragment.calendar.EditCalendarFragment;
import com.tundramobile.catchapp.ui.view.AppToolbar;
import com.tundramobile.catchapp.ui.view.CollageImageView;
import com.tundramobile.catchapp.ui.view.FontTextView;
import com.tundramobile.catchapp.ui.viewmodel.PlaceDetailsItem;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class EventFragment extends BaseFragment implements EventDetailsClickListener,
        CatchUpManager.CatchUpDetailsListener, DeleteRequestListener {

    private static final String TAG = EventFragment.class.getSimpleName();
    private static final String CATCH_UP = "CATCH_UP";

    @BindView(R.id.toolbar) AppToolbar toolbar;
    @BindView(R.id.appBarLayout) AppBarLayout appBarLayout;
    @BindView(R.id.headerImageView) ImageView headerImageView;
    @BindView(R.id.strip) FrameLayout strip;
    @BindView(R.id.title) FontTextView title;
    @BindView(R.id.screamer) View screamer;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    @BindView(R.id.avatarContainer) ViewGroup avatarContainer;
    @BindView(R.id.avatar) CollageImageView avatar;
    @BindView(R.id.initials) TextView initials;
    @BindView(R.id.emptyGroupText) TextView emptyGroupText;
    @BindView(R.id.emptyGroupImage) CollageImageView emptyGroupImage;

    @BindDimen(R.dimen.event_avatar_size) int avatarSize;

    @BindString(R.string.share_text) String textToShare;

    private Unbinder unbinder;
    private AppToolbar outerToolbar;
    private CatchUp catchUp;
    private int type;
    private EventDetailsRecyclerAdapter adapter;
    private GoogleApiClient googleApiClient;
    private CatchUpManager catchUpManager;
    private boolean clickInProcess;


    public static EventFragment newInstance(CatchUp catchUp, int type) {
        EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        args.putParcelable(CATCH_UP, catchUp);
        args.putInt(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            catchUp = savedInstanceState.getParcelable(CATCH_UP);
            type = savedInstanceState.getInt(TYPE);
        } else if (getArguments() != null) {
            catchUp = getArguments().getParcelable(CATCH_UP);
            type = getArguments().getInt(TYPE);
        }
        googleApiClient = ((MainActivity) getActivity()).getGoogleApiClient();
        catchUpManager = CatchUpManager.getInstance(getContext());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_event, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();
        requestPlaceDetails();
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        cancelProgressDialog();
        clickInProcess = false;
    }


    private void initLayout() {
        setupBar();
        setAvatar();
        setTypeContent();
        setUpRecyclerView();
        setHeaderImage();
    }


    private void setupBar() {
        toolbar.switchToEventDetailsMode(v -> onBackPressed());

        appBarLayout.addOnOffsetChangedListener((appBarLayout1, verticalOffset) -> {
            verticalOffset = Math.abs(verticalOffset);
            float halfScrollRange = (int) (appBarLayout.getTotalScrollRange() * 0.75f);
            float ratio = (float) verticalOffset / halfScrollRange;
            ratio = Math.max(0f, Math.min(1f, ratio));
            ViewCompat.setAlpha(avatarContainer, 1 - ratio);
        });
    }


    private void setHeaderImage() {
        String googlePlacesId = catchUp.getGooglePlacesId();
//        String googlePlacesId = "ChIJrTLr-GyuEmsRBfy61i59si0";
        if (!TextUtils.isEmpty(googlePlacesId)) {
            GPSUtil.requestPlacePhoto(googleApiClient, googlePlacesId, headerImageView, 0);
        } else {
            switch (type) {
                case CatchUpManager.CONFIRMED:
                    headerImageView.setBackgroundResource(R.color.green);
                    break;
                case CatchUpManager.INCOMING:
                    headerImageView.setBackgroundResource(R.color.red);
                    break;
                case CatchUpManager.OUTGOING:
                    headerImageView.setBackgroundResource(R.color.blue);
                    break;
            }
        }
    }


    private void setUpRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(llm);

        adapter = new EventDetailsRecyclerAdapter(catchUp, type, this);
        recyclerView.setAdapter(adapter);
    }


    private void setAvatar() {

        avatar.setBorderSize(12);

        if (catchUp.getType().equals(CatchUp.TYPE_ONE)) {
            if (catchUp.isOneToOneSingleUser()) {
                loadSingleAvatar(catchUp.isMine(MyUserManager.getCurrentUser(getContext()).getId()) ?
                        catchUp.getInvitedUser() : catchUp.getUser());
            } else {
                if (catchUp.isMine(MyUserManager.getCurrentUser(getContext()).getId())) {
                    loadOneToOneImage();
                } else {
                    loadSingleAvatar(catchUp.getUser());
                }
            }
        } else {
            loadInvitedImages();
        }
    }


    private void loadOneToOneImage() {
        avatar.setImageResource(R.drawable.rect_blue);
        initials.setText("1:1");
        initials.setVisibility(View.VISIBLE);
    }


    private void loadSingleAvatar(User user) {
        String url = user.getProfilePic();
        if (!TextUtils.isEmpty(url)) {
            ImageLoader.newInstance(avatar, avatarSize, false).loadImage(url);
        } else {
            avatar.setImageResource(R.drawable.rect_gray_common);
            initials.setText(Utils.getNameInitials(user.getFirstName(), user.getLastName()));
            initials.setVisibility(View.VISIBLE);
        }
    }


    private void loadInvitedImages() {
        if (catchUp.getInvited() != null && !catchUp.getInvited().isEmpty()) {
            emptyGroupImage.setImageResource(R.drawable.rect_gray_common);
            emptyGroupText.setVisibility(View.VISIBLE);
            ImageLoader.newInstance(avatar, avatarSize).loadImages(catchUp.getInvited());
        }
    }


    private void setTypeContent() {
        switch (type) {
            case CatchUpManager.CONFIRMED:
                title.setText(R.string.you_are_going);
                title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.done_ic, 0);
                strip.setBackgroundResource(R.color.green);
                break;
            case CatchUpManager.INCOMING:
                screamer.setVisibility(View.VISIBLE);
                title.setText(R.string.pick_a_time);
                strip.setBackgroundResource(R.color.red);
                break;
            case CatchUpManager.OUTGOING:
                title.setText(R.string.waiting_for_reply);
                strip.setBackgroundResource(R.color.blue);
                break;
        }
    }


    private void requestPlaceDetails() {
        if (Utils.isConnected(getContext())) {
            switch (type) {
                case CatchUpManager.CONFIRMED:
                    catchUpManager.getCatchUpDetailsConfirmed(catchUp.getId(), catchUp.getEventId(), this);
                    break;
                case CatchUpManager.INCOMING:
                    catchUpManager.getCatchUpDetailsIncoming(catchUp.getId(), this);
                    break;
                case CatchUpManager.OUTGOING:
                    catchUpManager.getCatchUpDetailsOutgoing(catchUp.getId(), this);
                    break;

            }
        } else {
            if (adapter != null) adapter.setPlaceDetails(null);
        }
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        outerToolbar = toolbar;
        outerToolbar.setVisibility(View.GONE);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(CATCH_UP, catchUp);
        outState.putInt(TYPE, type);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onStop() {
        super.onStop();
        outerToolbar.setVisibility(View.VISIBLE);
        cancelProgressDialog();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onMessageClick() {
        Set<Long> list = new HashSet<>();
        if (type == CatchUpManager.CONFIRMED) {
            for (User u : catchUp.getAttending()) {
                list.add((long) u.getId());
            }
        } else {
            for (User u : catchUp.getInvited()) {
                list.add((long) u.getId());
            }
        }
        list.add((long) catchUp.getUser().getId());
        ArrayList<Long> ids = new ArrayList<>(list);
        Collections.sort(ids, Long::compareTo);
        showFragment(ChatFragment.newInstance((long) catchUp.getUser().getId(), ids));
    }


    @Override
    public void onShareClick() {
        if (clickInProcess) return;
        clickInProcess = true;

        if (Utils.share(getContext(), textToShare)) {
            showProgressDialog();
        } else {
            clickInProcess = false;
        }
    }


    @Override
    public void onActionButtonClick(boolean isDelete) {
        if (Utils.isConnected(getContext())) {
            showProgressDialog();
            if (catchUp.isMine(MyUserManager.getCurrentUser(getContext()).getId())) {
                catchUpManager.deleteCatchUp(catchUp.getId(), this);
            } else {
                if (catchUp.getEventId() == 0) {
                    catchUpManager.cancelCatchUpInvitation(catchUp.getId(), this);
                } else {
                    catchUpManager.cancelCatchUpTimeSlotInvitation(catchUp.getId(), catchUp.getEventId(), this);
                }
            }
        } else {
            ErrorHandler.getInstance(getContext()).postError(R.string.offline_error);
        }
    }


    @Override
    public void onSlotDeleteListener(boolean isSuccess, String error) {
        cancelProgressDialog();
        if (getContext() != null) {
            if (!isSuccess) {
                if (!TextUtils.isEmpty(error)) {
                    ErrorHandler.getInstance(getContext()).postError(error);
                }
            } else {
                getActivity().onBackPressed();
            }
        }
    }


    @Override
    public void onTimeSlotClick(Event event, boolean isDelete) {
        if (isDelete) {
            if (Utils.isConnected(getContext())) {
                showProgressDialog();
                TimeSlotsManager.getInstance()
                        .deleteTimeSlot(MyUserManager.getInstance().getCurrentUserToken(getContext()),
                                catchUp.getId(), event.getId(), (isSuccess, error) -> {
                                    cancelProgressDialog();
                                    if (isSuccess) {
                                        if (adapter != null) {
                                            catchUp.getEvents().remove(event);
                                            adapter.updateTimeSlots(catchUp);
                                        }
                                    } else {
                                        if (!TextUtils.isEmpty(error) && getContext() != null) {
                                            ErrorHandler.getInstance(getContext()).postError(error);
                                        }
                                    }
                                });
            } else {
                ErrorHandler.getInstance(getContext()).postError(R.string.offline_error);
            }
        } else {
            EditCalendarFragment f = EditCalendarFragment.makeConfirm(getContext(), catchUp.getId(), event);
            showFragment(f);
        }
    }


    @Override
    public void onEditUserClick(User user) {
        if (Utils.isConnected(getContext())) {
            showProgressDialog();
            catchUpManager.deleteUserInvitationToEvent(catchUp.getId(), user.getId(), (isSuccess, error) -> {
                cancelProgressDialog();
                if (isSuccess) {
                    if (adapter != null) {
                        catchUp.getInvited().remove(user);
                        adapter.updateInvitedUsers(catchUp);
                    }
                } else {
                    if (!TextUtils.isEmpty(error) && getContext() != null) {
                        ErrorHandler.getInstance(getContext()).postError(error);
                    }
                }
            });
        } else {
            ErrorHandler.getInstance(getContext()).postError(R.string.offline_error);
        }
    }


    @Override
    public void onWebSiteClick(String url) {
        if (clickInProcess) return;
        clickInProcess = true;

        if (Utils.openBrowser(getContext(), url)) {
            showProgressDialog();
        } else {
            clickInProcess = false;
        }
    }


    @Override
    public void onPhoneClick(String phone) {
        if (clickInProcess) return;
        clickInProcess = true;

        if (Utils.openDialer(getContext(), phone)) {
            showProgressDialog();
        } else {
            clickInProcess = false;
        }
    }


    @Override
    public void onDetailsReceived(CatchUpDetail catchUpDetail, String error) {
        if (error != null) {
            ErrorHandler.getInstance(getContext()).postError(error);
            if (adapter != null) adapter.setPlaceDetails(null);
        } else {
            if (adapter != null) {
                GooglePlace place = catchUpDetail.getGooglePlace();
                if (place != null) {
                    adapter.setPlaceDetails(new PlaceDetailsItem(place));
                } else {
                    adapter.setPlaceDetails(null);
                }
            }
        }
    }
}

package com.tundramobile.catchapp.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.ui.adapter.holder.BaseEventHolder;
import com.tundramobile.catchapp.ui.adapter.holder.EventActionButtonHolder;
import com.tundramobile.catchapp.ui.adapter.holder.EventAttendingOrInvitedHolder;
import com.tundramobile.catchapp.ui.adapter.holder.EventMainInfoHolder;
import com.tundramobile.catchapp.ui.adapter.holder.EventPlaceDetailsHolder;
import com.tundramobile.catchapp.ui.adapter.holder.EventTimeSlotsHolder;
import com.tundramobile.catchapp.ui.fragment.CatchUpFragment;
import com.tundramobile.catchapp.ui.viewmodel.PlaceDetailsItem;

import java.util.ArrayList;

public class EventDetailsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_TIME_SLOTS = 1;
    private static final int VIEW_TYPE_ATTENDING_OR_INVITED = 2;
    private static final int VIEW_TYPE_PLACE_INFO = 3;
    private static final int VIEW_TYPE_ACTION_BUTTON = 4;

    private CatchUp catchUp;
    private PlaceDetailsItem placeDetails;
    private int type;
    private EventDetailsClickListener listener;
    private ArrayList<Integer> typesToShow = new ArrayList<>();


    public EventDetailsRecyclerAdapter(CatchUp catchUp, int type, EventDetailsClickListener listener) {
        this.catchUp = catchUp;
        this.type = type;
        this.listener = listener;

        defineContent();
    }


    public void setPlaceDetails(@Nullable PlaceDetailsItem placeDetails) {
        this.placeDetails = placeDetails;
        if (placeDetails != null) {
            notifyItemChanged(typesToShow.indexOf(VIEW_TYPE_HEADER));
            notifyItemChanged(typesToShow.indexOf(VIEW_TYPE_PLACE_INFO));
        } else {
            int position = typesToShow.indexOf(VIEW_TYPE_PLACE_INFO);
            typesToShow.remove(position);
            notifyItemRemoved(position);
        }
    }


    private void defineContent() {
        typesToShow = new ArrayList<>();

        // always show header
        typesToShow.add(VIEW_TYPE_HEADER);

        // decide to show view with time slots
        if (catchUp.getEvents() != null && !catchUp.getEvents().isEmpty()
                && type != CatchUpManager.CONFIRMED)
        {
            typesToShow.add(VIEW_TYPE_TIME_SLOTS);
        }

        // decide to show invited people
        if ((catchUp.isGroup() || catchUp.isOneToOneMulti()) &&
                catchUp.getInvited() != null && !catchUp.getInvited().isEmpty()) {
            typesToShow.add(VIEW_TYPE_ATTENDING_OR_INVITED);
        }

        // always show places information
        typesToShow.add(VIEW_TYPE_PLACE_INFO);

        // always show action button
        typesToShow.add(VIEW_TYPE_ACTION_BUTTON);
    }


    @Override
    public int getItemViewType(int position) {
        return typesToShow.get(position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new EventMainInfoHolder(li.inflate(R.layout.holder_event_main_info, parent, false),
                        listener);
            case VIEW_TYPE_TIME_SLOTS:
                return new EventTimeSlotsHolder(li.inflate(R.layout.holder_event_time_slots, parent,
                        false), listener);
            case VIEW_TYPE_ATTENDING_OR_INVITED:
                return new EventAttendingOrInvitedHolder(li.inflate(R.layout.holder_event_attending, parent,
                        false), listener);
            case VIEW_TYPE_PLACE_INFO:
                return new EventPlaceDetailsHolder(li.inflate(R.layout.holder_event_place_details,
                        parent, false), listener);
            case VIEW_TYPE_ACTION_BUTTON:
                return new EventActionButtonHolder(li.inflate(R.layout.holder_event_action_button,
                        parent, false), listener);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof EventMainInfoHolder) {
            ((EventMainInfoHolder) holder).bindContent(catchUp, type, placeDetails);
        } else if (holder instanceof EventPlaceDetailsHolder) {
            ((EventPlaceDetailsHolder) holder).bindContent(placeDetails, type);
        } else if (holder instanceof BaseEventHolder) {
            ((BaseEventHolder) holder).bindContent(catchUp, type);
        }
    }


    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof EventPlaceDetailsHolder) {
            ((EventPlaceDetailsHolder) holder).clearMap();
        }
        super.onViewRecycled(holder);
    }


    @Override
    public int getItemCount() {
        return typesToShow.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public void updateTimeSlots(CatchUp catchUp) {
        this.catchUp = catchUp;
        notifyItemChanged(VIEW_TYPE_TIME_SLOTS);
    }

    public void updateInvitedUsers(CatchUp catchUp) {
        this.catchUp = catchUp;
        notifyItemChanged(VIEW_TYPE_ATTENDING_OR_INVITED);
    }
}

package com.tundramobile.catchapp.ui.activity.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.ui.activity.BaseActivity;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.other.Regex;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmailActivity extends BaseActivity {

    @BindView(R.id.user_email) EditText userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_email);
        ButterKnife.bind(this);
        setupKeyboardDoneButton();
    }


    private void setupKeyboardDoneButton() {
        userEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    OnNextClick();
                }
                return false;
            }
        });
    }


    @OnClick(R.id.auth_fab_next)
    public void OnNextClick() {
        String email = userEmail.getText().toString();
        if(Regex.isEmailValid(email)) {
            MyUserManager.getCurrentUser(EmailActivity.this).setHomeEmail(email);
            Intent intent = new Intent(EmailActivity.this, UserNameActivity.class);
            startActivity(intent);
        } else {
            TopPopUpManager.showTopPopUp(this, getString(R.string.auth_3_email_incorrect), !TopPopUpManager.isError);
        }
    }
}

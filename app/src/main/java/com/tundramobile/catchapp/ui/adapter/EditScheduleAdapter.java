package com.tundramobile.catchapp.ui.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.ui.view.dateEditor.SlotRect;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EditScheduleAdapter extends RecyclerView.Adapter<EditScheduleAdapter.TimeItem> {
    private static int MILIS_IN_HOUR = 1000 * 60 * 60;
    OnTimeSlotSelected timeSLotListener;
    Date start;
    int timeStep;

    public EditScheduleAdapter(Context context,OnTimeSlotSelected timeSLotListener) {
        timeStep = (int) (context.getResources().getDimension(R.dimen.time_slot_height)/12);
        this.timeSLotListener = timeSLotListener;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MINUTE,0);
        start = calendar.getTime();
    }

    @Override
    public TimeItem onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimeItem(parent,timeSLotListener);
    }

    @Override
    public void onBindViewHolder(TimeItem holder, int position) {
        holder.bind(getDate(position));
    }

    public long getStartTime(){
        return start.getTime();
    }
    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    public Date getDate(int position){
        return new Date(start.getTime() + position*MILIS_IN_HOUR);
    }
    static class TimeItem extends RecyclerView.ViewHolder
    implements View.OnLongClickListener{
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.slots)
        View slotsView;
        OnTimeSlotSelected timeSlotListener;
        Date date;
        public TimeItem(ViewGroup parent,OnTimeSlotSelected timeSlotListener) {
            super(LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_time_row,parent,false));
            this.timeSlotListener = timeSlotListener;
            ButterKnife.bind(this,itemView);
            itemView.setOnLongClickListener(this);
        }

        public void bind(Date date){
            this.date = date;
            tvTime.setText(DateUtil.formatTime(date));
        }

        @Override
        public boolean onLongClick(View v) {
            Rect r = new Rect
                    (slotsView.getLeft(),itemView.getTop(),slotsView.getRight(),itemView.getBottom());
            timeSlotListener.OnTimeSlotSelected(r,date, new Date(date.getTime() + MILIS_IN_HOUR));
            return true;
        }
    }



    public int getScrollForDate(Date date){
        long diff = date.getTime() - start.getTime();
        return (int) (diff/ SlotRect.STEP_MILIS * timeStep);
    }

    public interface OnTimeSlotSelected {
        void OnTimeSlotSelected(Rect bounds, Date from, Date to);
    }
}

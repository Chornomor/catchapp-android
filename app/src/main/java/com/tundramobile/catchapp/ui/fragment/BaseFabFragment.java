package com.tundramobile.catchapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;

import com.tundramobile.catchapp.entity.FabOption;
import com.tundramobile.catchapp.interfaces.FabHandler;
import com.tundramobile.catchapp.other.Utils;

import java.util.ArrayList;

public abstract class BaseFabFragment extends BaseFragment {

    protected final static String TAG = BaseFabFragment.class.getSimpleName();
    private FabHandler fabHandler;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FabHandler) {
            fabHandler = (FabHandler) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FabHandler");
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFabOptions(getFabOptions(), getFabIcon(), isBlurRequired());

        if (showFabAtStart()) {
            showFab();
        } else {
            hideFab();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        fabHandler = null;
    }


    protected void showFab() {
        Utils.loge(TAG, "showFab");
        if (fabHandler != null) fabHandler.showFab();
    }


    protected void hideFab() {
        Utils.loge(TAG, "hideFab");
        if (fabHandler != null) fabHandler.hideFab();
    }


    protected boolean isFabOpen() {
        return getActivity() != null && getActivity() instanceof FabHandler
                && fabHandler != null && fabHandler.isFabOpen();
    }


    private void setupFabOptions(ArrayList<FabOption> options, @DrawableRes int fabIcon, boolean applyBlur) {
        if (fabHandler != null) fabHandler.setupFabOptions(options, fabIcon, applyBlur);
    }


    protected abstract ArrayList<FabOption> getFabOptions();

    protected abstract int getFabIcon();

    protected abstract boolean isBlurRequired();

    protected abstract boolean showFabAtStart();
}

package com.tundramobile.catchapp.ui.adapter.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.interfaces.EventDetailsClickListener;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.ui.view.AvatarView;
import com.tundramobile.catchapp.ui.view.EventUserEditView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventAttendingOrInvitedHolder extends BaseEventHolder {

    private static final int VISIBLE_AVATARS_COUNT = 4;

    @BindView(R.id.type) TextView typeText;
    @BindView(R.id.attendingList) ViewGroup attendingListView;
    @BindView(R.id.othersCount) TextView othersCount;
    @BindView(R.id.previewContainer) ViewGroup previewContainer;
    @BindView(R.id.editContainer) ViewGroup editContainer;


    public EventAttendingOrInvitedHolder(View itemView, EventDetailsClickListener listener) {
        super(itemView, listener);
        ButterKnife.bind(this, itemView);
    }


    @Override
    public void bindContent(CatchUp catchUp, int type) {
        super.bindContent(catchUp, type);

        boolean isInEditMode = catchUp.isMine(MyUserManager.getCurrentUser(itemView.getContext()).getId()) &&
                (catchUp.isOneToOneMulti() || catchUp.isGroup());

        typeText.setText(R.string.invited);

        if (isInEditMode) {
            setupEditList(catchUp.getInvited());
            editContainer.setVisibility(View.VISIBLE);
            previewContainer.setVisibility(View.GONE);
        } else {
            setupList(catchUp.getInvited());
            setupOtherCount(catchUp.getInvited().size());
            editContainer.setVisibility(View.GONE);
            previewContainer.setVisibility(View.VISIBLE);
        }
    }


    private void setupList(List<User> list) {
        attendingListView.removeAllViews();
        for (int i = 0; i < VISIBLE_AVATARS_COUNT; i++) {
            try {
                attendingListView.addView(generateAvatar(list.get(i)));
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }
    }


    private void setupEditList(List<User> list) {
        editContainer.removeAllViews();
        for (User user : list) {
            editContainer.addView(generateEditUser(user));
        }
    }


    private void setupOtherCount(int count) {
        if (count > VISIBLE_AVATARS_COUNT) {
            othersCount.setText(othersCount.getResources().getQuantityString(R.plurals.other,
                    count - VISIBLE_AVATARS_COUNT, "", count - VISIBLE_AVATARS_COUNT));
            othersCount.setVisibility(View.VISIBLE);
        } else {
            othersCount.setVisibility(View.GONE);
        }
    }


    private View generateAvatar(User user) {
        AvatarView view = new AvatarView(itemView.getContext());
        view.setupWith(user);
        return view;
    }


    private View generateEditUser(User user) {
        EventUserEditView view = new EventUserEditView(itemView.getContext());
        view.setOnCLickListener(listener);
        view.setupWith(user);
        return view;
    }

}

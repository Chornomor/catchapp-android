package com.tundramobile.catchapp.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.blaze.fastpermission.FastPermission;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.SimplePlace;
import com.tundramobile.catchapp.entity.request.GooglePlaceBody;
import com.tundramobile.catchapp.entity.request.MiniGooglePlaceBody;
import com.tundramobile.catchapp.entity.response.LocationResponse;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.other.LocationUtils;
import com.tundramobile.catchapp.ui.activity.LocationPickerActivity;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.adapter.PlaceAutocompleteAdapter;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;
import retrofit2.Call;
import retrofit2.Response;

import static com.tundramobile.catchapp.ui.fragment.NewEventFragment.SELECTED_PLACE;

/**
 * Created by Sviatoslav Zaitsev on 10.04.17.
 */

public class LocationSearchFragment extends BaseFragment {

    private static final int REQUEST_PERMISSION = 1488;

    @BindView(R.id.searchInput)  EditText searchView;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.current_location_btn) View curLocationBtn;
    @BindView(R.id.location_picker_save_btn) Button saveBtn;


    private Unbinder unbinder;

    private GoogleApiClient googleClient;
    private PlaceAutocompleteAdapter adapter;
    private AutocompleteFilter autocompleteFilter;

    private OnSavePlaceListener onSavePlaceListener;

    private SimplePlace selectedPlace;

    private String token = "";
    private int userId = -1;


    public LocationSearchFragment() {
    }


    public static LocationSearchFragment newInstance(String token, int userId) {
        LocationSearchFragment fragment = new LocationSearchFragment();
        Bundle args = new Bundle();
        args.putString("ARG_TOKEN", token);
        args.putInt("ARG_USER_ID", userId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrieveArgumentsData();
        if (getActivity() instanceof MainActivity) {
            googleClient = ((MainActivity) getActivity()).getGoogleApiClient();
        } else {
            // TODO: 20.04.17 Implement correct way with client disconnection for multiple activities
//            googleClient = new GoogleApiClient
//                    .Builder(getActivity())
//                    .addApi(Places.GEO_DATA_API)
//                    .addApi(Places.PLACE_DETECTION_API)
//                    .enableAutoManage(getActivity(), connectionResult -> { })
//                    .build();
        }

        autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT)
                .build();

        adapter = new PlaceAutocompleteAdapter(getContext(), googleClient, null, autocompleteFilter);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_locations_search, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        searchView.addTextChangedListener(new SearchTextWatcher());
        searchView.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_DONE){
                recyclerView.postDelayed(() -> {
                    if(recyclerView != null && recyclerView.getChildAt(0) != null)
                    recyclerView.getChildAt(0).performClick();
                }, 50);
            }
            return false;
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener((position, place) -> {
            curLocationBtn.setVisibility(View.VISIBLE);
            selectLocation(place);
            adapter.setCustomLocation("");
        });
        adapter.setOnCustomLocationClickListener(locationName -> {
            ((ProgressDialogHandler) getActivity()).showProgressDialog();
            RESTClient.getInstance().updateLocation(token, userId, -1, new MiniGooglePlaceBody(locationName),
                    new LocationResponseCallback(locationName));
        });
        adapter.setOnItemsCountChangedListener(count -> curLocationBtn.setVisibility(count > 0 ? View.GONE : View.VISIBLE));
    }


    @OnClick(R.id.current_location_btn)
    void OnCurLocationBtnClicked(View v) {
        if (LocationUtils.isGPSEnabled(getContext())) {
            FastPermission.request(
                    this,
                    REQUEST_PERMISSION,
                    this::requestCurrentPlace,
                    PermissionEnum.ACCESS_FINE_LOCATION
            );
        } else {
            Snackbar.make(getView(), "GPS signal is not available", Snackbar.LENGTH_LONG)
                    .setAction("Enable GPS", view -> {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    })
                    .show();
        }
    }


    @OnClick(R.id.location_picker_save_btn)
    void OnSaveBtnClicked(View v) {
        hideKeyboard();
        if (selectedPlace != null) {
            saveSelectedLocation();
        }
    }


    private void requestCurrentPlace() {

        Places.PlaceDetectionApi.getCurrentPlace(googleClient, null)
                .then(new ResultTransform<PlaceLikelihoodBuffer, Result>() {

                    @Nullable
                    @Override
                    public PendingResult<Result> onSuccess(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {
                        String searchAddress = placeLikelihoods.get(0).getPlace().getAddress().toString();
                        getActivity().runOnUiThread(() -> {

                            adapter.setCustomLocation(searchAddress);
                            if(recyclerView.getVisibility() == View.GONE) {
                                adapter.getFilter().filter("");
                                recyclerView.setVisibility(View.VISIBLE);
                            }
                            recyclerView.postDelayed(() -> {
                                View v = recyclerView.getChildAt(0);
                                if(v != null)
                                    v.performClick();
                            }, 50);
                        });

                        return null;
                    }
                });
    }


    public void setOnPlaceSavedListener(OnSavePlaceListener listener) {
        this.onSavePlaceListener = listener;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
        toolbar.switchToDefaultMode(getString(R.string.location_picker_title), (v) -> {
            hideKeyboard();
            getActivity().onBackPressed();
        });
    }


    private void retrieveArgumentsData() {
        Bundle args = getArguments();
        if (args != null) {
            token = args.getString(LocationPickerActivity.ARG_TOKEN);
            userId = args.getInt(LocationPickerActivity.ARG_USER_ID);
        }
    }


    private void selectLocation(@NonNull SimplePlace place) {
        hideKeyboard();
        selectedPlace = place;
        saveBtn.setVisibility(View.VISIBLE);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
    }


    private class SearchTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }


        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


        @Override
        public void afterTextChanged(Editable s) {
            recyclerView.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            saveBtn.setVisibility(View.GONE);
            adapter.resetSelection();
            adapter.setCustomLocation(s.toString());
            adapter.getFilter().filter(s.toString());
        }
    }

    private class LocationResponseCallback extends HandledCallback<LocationResponse> {
        private String locationName;

        public LocationResponseCallback(String locationName) {
            this.locationName = locationName;
        }

        @Override
        public void onSuccess(Call<LocationResponse> call, Response<LocationResponse> response) {
            // TODO: 20.04.17 Dialog
            ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
            int locationId = response.body().getSuccess();
            selectLocation(new SimplePlace(locationId, locationName));
        }


        @Override
        public void onFail(Call<LocationResponse> call, Response<LocationResponse> response) {
            ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
        }


        @Override
        public void onProcessFailed(Call<LocationResponse> call, Throwable t, String error) {
            ((ProgressDialogHandler) getActivity()).cancelProgressDialog();
        }
    }


    private void saveSelectedLocation() {
        Bundle args = new Bundle();
        args.putString(SELECTED_PLACE, new Gson().toJson(selectedPlace));

        Navigator.with(getActivity()).putData(SELECTED_PLACE, args);
        if(onSavePlaceListener != null) {
            onSavePlaceListener.onSavePlace(selectedPlace);
        }
        getActivity().onBackPressed();
    }


    public interface OnSavePlaceListener {
        void onSavePlace(SimplePlace place);
    }
}

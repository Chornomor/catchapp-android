package com.tundramobile.catchapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.FabOption;
import com.tundramobile.catchapp.entity.response.CatchUpsCounters;
import com.tundramobile.catchapp.event.GetCatchUpsEvent;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.manager.PagerManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.other.ErrorHandler;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;
import com.tundramobile.catchapp.ui.view.HomeTabLayout;
import com.tundramobile.catchapp.ui.view.NonSwipeViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;

public class HomeFragment extends BaseFabFragment {


    public HomeFragment() {
    }


    public static HomeFragment newInstance(Bundle args) {
        return new HomeFragment();
    }


    private static final String TAG = HomeFragment.class.getSimpleName();
    private static final String IS_EMPTY = "IS_EMPTY";

    @BindView(R.id.homeTabLayout) HomeTabLayout homeTabLayout;
    @BindView(R.id.viewPager) NonSwipeViewPager viewPager;
    @BindView(R.id.emptyListHolder) SwipeRefreshLayout emptyListHolder;

    @BindColor(R.color.white) int white;

    private Unbinder unbinder;
    private CatchUpManager catchUpManager;
    private TabsPagerAdapter adapter;
    private int requestsCount;
    private boolean updateBadges;
    private ErrorHandler errorHandler;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catchUpManager = CatchUpManager.getInstance(getContext());
        errorHandler = ErrorHandler.getInstance(getContext());
    }


    @Override
    protected ArrayList<FabOption> getFabOptions() {
        return null;
    }


    @Override
    protected int getFabIcon() {
        return R.drawable.plus;
    }


    @Override
    protected boolean isBlurRequired() {
        return false;
    }


    @Override
    protected boolean showFabAtStart() {
        return true;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, v);
        initLayout();
        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        updateBadges();
        if(forceUpdate){
            forceUpdateAll();
        } else {
            checkForRequiredUpdate();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void checkForRequiredUpdate() {
        for (int i = 0; i < adapter.getCount(); i++) {
            if (isUpdateRequired(i)) {
                getCatchUps(new GetCatchUpsEvent(i));
            }
        }
    }

    private void forceUpdateAll(){
        for (int i = 0; i < adapter.getCount(); i++) {
                getCatchUps(new GetCatchUpsEvent(i));
            }

    }


    private void initLayout() {
        adapter = PagerManager.getCatchUpPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(null);
        viewPager.setAdapter(adapter);
        homeTabLayout.setupWithViewPager(viewPager);
        emptyListHolder.setColorSchemeResources(R.color.purple);
        emptyListHolder.setOnRefreshListener(this::getAllCatchUps);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getAllCatchUps() {
        for (int i = 0; i < adapter.getCount(); i++) {
            getCatchUps(new GetCatchUpsEvent(i));
        }
    }


    @Subscribe
    public void getCatchUps(GetCatchUpsEvent event) {
        requestsCount++;
        boolean isOnline = Utils.isConnected(getContext());
        if (!isOnline) {
            errorHandler.postError(R.string.offline_error);
            showLoadingForType(event.getType(), false);
            onLoadFinished();
            return;
        }

        if (updateBadges) {
            updateBadges = false;
            updateBadges();
        }

        showLoadingForType(event.getType(), true);
        catchUpManager.getCatchUps(event.getType(), isOnline, (type, success, catchUps) -> {
            showLoadingForType(event.getType(), false);
            if (success) setCatchUpsForType(event.getType(), catchUps);
            onLoadFinished();
        });
    }


    private void setCatchUpsForType(int type, List<CatchUp> items) {
        Utils.logd(TAG, "setCatchUpsForType: " + type + ", size = " + items.size());
        if (viewPager != null)
            ((CatchUpFragment) adapter.getActiveFragment(viewPager, type)).setCatchUps(items);
    }


    private void showLoadingForType(int type, boolean show) {
        if (emptyListHolder != null && emptyListHolder.getVisibility() == View.VISIBLE) {
            emptyListHolder.setRefreshing(false);
        }

        if (viewPager != null)
            ((CatchUpFragment) adapter.getActiveFragment(viewPager, type)).showLoading(show);
    }


    private boolean isListEmptyForType(int type) {
        return viewPager != null && ((CatchUpFragment) adapter.getActiveFragment(viewPager, type)).isEmpty();
    }


    private boolean isUpdateRequired(int type) {
        if (viewPager != null) {
            Fragment f = adapter.getActiveFragment(viewPager, type);
            if (f != null) {
                return ((CatchUpFragment) f).isUpdateRequired();
            }
        }
        return false;
    }


    public void onLoadFinished() {
        if (requestsCount == 0) requestsCount = 1;
        requestsCount--;
        Utils.logd(TAG, "onLoadFinished: results left = " + requestsCount);

        if (requestsCount == 0) {
            showEmptyHolder(!hasNonEmptyResult());
        }
    }


    private void updateBadges() {
        if (Utils.isConnected(getContext())) {
            catchUpManager.getCatchUpsCounters(new HandledCallback<CatchUpsCounters>() {

                @Override
                public void onSuccess(Call<CatchUpsCounters> call, Response<CatchUpsCounters> response) {
                    Utils.logd(TAG, "onSuccess" + response);
                    if (homeTabLayout != null) {
                        homeTabLayout.setConfirmedCount(response.body().getConfirmedCount());
                        homeTabLayout.setIncomingCount(response.body().getIncomingCount());
                        homeTabLayout.setOutgoingCount(response.body().getOutgoingCount());
                    }
                }


                @Override
                public void onFail(Call<CatchUpsCounters> call, Response<CatchUpsCounters> response) {
                    Utils.loge(TAG, "onFail");
                }


                @Override
                public void onProcessFailed(Call<CatchUpsCounters> call, Throwable t, String error) {
                    Utils.loge(TAG, "catchupsCounters onProcessFailed");
                }
            });
        } else {
            updateBadges = true;
        }
    }


    private void showEmptyHolder(boolean show) {
        if (emptyListHolder != null)
            emptyListHolder.setVisibility(show ? View.VISIBLE : View.GONE);
        if (viewPager != null)
            viewPager.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
    }


    private boolean hasNonEmptyResult() {
        if (viewPager != null)
            for (int i = 0; i < viewPager.getAdapter().getCount(); i++) {
                if (!isListEmptyForType(i)) {
                    return true;
                }
            }
        return false;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_EMPTY, !hasNonEmptyResult());
        super.onSaveInstanceState(homeTabLayout.saveState(outState));
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            homeTabLayout.restoreState(savedInstanceState);
            showEmptyHolder(savedInstanceState.getBoolean(IS_EMPTY));
        }
    }
}

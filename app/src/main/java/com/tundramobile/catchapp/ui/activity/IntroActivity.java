package com.tundramobile.catchapp.ui.activity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.manager.SharedPrefManager;
import com.tundramobile.catchapp.ui.activity.authorization.NumberActivity;
import com.tundramobile.catchapp.ui.fragment.ScreenSlidePageFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eugenetroyanskii on 29.03.17.
 */
public class IntroActivity extends BaseActivity {

    private final static int NUM_PAGES = 3;
    private final static int DEFAULT_FAB_POSITION = 0;

    @BindView(R.id.intro_view_pager) ViewPager viewPager;
    @BindView(R.id.intro_tab_dots) LinearLayout dotsLayout;
    @BindView(R.id.intro_fab_next) FloatingActionButton fabNext;
    @BindView(R.id.intro_indicator1) View indicator1;
    @BindView(R.id.intro_indicator2) View indicator2;
    @BindView(R.id.intro_indicator3) View indicator3;

    private int width;
    private int currentPagerPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        selectIndicator(currentPagerPosition);
        calculateScreenSize();
        setupPager();
        setupFab();
    }


    private void setupFab() {
        fabNext.setOnClickListener(v -> {
            if (currentPagerPosition + 1 == NUM_PAGES) {
                SharedPrefManager.getInstance(IntroActivity.this).storeIsIntroShowed(true);

                Intent intent = new Intent(IntroActivity.this, NumberActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                viewPager.setCurrentItem(currentPagerPosition + 1);
            }
        });
    }


    private void setupPager() {
        ScreenSlidePagerAdapter introPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(introPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }


            @Override
            public void onPageSelected(int position) {
                currentPagerPosition = position;
                selectIndicator(currentPagerPosition);
                if (position + 1 == NUM_PAGES) {
                    fabNext.animate()
                            .translationX(-(width / 2 - fabNext.getWidth() / 2 - fabNext.getPaddingRight()))
                            .setInterpolator(new LinearInterpolator())
                            .withEndAction(() -> dotsLayout.setVisibility(View.GONE))
                            .start();
                    fabNext.setImageDrawable(getDrawable(R.drawable.done_btn));
                } else {
                    fabNext.animate().translationX(DEFAULT_FAB_POSITION).setInterpolator(new LinearInterpolator()).start();
                    fabNext.setImageDrawable(getDrawable(R.drawable.arrow_right));
                    dotsLayout.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    private void selectIndicator(int position) {
        switch (position) {
            case 0:
                indicator1.setBackground(getDrawable(R.drawable.tab_indicator_selected));
                indicator2.setBackground(getDrawable(R.drawable.tab_indicator_default));
                indicator3.setBackground(getDrawable(R.drawable.tab_indicator_default));
                break;
            case 1:
                indicator1.setBackground(getDrawable(R.drawable.tab_indicator_default));
                indicator2.setBackground(getDrawable(R.drawable.tab_indicator_selected));
                indicator3.setBackground(getDrawable(R.drawable.tab_indicator_default));
                break;
            case 2:
                indicator1.setBackground(getDrawable(R.drawable.tab_indicator_default));
                indicator2.setBackground(getDrawable(R.drawable.tab_indicator_default));
                indicator3.setBackground(getDrawable(R.drawable.tab_indicator_selected));
                break;
        }
    }


    private void calculateScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.getInstance(position + 1);
        }


        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
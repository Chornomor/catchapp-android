package com.tundramobile.catchapp.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.interfaces.OnSwipeTouchListener;


import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alexchern on 24.04.17.
 */

public class TopPopUpView extends LinearLayout {

    @BindView(R.id.tv_pop_up_message) TextView topPopUpMessage;
    @BindView(R.id.top_pop_up) LinearLayout popUpRoot;

    private View root;
    private Animation topPopUpAnim;
    private TopPopUpDismissListener dismissListener;

    private Timer timer;


    public TopPopUpView(Context context) {
        super(context);
        initRoot(context, null);
    }


    public TopPopUpView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initRoot(context, attrs);
    }


    public TopPopUpView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initRoot(context, attrs);
    }


    private void initRoot(@NonNull Context context, @Nullable AttributeSet attrs) {
        root = LayoutInflater.from(getContext()).inflate(R.layout.top_pop_up, null);
        root.setVisibility(View.GONE);
        ButterKnife.bind(this, root);
        initViews(context);
        addView(root, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void initViews(Context context) {
        popUpRoot.setOnTouchListener(new OnSwipeTouchListener(context) {

            public void onSwipeLeft() {
                hidePopUpByLeftSwipe();
            }
        });
    }


    public void showPopUp(String message, boolean isError) {
        int rootColor = isError ? getResources().getColor(R.color.incoming) : getResources().getColor(R.color.top_pop_up_info);
        popUpRoot.setBackgroundColor(rootColor);
        topPopUpMessage.setText(message);

        topPopUpAnim = AnimationUtils.loadAnimation(getContext(), R.anim.translation_show);
        long delay = getDelay(message);
        if (root.getVisibility() == View.GONE) {
            root.setVisibility(View.INVISIBLE);
            root.startAnimation(topPopUpAnim);

            timer = new Timer();
            AnimateTimerTask timerTask = new AnimateTimerTask();
            timer.schedule(timerTask, delay);
        }
        topPopUpAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                root.setVisibility(View.VISIBLE);
            }


            @Override
            public void onAnimationEnd(Animation animation) {
            }


            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private long getDelay(String message) {
        long delay = (long) (Math.ceil((message.length() / 15)) * 1000);
        return delay == 0 ? 1000 : delay;
    }


    private void hidePopUp() {
        topPopUpAnim = AnimationUtils.loadAnimation(getContext(), R.anim.translation_hide);
        if (root.getVisibility() == View.VISIBLE) {
            root.setVisibility(View.INVISIBLE);

            root.startAnimation(topPopUpAnim);
        }
        topPopUpAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }


            @Override
            public void onAnimationEnd(Animation animation) {
                root.setVisibility(View.GONE);
                dismissListener.onNeedToDismiss();
            }


            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private void hidePopUpByLeftSwipe() {
        topPopUpAnim = AnimationUtils.loadAnimation(getContext(), R.anim.translation_dismiss_x);
        if (root.getVisibility() == View.VISIBLE) {
            root.setVisibility(View.INVISIBLE);
            root.startAnimation(topPopUpAnim);
        }
        topPopUpAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }


            @Override
            public void onAnimationEnd(Animation animation) {
                root.setVisibility(View.GONE);
                dismissListener.onNeedToDismiss();

                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
            }


            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private class AnimateTimerTask extends TimerTask {

        @Override
        public void run() {
            if (getContext() != null) {
                ((Activity) getContext()).runOnUiThread(() -> {
                    hidePopUp();

                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }
                });
            }
        }
    }


    public void setDismissListener(TopPopUpDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }


    public interface TopPopUpDismissListener {
        void onNeedToDismiss();
    }
}
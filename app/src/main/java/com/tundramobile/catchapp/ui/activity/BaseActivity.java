package com.tundramobile.catchapp.ui.activity;

import android.content.IntentFilter;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;
import com.tundramobile.catchapp.net.NetworkReceiver;
import com.tundramobile.catchapp.ui.view.LoadingDialog;

/**
 * Created by eugenetroyanskii on 29.03.17.
 */

public class BaseActivity extends AppCompatActivity implements ProgressDialogHandler, NetworkReceiver.NetworkStateReceiverListener {

    private LoadingDialog loadingDialog;
    private NetworkReceiver networkReceiver;

    @Override
    public void showProgressDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);
        }
        loadingDialog.showProgressDialog();
    }


    @Override
    public void cancelProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.cancelProgressDialog();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterNetworkListener();
        cancelProgressDialog();
    }


    public void addFragment(@IdRes int containerViewId, @NonNull Fragment fragment, String fragmentTag) {
        Navigator.with(this)
                .disallowAddToBackStack()
                .tag(fragmentTag)
                .add(fragment);
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerNetworkListener();
    }


    public void registerNetworkListener() {
        if (networkReceiver == null) {
            networkReceiver = new NetworkReceiver(this);
            registerReceiver(networkReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }


    public void unregisterNetworkListener() {
        if (networkReceiver != null) {
            unregisterReceiver(networkReceiver);
            networkReceiver = null;
        }
    }


    @Override
    public void networkAvailable() {

    }


    @Override
    public void networkUnavailable() {
        cancelProgressDialog();
    }
}

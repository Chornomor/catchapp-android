package com.tundramobile.catchapp.ui.fragment.date_picker;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.tundramobile.catchapp.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by andreybofanov on 24.04.17.
 */

public class MonthPage extends LinearLayout implements View.OnClickListener {

    ArrayList<LinearLayout> rows= new ArrayList<>(6);
    ArrayList<View> dateViews= new ArrayList<>(6*7);
    LayoutParams params;
    int month,year;
    int startDayOfWeek;
    int maxDays;
    int days;
    int count;
    int selectedDay;

    DateChangedListener dateChangedListener;
    DateFormatProvider dateFormatProvider;
    LayoutInflater inflater;

    public MonthPage(Context context) {
        super(context);
        init();
    }

    public MonthPage(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        inflater = LayoutInflater.from(getContext());
        setOrientation(VERTICAL);
        params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.weight = 1;

        for(int i = 0; i < 6; i++){
            LinearLayout linearLayout = new LinearLayout(getContext());
            rows.add(linearLayout);
            addView(linearLayout,params);
        }
    }

    public void setDateChangedListener(DateChangedListener dateChangedListener) {
        this.dateChangedListener = dateChangedListener;
    }

    public void setDateFormatProvider(DateFormatProvider dateFormatProvider) {
        this.dateFormatProvider = dateFormatProvider;
    }

    public void setup(int month, int year){
        this.month = month;
        this.year = year;
        setupCounts();
        dateViews.clear();
        for(LinearLayout linearLayout : rows){
            fillUp(linearLayout);
        }
        update();
    }



    void setupCounts(){
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.set(year,month,1,0,0,0);
        startDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        count = calendar.getFirstDayOfWeek();
        if(count == 2 && startDayOfWeek == 1){
            startDayOfWeek = 8;
        }
        days = 1;
    }

    void fillUp(LinearLayout linearLayout){
        linearLayout.removeAllViews();
        for(int i = 0; i < 7; i ++){
            linearLayout.addView(getView(),params);
        }
    }

    View getView(){
        if(count < startDayOfWeek || days > maxDays){
            count++;
            if(dateFormatProvider != null){
                return dateFormatProvider.inflateEmptyView(inflater);
            }
            return new View(getContext());
        }
        View v;
        if(dateFormatProvider != null){
            v = dateFormatProvider.inflateView(inflater);
        } else {
            v = new TextView(getContext());
        }
        v.setTag(days);
        v.setTag(R.id.tag_day,days);
        v.setOnClickListener(this);
        dateViews.add(v);
        days++;
        count++;
        return v;
    }


    @Override
    public void onClick(View v) {
        selectedDay = (int) v.getTag(R.id.tag_day);
        if(dateChangedListener != null){
            Calendar calendar = new GregorianCalendar(year,month,selectedDay);
            dateChangedListener.onDateSelected(calendar.getTime());
        }
        dispatchSetSelected(false);
        v.setSelected(true);
    }

    public void setSelectedDay(int selectedDay) {
        this.selectedDay = selectedDay;
        Calendar calendar = new GregorianCalendar(year,month,selectedDay);
        if(dateChangedListener != null) {
            dateChangedListener.onDateSelected(calendar.getTime());
        }
        dispatchSetSelected(false);
        findViewWithTag(selectedDay).setSelected(true);
        invalidate();
    }


    @Override
    public void dispatchSetSelected(boolean selected) {
        super.dispatchSetSelected(selected);
        for(LinearLayout row : rows){
            row.dispatchSetSelected(selected);
        }
    }

    void update(){
        Calendar calendar =  new GregorianCalendar(year,month,1);
        dispatchSetSelected(false);
        for(View view : dateViews){
            updateView(calendar,view);
        }
    }

    void updateView(Calendar calendar,View view){
        Integer day = (Integer) view.getTag(R.id.tag_day);
        if(dateFormatProvider != null){
            calendar.set(Calendar.DATE,day);
            if(day == selectedDay){
                view.setSelected(true);
            }
            dateFormatProvider.formatView(view,calendar.getTime());
        } else {
            updateSimpleText((TextView) view,day);
        }
    }

    void updateSimpleText(TextView textView, int days){
        textView.setText(String.valueOf(days));
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.CENTER);
        textView.setOnClickListener(this);
        textView.setTag(R.id.tag_day,days);
    }
}

package com.tundramobile.catchapp.ui.fragment.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.manager.SharedPrefManager;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rebus.permissionutils.PermissionEnum;

/**
 * Created by eugenetroyanskii on 21.04.17.
 */

public class ManagePermissionsFragment extends BaseFragment {

    @BindView(R.id.manage_perm_calendar_state) ToggleButton calendarState;
    @BindView(R.id.manage_perm_camera_state) ToggleButton cameraState;
    @BindView(R.id.manage_perm_contacts_state) ToggleButton contactsState;
    @BindView(R.id.manage_perm_location_state) ToggleButton locationState;
    @BindView(R.id.manage_perm_photos_state) ToggleButton photosState;
    @BindView(R.id.manage_perm_notification_state) ToggleButton notificationsState;

    private Unbinder unbinder;
    private SharedPrefManager sharedPrefManager;


    public static ManagePermissionsFragment newInstance() {
        return new ManagePermissionsFragment();
    }


    public ManagePermissionsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manage_permission, container, false);
        unbinder = ButterKnife.bind(this, v);
        sharedPrefManager = SharedPrefManager.getInstance(getActivity());
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        setupPermissions();
        setTypeface();
    }


    private void setupPermissions() {
        cameraState.setChecked(FastPermission.isGranted(getContext(), PermissionEnum.CAMERA));
        locationState.setChecked(FastPermission.isGranted(getContext(), PermissionEnum.ACCESS_FINE_LOCATION)
                && FastPermission.isGranted(getContext(), PermissionEnum.ACCESS_COARSE_LOCATION));
        calendarState.setChecked(FastPermission.isGranted(getContext(), PermissionEnum.READ_CALENDAR));
        contactsState.setChecked(FastPermission.isGranted(getContext(), PermissionEnum.READ_CONTACTS));
        photosState.setChecked(FastPermission.isGranted(getContext(), PermissionEnum.READ_EXTERNAL_STORAGE));
        notificationsState.setChecked(sharedPrefManager.isNotificationEnabled());
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            cameraState.setClickable(true);
            locationState.setClickable(true);
            calendarState.setClickable(true);
            contactsState.setClickable(true);
            photosState.setClickable(true);
        }
    }


    /*we have to set typeface because it's ignored from style */
    private void setTypeface() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.montserrat_light));
        calendarState.setTypeface(typeface);
        cameraState.setTypeface(typeface);
        contactsState.setTypeface(typeface);
        locationState.setTypeface(typeface);
        photosState.setTypeface(typeface);
        notificationsState.setTypeface(typeface);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.manage_perm_calendar_state)
    public void onCalendarClick() {
        if (FastPermission.isGranted(getActivity(), PermissionEnum.READ_CALENDAR)) {
            showInfoDialog(getString(R.string.profile_info_dialog_calendar));
        } else {
            FastPermission.request(getActivity(), null, PermissionEnum.READ_CALENDAR);
        }
        calendarState.setChecked(true);
    }


    @OnClick(R.id.manage_perm_camera_state)
    public void onCameraClick() {
        if (FastPermission.isGranted(getActivity(), PermissionEnum.CAMERA)) {
            showInfoDialog(getString(R.string.profile_info_dialog_camera));
        } else {
            FastPermission.request(getActivity(), null, PermissionEnum.CAMERA);
        }
        cameraState.setChecked(true);
    }


    @OnClick(R.id.manage_perm_contacts_state)
    public void onContactsClick() {
        if (FastPermission.isGranted(getActivity(), PermissionEnum.READ_CONTACTS)) {
            showInfoDialog(getString(R.string.profile_info_dialog_contacts));
        } else {
            FastPermission.request(getActivity(), null, PermissionEnum.READ_CONTACTS);
        }
        contactsState.setChecked(true);
    }


    @OnClick(R.id.manage_perm_location_state)
    public void onLocationClick() {
        if (FastPermission.isGranted(getActivity(), PermissionEnum.ACCESS_COARSE_LOCATION, PermissionEnum.ACCESS_FINE_LOCATION)) {
            showInfoDialog(getString(R.string.profile_info_dialog_location));
        } else {
            FastPermission.request(getActivity(), null, PermissionEnum.ACCESS_COARSE_LOCATION);
        }
        locationState.setChecked(true);
    }


    @OnClick(R.id.manage_perm_photos_state)
    public void onPhotosClick() {
        if (FastPermission.isGranted(getActivity(), PermissionEnum.READ_EXTERNAL_STORAGE)) {
            showInfoDialog(getString(R.string.profile_info_dialog_storage));
        } else {
            FastPermission.request(getActivity(), null, PermissionEnum.READ_EXTERNAL_STORAGE);
        }
        photosState.setChecked(true);
    }


    @OnClick(R.id.manage_perm_notification_state)
    public void onNotificationClick() {
        if(sharedPrefManager.isNotificationEnabled()) {
            sharedPrefManager.setNotificationEnabled(false);
        } else {
            sharedPrefManager.setNotificationEnabled(true);
        }
    }


    @OnClick(R.id.profile_back_btn)
    public void onBack() {
        getActivity().onBackPressed();
    }


    @OnClick(R.id.profile_go_to_settings)
    public void onSettings() {
        startActivity(new Intent(Settings.ACTION_SETTINGS));
    }


    private void showInfoDialog(String messageTarget) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.profile_dialog_info_title))
                .setMessage(getString(R.string.profile_info_dialog_app_uses) + " "
                        + messageTarget + " "
                        + getString(R.string.profile_info_dialog_can_allow) + " "
                        + messageTarget + " "
                        + getString(R.string.profile_info_dialog_to_setting) + " "
                        + messageTarget + " "
                        + getString(R.string.profile_info_dialog_to_access))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.profile_info_dialog_ok_btn),
                        (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }
}
package com.tundramobile.catchapp.ui.viewmodel;

import com.tundramobile.catchapp.entity.response.Notification;
import com.tundramobile.catchapp.manager.CatchUpManager;
import com.tundramobile.catchapp.other.TimeUtils;


/**
 * Created by Sviatoslav Zaitsev on 21.04.17.
 */

public class NotificationItem {
    public long id;
    private String photo;
    private String message;
    private String when;
    private String user;
    private String catchUp;
    public int catchUpId;
    private String notificationType;

    public NotificationItem(String photo, String message, String when) {
        this.photo = photo;
        this.message = message;
        this.when = when;
    }

    public NotificationItem(Notification notification) {
        this.id = notification.getId();
        this.photo = notification.getUser().getProfilePicUrl();
        this.when = TimeUtils.getRelativeTime(notification.getModified());
        this.user = notification.getUser().getName();
        this.catchUp = notification.getCatchUp() != null
                ? notification.getCatchUp().getName()
                : "";
        catchUpId = notification.getCatchUp().getId();
        this.message = compileMessage(notification.getNotificationType());
    }

    public String getIdString(){
        return String.valueOf(id);
    }

    public String getPhoto() {
        return photo;
    }

    public String getMessage() {
        return message;
    }

    public String getWhen() {
        return when;
    }

    public String getUser() {
        return user;
    }

    public String getCatchUp() {
        return catchUp;
    }

    public int getCatchupType(){
        switch (notificationType) {
            case "NOTIFICATIONTYPE_ACCEPT_YOURS":
                return CatchUpManager.CONFIRMED;
            case "NOTIFICATIONTYPE_INVITED":
                return CatchUpManager.INCOMING;
            default:
                return -1;
        }
    }

    // TODO: 23.04.17 Translation
    private String compileMessage(String notificationType) {
        String message = "";
        message = user + " ";
        this.notificationType = notificationType;
        switch (notificationType) {
            case "NOTIFICATIONTYPE_ACCEPT_YOURS":
                message += "has accepted your invitation to";
                break;
            case "NOTIFICATIONTYPE_INVITED":
                message += "sent you an event invitation to";
                break;
            case "NOTIFICATIONTYPE_DELETEDCOMPLETELY":
                message += "has deleted";
                break;
            case "NOTIFICATIONTYPE_DELETEDSLOT":
                message += "has deleted timeslot for";
                break;
            case "NOTIFICATIONTYPE_LEFTCATCHUP":
                message += "has left";
                break;
            case "NOTIFICATIONTYPE_USERJOINED":
                message += "has joined";
                break;
            case "NOTIFICATIONTYPE_WELCOME":
                message += "Welcome to CatchApp";
                break;
            default:
                message += "";
                break;
        }
        message += " " + catchUp;
        message = message.trim();
        return message;
    }
}

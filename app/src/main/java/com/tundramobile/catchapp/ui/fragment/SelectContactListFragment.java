package com.tundramobile.catchapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.event.GetContactsEvent;
import com.tundramobile.catchapp.interfaces.RecyclerItemClickListener;
import com.tundramobile.catchapp.interfaces.SelectorListener;
import com.tundramobile.catchapp.interfaces.TabDataI;
import com.tundramobile.catchapp.manager.ContactManager;
import com.tundramobile.catchapp.manager.Navigator;
import com.tundramobile.catchapp.ui.adapter.ContactsRecyclerAdapter;
import com.tundramobile.catchapp.ui.adapter.holder.ContactHolder;
import com.tundramobile.catchapp.ui.view.AppToolbar;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by eugenetroyanskii on 18.04.17.
 */

public class SelectContactListFragment extends BaseFragment implements TabDataI, RecyclerItemClickListener<Contact> {

    private static final String CONTACTS = "CONTACTS";

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.searchInput) EditText searchInput;


    private ContactsRecyclerAdapter adapter;
    private SelectorListener selectorListener;
    private Unbinder unbinder;
    public TabData tabData;
    private int type;

    private boolean onlyRegistered;


    public static SelectContactListFragment newInstance(TabData tabData, int type) {
        SelectContactListFragment fragment = new SelectContactListFragment();
        fragment.setTabData(tabData);
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            type = savedInstanceState.getInt(TYPE);
        } else if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
        }
        Bundle args = Navigator.with(getActivity()).popData("registered");
        if(args != null) {
            onlyRegistered = args.getBoolean("registered");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact_list, container, false);
        unbinder = ButterKnife.bind(this, v);
        setUpRecyclerView();
        initSearchField();
        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (type != ContactManager.GROUPS)
            EventBus.getDefault().post(new GetContactsEvent(type));

        if (savedInstanceState != null) {
            setContacts(savedInstanceState.getParcelableArrayList(CONTACTS));
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SelectorListener) {
            selectorListener = (SelectorListener) context;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    protected void onToolbarChanged(AppToolbar toolbar) {
    }


    public void setContacts(ArrayList<Contact> contacts) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if(onlyRegistered) {
                    ArrayList<Contact> result = new ArrayList<>();
                    for (Contact curContact : contacts) {
                        if(curContact.getContactType() == Contact.REGISTERED_CONTACT_IN_SYSTEM) {
                            result.add(curContact);
                        }
                    }
                    if (adapter != null) adapter.setItems(result);
                } else {
                    if (adapter != null) adapter.setItems(contacts);
                }


            });
        }
    }


    private void initSearchField() {
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null) adapter.getFilter().filter(s);
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void setUpRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);

        adapter = new ContactsRecyclerAdapter(this);
        recyclerView.setAdapter(adapter);
    }


    public ContactsRecyclerAdapter getAdapter() {
        return adapter;
    }


    public void selectItemByPosition(int position, Contact contact) {
        adapter.setSelected(getViewHolderByPosition(position), contact);
    }


    public void selectItemByContact(Contact contact) {
        int position = -1;
        for (int i = 0; i < adapter.getContacts().size(); i++) {
            Contact item = adapter.getContacts().get(i);
            if (item.equals(contact)) {
                position = i;
            }
        }
        if (position >= 0) {
            adapter.setSelected(getViewHolderByPosition(position), contact);
        }
    }


    @Override
    public void setTabData(TabData tabData) {
        this.tabData = tabData;
    }


    @Override
    public TabData getTabData() {
        return tabData;
    }


    @Override
    public void onItemClick(int position, Contact model) {
        if (selectorListener != null) {
            selectorListener.onItemSelected(model, position, adapter.setSelected(getViewHolderByPosition(position), model));
        }
    }


    private ContactHolder getViewHolderByPosition(int position) {
        if (recyclerView != null) {
            return ((ContactHolder) recyclerView.findViewHolderForAdapterPosition(position));
        } else return null;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(TYPE, type);
        outState.putParcelableArrayList(CONTACTS, adapter.getContacts());
        super.onSaveInstanceState(outState);
    }
}

package com.tundramobile.catchapp.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.pushwoosh.BaseRegistrationReceiver;
import com.pushwoosh.PushManager;
import com.tundramobile.catchapp.BuildConfig;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.LoginRespondOTP;
import com.tundramobile.catchapp.manager.MyUserManager;
import com.tundramobile.catchapp.manager.SharedPrefManager;
import com.tundramobile.catchapp.manager.TopPopUpManager;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.other.DateUtil;
import com.tundramobile.catchapp.other.Dialogs;
import com.tundramobile.catchapp.firebase.FirebaseUtils;
import com.tundramobile.catchapp.other.Regex;
import com.tundramobile.catchapp.ui.activity.authorization.NumberActivity;
import com.tundramobile.catchapp.ui.activity.authorization.UserNameActivity;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by eugenetroyanskii on 29.03.17.
 */

public class SplashActivity extends BaseActivity {

    private final String TAG = "SplashActivity";

    private User currentUser;
    private SharedPrefManager spManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        spManager = SharedPrefManager.getInstance(this);
        currentUser = MyUserManager.getCurrentUser(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceivers();
        PushManager pushManager = PushManager.getInstance(this);
        try {
            pushManager.onStartup(this);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        pushManager.registerForPushNotifications();
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceivers();
    }


    public void registerReceivers() {
        registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));
    }


    public void unregisterReceivers() {
        try {
            unregisterReceiver(mBroadcastReceiver);
        } catch (Exception e) {
            //pass through
        }
    }


    BroadcastReceiver mBroadcastReceiver = new BaseRegistrationReceiver() {
        @Override
        public void onRegisterActionReceive(Context context, Intent intent) {
            checkMessage(intent);
        }
    };


    private void checkMessage(Intent intent) {
        if (null != intent) {
            if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT)) {
                showMessage("push message is " + intent.getExtras().getString(PushManager.PUSH_RECEIVE_EVENT));
            } else if (intent.hasExtra(PushManager.REGISTER_EVENT)) {
                Log.e(TAG, "register");
            } else if (intent.hasExtra(PushManager.UNREGISTER_EVENT)) {
                showMessage("unregister");
            } else if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT)) {
                showMessage("register error");
            } else if (intent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT)) {
                showMessage("unregister error");
            }

            resetIntentValues();
        }
    }


    /**
     * Will check main Activity intent and clear it of any PushWoosh data
     */
    private void resetIntentValues() {
        Intent mainAppIntent = getIntent();

        if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT)) {
            mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
        } else if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT)) {
            mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
        } else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT)) {
            mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
        } else if (mainAppIntent.hasExtra(PushManager.REGISTER_ERROR_EVENT)) {
            mainAppIntent.removeExtra(PushManager.REGISTER_ERROR_EVENT);
        } else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT)) {
            mainAppIntent.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
        }

        setIntent(mainAppIntent);
    }


    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    HandledCallback<ListResponseModel<User>> fetchUserCallback = new HandledCallback<ListResponseModel<User>>() {
        @Override
        public void onSuccess(Call<ListResponseModel<User>> call, Response<ListResponseModel<User>> response) {
            cancelProgressDialog();
            if (response.body() == null || !Regex.isEmailValid(currentUser.getHomeEmail())) {
                startLogin();
            } else {
                if (response.body().getResult().size() > 0) {
                    User currentUser = response.body().getResult().get(0);
                    MyUserManager.setCurrentUser(SplashActivity.this, currentUser);
                    if (!currentUser.getHomeEmail().isEmpty() && Regex.isEmailValid(currentUser.getHomeEmail())) {
                        authWithFirebase(currentUser.getMobile());
                    }
                } else {
                    startLogin();
                }
            }
        }


        @Override
        public void onFail(Call<ListResponseModel<User>> call, Response<ListResponseModel<User>> response) {
            TopPopUpManager.showTopPopUp(SplashActivity.this, "Token incorrect or invalid. Please login.", !TopPopUpManager.isError);
            startLogin();
        }


        @Override
        public void onProcessFailed(Call<ListResponseModel<User>> call, Throwable t, String error) {
            TopPopUpManager.showTopPopUp(SplashActivity.this, error, TopPopUpManager.isError);
        }
    };


    private void enterInApp() {
        if (BuildConfig.SKIP_REGISTRATION) {
            Intent intent = new Intent(SplashActivity.this, PermissionsAccessActivity.class);
            startActivity(intent);
        } else {
            DateUtil.updateTimeFormat(this);
            if (spManager.getIsIntroShowed()) {
                if (spManager.getUserToken().isEmpty()) {
                    startLogin();
                } else {
                    MyUserManager.getInstance().fetchUser(new LoginRespondOTP(spManager.getUserToken(), MyUserManager.getCurrentUser(SplashActivity.this).getId()), fetchUserCallback);
                }
            } else {
                Intent mainIntent = new Intent(SplashActivity.this, IntroActivity.class);
                startActivity(mainIntent);
            }
        }
    }

    private void authWithFirebase(String mobile) {
        FirebaseUtils.authorize(
                mobile,
                successResult -> startMainActivity(),
                exception -> Dialogs.showFailureDialog(this, exception.getLocalizedMessage())
        );
    }


    private void startMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void startLogin() {
        Intent intent = new Intent(SplashActivity.this, NumberActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public void networkAvailable() {
        enterInApp();
    }
}

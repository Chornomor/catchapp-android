package com.tundramobile.catchapp.ui.fragment.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.manager.SharedPrefManager;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by eugenetroyanskii on 24.04.17.
 */

public class NotificationSettingsFragment extends BaseFragment {

    @BindView(R.id.profile_email_notification_switch) Switch emailNotification;
    @BindView(R.id.profile_notification1_switch) Switch notification1;
    @BindView(R.id.profile_notification2_switch) Switch notification2;
    @BindView(R.id.profile_notification3_switch) Switch notification3;
    @BindView(R.id.profile_notification4_switch) Switch notification4;

    private Unbinder unbinder;
    private SharedPrefManager sharedPrefManager;


    public static NotificationSettingsFragment newInstance() {
        return new NotificationSettingsFragment();
    }


    public NotificationSettingsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_notification, container, false);
        unbinder = ButterKnife.bind(this, v);
        sharedPrefManager = SharedPrefManager.getInstance(getActivity());
        setupSwitches();
        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void setupSwitches() {
        emailNotification.setChecked(sharedPrefManager.isEmailNotificationEnabled());
        notification1.setChecked(sharedPrefManager.isSettingsNotification1Enabled());
        notification2.setChecked(sharedPrefManager.isSettingsNotification2Enabled());
        notification3.setChecked(sharedPrefManager.isSettingsNotification3Enabled());
        notification4.setChecked(sharedPrefManager.isSettingsNotification4Enabled());
    }


    @OnClick(R.id.profile_back_btn)
    public void onBack() {
        onBackPressed();
    }


    @OnClick(R.id.profile_save_btn)
    public void onSave() {
        sharedPrefManager.saveEmailNotification(emailNotification.isChecked());
        sharedPrefManager.saveSettingsNotification1(notification1.isChecked());
        sharedPrefManager.saveSettingsNotification2(notification2.isChecked());
        sharedPrefManager.saveSettingsNotification3(notification3.isChecked());
        sharedPrefManager.saveSettingsNotification4(notification4.isChecked());
        onBackPressed();
    }


    @OnClick(R.id.profile_email_notification_switch)
    public void onEmailNotification() {

    }


    @OnClick(R.id.profile_notification1_switch)
    public void onNotification1() {

    }


    @OnClick(R.id.profile_notification2_switch)
    public void onNotification2() {

    }


    @OnClick(R.id.profile_notification3_switch)
    public void onNotification3() {

    }


    @OnClick(R.id.profile_notification4_switch)
    public void onNotification4() {

    }
}

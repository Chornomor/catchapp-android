package com.tundramobile.catchapp.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.github.mmin18.widget.RealtimeBlurView;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.FabOption;
import com.tundramobile.catchapp.interfaces.FabHandler;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.fragment.BaseFabFragment;

import java.util.ArrayList;

import butterknife.BindView;

public class FabActivity extends StackActivity implements FabHandler, Animation.AnimationListener {

    private static final String TAG = FabActivity.class.getSimpleName();
    private static final String APPLY_BLUR = "APPLY_BLUR";
    private static final String FAB_OPTIONS = "FAB_OPTIONS";
    private static final String FAB_ICON = "FAB_ICON";

    @Nullable @BindView(R.id.expandableView) View expandableView;
    @Nullable @BindView(R.id.mainFab) FloatingActionButton fab;
    @Nullable @BindView(R.id.fabContainer1) View fabContainer1;
    @Nullable @BindView(R.id.fabContainer2) View fabContainer2;
    @Nullable @BindView(R.id.fabContainer3) View fabContainer3;
    @Nullable @BindView(R.id.fabOption1) FloatingActionButton fabOption1Btn;
    @Nullable @BindView(R.id.fabOption2) FloatingActionButton fabOption2Btn;
    @Nullable @BindView(R.id.fabOption3) FloatingActionButton fabOption3Btn;
    @Nullable @BindView(R.id.fabOption1Text) TextView fabOption1Text;
    @Nullable @BindView(R.id.fabOption2Text) TextView fabOption2Text;
    @Nullable @BindView(R.id.fabOption3Text) TextView fabOption3Text;
    @Nullable @BindView(R.id.blurView) RealtimeBlurView blur;

    private boolean isFabOpen = false;
    private Animation fab_open;
    private Animation fab_close;
    private Animation rotate_forward;
    private Animation rotate_backward;
    private boolean hasFabOptions;
    private int optionsCount;
    private boolean applyBlur;
    private ArrayList<FabOption> fabOptions;
    private int mainFabIcon;
    private boolean isFabAnimationRun;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initAnimations();
    }


    private void initAnimations() {
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        rotate_backward.setAnimationListener(this);
        fab_close.setAnimationListener(this);
        fab_open.setAnimationListener(this);
    }


    public void animateFAB() {
        if (!isFabAnimationRun) {
            isFabAnimationRun = true;
            if (fab != null) fab.startAnimation(isFabOpen ? rotate_backward : rotate_forward);
            animateOption(fabOption1Btn, 0);
            animateOption(fabOption2Btn, 1);
            animateOption(fabOption3Btn, 2);
            startAnimationFinishedChecker();
            isFabOpen = !isFabOpen;
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        isFabAnimationRun = false;
    }


    private void startAnimationFinishedChecker() {
        new Handler().postDelayed(() -> isFabAnimationRun = false, 400);
    }


    private void animateOption(FloatingActionButton fab, int position) {
        if (fab != null && optionsCount > position) {
            fab.startAnimation(isFabOpen ? fab_close : fab_open);
            fab.setClickable(!isFabOpen);
        }
    }


    @Override
    public void setupFabOptions(ArrayList<FabOption> fabOptions, int fabIcon, boolean applyBlur) {
        this.applyBlur = applyBlur;
        this.fabOptions = fabOptions;
        this.mainFabIcon = fabIcon;
        if (fab != null) fab.setImageResource(fabIcon);

        if (fabOptions != null && !fabOptions.isEmpty()) {
            hasFabOptions = true;
            optionsCount = fabOptions.size();

            prepareFabOption(fabOption1Btn, fabOption1Text, fabOptions.get(0));

            if (optionsCount > 1) {
                prepareFabOption(fabOption2Btn, fabOption2Text, fabOptions.get(1));
            }

            if (optionsCount > 2) {
                prepareFabOption(fabOption3Btn, fabOption3Text, fabOptions.get(2));
            }
        } else {
            hasFabOptions = false;
        }
    }


    private void prepareFabOption(FloatingActionButton fab, TextView title, FabOption fabOption) {
        if (fab != null) fab.setImageResource(fabOption.getIcon());
        if (title != null) title.setText(fabOption.getTitle());
    }


    @Override
    public void hideFab() {
        Utils.loge(TAG, "hideFab");
        if (expandableView != null) expandableView.setVisibility(View.INVISIBLE);
    }


    @Override
    public void showFab() {
        Utils.loge(TAG, "showFab");
        if (expandableView != null) expandableView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == fab_close) {
            setOptionTextVisible(fabOption1Text, 0, false);
            setOptionTextVisible(fabOption2Text, 1, false);
            setOptionTextVisible(fabOption3Text, 2, false);
        }

        new Handler().postDelayed(() -> onAnimationEnd(animation), animation.getDuration() + 50);
    }


    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == fab_open) {
            setOptionTextVisible(fabOption1Text, 0, true);
            setOptionTextVisible(fabOption2Text, 1, true);
            setOptionTextVisible(fabOption3Text, 2, true);
        }

        if (animation == rotate_backward) {
            if (blur != null) blur.setVisibility(View.INVISIBLE);
        }
    }


    private void setOptionTextVisible(TextView text, int position, boolean isVisible) {
        if (text != null) {
            if (isVisible && optionsCount > position) {
                text.setVisibility(View.VISIBLE);
            } else {
                text.setVisibility(View.INVISIBLE);
            }
        }
    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    protected void onFabClicked() {
        if (applyBlur && blur != null && blur.getVisibility() == View.INVISIBLE) {
            blur.setVisibility(View.VISIBLE);
        }

        if (hasFabOptions) {
            animateFAB();
        }
    }


    @Override
    public boolean isFabOpen() {
        return isFabOpen;
    }


    @Override
    public void hideFabIfNotRequired() {
        if (getCurrentFragment() != null && !(getCurrentFragment() instanceof BaseFabFragment)) {
            Utils.logd(TAG, "hideFabIfNotRequired: " + getCurrentFragment());
            hideFab();
        }
    }


    @Override
    public void onBackPressed() {
        if (isFabOpen) {
            animateFAB();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Utils.logd(TAG, "onSaveInstanceState");
        outState.putParcelableArrayList(FAB_OPTIONS, fabOptions);
        outState.putInt(FAB_ICON, mainFabIcon);
        outState.getBoolean(APPLY_BLUR, applyBlur);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Utils.logd(TAG, "onRestoreInstanceState");
        fabOptions = savedInstanceState.getParcelableArrayList(FAB_OPTIONS);
        mainFabIcon = savedInstanceState.getInt(FAB_ICON);
        applyBlur = savedInstanceState.getBoolean(APPLY_BLUR);
        setupFabOptions(fabOptions, mainFabIcon, applyBlur);
    }
}

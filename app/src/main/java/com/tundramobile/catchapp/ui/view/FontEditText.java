package com.tundramobile.catchapp.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.other.Utils;

public class FontEditText extends AppCompatEditText {

    private final static String TAG = FontEditText.class.getSimpleName();


    public FontEditText(Context context) {
        super(context);
        setCustomFontConstructor(context, null);
    }


    public FontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFontConstructor(context, attrs);
    }


    public FontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFontConstructor(context, attrs);
    }


    private void setCustomFontConstructor(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.FontTextView);
        String customFont = a.getString(R.styleable.FontTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }


    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = Utils.getTypeface(ctx, asset);
        if (tf != null) {
            setTypeface(tf);
//            Utils.logd(TAG, "setCustomFont: " + asset);
            return true;
        } else {
//            Utils.loge(TAG, "Failed to set font. String val: " + getText().toString());
            return false;
        }
    }
}

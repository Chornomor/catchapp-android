package com.tundramobile.catchapp.manager;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tundramobile.catchapp.BuildConfig;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.request.RequestOTPBody;
import com.tundramobile.catchapp.entity.request.RequestUpdateUser;
import com.tundramobile.catchapp.entity.request.RequestUpdateUserPhoto;
import com.tundramobile.catchapp.entity.request.RespondOTP;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.LoginRequestOTP;
import com.tundramobile.catchapp.entity.response.LoginRespondOTP;
import com.tundramobile.catchapp.entity.response.UpdateUserResponse;
import com.tundramobile.catchapp.interfaces.UpdateUserPhotoListener;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.NetworkManager;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.ui.activity.MainActivity;
import com.tundramobile.catchapp.ui.activity.PermissionsAccessActivity;
import com.tundramobile.catchapp.ui.activity.authorization.UserNameActivity;

import java.io.File;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alexchern on 31.03.17.
 */

public class MyUserManager {

    private static volatile MyUserManager instance;

    private User currentUser;
    private String currentUserToken;

    private UpdateUserPhotoListener updateUserPhotoListener;


    public static User getCurrentUser(Context context) {
        return getInstance().getUser(context);
    }


    public static void setCurrentUser(Context context, User user) {
        getInstance().setUser(context, user);
    }


    private void setUser(Context context, User user) {
        this.currentUser = user;
        storeUser(context);
    }


    public User getUser(Context context) {
        if (currentUser == null) {
            currentUser = getStoredUser(context);
        }
        if (currentUser == null) {
            currentUser = new User();
        }
        if (BuildConfig.USER_ID != -1) {
            currentUser.setId(BuildConfig.USER_ID);
        }
        return currentUser;
    }


    private void storeUser(Context context) {
        String user = new Gson().toJson(currentUser);
        SharedPrefManager.getInstance(context).storeCurrentUser(user);
    }


    private User getStoredUser(Context context) {
        String currentUser = SharedPrefManager.getInstance(context).getCurrentUser();
        return new Gson().fromJson(currentUser, User.class);
    }


    private MyUserManager() {
    }


    public static MyUserManager getInstance() {
        if (instance == null) {
            synchronized (MyUserManager.class) {
                if (instance == null) {
                    instance = new MyUserManager();
                }
            }
        }
        return instance;
    }


    public String getCurrentUserToken(Context context) {
        if (BuildConfig.USER_TOKEN != null) {
            return BuildConfig.USER_TOKEN;
        } else {
            if (currentUserToken == null) {
                currentUserToken = SharedPrefManager.getInstance(context).getUserToken();
            }
            return currentUserToken;
        }
    }


    public void setCurrentUserToken(Context context, String currentUserToken) {
        SharedPrefManager.getInstance(context).storeUserToken(currentUserToken);
        this.currentUserToken = currentUserToken;
    }


    public void requestOTP(RequestOTPBody otpBody, HandledCallback<LoginRequestOTP> phoneCallback) {
        RESTClient.getInstance().requestOTP(otpBody, phoneCallback);
    }


    public void respondOTP(RespondOTP otpBody, HandledCallback<LoginRespondOTP> pinCallback) {
        RESTClient.getInstance().respondOTP(otpBody, pinCallback);
    }


    public void fetchUser(LoginRespondOTP userData, Callback<ListResponseModel<User>> callback) {
        RESTClient.getInstance().fetchUser(userData, callback);
    }

//    public void fetchListUsersByIds(String token, int myId, List<String> requestingUsersIds, Callback<ListResponseModel<User>> callback) {
//        StringBuilder sb = new StringBuilder();
//        for(int i = 0; i < requestingUsersIds.size(); ++i) {
//            if (requestingUsersIds.size() == i + 1) {
//                sb.append(requestingUsersIds.get(i));
//            } else {
//                sb.append(requestingUsersIds.get(i)).append(",");
//            }
//        }
//        RESTClient.getInstance().fetchUsersByIds(token, myId, sb.toString(), callback);
//    }


    public void updateUser(RequestUpdateUser updateUserModel, HandledCallback<UpdateUserResponse> updateUserCallback) {
        RESTClient.getInstance().updateUser(updateUserModel, updateUserCallback);
    }


    public void logout(String token, HandledCallback<UpdateUserResponse> logoutCallback) {
        RESTClient.getInstance().logout(token, logoutCallback);
    }


    public void uploadUserPhoto(String token, File userPhoto, UpdateUserPhotoListener updateUserPhotoListener) {
        this.updateUserPhotoListener = updateUserPhotoListener;
        RESTClient.getInstance().uploadUserPhoto(token, userPhoto, updatePhotoUserCallback);
    }


    // TODO: 25.04.17 implement correct errors
    private HandledCallback<UpdateUserResponse> updatePhotoUserCallback = new HandledCallback<UpdateUserResponse>() {
        @Override
        public void onSuccess(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            RequestUpdateUserPhoto updateUserModel = new RequestUpdateUserPhoto(currentUserToken, currentUser, response.body().getSuccess());
            RESTClient.getInstance().updateUserPhoto(updateUserModel, updateUserCallback);
        }

        @Override
        public void onFail(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            updateUserPhotoListener.onUserUpdated(null, "Something was wrong when try to upload photo. Please try another one.");
        }


        @Override
        public void onProcessFailed(Call<UpdateUserResponse> call, Throwable t, String error) {
            updateUserPhotoListener.onUserUpdated(null, error);
        }
    };


    private HandledCallback<UpdateUserResponse> updateUserCallback = new HandledCallback<UpdateUserResponse>() {
        @Override
        public void onSuccess(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            if (response.body().getSuccess() == (currentUser.getId())) {
                RESTClient.getInstance().fetchUser(new LoginRespondOTP(currentUserToken, currentUser.getId()), fetchUserCallback);
            } else {
                updateUserPhotoListener.onUserUpdated(null, "Invalid token.");
            }
        }

        @Override
        public void onFail(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
            updateUserPhotoListener.onUserUpdated(null, "Something was wrong when try update user.");
        }


        @Override
        public void onProcessFailed(Call<UpdateUserResponse> call, Throwable t, String error) {
            updateUserPhotoListener.onUserUpdated(null, error);
        }
    };


    private HandledCallback<ListResponseModel<User>> fetchUserCallback = new HandledCallback<ListResponseModel<User>>() {
        @Override
        public void onSuccess(Call<ListResponseModel<User>> call, Response<ListResponseModel<User>> response) {
            User currentUser = response.body().getResult().get(0);
            updateUserPhotoListener.onUserUpdated(currentUser, "");
        }


        @Override
        public void onFail(Call<ListResponseModel<User>> call, Response<ListResponseModel<User>> response) {
            updateUserPhotoListener.onUserUpdated(null, "Something was wrong when try to fetch user.");
        }


        @Override
        public void onProcessFailed(Call<ListResponseModel<User>> call, Throwable t, String error) {
            updateUserPhotoListener.onUserUpdated(null, error);
        }
    };
}

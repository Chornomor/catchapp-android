package com.tundramobile.catchapp.manager;

import android.support.v4.app.FragmentManager;

import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.TabData;
import com.tundramobile.catchapp.ui.adapter.TabsPagerAdapter;
import com.tundramobile.catchapp.ui.fragment.BaseFragment;
import com.tundramobile.catchapp.ui.fragment.calendar.CalendarSubTabFragment;
import com.tundramobile.catchapp.ui.fragment.CatchUpFragment;
import com.tundramobile.catchapp.ui.fragment.ContactListFragment;
import com.tundramobile.catchapp.ui.fragment.ContactsFragment;
import com.tundramobile.catchapp.ui.fragment.calendar.EditCalendarSubTabFragment;
import com.tundramobile.catchapp.ui.fragment.SelectContactListFragment;
import com.tundramobile.catchapp.ui.fragment.calendar.EditItemsHolder;
import com.tundramobile.catchapp.ui.view.dateEditor.ScheduleRecycler;

import java.util.ArrayList;
import java.util.List;

public class PagerManager {

    public static List<TabData> getHomePageTabData() {
        List<TabData> tabData = new ArrayList<>();

        tabData.add(TabData.newBuilder()
                .imageDefault(R.drawable.home_inactive)
                .imageSelected(R.drawable.home_active)
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.home).build());
        tabData.add(TabData.newBuilder()
                .imageDefault(R.drawable.calendar_inactive)
                .imageSelected(R.drawable.calendar_active)
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.calendar).build());
        tabData.add(TabData.newBuilder()
                .imageDefault(R.drawable.chat_inactive)
                .imageSelected(R.drawable.chat_active)
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.chat).build());
        tabData.add(TabData.newBuilder()
                .imageDefault(R.drawable.notifications_inactive)
                .imageSelected(R.drawable.notifications_active)
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.notification).build());
        tabData.add(TabData.newBuilder()
                .imageDefault(R.drawable.contacts_inactive)
                .imageSelected(R.drawable.contacts_active)
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.contacts).build());
        return tabData;
    }


    public static TabsPagerAdapter getCatchUpPagerAdapter(FragmentManager fragmentManager) {
        ArrayList<BaseFragment> fragments = new ArrayList<>(3);

        TabData confirmedTabData = TabData.newBuilder()
                .selectedColor(R.color.green)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.badge_def_color)
                .badgeSelectedColor(R.color.green)
                .title(R.string.confirmed).build();
        fragments.add(CatchUpFragment.newConfirmedInstance(confirmedTabData));

        TabData incomingTabData = TabData.newBuilder()
                .selectedColor(R.color.red)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.red)
                .badgeSelectedColor(R.color.red)
                .title(R.string.incoming).build();
        fragments.add(CatchUpFragment.newIncomingInstance(incomingTabData));

        TabData outgoingTabData = TabData.newBuilder()
                .selectedColor(R.color.blue)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.badge_def_color)
                .badgeSelectedColor(R.color.blue)
                .title(R.string.outgoing).build();
        fragments.add(CatchUpFragment.newOutgoingInstance(outgoingTabData));

        return new TabsPagerAdapter(fragmentManager, fragments);
    }


    public static TabsPagerAdapter getContactsPagerAdapter(FragmentManager fragmentManager, int type) {
        ArrayList<BaseFragment> fragments = new ArrayList<>(3);

        TabData contactsTabData = TabData.newBuilder()
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.contacts).build();
        fragments.add(ContactListFragment.newContactsInstance(contactsTabData, type));

        TabData recentTabData = TabData.newBuilder()
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.recent).build();
        fragments.add(ContactListFragment.newRecentInstance(recentTabData, type));

        if (type != ContactsFragment.TYPE_SELECTING_CONTACTS) {
            TabData groupsTabData = TabData.newBuilder()
                    .selectedColor(R.color.purple)
                    .defaultColor(R.color.gray)
                    .title(R.string.groups).build();
            fragments.add(ContactListFragment.newGroupsInstance(groupsTabData));
        }

        return new TabsPagerAdapter(fragmentManager, fragments);
    }



    public static TabsPagerAdapter getSelectingContactsPagerAdapter(FragmentManager fragmentManager) {
        ArrayList<BaseFragment> fragments = new ArrayList<>(3);

        TabData contactsTabData = TabData.newBuilder()
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.contacts).build();
        fragments.add(SelectContactListFragment.newInstance(contactsTabData, ContactManager.CONTACTS));

        TabData recentTabData = TabData.newBuilder()
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.recent).build();
        fragments.add(SelectContactListFragment.newInstance(recentTabData,  ContactManager.RECENT));


        TabData groupsTabData = TabData.newBuilder()
                .selectedColor(R.color.purple)
                .defaultColor(R.color.gray)
                .title(R.string.groups).build();
        fragments.add(SelectContactListFragment.newInstance(groupsTabData, ContactManager.GROUPS));

        return new TabsPagerAdapter(fragmentManager, fragments);
    }


    public static TabsPagerAdapter getCalendarPagerAdapter(FragmentManager fragmentManager) {
        ArrayList<BaseFragment> fragments = new ArrayList<>(2);

        TabData allTabData = TabData.newBuilder()
                .selectedColor(R.color.green)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.badge_def_color)
                .badgeSelectedColor(R.color.green)
                .title(R.string.calendar_tab_all).build();
        fragments.add(CalendarSubTabFragment.newIncomingInstance(allTabData));

        TabData confirmedTabData = TabData.newBuilder()
                .selectedColor(R.color.red)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.red)
                .badgeSelectedColor(R.color.red)
                .title(R.string.calendar_tab_confirmed).build();
        fragments.add(CalendarSubTabFragment.newConfirmedInstance(confirmedTabData));

        return new TabsPagerAdapter(fragmentManager, fragments);
    }
    public static TabsPagerAdapter getEditCalendarPagerAdapter(FragmentManager fragmentManager, EditItemsHolder holder, ScheduleRecycler.ItemsHelper itemsHelper) {
        ArrayList<BaseFragment> fragments = new ArrayList<>(2);

        TabData allTabData = TabData.newBuilder()
                .selectedColor(R.color.green)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.badge_def_color)
                .badgeSelectedColor(R.color.green)
                .title(R.string.calendar_tab_all).build();
        fragments.add(EditCalendarSubTabFragment.newIncomingInstance(allTabData,holder,itemsHelper));

        TabData confirmedTabData = TabData.newBuilder()
                .selectedColor(R.color.red)
                .defaultColor(R.color.gray)
                .badgeDefaultColor(R.color.red)
                .badgeSelectedColor(R.color.red)
                .title(R.string.calendar_tab_confirmed).build();
        fragments.add(EditCalendarSubTabFragment.newConfirmedInstance(confirmedTabData,holder,itemsHelper));

        return new TabsPagerAdapter(fragmentManager, fragments);
    }
}

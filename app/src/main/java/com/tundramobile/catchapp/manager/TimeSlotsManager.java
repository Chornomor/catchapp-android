package com.tundramobile.catchapp.manager;

import android.util.Log;

import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.Event;
import com.tundramobile.catchapp.entity.TimeSlot;
import com.tundramobile.catchapp.entity.response.BooleanIsSuccessResponse;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.net.interfaces.DeleteRequestListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by eugenetroyanskii on 10.04.17.
 */

public class TimeSlotsManager {

    private static TimeSlotsManager instance;

    private List<CatchUp> confirmedCatchUps;
    private List<CatchUp> incomingCatchUps;
    private List<CatchUp> outComingCatchUps;

    private ArrayList<TimeSlot> allSlots;

    private DeleteRequestListener deleteSlotListener;

    private TimeSlotsManager() {
    }

    public static TimeSlotsManager getInstance() {
        if (instance == null) {
            instance = new TimeSlotsManager();
        }
        return instance;
    }

    public void setConfirmedCatchUps(List<CatchUp> catchUps) {
        confirmedCatchUps = new ArrayList<>();
        confirmedCatchUps.addAll(catchUps);
        for(CatchUp catchUp : catchUps){
            catchUp.setTabType(CatchUpManager.CONFIRMED);
        }
    }


    public void setIncomingCatchUps(List<CatchUp> catchUps) {
        incomingCatchUps = new ArrayList<>();
        incomingCatchUps.addAll(catchUps);
        for(CatchUp catchUp : catchUps){
            catchUp.setTabType(CatchUpManager.INCOMING);
        }
    }


    public void setOutComingCatchUps(List<CatchUp> catchUps) {
        outComingCatchUps = new ArrayList<>();
        outComingCatchUps.addAll(catchUps);
        for(CatchUp catchUp : catchUps){
            catchUp.setTabType(CatchUpManager.OUTGOING);
        }
    }


    public ArrayList<TimeSlot> getAllTimeSlots() {
        allSlots = new ArrayList<>();
        getSlotsFromCatchUps(confirmedCatchUps);
        getSlotsFromCatchUps(incomingCatchUps);
        getSlotsFromCatchUps(outComingCatchUps);
        return allSlots;
    }

    public ArrayList<TimeSlot> getConfirmedTimeSlots() {
        allSlots = new ArrayList<>();
        getSlotsFromCatchUps(confirmedCatchUps);
        return allSlots;
    }

    public void removeTimeSlot(long timeSlotId) {
        if (removeSlotIn(timeSlotId, confirmedCatchUps)){
            return;
        }
        if (removeSlotIn(timeSlotId, incomingCatchUps)){
            return;
        }
        if (removeSlotIn(timeSlotId, outComingCatchUps)){
            return;
        }
    }

    public boolean removeSlotIn(long timeSlotId, List<CatchUp> list) {

        for (CatchUp catchUp : list) {
            Event target = null;
            if (catchUp.getEvents() == null) {
                continue;
            }
            for (Event event : catchUp.getEvents()) {
                if (event.getId() == timeSlotId) {
                    target = event;
                    break;
                }
            }
            if (target != null) {
                catchUp.getEvents().remove(target);
                return true;
            }
        }
        return false;
    }


    private void getSlotsFromCatchUps(List<CatchUp> catchUps) {
        int counter = 0;
        if(catchUps == null) {
            return;
        }
        for (CatchUp catchup : catchUps) {
            TimeSlot slot;
            for(Event event : catchup.getEvents()) {
                slot = new TimeSlot();
                slot.setCatchUp(catchup);
                slot.setSlotId(event.getId());
                slot.setFrom(event.getFrom());
                slot.setTo(event.getTo());
                Log.d("DEBUG", ++counter + " slot added " + slot.toString());
                allSlots.add(slot);
            }
        }
    }


    public void deleteTimeSlot(String token, long catchUpId, long slotId, DeleteRequestListener listener) {
        deleteSlotListener = listener;
        RESTClient.getInstance().deleteSlot(token, catchUpId, slotId, deleteTimeSlotCallback);
    }


    public void cancelAttendanceForTimeSlot(String token, int catchUpId, int slotId, DeleteRequestListener listener) {
        deleteSlotListener = listener;
        RESTClient.getInstance().cancelAttendanceForTimeSlot(token, catchUpId, slotId, deleteTimeSlotCallback);
    }


    private HandledCallback<BooleanIsSuccessResponse> deleteTimeSlotCallback = new HandledCallback<BooleanIsSuccessResponse>() {
        @Override
        public void onSuccess(Call<BooleanIsSuccessResponse> call, Response<BooleanIsSuccessResponse> response) {
            if (response.body() != null) {
                deleteSlotListener.onSlotDeleteListener(response.body().isSuccess(), response.message());
            } else {
                deleteSlotListener.onSlotDeleteListener(false, "Unidentified error");
            }
        }


        @Override
        public void onFail(Call<BooleanIsSuccessResponse> call, Response<BooleanIsSuccessResponse> response) {
            deleteSlotListener.onSlotDeleteListener(false, response.message());
        }


        @Override
        public void onProcessFailed(Call<BooleanIsSuccessResponse> call, Throwable t, String error) {
            deleteSlotListener.onSlotDeleteListener(false, error);
        }
    };
}

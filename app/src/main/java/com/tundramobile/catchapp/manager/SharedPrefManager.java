package com.tundramobile.catchapp.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tundramobile.catchapp.entity.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eugenetroyanskii on 30.03.17.
 */

public class SharedPrefManager {

    private static final String APP_PREFERENCES = "app_shared_pref";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static SharedPrefManager instance = null;


    private SharedPrefManager() {
    }


    public static SharedPrefManager getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPrefManager();
            initSharedPreference(context);
        }
        return instance;
    }


    private static void initSharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public void storeIsIntroShowed(boolean isShowed) {
        editor.putBoolean(SharedPrefConstants.IS_INTRO_SHOWED, isShowed).apply();
    }


    public boolean getIsIntroShowed() {
        return sharedPreferences.getBoolean(SharedPrefConstants.IS_INTRO_SHOWED, false);
    }


    public void storeCurrentUser(String user) {
        editor.putString(SharedPrefConstants.CURRENT_USER, user).apply();
    }


    public String getCurrentUser() {
        return sharedPreferences.getString(SharedPrefConstants.CURRENT_USER, "");
    }


    public void storeUserToken(String token) {
        editor.putString(SharedPrefConstants.USER_TOKEN, token).apply();
    }


    public String getUserToken() {
        return sharedPreferences.getString(SharedPrefConstants.USER_TOKEN, "");
    }


    public void setNotificationEnabled(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.IS_NOTIFICATION_ENABLED, isEnabled).apply();
    }


    public boolean isNotificationEnabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.IS_NOTIFICATION_ENABLED, true);
    }


    public void saveContacts(List<Contact> contacts) {
        String json = new Gson().toJson(contacts);
        editor.putString(SharedPrefConstants.CONTACTS, json).apply();
    }


    public void saveRecent(List<Contact> recent) {
        String json = new Gson().toJson(recent);
        editor.putString(SharedPrefConstants.RECENT, json).apply();
    }


    public ArrayList<Contact> getContacts() {
        return new Gson()
                .fromJson(sharedPreferences.getString(SharedPrefConstants.CONTACTS, "")
                        , new TypeToken<List<Contact>>() {
                        }.getType());
    }


    public ArrayList<Contact> getRecent() {
        return new Gson()
                .fromJson(sharedPreferences.getString(SharedPrefConstants.RECENT, "")
                        , new TypeToken<List<Contact>>() {
                        }.getType());
    }


    public void saveEmailNotification(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.EMAIL_NOTIFICATION, isEnabled).apply();
    }


    public boolean isEmailNotificationEnabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.EMAIL_NOTIFICATION, true);
    }


    public void saveSettingsNotification1(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.NOTIFICATION_1, isEnabled).apply();
    }


    public boolean isSettingsNotification1Enabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.NOTIFICATION_1, true);
    }


    public void saveSettingsNotification2(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.NOTIFICATION_2, isEnabled).apply();
    }


    public boolean isSettingsNotification2Enabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.NOTIFICATION_2, true);
    }


    public void saveSettingsNotification3(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.NOTIFICATION_3, isEnabled).apply();
    }


    public boolean isSettingsNotification3Enabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.NOTIFICATION_3, true);
    }


    public void saveSettingsNotification4(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.NOTIFICATION_4, isEnabled).apply();
    }


    public boolean isSettingsNotification4Enabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.NOTIFICATION_4, true);
    }


    public void saveScheduleView(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.SCHEDULE_VIEW, isEnabled).apply();
    }


    public boolean isScheduleViewEnabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.SCHEDULE_VIEW, true);
    }


    public void saveHoursView(boolean isEnabled) {
        editor.putBoolean(SharedPrefConstants.HOURS_VIEW, isEnabled).apply();
    }


    public boolean isHoursViewEnabled() {
        return sharedPreferences.getBoolean(SharedPrefConstants.HOURS_VIEW, true);
    }


    public void cleanStoredUserData() {
        storeUserToken(null);
        storeCurrentUser(null);
        saveContacts(null);
        saveRecent(null);
        setNotificationEnabled(true);
        saveScheduleView(true);
        saveEmailNotification(true);
        saveHoursView(true);
    }
}

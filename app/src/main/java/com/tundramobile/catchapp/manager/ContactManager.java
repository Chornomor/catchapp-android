package com.tundramobile.catchapp.manager;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.request.CreateGroupBody;
import com.tundramobile.catchapp.entity.request.RequestContactBody;
import com.tundramobile.catchapp.entity.response.ContactModel;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.other.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by eugenetroyanskii on 05.04.17.
 */

public class ContactManager {

    private final static int DEFAULT_INDEX_OF_RESULT = 0;

    public static String token;

    private static final String TAG = ContactManager.class.getSimpleName();
    public static final int CONTACTS = 0;
    public static final int RECENT = 1;
    public static final int GROUPS = 2;

    private OnObtainContactListener contactsListener;
    private OnCreateGroupListener createGroupListener;
    private OnUpdateGroupListener updateGroupListener;

    private static ContactManager instance;
    private SharedPrefManager sharedPrefManager;

    private LinkedHashMap<String, String> userContactsOnDevice = new LinkedHashMap<>();
    private List<Contact> storedContacts;
    private List<Contact> storedRecent;


    public static ContactManager getInstance(Context context) {
        if (instance == null) {
            instance = new ContactManager(context);
        }
        return instance;
    }

    public static void clear(){
       instance = null;
    }


    private ContactManager(Context context) {
        sharedPrefManager = SharedPrefManager.getInstance(context);
        token = MyUserManager.getInstance().getCurrentUserToken(context);
    }


    public void getPhoneContacts(Context context, boolean getStoredIfExist, @NonNull OnObtainContactListener listener) {

        if (getStoredIfExist) {
            Utils.logd(TAG, "getPhoneContacts: from storage -> " + true + "[...]");
            if (storedContacts != null && storedContacts.isEmpty()) {
                listener.onObtainContacts(storedContacts, null);
                return;
            } else {
                storedContacts = sharedPrefManager.getContacts();
                if (storedContacts != null && !storedContacts.isEmpty()) {
                    listener.onObtainContacts(storedContacts, null);
                    return;
                }
            }
        }

        Utils.loge(TAG, "getPhoneContacts: from storage -> " + getStoredIfExist + (getStoredIfExist ? "[WTF]" : "[...]"));

        new Thread(() -> {
            this.contactsListener = listener;

            ContentResolver cr = context.getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

            RequestContactBody contactBody = new RequestContactBody();
            if (cur != null && cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                String phoneNo = pCur.getString(pCur.getColumnIndex(
                                        ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
                            /*if phone number have not normal format for example
                             some short number for taxi it will be null*/
                                if (phoneNo != null) {
                                    userContactsOnDevice.put(phoneNo, name);
                                }
                            }
                            pCur.close();
                        } else {
                            Log.d("DEBUG", "Cursor error while try to obtain userContactsOnDevice");
                        }
                    }
                }
                cur.close();
            } else {
                Log.d("DEBUG", "Cursor error while try to obtain userContactsOnDevice");
            }

            if (context instanceof AppCompatActivity) {
                ((AppCompatActivity) context).runOnUiThread(() -> tryToGetPhoneNumbers(listener, contactBody));
            }
        }).run();
    }


    private void tryToGetPhoneNumbers(@NonNull OnObtainContactListener listener, RequestContactBody contactBody) {
        if (!userContactsOnDevice.isEmpty()) {
            for (Map.Entry contact : userContactsOnDevice.entrySet()) {
                contactBody.addContact(contact.getKey().toString());
            }
            requestContacts(contactBody);
        } else {
            Utils.loge(TAG, "no contacts available");
            listener.onObtainContacts(null, "No contacts to show");
        }
    }


    public String getUserContactNameOnDevice(String phone) {
        return userContactsOnDevice.get(phone);
    }


    private void requestContacts(RequestContactBody contacts) {
        RESTClient.getInstance().getContacts(token, contacts, requestContactsCallback);
    }


    public void getRecent(boolean getStoredIfExist, @NonNull OnObtainContactListener listener) {
        this.contactsListener = listener;

        if (getStoredIfExist) {
            Utils.logd(TAG, "getRecent: from storage -> " + true + "[...]");
            if (storedRecent != null && storedRecent.isEmpty()) {
                listener.onObtainRecentContacts(storedRecent);
                return;
            } else {
                storedRecent = sharedPrefManager.getRecent();
                if (storedRecent != null && !storedRecent.isEmpty()) {
                    listener.onObtainRecentContacts(storedRecent);
                    return;
                }
            }
        }
        Utils.loge(TAG, "getRecent: from storage -> " + getStoredIfExist + (getStoredIfExist ? "[WTF]" : "[...]"));

        RESTClient.getInstance().getRecentContacts(token, getRecentContactCallback);
    }


    public void getGroups(@NonNull OnObtainContactListener listener) {
        this.contactsListener = listener;
        RESTClient.getInstance().getContactGroups(token, getGroupContactsCallback);
    }


    public void createGroup(@NonNull OnCreateGroupListener listener, CreateGroupBody body) {
        this.createGroupListener = listener;
        RESTClient.getInstance().createGroup(token, body, createGroupCallback);
    }


    public void updateGroup(CreateGroupBody body, int groupId, @NonNull OnUpdateGroupListener listener) {
        this.updateGroupListener = listener;
        RESTClient.getInstance().editGroup(token, groupId, body, updateGroupCallback);
    }


    private HandledCallback<ListResponseModel<Contact>> updateGroupCallback = new HandledCallback<ListResponseModel<Contact>>() {
        @Override
        public void onSuccess(Call<ListResponseModel<Contact>> call, Response<ListResponseModel<Contact>> response) {
            updateGroupListener.onGroupUpdated(response.body().getResult().get(DEFAULT_INDEX_OF_RESULT), null);
        }


        @Override
        public void onFail(Call<ListResponseModel<Contact>> call, Response<ListResponseModel<Contact>> response) {
            String error = "Internal server error";
            if (response.message() != null && !response.message().isEmpty()) {
                error = response.message();
            }
            updateGroupListener.onGroupUpdated(null, error);
        }


        @Override
        public void onProcessFailed(Call<ListResponseModel<Contact>> call, Throwable t, String error) {
            updateGroupListener.onGroupUpdated(null, error);
        }
    };

    private HandledCallback<ContactModel> requestContactsCallback = new HandledCallback<ContactModel>() {
        @Override
        public void onSuccess(Call<ContactModel> call, Response<ContactModel> response) {
            ContactModel contacts = response.body();
            List<Contact> resultContacts = new ArrayList<>();
            for (int i = 0; i < contacts.getResult().size(); ++i) {
                Contact contact = contacts.getResult().get(i);
                if (contact.getName().equals(contact.getMobile())) {
                    contact.setName(userContactsOnDevice.get(contact.getMobile()));
                } else {
                    contact.setContactType(Contact.REGISTERED_CONTACT_IN_SYSTEM);
                }
                resultContacts.add(contact);
            }

            Collections.sort(resultContacts, (o1, o2) -> o1.toString().compareTo(o2.toString()));


            if (sharedPrefManager != null) {
                storedContacts = resultContacts;
                sharedPrefManager.saveContacts(resultContacts);
            }

            if (contactsListener != null)
                contactsListener.onObtainContacts(resultContacts, null);
        }


        @Override
        public void onFail(Call<ContactModel> call, Response<ContactModel> response) {
            String error;
            if (response.code() == 500) {
                error = "Internal server error";
            } else {
                error = response.body() == null
                        ? "Unknown Error"
                        : response.body().getError();
            }
            if (contactsListener != null)
                contactsListener.onObtainContacts(null, error);
        }


        @Override
        public void onProcessFailed(Call<ContactModel> call, Throwable t, String error) {
            if (contactsListener != null)
                contactsListener.onObtainContacts(null, error);
        }
    };


    private HandledCallback<ListResponseModel<Contact>> getRecentContactCallback =
            new HandledCallback<ListResponseModel<Contact>>() {
                @Override
                public void onSuccess(Call<ListResponseModel<Contact>> call,
                                      Response<ListResponseModel<Contact>> response) {

                    List<Contact> recent = response.body().getResult();

                    storedRecent = recent;
                    if (sharedPrefManager != null)
                        sharedPrefManager.saveRecent(recent);
                    if (contactsListener != null)
                        contactsListener.onObtainRecentContacts(recent);
                }


                @Override
                public void onFail(Call<ListResponseModel<Contact>> call,
                                   Response<ListResponseModel<Contact>> response) {
                    ListResponseModel<Contact> recentContactList = new ListResponseModel<>();
                    if (response.code() == 500) {
                        recentContactList.setError("Internal server error");
                    } else {
                        recentContactList.setError(response.body() == null
                                ? "Unknown error"
                                : response.body().getError());
                    }
                    if (contactsListener != null)
                        contactsListener.onObtainRecentContacts(recentContactList.getResult());
                }


                @Override
                public void onProcessFailed(Call<ListResponseModel<Contact>> call, Throwable t, String error) {
                    ListResponseModel<Contact> recentContactList = new ListResponseModel<>();
                    recentContactList.setError(error);
                    if (contactsListener != null)
                        contactsListener.onObtainRecentContacts(recentContactList.getResult());
                }
            };


    private HandledCallback<ListResponseModel<Contact>> getGroupContactsCallback =
            new HandledCallback<ListResponseModel<Contact>>() {
                @Override
                public void onSuccess(Call<ListResponseModel<Contact>> call,
                                      Response<ListResponseModel<Contact>> response) {
                    contactsListener.onObtainContactGroups(response.body().getResult());
                }


                @Override
                public void onFail(Call<ListResponseModel<Contact>> call,
                                   Response<ListResponseModel<Contact>> response) {
                    ListResponseModel<Contact> groupList = new ListResponseModel<>();
                    if (response.code() == 500) {
                        groupList.setError("Internal server error");
                    } else {
                        groupList.setError(response.body() == null
                                ? "Unknown Error"
                                : response.body().getError());
                    }
                    if (contactsListener != null)
                        contactsListener.onObtainContactGroups(groupList.getResult());
                }


                @Override
                public void onProcessFailed(Call<ListResponseModel<Contact>> call, Throwable t, String error) {
                    ListResponseModel<Contact> groupList = new ListResponseModel<>();
                    groupList.setError(error);
                    if (contactsListener != null)
                        contactsListener.onObtainContactGroups(groupList.getResult());
                }
            };


    private HandledCallback<ListResponseModel<Contact>> createGroupCallback = new HandledCallback<ListResponseModel<Contact>>() {
        @Override
        public void onSuccess(Call<ListResponseModel<Contact>> call, Response<ListResponseModel<Contact>> response) {
            createGroupListener.onGroupCreated(response.body().getResult().get(DEFAULT_INDEX_OF_RESULT));
            // TODO: 07.04.17 handle error
        }


        @Override
        public void onFail(Call<ListResponseModel<Contact>> call, Response<ListResponseModel<Contact>> response) {
            // TODO: 07.04.17 handle error
        }


        @Override
        public void onProcessFailed(Call<ListResponseModel<Contact>> call, Throwable t, String error) {
            // TODO: 07.04.17 handle error
        }
    };


    public interface OnCreateGroupListener {
        void onGroupCreated(Contact group);
    }

    public interface OnUpdateGroupListener {
        void onGroupUpdated(Contact group, String error);
    }


    public interface OnObtainContactListener {
        void onObtainContacts(List<Contact> contacts, String error);

        void onObtainRecentContacts(List<Contact> recentContactList);

        void onObtainContactGroups(List<Contact> groupList);

    }
}

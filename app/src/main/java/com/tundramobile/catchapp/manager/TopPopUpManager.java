package com.tundramobile.catchapp.manager;

import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.tundramobile.catchapp.ui.view.TopPopUpView;

/**
 * Created by alexchern on 24.04.17.
 */

public class TopPopUpManager {

    public static final boolean isError = true;


    public static void showTopPopUp(Context context, String message, boolean isError) {
        TopPopUpView view = new TopPopUpView(context);
        PopupWindow popupWindow = new PopupWindow(
                view,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.showAtLocation(view, Gravity.TOP, 0, 0);

        view.setDismissListener(popupWindow::dismiss);
        popupWindow.setOutsideTouchable(true);
        view.showPopUp(message, isError);
    }
}

package com.tundramobile.catchapp.manager;

/**
 * Created by eugenetroyanskii on 30.03.17.
 */

public interface SharedPrefConstants {

    String EMAIL_NOTIFICATION = "email_notification";
    String NOTIFICATION_1 = "notification_1";
    String NOTIFICATION_2 = "notification_2";
    String NOTIFICATION_3 = "notification_3";
    String NOTIFICATION_4 = "notification_4";
    String SCHEDULE_VIEW = "schedule_view";
    String HOURS_VIEW = "hours_view";
    String IS_NOTIFICATION_ENABLED = "is_notification_enabled";
    String IS_INTRO_SHOWED = "is_tutorial_watched";
    String CURRENT_USER = "current_user";
    String USER_TOKEN = "user_token";
    String CONTACTS = "contacts";
    String RECENT = "recent";

}

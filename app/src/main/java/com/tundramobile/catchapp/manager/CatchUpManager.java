package com.tundramobile.catchapp.manager;

import android.content.Context;
import android.util.Log;

import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.CatchUpDetail;
import com.tundramobile.catchapp.entity.request.CreateCatchAppBody;
import com.tundramobile.catchapp.entity.response.BooleanIsSuccessResponse;
import com.tundramobile.catchapp.entity.response.CatchUpsCounters;
import com.tundramobile.catchapp.entity.response.CreateCatchUpResponse;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.ReportResponse;
import com.tundramobile.catchapp.interfaces.ListResultListener;
import com.tundramobile.catchapp.net.HandledCallback;
import com.tundramobile.catchapp.net.RESTClient;
import com.tundramobile.catchapp.net.interfaces.DeleteRequestListener;
import com.tundramobile.catchapp.other.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class CatchUpManager {

    private static final String TAG = CatchUpManager.class.getSimpleName();
    public static final int CONFIRMED = 0;
    public static final int INCOMING = 1;
    public static final int OUTGOING = 2;

    private int userId;
    private String token;

    private DeleteRequestListener deleteListener;
    private CatchUpDetailsListener catchUpDetailsListener;

    private static CatchUpManager instance;


    private CatchUpManager(Context c) {
        token = MyUserManager.getInstance().getCurrentUserToken(c);
        userId = MyUserManager.getCurrentUser(c).getId();
    }

    public static void destroy(){
        instance = null;
    }


    public static CatchUpManager getInstance(Context c) {
        if (instance == null) {
            instance = new CatchUpManager(c);
        }

        return instance;
    }

    public void  passType(List<CatchUp> list, int tabType){
        for(CatchUp catchUp : list){
            catchUp.setTabType(tabType);
        }
    }


    public void getCatchUps(int type, boolean isOnline, final ListResultListener<CatchUp> listener) {
        switch (type) {
            case CONFIRMED:
                if (isOnline) {
                    Utils.logd(TAG, "getCatchUps: [CONFIRMED] ONLINE");
                    RESTClient.getInstance().getConfirmedCatchUps(userId, token,
                            new HandledCallback<ListResponseModel<CatchUp>>() {
                                @Override
                                public void onSuccess(Call<ListResponseModel<CatchUp>> call,
                                                      Response<ListResponseModel<CatchUp>> response) {
                                    List<CatchUp> list = response.body().getResult();
                                    TimeSlotsManager.getInstance().setConfirmedCatchUps(list);
                                    passType(list, type);
                                    listener.onResultReady(type, true, response.body().getResult());
                                }


                                @Override
                                public void onFail(Call<ListResponseModel<CatchUp>> call,
                                                   Response<ListResponseModel<CatchUp>> response) {
                                    listener.onResultReady(type, false, null);
                                }


                                @Override
                                public void onProcessFailed(Call<ListResponseModel<CatchUp>> call,
                                                            Throwable t, String error) {
                                    listener.onResultReady(type, false, null);
                                }
                            });
                } else {
                    Utils.logd(TAG, "getCatchUps: [CONFIRMED] OFFLINE");
                    // implement offline logic
                }
                break;
            case INCOMING:
                if (isOnline) {
                    Utils.logd(TAG, "getCatchUps: [INCOMING] ONLINE");
                    RESTClient.getInstance().getIncomingCatchUps(userId, token,
                            new HandledCallback<ListResponseModel<CatchUp>>() {
                                @Override
                                public void onSuccess(Call<ListResponseModel<CatchUp>> call,
                                                      Response<ListResponseModel<CatchUp>> response) {
                                    List<CatchUp> list = response.body().getResult();
                                    TimeSlotsManager.getInstance().setIncomingCatchUps(list);
                                    passType(list, type);
                                    listener.onResultReady(type, true, response.body().getResult());
                                }


                                @Override
                                public void onFail(Call<ListResponseModel<CatchUp>> call,
                                                   Response<ListResponseModel<CatchUp>> response) {
                                    listener.onResultReady(type, false, null);
                                }


                                @Override
                                public void onProcessFailed(Call<ListResponseModel<CatchUp>> call,
                                                            Throwable t, String error) {
                                    listener.onResultReady(type, false, null);
                                }
                            });
                } else {
                    Utils.logd(TAG, "getCatchUps: [INCOMING] OFFLINE");
                    // implement offline logic
                }
                break;
            case OUTGOING:
                if (isOnline) {
                    Utils.logd(TAG, "getCatchUps: [OUTGOING] ONLINE");
                    RESTClient.getInstance().getOutgoingCatchUps(userId, token,
                            new HandledCallback<ListResponseModel<CatchUp>>() {
                                @Override
                                public void onSuccess(Call<ListResponseModel<CatchUp>> call,
                                                      Response<ListResponseModel<CatchUp>> response) {
                                    List<CatchUp> list = response.body().getResult();
                                    TimeSlotsManager.getInstance().setOutComingCatchUps(list);
                                    passType(list, type);
                                    listener.onResultReady(type, true, response.body().getResult());
                                }


                                @Override
                                public void onFail(Call<ListResponseModel<CatchUp>> call,
                                                   Response<ListResponseModel<CatchUp>> response) {
                                    listener.onResultReady(type, false, null);
                                }


                                @Override
                                public void onProcessFailed(Call<ListResponseModel<CatchUp>> call,
                                                            Throwable t, String error) {
                                    listener.onResultReady(type, false, null);
                                }
                            });
                } else {
                    Utils.logd(TAG, "getCatchUps: [OUTGOING] ONLINE");
                    // implement offline logic
                }
                break;
        }
    }


    public void createCatchUp(String token, int userId, CreateCatchAppBody body, HandledCallback<CreateCatchUpResponse> callback) {
        RESTClient.getInstance().createCatchApp(token, userId, body, callback);
    }


    public void getCatchUpsCounters(HandledCallback<CatchUpsCounters> callback) {
        RESTClient.getInstance().getCatchUpsCounters(token, userId, callback);
    }


    public void deleteCatchUp(int catchUpId, DeleteRequestListener listener) {
        deleteListener = listener;
        RESTClient.getInstance().deleteCatchUp(token, catchUpId, deleteCallback);
    }


    public void deleteUserInvitationToEvent(int catchUpId, int userId, DeleteRequestListener listener) {
        deleteListener = listener;
        RESTClient.getInstance().deleteUserInvitationToEvent(token, catchUpId, userId, deleteCallback);
    }


    public void cancelCatchUpInvitation(int catchUpId, DeleteRequestListener listener) {
        deleteListener = listener;
        RESTClient.getInstance().cancelCatchUpInvitation(token, catchUpId, deleteCallback);
    }


    public void cancelCatchUpTimeSlotInvitation(int catchUpId, int slotId, DeleteRequestListener listener) {
        deleteListener = listener;
        RESTClient.getInstance().cancelAttendanceForTimeSlot(token, catchUpId, slotId, deleteCallback);
    }


    public void reportCatchUp(int catchUpId, String reportMessage) {
        RESTClient.getInstance().reportCatchUp(token, catchUpId, reportMessage, reportCallback);
    }


    public void getCatchUpDetailsConfirmed(int catchUpId, int slotId, CatchUpDetailsListener listener) {
        catchUpDetailsListener = listener;
        RESTClient.getInstance().getCatchUpDetailsConfirmed(token, catchUpId, slotId, detailsCallback);
    }


    public void getCatchUpDetailsIncoming(int catchUpId, CatchUpDetailsListener listener) {
        catchUpDetailsListener = listener;
        RESTClient.getInstance().getCatchUpDetailsIncoming(token, catchUpId, detailsCallback);
    }


    public void getCatchUpDetailsOutgoing(int catchUpId, CatchUpDetailsListener listener) {
        catchUpDetailsListener = listener;
        RESTClient.getInstance().getCatchUpDetailsOutgoing(token, catchUpId, detailsCallback);
    }


    private HandledCallback<ReportResponse> reportCallback = new HandledCallback<ReportResponse>() {
        @Override
        public void onSuccess(Call<ReportResponse> call, Response<ReportResponse> response) {
            Log.d("DEBUG", response.toString());
            // TODO: 11.04.17 handle response
        }


        @Override
        public void onFail(Call<ReportResponse> call, Response<ReportResponse> response) {
            Log.d("DEBUG", response.toString());
            // TODO: 11.04.17 handle response
        }


        @Override
        public void onProcessFailed(Call<ReportResponse> call, Throwable t, String error) {
            Log.d("DEBUG", call.toString());
            // TODO: 11.04.17 handle response
        }
    };


    private HandledCallback<BooleanIsSuccessResponse> deleteCallback = new HandledCallback<BooleanIsSuccessResponse>() {
        @Override
        public void onSuccess(Call<BooleanIsSuccessResponse> call, Response<BooleanIsSuccessResponse> response) {
            if (response.body() != null) {
                deleteListener.onSlotDeleteListener(response.body().isSuccess(), response.message());
            } else {
                deleteListener.onSlotDeleteListener(false, "Unidentified error");
            }
        }


        @Override
        public void onFail(Call<BooleanIsSuccessResponse> call, Response<BooleanIsSuccessResponse> response) {
            deleteListener.onSlotDeleteListener(false, response.message());
        }


        @Override
        public void onProcessFailed(Call<BooleanIsSuccessResponse> call, Throwable t, String error) {
            deleteListener.onSlotDeleteListener(false, error);
        }
    };

    private HandledCallback<ListResponseModel<CatchUpDetail>> detailsCallback = new HandledCallback<ListResponseModel<CatchUpDetail>>() {
        @Override
        public void onSuccess(Call<ListResponseModel<CatchUpDetail>> call, Response<ListResponseModel<CatchUpDetail>> response) {
            if (catchUpDetailsListener == null) return;

            if (response.body() != null) {
                catchUpDetailsListener.onDetailsReceived(response.body().getResult().get(0), null);
            } else {
                deleteListener.onSlotDeleteListener(false, "Unidentified error");
            }
        }


        @Override
        public void onFail(Call<ListResponseModel<CatchUpDetail>> call, Response<ListResponseModel<CatchUpDetail>> response) {
            if (catchUpDetailsListener != null)
                catchUpDetailsListener.onDetailsReceived(null, response.message());
        }


        @Override
        public void onProcessFailed(Call<ListResponseModel<CatchUpDetail>> call, Throwable t, String error) {
            if (catchUpDetailsListener != null)
                catchUpDetailsListener.onDetailsReceived(null, error);
        }
    };


    public interface CatchUpDetailsListener {
        void onDetailsReceived(CatchUpDetail catchUpDetail, String error);
    }
}

package com.tundramobile.catchapp.manager;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.tundramobile.catchapp.R;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.other.Utils;
import com.tundramobile.catchapp.ui.view.CollageImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ImageLoader {

    private static final String TAG = ImageLoader.class.getSimpleName();
    private List<Bitmap> bitmaps = new ArrayList<>();
    private WeakReference<ImageView> reference;
    private int avatarSize = -1;
    private int counter;
    private int needToLoad;
    private boolean randomizeEmptyColor;
    private boolean setErrorImage;


    private ImageLoader(ImageView imageView, int avatarSize,
                        boolean randomizeEmptyColor, boolean setErrorImage) {
        reference = new WeakReference<>(imageView);
        this.avatarSize = avatarSize;
        this.randomizeEmptyColor = randomizeEmptyColor;
        this.setErrorImage = setErrorImage;
    }


    public static ImageLoader newInstance(ImageView imageView, int avatarSize,
                                          boolean randomizeEmptyColor) {
        return new ImageLoader(imageView, avatarSize, randomizeEmptyColor, true);
    }


    public static ImageLoader newInstance(ImageView imageView,
                                          boolean randomizeEmptyColor) {
        return new ImageLoader(imageView, -1, randomizeEmptyColor, true);
    }


    public static ImageLoader newInstance(ImageView imageView, int avatarSize) {
        return new ImageLoader(imageView, avatarSize, false, false);
    }


    public void loadImages(@NonNull List<?> items) {
        ImageView imageView = getImageView();
        List<String> urls = prepareUrls(items);
        needToLoad = urls.size();

        final long timestamp = System.currentTimeMillis();
        if (urls.isEmpty() && imageView != null) {
            imageView.setImageResource(R.drawable.rect_gray_common);
            return;
        }
        for (String curUrl : urls) {
            if (imageView == null) break;

            BitmapRequestBuilder builder =
                    Glide
                            .with(imageView.getContext())
                            .load(curUrl)
                            .asBitmap();

            if (avatarSize != -1) {
                builder.override(avatarSize, avatarSize);
            }

            builder.into(new SimpleTarget<Bitmap>(avatarSize, avatarSize) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    bitmaps.add(resource);
                    processResult(timestamp);
                }


                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    processResult(timestamp);
                }
            });
        }
    }


    public void loadImage(@NonNull String url) {
        loadImage(new Contact(url));
    }


    public void loadImage(@NonNull Contact contact) {
        List<Contact> list = new ArrayList<>(1);
        list.add(contact);
        loadImages(list);
    }


    public void loadImage(@NonNull User user) {
        List<User> list = new ArrayList<>(1);
        list.add(user);
        loadImages(list);
    }


    private List<String> prepareUrls(List<?> items) {
        List<String> urls = new ArrayList<>();

        for (int i = 0; i < items.size() && urls.size() < 5; i++) {
            Object item = items.get(i);
            if (item instanceof User) {
                if (((User) item).getProfilePic() != null)
                    urls.add(((User) item).getProfilePic());
            } else if (item instanceof Contact) {
                if (((Contact) item).getProfilePic() != null)
                    urls.add(((Contact) item).getProfilePic());
            }
        }
        return urls;
    }


    private void processResult(long timestamp) {
        counter++;
        if (counter == needToLoad) {
            if (bitmaps.size() > 4) bitmaps = bitmaps.subList(0, 4);
            ImageView imageView = getImageView();
            if (imageView != null) {
                if (bitmaps.isEmpty() && setErrorImage) {
                    imageView.setImageResource(randomizeEmptyColor ?
                            Utils.getRandomGreyShape() : R.drawable.rect_gray_common);
                } else {
                    if (imageView instanceof CollageImageView) {
                        ((CollageImageView) imageView)
                                .setCollageImages(bitmaps.toArray(new Bitmap[bitmaps.size()]));
                    } else {
                        imageView.setImageBitmap(bitmaps.get(0));
                    }
                }
                Log.d(TAG, "Loading time: " + (System.currentTimeMillis() - timestamp));
            }
        }
    }


    private ImageView getImageView() {
        return reference != null ? reference.get() : null;
    }

}

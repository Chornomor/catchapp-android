package com.tundramobile.catchapp.manager;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

/**
 * Created by Sviatoslav Zaitsev on 14.04.17.
 */

public class Navigator {

    public static final String TRANSFER_DATA = "TRANSFER_DATA";

    public static Builder with(FragmentActivity activity) {
        return new Builder(activity);
    }

    public static class Builder {
        private FragmentActivity activity;
        private FragmentManager fragmentManager;
        private int containerId;
        private String tag;
        private boolean commitAllowingStateLoss;
        private boolean disallowAddToBackStack;

        public Builder(FragmentActivity activity) {
            this.activity = activity;
            this.fragmentManager = activity.getSupportFragmentManager();
            this.containerId = activity.getResources().getIdentifier("fragmentContainer", "id", activity.getPackageName());
        }

        public Builder container(@IdRes int id) {
            this.containerId = id;
            return this;
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder commitAllowingStateLoss() {
            this.commitAllowingStateLoss = true;
            return this;
        }

        public Builder disallowAddToBackStack() {
            this.disallowAddToBackStack = true;
            return this;
        }

        /** Data transfer methods **/
        public Builder putData(String name, Bundle args) {
            if(activity != null) {
                Bundle transferData = activity.getIntent().getBundleExtra(TRANSFER_DATA);
                if(transferData == null) {
                    transferData = new Bundle();
                }
                transferData.putBundle(name, args);
                activity.getIntent().putExtra(TRANSFER_DATA, transferData);
            }
            return this;
        }

        public Bundle popData(String name) {
            if(activity == null || activity.getIntent().getBundleExtra(TRANSFER_DATA) == null) return null;

            Bundle result = activity.getIntent().getBundleExtra(TRANSFER_DATA).getBundle(name);
            if(result != null) {
                activity.getIntent().getBundleExtra(TRANSFER_DATA).remove(name);
            }
            return result;
        }

        /** Data transfer methods **/
        public void add(Fragment fragment) {
            FragmentTransaction tr = fragmentManager.beginTransaction();
            tr.add(containerId, fragment, tag);

            setTransactionParams(tr);

            commit(tr);
        }

        public void replace(Fragment fragment) {
            FragmentTransaction tr = fragmentManager.beginTransaction();
            setTransactionParams(tr);

            tr.replace(containerId, fragment, tag);

            commit(tr);
        }

        //
        private void setTransactionParams(FragmentTransaction tr) {
            if(disallowAddToBackStack)  tr.disallowAddToBackStack();
        }

        private void commit(FragmentTransaction tr) {
            if(containerId <= 0) {
                Log.e(Navigator.class.getSimpleName(), "Fragment container id isn't correct. Please set correct id");
            }
            if(commitAllowingStateLoss) {
                tr.commitAllowingStateLoss();
            } else {
                tr.commit();
            }
        }
    }
}

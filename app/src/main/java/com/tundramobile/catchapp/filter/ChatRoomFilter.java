package com.tundramobile.catchapp.filter;


import android.widget.Filter;

import com.tundramobile.catchapp.ui.adapter.ChatRoomRecyclerAdapter;
import com.tundramobile.catchapp.ui.viewmodel.ChatRoomViewModel;

import java.util.ArrayList;
import java.util.List;

// TODO: 24.04.17 MOTHER OF GOD!!! PLEASE, REFACTOR ME!!! Project contains a lot of adapters, holders, layouts, etc with same code
public class ChatRoomFilter extends Filter {

    private final ChatRoomRecyclerAdapter adapter;
    private List<ChatRoomViewModel> originalList;
    private final List<ChatRoomViewModel> filteredList;


    public ChatRoomFilter(ChatRoomRecyclerAdapter adapter, List<ChatRoomViewModel> originalList) {
        super();
        this.adapter = adapter;
        setItems(originalList);
        this.filteredList = new ArrayList<>();
    }


    public void setItems(List<ChatRoomViewModel> originalList) {
        this.originalList = originalList != null ? new ArrayList<>(originalList) : null;
    }


    public boolean isItemsEmpty() {
        return originalList == null || originalList.isEmpty();
    }


    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (charSequence.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = charSequence.toString().toLowerCase().trim();
            for (ChatRoomViewModel item : originalList) {
                if (item.getName().toLowerCase().contains(filterPattern)) {
                    filteredList.add(item);
                }
            }
        }

        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        adapter.setItems((ArrayList<ChatRoomViewModel>) filterResults.values);
    }
}

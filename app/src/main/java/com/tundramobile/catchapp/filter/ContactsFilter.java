package com.tundramobile.catchapp.filter;


import android.widget.Filter;

import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.ui.adapter.ContactsRecyclerAdapter;

import java.util.ArrayList;

public class ContactsFilter extends Filter {

    private final ContactsRecyclerAdapter adapter;
    private ArrayList<Contact> originalList;
    private final ArrayList<Contact> filteredList;


    public ContactsFilter(ContactsRecyclerAdapter adapter, ArrayList<Contact> originalList) {
        super();
        this.adapter = adapter;
        setItems(originalList);
        this.filteredList = new ArrayList<>();
    }


    public void setItems(ArrayList<Contact> originalList) {
        this.originalList = originalList != null ? new ArrayList<>(originalList) : null;
    }


    public boolean isItemsEmpty() {
        return originalList == null || originalList.isEmpty();
    }


    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (charSequence.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = charSequence.toString().toLowerCase().trim();
            for (Contact item : originalList) {
                if (item.getName().toLowerCase().contains(filterPattern)) {
                    filteredList.add(item);
                }
            }
        }

        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        adapter.setItems((ArrayList<Contact>) filterResults.values);
    }
}

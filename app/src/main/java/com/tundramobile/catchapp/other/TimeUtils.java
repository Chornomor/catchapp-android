package com.tundramobile.catchapp.other;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import com.tundramobile.catchapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Sviatoslav Zaitsev on 26.04.17.
 */

public class TimeUtils {
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());


    public static String getRelativeTime(String time) {
        long timeInMillis = 0;
        try {
            timeInMillis = dateFormat.parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();
        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(timeInMillis, now, DateUtils.MINUTE_IN_MILLIS);
        return ago.toString();
    }

    public static String getTimeFromTimestamp(long timestamp) {
        Date d = new Date(timestamp * 1000L);
        return DateUtils.getRelativeTimeSpanString(d.getTime(), System.currentTimeMillis(),
                DateUtils.DAY_IN_MILLIS).toString();
    }


    public static String getFormattedTimestamp(Context context, long timestamp, String pattern) {
        SimpleDateFormat sdf;
        Date d = new Date(timestamp * 1000L);
        if (DateUtils.isToday(d.getTime())) {
            sdf = new SimpleDateFormat("hh:mm", Locale.getDefault());
            return context.getString(R.string.today) + ", " + sdf.format(d);
        } else {
            sdf = new SimpleDateFormat(pattern, Locale.getDefault());
            return sdf.format(d);
        }
    }

    public static String getRelativeTime(Double timestamp) {
        if(timestamp == null)
            return "";
        Date date = new Date((long) (timestamp * 1000L));
        Log.d("Time", "Time: " + dateFormat.format(date.getTime()));
        int gmtOffset = TimeZone.getDefault().getRawOffset();
        return getRelativeTime(dateFormat.format(date.getTime()));
//        return DateUtils.getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(),
//                DateUtils.DAY_IN_MILLIS).toString();
    }
}

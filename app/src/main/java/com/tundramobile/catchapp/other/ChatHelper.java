package com.tundramobile.catchapp.other;

import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tundramobile.catchapp.firebase.Types;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sviatoslav Zaitsev on 28.04.17.
 */

public class ChatHelper {
    private static final String NODE_CHAT = "chat";
    private static final String NODE_PARTICIPANTS = "chatParticipant";
    private static final String NODE_MESSAGES = "message";

    private static final String TAG = ChatHelper.class.getSimpleName();

    // TODO: 28.04.17 Try to use Rx or smth other instead of static variables
    private static int updateRequired;
    private static int updateCompleated;

    /** Room removing **/
    public static void removeRoom(String roomKey, List<Long> participants,
                                  OnSuccessListener onSuccessListener,
                                  OnFailureListener onFailureListener) {
        Log.d(TAG, "removeRoom: Try ot remove room " + roomKey + " for members " + participants.toString());

        updateRequired = participants.size();
        updateCompleated = 0;

        chatRef().child(roomKey).removeValue()
                .addOnSuccessListener(aVoid -> {
                    for(Long curID : participants) {
                        removeRoomInParticipant("" + curID, roomKey , onSuccessListener, onFailureListener);
                    }
                })
                .addOnFailureListener(onFailureListener::onFailure);
    }

    private static void removeRoomInParticipant(String participantKey, String roomKey,
                                                OnSuccessListener onSuccessListener,
                                                OnFailureListener onFailureListener) {
        participantRef().child(participantKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> rooms = dataSnapshot.getValue(Types.stringsList);
                if(rooms == null) {
                    rooms = new ArrayList<>();
                }
                if(rooms.contains(roomKey)) {
                    rooms.remove(roomKey);
                }
                updateParticipantRoomsList(participantKey, rooms, onSuccessListener, onFailureListener);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFailureListener.onFailure(databaseError.toException());
            }
        });
    }

    /****/
    private static void updateParticipantRoomsList(String participantKey, List<String> rooms,
                                                   OnSuccessListener onSuccessListener,
                                                   OnFailureListener onFailureListener) {
        participantRef().child(participantKey).setValue(rooms)
                .addOnCompleteListener(task -> {
                    updateCompleated++;
                    if(updateCompleated >= updateRequired) {
                        onSuccessListener.onSuccess(null);
                    }
                });
//                .addOnSuccessListener(aVoid -> {
//                    if(updateCompleated >= updateRequired) {
//                        onSuccessListener.onSuccess(null);
//                    }
//                })
//                .addOnFailureListener(e -> {});
    }

    /** Quick Reference **/
    private static DatabaseReference chatRef() {
        return FirebaseDatabase.getInstance().getReference().child(NODE_CHAT);
    }

    private static DatabaseReference participantRef() {
        return FirebaseDatabase.getInstance().getReference().child(NODE_PARTICIPANTS);
    }

    private static DatabaseReference messageRef() {
        return FirebaseDatabase.getInstance().getReference().child(NODE_MESSAGES);
    }
}

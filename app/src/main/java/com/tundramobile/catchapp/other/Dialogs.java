package com.tundramobile.catchapp.other;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.tundramobile.catchapp.interfaces.ProgressDialogHandler;

/**
 * Created by Sviatoslav Zaitsev on 27.04.17.
 */

public class Dialogs {
    public static void showFailureDialog(Context context, String message) {
        if(context instanceof ProgressDialogHandler) {
            ((ProgressDialogHandler) context).cancelProgressDialog();
        }

        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }
}

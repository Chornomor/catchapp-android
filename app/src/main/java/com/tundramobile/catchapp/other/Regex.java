package com.tundramobile.catchapp.other;

import android.text.TextUtils;
import android.util.Patterns;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.tundramobile.catchapp.entity.Country;

/**
 * Created by alexchern on 31.03.17.
 */

public class Regex {

    public static boolean isTextPhoneNumber(String text) {
        return Patterns.PHONE.matcher(text).matches();
    }

    public static boolean validatePhoneNumber(String phoneNo, Country country) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber number = new Phonenumber.PhoneNumber();
        try {
            number = phoneUtil.parse(phoneNo, country.getNameCode().toUpperCase());
        } catch (NumberParseException e) {
            e.printStackTrace();
        }

        return phoneUtil.isValidNumber(number);
    }


    public static boolean isEmailValid(String email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}

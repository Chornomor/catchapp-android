package com.tundramobile.catchapp.other;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.tundramobile.catchapp.BuildConfig;
import com.tundramobile.catchapp.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    private static int cacheSize = 4 * 1024 * 1024; // 4MB
    private static final LruCache<String, Typeface> cache = new LruCache<>(cacheSize);


    public static void logd(String tag, String str) {
        if (!BuildConfig.DEBUG)
            return;
        if (str.length() > 4000) {
            Log.d(tag, str.substring(0, 4000));
            logd(tag, str.substring(4000));
        } else {
            Log.d(tag, str);
        }
    }


    public static void loge(String tag, String str) {
        if (!BuildConfig.DEBUG)
            return;
        if (str.length() > 4000) {
            Log.e(tag, str.substring(0, 4000));
            loge(tag, str.substring(4000));
        } else {
            Log.e(tag, str);
        }
    }


    public static Typeface getTypeface(Context c, String assetPath) {
        try {
            synchronized (cache) {
                if (cache.get(assetPath) == null) {
                    Typeface t = Typeface.createFromAsset(c.getAssets(), assetPath);
                    cache.put(assetPath, t);
                }
                return cache.get(assetPath);
            }
        } catch (Exception e) {
            loge(TAG, "Could not get typeface '" + assetPath
                    + "' because " + e.getMessage());
            return null;
        }
    }


    public static void closeKeyboard(Activity activity) {
        if (activity == null) return;
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }


    public static int getRandomGreyShape() {
        Random r = new Random();
        switch (r.nextInt(2)) {
            case 0:
            default:
                return R.drawable.random_grey_1;
            case 1:
                return R.drawable.random_grey_2;
            case 2:
                return R.drawable.rect_gray;
        }
    }


    public static String getNameInitials(String name) {
        String[] splitName = name.split(" ");
        if (splitName.length > 1) {
            return getNameInitials(splitName[0], splitName[1]);
        } else if (name.length() > 1) {
            return name.substring(0, 2);
        } else {
            return name;
        }
    }


    public static String getNameInitials(String firstName, String lastName) {
        String initialsString = "";
        if (!TextUtils.isEmpty(firstName)) {
            if (firstName.length() > 1) {
                initialsString = firstName.substring(0, !TextUtils.isEmpty(lastName) ? 1 : 2);
            } else {
                initialsString = firstName;
            }
        }
        if (!TextUtils.isEmpty(lastName)) {
            initialsString += lastName.substring(0, 1);
        }
        return initialsString;
    }


    public static boolean openBrowser(Context context, String url) {

        if (TextUtils.isEmpty(url)) return false;

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
            return true;
        } else {
            loge(TAG, "No Intent available to handle action");
            return false;
        }
    }


    public static boolean openDialer(Context context, String phoneNumber) {

        if (TextUtils.isEmpty(phoneNumber)) return false;

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
            return true;
        } else {
            loge(TAG, "No Intent available to handle action");
            return false;
        }
    }


    public static boolean share(Context context, String shareText) {

        if (TextUtils.isEmpty(shareText)) return false;

        Intent shareIntent = ShareCompat.IntentBuilder.from((Activity) context)
                .setType("text/plain")
                .setText(shareText)
                .getIntent();

        if (shareIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(shareIntent);
            return true;
        } else {
            loge(TAG, "No Intent available to handle action");
            return false;
        }
    }
}

package com.tundramobile.catchapp.other;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.tundramobile.catchapp.manager.TopPopUpManager;

import java.util.Deque;
import java.util.LinkedList;

public class ErrorHandler {

    private static final String TAG = ErrorHandler.class.getSimpleName();
    private String currentError = null;
    private Deque<String> queue = new LinkedList<>();
    private Context context;
    private Handler handler = new Handler(Looper.getMainLooper());


    public static ErrorHandler getInstance(Context c) {
        return new ErrorHandler(c);
    }


    private ErrorHandler(Context context) {
        this.context = context;
    }


    public synchronized void postError(@StringRes int error) {
        postError(context.getString(error));
    }


    public synchronized void postError(String error) {
        Utils.logd(TAG, "postError: " + error);
        if (currentError == null) {
            showError(error);
        } else if (currentError.equals(error)) {
            // do nothing
        } else {
            queue.add(error);
        }
    }


    private void showError(String error) {
        currentError = error;
        TopPopUpManager.showTopPopUp(context, currentError, TopPopUpManager.isError);
        handler.post(this::checkQueue);
    }


    private void checkQueue() {
        if (queue.isEmpty()) {
            currentError = null;
        } else {
            showError(queue.poll());
        }
    }
}
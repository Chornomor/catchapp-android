package com.tundramobile.catchapp.other;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.blaze.fastpermission.FastPermission;
import com.tundramobile.catchapp.ui.viewmodel.ScheduleItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import rebus.permissionutils.PermissionEnum;

/**
 * Created by andreybofanov on 26.04.17.
 */

public class CalendarUtil {

    public static ArrayList<ScheduleItem> readCalendarEvent(Context context) {

        ArrayList<ScheduleItem> schedules = new ArrayList<>();
        if(!FastPermission.isGranted(context, PermissionEnum.READ_CALENDAR)) {
            return schedules;
        }
        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[] { "calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation" }, null,
                        null, null);
        if(cursor == null){
            return schedules;
        }
        Date today = new Date();
        while (cursor.moveToNext()){
            try {
                Date from = getDate(Long.parseLong(cursor.getString(3)));
                if (from.before(today)) {
                    continue;
                }
                Date to = getDate(Long.parseLong(cursor.getString(4)));
                ScheduleItem scheduleItem = new ScheduleItem(cursor.getString(1), from, to);
                scheduleItem.setToExternal();
                schedules.add(scheduleItem);
            } catch (NumberFormatException e){
                e.printStackTrace();
            }
        }
        return schedules;
    }

    public static Date getDate(long milliSeconds) {
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

        milliSeconds -= offsetInMillis;
        return new Date(milliSeconds);
    }
}

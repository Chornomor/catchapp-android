package com.tundramobile.catchapp.other;

import android.content.Context;
import android.provider.Settings;
import android.text.format.DateUtils;

import com.tundramobile.catchapp.manager.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by andreybofanov on 10.04.17.
 */

public class DateUtil {
    public static SimpleDateFormat TIME = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat NEW_EVENT_DATE = new SimpleDateFormat("dd MMMM");
    public static final SimpleDateFormat HEADER_DATE = new SimpleDateFormat("EEE dd MMM");
    public static final String DATE_PHOTO_NAME_FORMAT = "yy.MM.dd.mm.ss";


    public static String formatTime(Date date) {
        if (date == null)
            return null;
        return TIME.format(date);
    }


    public static String formatTime(long timestamp) {
        Date date = new Date(timestamp * 1000);
        return TIME.format(date);
    }


    public static String getTimeWithTimeFromTimestamp(long timestamp) {
        Date d = new Date(timestamp * 1000L);
        return DateUtils.getRelativeTimeSpanString(d.getTime(), System.currentTimeMillis(),
                DateUtils.DAY_IN_MILLIS).toString() + ", " + formatTime(d);
    }


    public static void updateTimeFormat(Context context) {
        SharedPrefManager sharedPrefManager = SharedPrefManager.getInstance(context);
        boolean isFirstLaunch = !sharedPrefManager.getIsIntroShowed();

        int timeFormat = 0;
        if (isFirstLaunch) {
            try {
                timeFormat = Settings.System.getInt(context.getContentResolver(), Settings.System.TIME_12_24);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            sharedPrefManager.saveHoursView(timeFormat == 24);
        } else {
            timeFormat = sharedPrefManager.isHoursViewEnabled() ? 24 : 12;
        }
        switch (timeFormat) {
            case 12:
                TIME = new SimpleDateFormat("hh:mm a");
                break;
            case 24:
                TIME = new SimpleDateFormat("HH:mm");
                break;
        }
    }


    public static String formatNewEventDate(Date date) {
        if (date == null)
            return null;
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(date);
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        if (calendarDate.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendarDate.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today";
        } else {
            return NEW_EVENT_DATE.format(date);
        }
    }


    public static String formatHeader(Date date) {
        if (date == null)
            return null;
        return HEADER_DATE.format(date);
    }
}

package com.tundramobile.catchapp.other;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.tundramobile.catchapp.R;

/**
 * Created by Sviatoslav Zaitsev on 15.05.16.
 */

// TODO: 14-Jun-16 Implement just setup method for recycler.
public class AdapterItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private OnItemSwipedListener listener;
    private Context context;

    // we want to cache these and not allocate anything repeatedly in the onChildDraw method
    private Drawable background;
    private Drawable xMark;
    private int xMarkMargin;
    private boolean initiated;
    private int actionMsgSize;

    public AdapterItemTouchHelper(Context context, OnItemSwipedListener listener){
      super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
      this.context = context;
      this.listener = listener;
      init();
    }

    private void init() {
      background = new ColorDrawable(ContextCompat.getColor(context, R.color.red));
      xMark = ContextCompat.getDrawable(context, R.drawable.ic_action_delete);
      xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
      xMarkMargin = (int) context.getResources().getDimension(R.dimen.auth_screen_paddingStart);
      actionMsgSize = (int) context.getResources().getDimension(R.dimen.new_event_header_text_size_15px);
      initiated = true;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
      //TODO: Not implemented here
      return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
      //Remove item
      listener.onItemSwiped(context, viewHolder.getAdapterPosition());
    }

    @Override
    public void onChildDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
      View itemView = viewHolder.itemView;

      // not sure why, but this method get's called for viewholder that are already swiped away
      if (viewHolder.getAdapterPosition() == -1) {
         // not interested in those
         return;
      }

      if (!initiated) {
         init();
      }

      // draw red background
      if(dX > 0) {
         background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + (int) dX, itemView.getBottom());
      } else {
         background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
      }
      background.draw(canvas);

      // draw x mark
      int itemHeight = itemView.getBottom() - itemView.getTop();
      int intrinsicWidth = xMark.getIntrinsicWidth();
      int intrinsicHeight = xMark.getIntrinsicWidth();

      int xMarkLeft = dX < 0
              ? itemView.getRight() - xMarkMargin - intrinsicWidth
              : itemView.getLeft()  + xMarkMargin;
      int xMarkRight = dX < 0
              ? itemView.getRight() - xMarkMargin
              : itemView.getLeft()  + xMarkMargin + intrinsicWidth;
      int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
      int xMarkBottom = xMarkTop + intrinsicHeight;
      xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

      xMark.draw(canvas);

      drawActionMsg(canvas, itemView);

      super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    private void drawActionMsg(Canvas canvas, View itemView) {
      Paint textPaint = new Paint();
      textPaint.setColor(Color.WHITE);
      textPaint.setTextAlign(Paint.Align.CENTER);
      textPaint.setTextSize(actionMsgSize);
      int xPos = itemView.getLeft() + (itemView.getWidth() / 2);
      int textCenterY = (int) ((textPaint.descent() + textPaint.ascent()) / 2);
      int yPos = itemView.getTop() + itemView.getHeight() / 2 - textCenterY;
      canvas.drawText("Removing...", xPos, yPos, textPaint);
    }

    /**
    * We're gonna setup another ItemDecorator that will draw the red background in the empty space while the items are animating to thier new positions
    * after an item is removed.
    */
    public static void setUpAnimationDecoratorHelper(final Context context, RecyclerView list) {
        list.addItemDecoration(new RecyclerView.ItemDecoration() {

            // we want to cache this and not allocate anything repeatedly in the onDraw method
            Drawable background;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(ContextCompat.getColor(context, R.color.red));
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {

                if (!initiated) {
                   init();
                }

                // only if animation is in progress
                if (parent.getItemAnimator().isRunning()) {

                    // some items might be animating down and some items might be animating up to close the gap left by the removed item
                    // this is not exclusive, both movement can be happening at the same time
                    // to reproduce this leave just enough items so the first one and the last one would be just a little off screen
                    // then remove one from the middle

                    // find first child with translationY > 0
                    // and last one with translationY < 0
                    // we're after a rect that is not covered in recycler-view views at this point in time
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;

                    // this is fixed
                    int left = 0;
                    int right = parent.getWidth();

                    // this we need to find out
                    int top = 0;
                    int bottom = 0;

                    // find relevant translating views
                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                      View child = parent.getLayoutManager().getChildAt(i);
                      if (child.getTranslationY() < 0) {
                         // view is coming down
                         lastViewComingDown = child;
                      } else if (child.getTranslationY() > 0) {
                         // view is coming up
                         if (firstViewComingUp == null) {
                            firstViewComingUp = child;
                         }
                      }
                    }

                    if (lastViewComingDown != null && firstViewComingUp != null) {
                      // views are coming down AND going up to fill the void
                      top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                      bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    } else if (lastViewComingDown != null) {
                      // views are going down to fill the void
                      top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                      bottom = lastViewComingDown.getBottom();
                    } else if (firstViewComingUp != null) {
                      // views are coming up to fill the void
                      top = firstViewComingUp.getTop();
                      bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }

                    background.setBounds(left, top, right, bottom);
                    background.draw(c);
                }
                super.onDraw(c, parent, state);
            }
        });
    }

    // TODO: 28.04.17 Make it more universal
    public interface OnItemSwipedListener {
        void onItemSwiped(Context context, int adapterPosition);
    }
}
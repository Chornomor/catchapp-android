package com.tundramobile.catchapp.other;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by Sviatoslav Zaitsev on 28.03.2016.
 */
public class LocationUtils {

    public static boolean isGPSEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
}
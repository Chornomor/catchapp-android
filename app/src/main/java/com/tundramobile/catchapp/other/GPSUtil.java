package com.tundramobile.catchapp.other;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;

/**
 * Created by Sviatoslav Zaitsev on 20.04.17.
 */

public class GPSUtil {

    public static final int PHOTO_ALPHA_ANIM_DURATION = 1000;

    public static void requestPlacePhoto(@Nullable GoogleApiClient client, String placeId,
                                         ImageView targetView, int placeholderRes) {
        targetView.setImageResource(placeholderRes);
        targetView.post(() -> {
            int width = targetView.getWidth();
            int height = targetView.getHeight();
            AppCompatActivity activity = (AppCompatActivity) targetView.getContext();

            if (client == null) return;
            Places.GeoDataApi.getPlacePhotos(client, placeId).then(new ResultTransform<PlacePhotoMetadataResult, Result>() {
                @Nullable
                @Override
                public PendingResult<Result> onSuccess(@NonNull PlacePhotoMetadataResult placePhotoMetadataResult) {
                    if (!placePhotoMetadataResult.getStatus().isSuccess())
                        return null;

                    PlacePhotoMetadataBuffer buffer = placePhotoMetadataResult.getPhotoMetadata();
                    if (buffer.getCount() > 0) {
                        PlacePhotoMetadata metadata = buffer.get(0);
                        if (metadata != null) {
                            Bitmap photo = metadata.getScaledPhoto(client, width, height).await().getBitmap();
                            if (activity != null) {
                                Drawable drawable = new BitmapDrawable(activity.getResources(), photo);
                                activity.runOnUiThread(() -> {
                                    targetView.setImageDrawable(drawable);
                                    animateDrawable(drawable);

                                });
                            }
                        }
                        buffer.release();
                    }
                    return null;
                }
            });
        });
    }


    private static void animateDrawable(Drawable drawable) {
        ObjectAnimator animator = ObjectAnimator
                .ofPropertyValuesHolder(drawable,
                        PropertyValuesHolder.ofInt("alpha", 0, 255));
        animator.setTarget(drawable);
        animator.setDuration(PHOTO_ALPHA_ANIM_DURATION);
        animator.start();
    }
}

package com.tundramobile.catchapp.event;

public class GetCatchUpsEvent {

    private final int type;


    public GetCatchUpsEvent(int type) {
        this.type = type;
    }


    public int getType() {
        return type;
    }
}

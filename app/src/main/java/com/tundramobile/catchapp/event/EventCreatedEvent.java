package com.tundramobile.catchapp.event;

/**
 * Created by alexchern on 25.04.17.
 */

public class EventCreatedEvent {

    private String title;
    private String message;


    public EventCreatedEvent(String title, String message) {
        this.title = title;
        this.message = message;
    }


    public String getTitle() {
        return title;
    }


    public String getMessage() {
        return message;
    }
}

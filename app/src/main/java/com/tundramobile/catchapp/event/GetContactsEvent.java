package com.tundramobile.catchapp.event;

public class GetContactsEvent {

    private final int type;


    public GetContactsEvent(int type) {
        this.type = type;
    }


    public int getType() {
        return type;
    }
}

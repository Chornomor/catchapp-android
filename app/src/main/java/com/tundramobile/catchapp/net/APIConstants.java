package com.tundramobile.catchapp.net;

/**
 * Created by alexchern on 30.03.17.
 */

public interface APIConstants {

    String BASE_URL = "http://dev.new.catchappdata.co.uk/api/";

    String RESPONSE_SUCCESS = "success";

    class EVENT_STATUS {
        public static final String OUTGOING = "EVENTSTATUS_OUTGOING";
        public static final String INCOMING = "EVENTSTATUS_INCOMING";
        public static final String CONFIRMED = "EVENTSTATUS_CONFIRMED";
    }

    String REQUEST_OTP = "login/requestOTP";
    String RESPOND_OTP = "login/respondOTP";
    String LOGOUT = "logout";
    String UPLOAD_PHOTO = "image/upload";
    String UPDATE_USER = "update/";
    String FETCH_USER = "fetch/";
    String ADDRESSBOOK = "addressbook";
    String CONTACTS = "/contacts";
    String RECENT = "/recent";
    String GROUPS = "/groups";
    String GROUP = "/group";
    String CREATE = "/create";
    String _CREATE_ = "create";
    String USERS = "users/";
    String CATCHUPS = "/catchups/";
    String CATCHUP_SLASH = "catchup/";
    String CANCEL = "cancel/";
    String DELETE = "delete/";
    String ACCEPT = "accept/";
    String REPORT = "report/";
    String DELETE_USER = "delete-user/";
    String CONFIRMED = CATCHUPS + "confirmed";
    String INCOMING = CATCHUPS + "incoming";
    String OUTGOING = CATCHUPS + "outgoing";
    String SLASH_CATCHUPS = "/catchups";
    String DETAILS = "details/";
    String UPDATE_LOCATION = "update/";
    String NOTIFICATIONS = "notifications";
}

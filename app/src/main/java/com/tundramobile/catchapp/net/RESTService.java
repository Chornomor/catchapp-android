package com.tundramobile.catchapp.net;

import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.CatchUpDetail;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.request.CreateCatchAppBody;
import com.tundramobile.catchapp.entity.request.CreateGroupBody;
import com.tundramobile.catchapp.entity.request.GooglePlaceBody;
import com.tundramobile.catchapp.entity.request.MiniGooglePlaceBody;
import com.tundramobile.catchapp.entity.request.ReportBody;
import com.tundramobile.catchapp.entity.request.RequestContactBody;
import com.tundramobile.catchapp.entity.request.RequestOTPBody;
import com.tundramobile.catchapp.entity.request.RequestUpdateUser;
import com.tundramobile.catchapp.entity.request.RequestUpdateUserPhoto;
import com.tundramobile.catchapp.entity.request.RespondOTP;
import com.tundramobile.catchapp.entity.response.BaseResponseModel;
import com.tundramobile.catchapp.entity.response.BooleanIsSuccessResponse;
import com.tundramobile.catchapp.entity.response.CatchUpsCounters;
import com.tundramobile.catchapp.entity.response.ContactModel;
import com.tundramobile.catchapp.entity.response.BooleanIsSuccessResponse;
import com.tundramobile.catchapp.entity.response.CreateCatchUpResponse;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.LocationResponse;
import com.tundramobile.catchapp.entity.response.LoginRequestOTP;
import com.tundramobile.catchapp.entity.response.LoginRespondOTP;
import com.tundramobile.catchapp.entity.response.Notification;
import com.tundramobile.catchapp.entity.response.ReportResponse;
import com.tundramobile.catchapp.entity.response.UpdateUserResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alexchern on 30.03.17.
 */

public interface RESTService {

    @Headers("Accept: application/json")
    @POST(APIConstants.REQUEST_OTP)
    Call<LoginRequestOTP> requestOTP(@Body RequestOTPBody requestOTPBody);


    @Headers("Accept: application/json")
    @POST(APIConstants.RESPOND_OTP)
    Call<LoginRespondOTP> respondOTP(@Body RespondOTP respondOTPBody);


    @POST(APIConstants.FETCH_USER)
    Call<ListResponseModel<com.tundramobile.catchapp.entity.User>> fetchUser(@Header("token") String token,
                                                                             @Header("userID") int userID,
                                                                             @Query("class") String tableClass,
                                                                             @Query("ids") String ids,
                                                                             @Query("weak") boolean isWeak);


    @Headers("Accept: application/json")
    @POST(APIConstants.UPDATE_USER)
    Call<UpdateUserResponse> updateUser(@Header("content-type") String content_type,
                                        @Header("token") String token,
                                        @Header("userID") int userID,
                                        @Query("class") String tableClass,
                                        @Query("id") int id,
                                        @Body RequestUpdateUser.UpdateUserModel user);

    @Headers("Accept: application/json")
    @POST(APIConstants.UPDATE_USER)
    Call<UpdateUserResponse> updateUser(@Header("content-type") String content_type,
                                        @Header("token") String token,
                                        @Header("userID") int userID,
                                        @Query("class") String tableClass,
                                        @Query("id") int id,
                                        @Body RequestUpdateUserPhoto.UpdateUserPhotoModel user);

    @Headers("Accept: application/json")
    @POST(APIConstants.LOGOUT)
    Call<UpdateUserResponse> logout(@Header("token") String token);

    @Headers("Accept: application/json")
    @Multipart
    @POST(APIConstants.UPLOAD_PHOTO)
    Call<UpdateUserResponse> uploadPhoto(@Header("token") String token,
                                         @Part MultipartBody.Part data);


    @GET(APIConstants.USERS + "{user_id}" + APIConstants.CONFIRMED)
    Call<ListResponseModel<CatchUp>> getConfirmedCatchUps(@Header("token") String token,
                                                          @Path("user_id") int userId);


    @GET(APIConstants.USERS + "{user_id}" + APIConstants.INCOMING)
    Call<ListResponseModel<CatchUp>> getIncomingCatchUps(@Header("token") String token,
                                                         @Path("user_id") int userId);


    @GET(APIConstants.USERS + "{user_id}" + APIConstants.OUTGOING)
    Call<ListResponseModel<CatchUp>> getOutgoingCatchUps(@Header("token") String token,
                                                         @Path("user_id") int userId);


    @Headers("accept: application/json")
    @DELETE(APIConstants.CATCHUP_SLASH + APIConstants.DELETE + "{catchup_id}")
    Call<BooleanIsSuccessResponse> deleteCatchUp(@Header("token") String token,
                                                 @Path("catchup_id") int catchUpId);

    @Headers("accept: application/json")
    @DELETE(APIConstants.CATCHUP_SLASH + APIConstants.DELETE + "{catchup_id}/{slot_id}")
    Call<BooleanIsSuccessResponse> deleteSlot(@Header("token") String token,
                                              @Path("catchup_id") long catchUpId,
                                              @Path("slot_id") long slotId);

    @Headers("accept: application/json")
    @POST(APIConstants.CATCHUP_SLASH + APIConstants.CANCEL + "{catchup_id}/{slot_id}")
    Call<BooleanIsSuccessResponse> cancelAttendanceForTimeSlot(@Header("token") String token,
                                                               @Path("catchup_id") int catchUpId,
                                                               @Path("slot_id") int slotId);

    @Headers("accept: application/json")
    @DELETE(APIConstants.CATCHUP_SLASH + APIConstants.DELETE_USER + "{catchup_id}/{user_id}")
    Call<BooleanIsSuccessResponse> deleteInviteToEvent(@Header("token") String token,
                                                       @Path("catchup_id") int catchUpId,
                                                       @Path("user_id") int slotId);

    @Headers("accept: application/json")
    @POST(APIConstants.CATCHUP_SLASH + APIConstants.CANCEL + "{catchup_id}")
    Call<BooleanIsSuccessResponse> cancelCatchUpInvitation(@Header("token") String token,
                                                           @Path("catchup_id") int catchUpId);


    // TODO: 11.04.17 need to create response model if it will be necessary
    @Headers("accept: application/json")
    @POST(APIConstants.CATCHUP_SLASH + APIConstants.ACCEPT + "{catchup_id}/{event_id}")
    Call<BaseResponseModel> acceptCatchUpTimeSlot(@Header("token") String token,
                                                  @Header("userID") int userId,
                                                  @Path("catchup_id") int catchUpId,
                                                  @Path("event_id") int slotId);

    @Headers("accept: application/json")
    @POST(APIConstants.CATCHUP_SLASH + APIConstants.REPORT + "{catchup_id}")
    Call<ReportResponse> reportCatchUp(@Header("token") String token,
                                       @Path("catchup_id") int catchUpId,
                                       @Body ReportBody message);

    @GET(APIConstants.USERS + "{user_id}" + APIConstants.SLASH_CATCHUPS)
    Call<CatchUpsCounters> getCatchUpsCounters(@Header("token") String token,
                                               @Path("user_id") int userId);

    @Headers("accept: application/json")
    @POST(APIConstants.ADDRESSBOOK + APIConstants.CONTACTS)
    Call<ContactModel> getContacts(@Header("access-control-allow-origin") String access,
                                   @Header("token") String token,
                                   @Body RequestContactBody contacts);


    @Headers("accept: application/json")
    @GET(APIConstants.ADDRESSBOOK + APIConstants.RECENT)
    Call<ListResponseModel<Contact>> getRecentContacts(@Header("access-control-allow-origin") String access,
                                                       @Header("token") String token);


    @Headers("accept: application/json")
    @GET(APIConstants.ADDRESSBOOK + APIConstants.GROUPS)
    Call<ListResponseModel<Contact>> getContactGroups(@Header("access-control-allow-origin") String access,
                                                      @Header("token") String token);

    @Headers("accept: application/json")
    @POST(APIConstants.ADDRESSBOOK + APIConstants.GROUP + APIConstants.CREATE)
    Call<ListResponseModel<Contact>> createGroup(@Header("token") String token,
                                                 @Body CreateGroupBody body);

    @Headers("accept: application/json")
    @DELETE(APIConstants.ADDRESSBOOK + APIConstants.GROUP + "/{group_id}")
    Call<LoginRequestOTP> deleteGroup(@Header("token") String token,
                                      @Path("group_id") int groupId);

    @Headers("accept: application/json")
    @POST(APIConstants.ADDRESSBOOK + APIConstants.GROUP + "/{group_id}")
    Call<ListResponseModel<Contact>> editGroup(@Header("token") String token,
                                               @Path("group_id") int groupId,
                                               @Body CreateGroupBody groupBody);

    @Headers("accept: application/json")
    @POST(APIConstants.CATCHUP_SLASH + APIConstants._CREATE_)
    Call<CreateCatchUpResponse> createCatchup(@Header("token") String token,
                                                                 @Header("userID") int userID,
                                                                 @Body CreateCatchAppBody body);

    @Headers("accept: application/json")
    @GET(APIConstants.CATCHUP_SLASH + APIConstants.DETAILS + "{catchup_id}/" + APIConstants.EVENT_STATUS.OUTGOING)
    Call<ListResponseModel<CatchUpDetail>> getCatchUpDetailsOutgoing(@Header("token") String token,
                                                                     @Path("catchup_id") int catchupId);

    @Headers("accept: application/json")
    @GET(APIConstants.CATCHUP_SLASH + APIConstants.DETAILS + "{catchup_id}/" + APIConstants.EVENT_STATUS.INCOMING)
    Call<ListResponseModel<CatchUpDetail>> getCatchUpDetailsIncoming(@Header("token") String token,
                                                                     @Path("catchup_id") int catchupId);

    @Headers("accept: application/json")
    @GET(APIConstants.CATCHUP_SLASH + APIConstants.DETAILS + "{catchup_id}/"
            + APIConstants.EVENT_STATUS.CONFIRMED + "/{slot_id}")
    Call<ListResponseModel<CatchUpDetail>> getCatchUpDetailsConfirmed(@Header("token") String token,
                                                                      @Path("catchup_id") int catchupId,
                                                                      @Path("slot_id") int slotId);

    @Headers("accept: application/json")
    @POST(APIConstants.UPDATE_LOCATION)
    Call<LocationResponse> updateLocation(@Header("content-type") String content_type,
                                          @Header("token") String token,
                                          @Header("userID") int userID,
                                          @Query("class") String tableClass,
                                          @Query("id") int id,
                                          @Body MiniGooglePlaceBody place);

    /** Notifications requests **/
    @Headers("accept: application/json")
    @GET(APIConstants.NOTIFICATIONS)
    Call<ListResponseModel<Notification>> getNotifications(
            @Header("token") String token);
}
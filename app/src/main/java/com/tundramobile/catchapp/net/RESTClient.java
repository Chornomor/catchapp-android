package com.tundramobile.catchapp.net;

import android.support.annotation.NonNull;

import com.tundramobile.catchapp.entity.CatchUp;
import com.tundramobile.catchapp.entity.CatchUpDetail;
import com.tundramobile.catchapp.entity.Contact;
import com.tundramobile.catchapp.entity.User;
import com.tundramobile.catchapp.entity.request.CreateCatchAppBody;
import com.tundramobile.catchapp.entity.request.CreateGroupBody;
import com.tundramobile.catchapp.entity.request.MiniGooglePlaceBody;
import com.tundramobile.catchapp.entity.request.ReportBody;
import com.tundramobile.catchapp.entity.request.RequestContactBody;
import com.tundramobile.catchapp.entity.request.RequestOTPBody;
import com.tundramobile.catchapp.entity.request.RequestUpdateUser;
import com.tundramobile.catchapp.entity.request.RequestUpdateUserPhoto;
import com.tundramobile.catchapp.entity.request.RespondOTP;
import com.tundramobile.catchapp.entity.response.BaseResponseModel;
import com.tundramobile.catchapp.entity.response.BooleanIsSuccessResponse;
import com.tundramobile.catchapp.entity.response.CatchUpsCounters;
import com.tundramobile.catchapp.entity.response.ContactModel;
import com.tundramobile.catchapp.entity.response.CreateCatchUpResponse;
import com.tundramobile.catchapp.entity.response.ListResponseModel;
import com.tundramobile.catchapp.entity.response.LocationResponse;
import com.tundramobile.catchapp.entity.response.LoginRequestOTP;
import com.tundramobile.catchapp.entity.response.LoginRespondOTP;
import com.tundramobile.catchapp.entity.response.Notification;
import com.tundramobile.catchapp.entity.response.ReportResponse;
import com.tundramobile.catchapp.entity.response.UpdateUserResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

/**
 * Created by alexchern on 30.03.17.
 */

public class RESTClient {

    private static RESTClient instance;


    private RESTClient() {
    }


    public static synchronized RESTClient getInstance() {
        if (instance == null) {
            synchronized (RESTClient.class) {
                if (instance == null) {
                    instance = new RESTClient();
                }
            }
        }
        return instance;
    }


    public void updateUser(RequestUpdateUser updateUserBody, Callback<UpdateUserResponse> callback) {
        NetworkManager.getRESTService().updateUser("application/json", updateUserBody.getToken(), updateUserBody.getUserID(), "User", updateUserBody.getUserID(), updateUserBody.getUser()).enqueue(callback);
    }


    public void updateUserPhoto(RequestUpdateUserPhoto updateUserBody, Callback<UpdateUserResponse> callback) {
        NetworkManager.getRESTService().updateUser("application/json", updateUserBody.getToken(), updateUserBody.getUserID(), "User", updateUserBody.getUserID(), updateUserBody.getUser()).enqueue(callback);
    }


    public void logout(String token, Callback<UpdateUserResponse> callback) {
        NetworkManager.getRESTService().logout(token).enqueue(callback);
    }


    public void uploadUserPhoto(String token, File userPhoto, Callback<UpdateUserResponse> callback) {
        MultipartBody.Part photoBody = null;
        if (userPhoto != null) {
            RequestBody requestBodyPhoto = RequestBody.create(MediaType.parse("image/png"), userPhoto);
            photoBody = MultipartBody.Part.createFormData("data", userPhoto.getName(), requestBodyPhoto);
        }
        NetworkManager.getRESTService().uploadPhoto(token, photoBody).enqueue(callback);
    }


    public void fetchUser(LoginRespondOTP requestUserBody, Callback<ListResponseModel<User>> callback, Long... userIds) {
        String userIdsString = "";
        if(userIds.length == 0){
            userIdsString += requestUserBody.getId();
        } else {
            userIdsString += userIds[0];
            for(int i = 1; i < userIds.length; i++){
                userIdsString += ",";
                userIdsString += userIds[i];
            }
        }
        NetworkManager.getRESTService().fetchUser(requestUserBody.getToken(), requestUserBody.getId(), "User", userIdsString, true).enqueue(callback);
    }


    public void fetchUsersByIds(String token, int myId, String requestingUsersIds, Callback<ListResponseModel<User>> callback) {
        NetworkManager.getRESTService().fetchUser(token, myId, "User", requestingUsersIds, true).enqueue(callback);
    }


    public void respondOTP(RespondOTP requestOTPBody, Callback<LoginRespondOTP> callback) {
        NetworkManager.getRESTService().respondOTP(requestOTPBody).enqueue(callback);
    }


    public void requestOTP(RequestOTPBody requestOTPBody, Callback<LoginRequestOTP> callback) {
        NetworkManager.getRESTService().requestOTP(requestOTPBody).enqueue(callback);
    }


    public void getConfirmedCatchUps(int userId, String token, @NonNull HandledCallback<ListResponseModel<CatchUp>> callback) {
        NetworkManager.getRESTService().getConfirmedCatchUps(token, userId).enqueue(callback);
    }


    public void getIncomingCatchUps(int userId, String token, @NonNull HandledCallback<ListResponseModel<CatchUp>> callback) {
        NetworkManager.getRESTService().getIncomingCatchUps(token, userId).enqueue(callback);
    }


    public void getOutgoingCatchUps(int userId, String token, @NonNull HandledCallback<ListResponseModel<CatchUp>> callback) {
        NetworkManager.getRESTService().getOutgoingCatchUps(token, userId).enqueue(callback);
    }


    public void deleteCatchUp(String token, int catchUpId, @NonNull HandledCallback<BooleanIsSuccessResponse> callback) {
        NetworkManager.getRESTService().deleteCatchUp(token, catchUpId).enqueue(callback);
    }


    public void deleteSlot(String token, long catchUpId, long slotId, @NonNull HandledCallback<BooleanIsSuccessResponse> callback) {
        NetworkManager.getRESTService().deleteSlot(token, catchUpId, slotId).enqueue(callback);
    }


    public void cancelAttendanceForTimeSlot(String token, int catchUpId, int slotId, @NonNull HandledCallback<BooleanIsSuccessResponse> callback) {
        NetworkManager.getRESTService().cancelAttendanceForTimeSlot(token, catchUpId, slotId).enqueue(callback);
    }


    public void deleteUserInvitationToEvent(String token, int catchUpId, int user_id, @NonNull HandledCallback<BooleanIsSuccessResponse> callback) {
        NetworkManager.getRESTService().deleteInviteToEvent(token, catchUpId, user_id).enqueue(callback);
    }


    public void cancelCatchUpInvitation(String token, int catchUpId, @NonNull HandledCallback<BooleanIsSuccessResponse> callback) {
        NetworkManager.getRESTService().cancelCatchUpInvitation(token, catchUpId).enqueue(callback);
    }


    public void acceptCatchUpTimeSlot(String token, int userId, int catchUpId, int slotId, @NonNull HandledCallback<BaseResponseModel> callback) {
        NetworkManager.getRESTService().acceptCatchUpTimeSlot(token, userId, catchUpId, slotId).enqueue(callback);
    }


    public void reportCatchUp(String token, int catchUpId, String reportMassage, @NonNull HandledCallback<ReportResponse> callback) {
        NetworkManager.getRESTService().reportCatchUp(token, catchUpId, new ReportBody(reportMassage)).enqueue(callback);
    }


    public void createCatchApp(String token, int userId, CreateCatchAppBody body, @NonNull HandledCallback<CreateCatchUpResponse> callback) {
        NetworkManager.getRESTService().createCatchup(token, userId, body).enqueue(callback);
    }

    public void getCatchUpsCounters(String token, int userId, @NonNull HandledCallback<CatchUpsCounters> callback) {
        NetworkManager.getRESTService().getCatchUpsCounters(token, userId).enqueue(callback);
    }


    public void getContacts(String token, RequestContactBody contacts, @NonNull HandledCallback<ContactModel> callback) {
        NetworkManager.getRESTService().getContacts("*", token, contacts).enqueue(callback);
    }


    public void getRecentContacts(String token, @NonNull HandledCallback<ListResponseModel<Contact>> callback) {
        NetworkManager.getRESTService().getRecentContacts("*", token).enqueue(callback);
    }


    public void getContactGroups(String token, @NonNull HandledCallback<ListResponseModel<Contact>> callback) {
        NetworkManager.getRESTService().getContactGroups("*", token).enqueue(callback);
    }


    public void createGroup(String token, CreateGroupBody body, @NonNull HandledCallback<ListResponseModel<Contact>> callback) {
        NetworkManager.getRESTService().createGroup(token, body).enqueue(callback);
    }


    public void deleteGroup(String token, int groupId, @NonNull HandledCallback<LoginRequestOTP> callback) {
        NetworkManager.getRESTService().deleteGroup(token, groupId).enqueue(callback);
    }


    public void editGroup(String token, int groupId, CreateGroupBody groupBody, @NonNull HandledCallback<ListResponseModel<Contact>> callback) {
        NetworkManager.getRESTService().editGroup(token, groupId, groupBody).enqueue(callback);
    }


    public void getCatchUpDetailsIncoming(String token, int catchUpId, @NonNull HandledCallback<ListResponseModel<CatchUpDetail>> callback) {
        NetworkManager.getRESTService().getCatchUpDetailsIncoming(token, catchUpId).enqueue(callback);
    }


    public void getCatchUpDetailsOutgoing(String token, int catchUpId, @NonNull HandledCallback<ListResponseModel<CatchUpDetail>> callback) {
        NetworkManager.getRESTService().getCatchUpDetailsOutgoing(token, catchUpId).enqueue(callback);
    }


    public void getCatchUpDetailsConfirmed(String token, int catchUpId, int slotId, @NonNull HandledCallback<ListResponseModel<CatchUpDetail>> callback) {
        NetworkManager.getRESTService().getCatchUpDetailsConfirmed(token, catchUpId, slotId).enqueue(callback);
    }

    public void updateLocation(String token, int userId, int locationId, MiniGooglePlaceBody body, @NonNull HandledCallback<LocationResponse> callback) {
        NetworkManager.getRESTService().updateLocation("application/json",
                token, userId, "CatchAppLocation", locationId, body).enqueue(callback);
    }

    public void getNotifications(String token, @NonNull HandledCallback<ListResponseModel<Notification>> callback) {
        NetworkManager.getRESTService().getNotifications(token).enqueue(callback);
    }
}

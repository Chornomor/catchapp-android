package com.tundramobile.catchapp.net;

import com.tundramobile.catchapp.entity.response.BaseResponseModel;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class HandledCallback<T extends BaseResponseModel> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.code() != HttpURLConnection.HTTP_OK) {
            onFail(call, response);
        } else {
            if (response.body().getError() == null || response.body().getError().isEmpty()) {
                onSuccess(call, response);
            } else {
                onFail(call, response);
            }
        }
    }


    @Override
    public void onFailure(Call<T> call, Throwable t) {
        String error = "";
        if (t.getMessage() != null && t.getMessage().contains("Unable to resolve host")) {
            error = "No internet connection";
        } else {
            error = "Internal server error";
        }
        onProcessFailed(call, t, error);
    }


    public abstract void onSuccess(Call<T> call, Response<T> response);

    public abstract void onFail(Call<T> call, Response<T> response);

    public abstract void onProcessFailed(Call<T> call, Throwable t, String error);
}

package com.tundramobile.catchapp.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by eugenetroyanskii on 27.04.17.
 */

public class NetworkReceiver extends BroadcastReceiver {

    NetworkStateReceiverListener listener;


    public NetworkReceiver(NetworkStateReceiverListener listener) {
        this.listener = listener;
    }


    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (NetworkManager.isNetworkAvailable(context)) {
            listener.networkAvailable();
        } else {
            listener.networkUnavailable();
        }

    }



    public interface NetworkStateReceiverListener {
        void networkAvailable();


        void networkUnavailable();
    }
}
package com.tundramobile.catchapp.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.tundramobile.catchapp.other.Utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alexchern on 30.03.17.
 */

public class NetworkManager {

    private static RESTService restService;
    private static boolean isMultipart = false;

    /*timeout values in seconds*/
    public static final int CONNECTION_TIMEOUT = 10;
    public static final int WRITE_TIMEOUT = 10;
    public static final int READ_TIMEOUT = 10;


    public static RESTService getRESTService() {
        if (restService == null) {
            synchronized (NetworkManager.class) {
                if (restService == null) {

                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(new RESTInterceptor())
                            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                            .build();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(APIConstants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(client)
                            .build();

                    restService = retrofit.create(RESTService.class);
                }
            }
        }
        return restService;
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private static class RESTInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();
            if (!isContentTypeMultiPart(request)) {
                builder.addHeader("content-type","application/json");
                request = builder.build();
            } else {
                builder.addHeader("content-type","multipart/form-data");
                builder.addHeader("access-control-allow-origin","*");
                request = builder.build();
            }
            Buffer buffer = new Buffer();
            if (request.body() != null)
                request.body().writeTo(buffer);
            /*we use this hack for prevent OOM crash when send multipart*/
            if (isMultipart) {
                isMultipart = false;
                Utils.logd("HTTP Request", "MULTIPART REQUEST DON'T LOGING");
            } else {
                Utils.logd("HTTP Request", "Request to " + request.url().toString()
                        + "\n" + request.headers().toString()
                        + "\n" + buffer.readUtf8());
            }
            long t1 = System.nanoTime();
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();

            String msg = response.body().string();
            msg = msg.replace("\r", ""); // Note: Messages with '\r' not displayed correctly in logcat

            Utils.logd("HTTP Response", String.format("Response from %s in %.1fms%n\n%s",
                    response.request().url().toString(), (t2 - t1) / 1e6d, msg));

            Utils.logd("HTTP Response", "Response code = " + response.code());

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), msg))
                    .build();
        }
    }


    private static boolean isContentTypeMultiPart(Request request) {
        isMultipart = request.body() != null && request.body().contentType() != null && request.body().contentType().toString().contains("multipart/form-data");
        return isMultipart;
    }
}
package com.tundramobile.catchapp.net.interfaces;

/**
 * Created by eugenetroyanskii on 11.04.17.
 */

public interface DeleteRequestListener {

    void onSlotDeleteListener(boolean isSuccess, String error);

}

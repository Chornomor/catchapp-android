package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eugenetroyanskii on 05.04.17.
 */

public class Contact implements Parcelable {

    public final static int UNREGISTERED_CONTACT_IN_SYSTEM = 0;
    public final static int REGISTERED_CONTACT_IN_SYSTEM = 1;

    private String name;
    private String mobile;
    private int uid;
    private String created;
    private int contactType = UNREGISTERED_CONTACT_IN_SYSTEM;

    @SerializedName("profilePic_url")
    private String profilePic;
    /*by default we suggest that any of contacts is registered
    * after we get response from server we are change this value*/

    private List<Contact> members;
    private int count;


    public int getContactType() {
        return contactType;
    }


    public void setContactType(int contactType) {
        this.contactType = contactType;
    }


    public Contact(String profilePic) {
        this.profilePic = profilePic;
    }


    public int getCount() {
        return count;
    }


    public List<Contact> getMembers() {
        return members;
    }


    public int getUid() {
        return uid;
    }


    public String getCreated() {
        return created;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getProfilePic() {
        return profilePic;
    }


    public String getMobile() {
        return mobile;
    }

    public boolean isGroupContact(){
        return members != null && !members.isEmpty();
    }


    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", uid=" + uid +
                ", created='" + created + '\'' +
                ", profilePic='" + profilePic + '\'' +
                ", members=" + members +
                ", count=" + count +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.uid);
        dest.writeString(this.created);
        dest.writeString(this.name);
        dest.writeString(this.profilePic);
        dest.writeString(this.mobile);
        dest.writeList(this.members);
        dest.writeInt(this.count);
    }


    public Contact() {
    }


    protected Contact(Parcel in) {
        this.uid = in.readInt();
        this.created = in.readString();
        this.name = in.readString();
        this.profilePic = in.readString();
        this.mobile = in.readString();
        this.members = new ArrayList<>();
        in.readList(this.members, Contact.class.getClassLoader());
        this.count = in.readInt();
    }


    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }


        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (uid != contact.uid) return false;
        if (contactType != contact.contactType) return false;
        if (count != contact.count) return false;
        if (name != null ? !name.equals(contact.name) : contact.name != null) return false;
        if (mobile != null ? !mobile.equals(contact.mobile) : contact.mobile != null) return false;
        if (created != null ? !created.equals(contact.created) : contact.created != null)
            return false;
        if (profilePic != null ? !profilePic.equals(contact.profilePic) : contact.profilePic != null)
            return false;
        return members != null ? members.equals(contact.members) : contact.members == null;

    }


    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + uid;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + contactType;
        result = 31 * result + (profilePic != null ? profilePic.hashCode() : 0);
        result = 31 * result + (members != null ? members.hashCode() : 0);
        result = 31 * result + count;
        return result;
    }
}

package com.tundramobile.catchapp.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class GooglePlace {

    private int id;
    private String googlePlacesID;
    private double lat;
    private double lng;
    private String name;
    private ArrayList<String> types;
    private float rating;
    private String address;
    private ArrayList<OpeningHours> openingHours;
    private String urlString;
    private String mapsUrlString;
    private String phoneNumber;
    private String formattedPhoneNumber;
    private int priceLevel;
    private ArrayList<String> photoIDs;


    public int getId() {
        return id;
    }


    public String getGooglePlacesID() {
        return googlePlacesID;
    }


    public double getLat() {
        return lat;
    }


    public double getLng() {
        return lng;
    }


    public String getName() {
        return name;
    }


    public ArrayList<String> getTypes() {
        return types;
    }


    public float getRating() {
        return rating;
    }


    public String getAddress() {
        return address;
    }


    public ArrayList<OpeningHours> getOpeningHours() {
        return openingHours;
    }


    public String getUrlString() {
        return urlString;
    }


    public String getMapsUrlString() {
        return mapsUrlString;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }


    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }


    public int getPriceLevel() {
        return priceLevel;
    }


    public ArrayList<String> getPhotoIDs() {
        return photoIDs;
    }


    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }
}

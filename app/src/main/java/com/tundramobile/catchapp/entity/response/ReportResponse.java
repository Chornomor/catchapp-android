package com.tundramobile.catchapp.entity.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eugenetroyanskii on 11.04.17.
 */

public class ReportResponse extends BaseResponseModel {

    @SerializedName("is_success")
    private boolean isSuccess;
    private String success;


    public boolean isSuccess() {
        return isSuccess;
    }


    public String getSuccess() {
        return success;
    }
}

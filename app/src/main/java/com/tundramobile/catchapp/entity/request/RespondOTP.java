package com.tundramobile.catchapp.entity.request;

/**
 * Created by eugenetroyanskii on 03.04.17.
 */

public class RespondOTP {

    String mobile;
    String otp;

    public RespondOTP(String mobile, String otp) {
        this.otp = otp;
        this.mobile = mobile;
    }
}

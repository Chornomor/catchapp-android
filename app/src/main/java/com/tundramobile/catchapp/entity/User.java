package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexchern on 31.03.17.
 */

public class User implements Parcelable {

    private int id;
    private String mobile = "";
    private String firstName = "";
    private String lastName = "";
    private String deviceToken = "";
    private String homeEmail = "";
    @SerializedName("profilePic_url")
    private String profilePic;
    private String workEmail = "";
    private String bio = "";
    private String jobTitle = "";
    private String skype = "";
    private int loginCount;
    private String address = "";
    private int permissionLocation;
    private int permissionNotification;
    private int permissionCalendarAny;
    private int permissionCalendarNative;
    private int permissionContacts;


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getMobile() {
        return mobile;
    }


    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getDeviceToken() {
        return deviceToken;
    }


    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }


    public String getHomeEmail() {
        return homeEmail;
    }


    public void setHomeEmail(String homeEmail) {
        this.homeEmail = homeEmail;
    }


    public String getProfilePic() {
        return profilePic;
    }


    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }


    public String getWorkEmail() {
        return workEmail;
    }


    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }


    public String getBio() {
        return bio;
    }


    public void setBio(String bio) {
        this.bio = bio;
    }


    public String getJobTitle() {
        return jobTitle;
    }


    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }


    public String getSkype() {
        return skype;
    }


    public void setSkype(String skype) {
        this.skype = skype;
    }


    public int getLoginCount() {
        return loginCount;
    }


    public void setLoginCount(int loginCount) {
        this.loginCount = loginCount;
    }


    public String getAddress() {
        return address;
    }


    public void setAddress(String address) {
        this.address = address;
    }


    public int getPermissionLocation() {
        return permissionLocation;
    }


    public void setPermissionLocation(int permissionLocation) {
        this.permissionLocation = permissionLocation;
    }


    public int getPermissionNotification() {
        return permissionNotification;
    }


    public void setPermissionNotification(int permissionNotification) {
        this.permissionNotification = permissionNotification;
    }


    public int getPermissionCalendarAny() {
        return permissionCalendarAny;
    }


    public void setPermissionCalendarAny(int permissionCalendarAny) {
        this.permissionCalendarAny = permissionCalendarAny;
    }


    public int getPermissionCalendarNative() {
        return permissionCalendarNative;
    }


    public void setPermissionCalendarNative(int permissionCalendarNative) {
        this.permissionCalendarNative = permissionCalendarNative;
    }


    public int getPermissionContacts() {
        return permissionContacts;
    }


    public void setPermissionContacts(int permissionContacts) {
        this.permissionContacts = permissionContacts;
    }


    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }


    @Override
    public String toString() {
        return "UserModel{" +
                ", id=" + id +
                ", mobile='" + mobile + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", deviceToken='" + deviceToken + '\'' +
                ", homeEmail='" + homeEmail + '\'' +
                ", profilePic=" + profilePic +
                ", workEmail='" + workEmail + '\'' +
                ", bio='" + bio + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", skype='" + skype + '\'' +
                ", loginCount=" + loginCount +
                ", address='" + address + '\'' +
                ", permissionLocation=" + permissionLocation +
                ", permissionNotification=" + permissionNotification +
                ", permissionCalendarAny=" + permissionCalendarAny +
                ", permissionCalendarNative=" + permissionCalendarNative +
                ", permissionContacts=" + permissionContacts +
                '}';
    }


    public User() {
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.mobile);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.deviceToken);
        dest.writeString(this.homeEmail);
        dest.writeString(this.profilePic);
        dest.writeString(this.workEmail);
        dest.writeString(this.bio);
        dest.writeString(this.jobTitle);
        dest.writeString(this.skype);
        dest.writeInt(this.loginCount);
        dest.writeString(this.address);
        dest.writeInt(this.permissionLocation);
        dest.writeInt(this.permissionNotification);
        dest.writeInt(this.permissionCalendarAny);
        dest.writeInt(this.permissionCalendarNative);
        dest.writeInt(this.permissionContacts);
    }


    protected User(Parcel in) {
        this.id = in.readInt();
        this.mobile = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.deviceToken = in.readString();
        this.homeEmail = in.readString();
        this.profilePic = in.readString();
        this.workEmail = in.readString();
        this.bio = in.readString();
        this.jobTitle = in.readString();
        this.skype = in.readString();
        this.loginCount = in.readInt();
        this.address = in.readString();
        this.permissionLocation = in.readInt();
        this.permissionNotification = in.readInt();
        this.permissionCalendarAny = in.readInt();
        this.permissionCalendarNative = in.readInt();
        this.permissionContacts = in.readInt();
    }


    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }


        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}

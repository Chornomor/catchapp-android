package com.tundramobile.catchapp.entity.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Meta implements Parcelable {

    @SerializedName("mode")
    String mode;


    public Meta() {
    }


    public String getMode() {
        return mode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meta meta = (Meta) o;

        return mode != null ? mode.equals(meta.mode) : meta.mode == null;

    }


    @Override
    public int hashCode() {
        return mode != null ? mode.hashCode() : 0;
    }


    @Override
    public String toString() {
        return "Meta{" +
                "mode='" + mode + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mode);
    }


    protected Meta(Parcel in) {
        this.mode = in.readString();
    }


    public static final Parcelable.Creator<Meta> CREATOR = new Parcelable.Creator<Meta>() {
        @Override
        public Meta createFromParcel(Parcel source) {
            return new Meta(source);
        }


        @Override
        public Meta[] newArray(int size) {
            return new Meta[size];
        }
    };
}

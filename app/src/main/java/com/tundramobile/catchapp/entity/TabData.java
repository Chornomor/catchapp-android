package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public class TabData implements Parcelable {

    private int imageDefault;
    private int imageSelected;
    private int title;
    private int defaultColor;
    private int selectedColor;
    private int badgeSelectedColor;
    private int badgeDefaultColor;
    private int badgeCount;

    public TabData() {
    }

    private TabData(Builder builder) {
        imageDefault = builder.imageDefault;
        imageSelected = builder.imageSelected;
        title = builder.title;
        defaultColor = builder.defaultColor;
        selectedColor = builder.selectedColor;
        badgeSelectedColor = builder.badgeSelectedColor;
        badgeDefaultColor = builder.badgeDefaultColor;
        badgeCount = builder.badgeCount;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private int imageDefault;
        private int imageSelected;
        private int title;
        private int defaultColor;
        private int selectedColor;
        private int badgeSelectedColor;
        private int badgeDefaultColor;
        private int badgeCount;

        private Builder() {
        }

        public Builder imageDefault(@DrawableRes int val) {
            imageDefault = val;
            return this;
        }

        public Builder imageSelected(@DrawableRes int val) {
            imageSelected = val;
            return this;
        }

        public Builder title(@StringRes int val) {
            title = val;
            return this;
        }

        public Builder defaultColor(@ColorRes int val) {
            defaultColor = val;
            return this;
        }

        public Builder selectedColor(@ColorRes int val) {
            selectedColor = val;
            return this;
        }

        public Builder badgeSelectedColor(@ColorRes int val) {
            badgeSelectedColor = val;
            return this;
        }

        public Builder badgeDefaultColor(@ColorRes int val) {
            badgeDefaultColor = val;
            return this;
        }

        public Builder badgeCount(int val) {
            badgeCount = val;
            return this;
        }

        public TabData build() {
            return new TabData(this);
        }
    }

    public int getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(@DrawableRes int imageDefault) {
        this.imageDefault = imageDefault;
    }

    public int getImageSelected() {
        return imageSelected;
    }

    public void setImageSelected(@DrawableRes int imageSelected) {
        this.imageSelected = imageSelected;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(@StringRes int title) {
        this.title = title;
    }

    public int getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(@ColorRes int defaultColor) {
        this.defaultColor = defaultColor;
    }

    public int getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(@ColorRes int selectedColor) {
        this.selectedColor = selectedColor;
    }

    public int getBadgeSelectedColor() {
        return badgeSelectedColor;
    }

    public void setBadgeSelectedColor(@ColorRes int badgeSelectedColor) {
        this.badgeSelectedColor = badgeSelectedColor;
    }

    public int getBadgeDefaultColor() {
        return badgeDefaultColor;
    }

    public void setBadgeDefaultColor(@ColorRes int badgeDefaultColor) {
        this.badgeDefaultColor = badgeDefaultColor;
    }

    public int getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(int badgeCount) {
        this.badgeCount = badgeCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.imageDefault);
        dest.writeInt(this.imageSelected);
        dest.writeInt(this.title);
        dest.writeInt(this.defaultColor);
        dest.writeInt(this.selectedColor);
        dest.writeInt(this.badgeSelectedColor);
        dest.writeInt(this.badgeDefaultColor);
        dest.writeInt(this.badgeCount);
    }

    protected TabData(Parcel in) {
        this.imageDefault = in.readInt();
        this.imageSelected = in.readInt();
        this.title = in.readInt();
        this.defaultColor = in.readInt();
        this.selectedColor = in.readInt();
        this.badgeSelectedColor = in.readInt();
        this.badgeDefaultColor = in.readInt();
        this.badgeCount = in.readInt();
    }

    public static final Creator<TabData> CREATOR = new Creator<TabData>() {
        @Override
        public TabData createFromParcel(Parcel source) {
            return new TabData(source);
        }

        @Override
        public TabData[] newArray(int size) {
            return new TabData[size];
        }
    };
}

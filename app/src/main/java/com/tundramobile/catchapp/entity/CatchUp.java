package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CatchUp implements Parcelable {
    public static final String TYPE_ONE = "one-on-one";
    public static final String TYPE_GROUP = "group";

    @SerializedName("catchup_id")
    private int id;

    @SerializedName("catchup_name")
    private String name;

    @SerializedName("catchup_initiatorMessage")
    private String initiatorMessage;

    @SerializedName("initiator_name")
    private String initiatorName;

    @SerializedName("event_id")
    private int eventId;

    @SerializedName("catchup_attending_count")
    private int attendingCount;

    @SerializedName("catchup_location_name")
    private String locationName;

    @SerializedName("google_places_id")
    private String googlePlacesId;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("initiator")
    private User user;

    @SerializedName("invited")
    private List<User> invited = new ArrayList<>();

    @SerializedName("attending")
    private List<User> attending = new ArrayList<>();

    @SerializedName("type")
    private String type;

    @SerializedName("events")
    private List<Event> events = new ArrayList<>();

    private int tabType;


    public CatchUp() {
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getInitiatorMessage() {
        return initiatorMessage;
    }


    public void setInitiatorMessage(String initiatorMessage) {
        this.initiatorMessage = initiatorMessage;
    }


    public String getInitiatorName() {
        return initiatorName;
    }


    public void setInitiatorName(String initiatorName) {
        this.initiatorName = initiatorName;
    }


    public int getEventId() {
        return eventId;
    }


    public void setEventId(int eventId) {
        this.eventId = eventId;
    }


    public int getAttendingCount() {
        return attendingCount;
    }


    public void setAttendingCount(int attendingCount) {
        this.attendingCount = attendingCount;
    }


    public String getLocationName() {
        return locationName;
    }


    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }


    public String getGooglePlacesId() {
        return googlePlacesId;
    }


    public void setGooglePlacesId(String googlePlacesId) {
        this.googlePlacesId = googlePlacesId;
    }


    public String getImageUrl() {
        return imageUrl;
    }


    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }


    public List<User> getInvited() {
        return invited;
    }


    public void setInvited(List<User> invited) {
        this.invited = invited;
    }


    public List<User> getAttending() {
        return attending;
    }


    public void setAttending(List<User> attending) {
        this.attending = attending;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public List<Event> getEvents() {
        return events;
    }


    public void setEvents(List<Event> events) {
        this.events = events;
    }


    public boolean isMine(long userId) {
        return getUser().getId() == userId;
    }


    public User getInvitedUser() {
        return getInvited().get(0);
    }


    public boolean isOneToOneSingleUser() {
        return type.equals(TYPE_ONE) && getInvited().size() <= 1;
    }


    public boolean isOneToOneMulti() {
        return type.equals(TYPE_ONE) && getInvited().size() > 1;
    }


    public boolean isGroup() {
        return type.equals(TYPE_GROUP);
    }


    public boolean isOneToOne() {
        return type.equals(TYPE_ONE);
    }

    public int getTabType() {
        return tabType;
    }

    public void setTabType(int tabType) {
        this.tabType = tabType;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.initiatorMessage);
        dest.writeString(this.initiatorName);
        dest.writeInt(this.eventId);
        dest.writeInt(this.attendingCount);
        dest.writeString(this.locationName);
        dest.writeString(this.googlePlacesId);
        dest.writeString(this.imageUrl);
        dest.writeParcelable(this.user, flags);
        dest.writeTypedList(this.invited);
        dest.writeTypedList(this.attending);
        dest.writeString(this.type);
        dest.writeTypedList(this.events);
    }


    protected CatchUp(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.initiatorMessage = in.readString();
        this.initiatorName = in.readString();
        this.eventId = in.readInt();
        this.attendingCount = in.readInt();
        this.locationName = in.readString();
        this.googlePlacesId = in.readString();
        this.imageUrl = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.invited = in.createTypedArrayList(User.CREATOR);
        this.attending = in.createTypedArrayList(User.CREATOR);
        this.type = in.readString();
        this.events = in.createTypedArrayList(Event.CREATOR);
    }


    public static final Creator<CatchUp> CREATOR = new Creator<CatchUp>() {
        @Override
        public CatchUp createFromParcel(Parcel source) {
            return new CatchUp(source);
        }


        @Override
        public CatchUp[] newArray(int size) {
            return new CatchUp[size];
        }
    };
}

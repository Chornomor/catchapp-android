package com.tundramobile.catchapp.entity.request;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.tundramobile.catchapp.entity.SimplePlace;
import com.tundramobile.catchapp.interfaces.OnEventReadyToCreateListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eugenetroyanskii on 19.04.17.
 */

public class CreateCatchAppBody {

    private static final String TYPE_ONE = "one-on-one";
    private static final String TYPE_GROUP = "group";

    private String eventName;
    private String message;
    private List<Integer> invitedUsers;
    private List<List<Long>> times;
    private String fundamentalType;
    private String googlePlacesID;
    private int location = 0;

    private transient OnEventReadyToCreateListener eventReadyToCreateListener;


    public CreateCatchAppBody(OnEventReadyToCreateListener eventReadyToCreateListener) {
        this.eventReadyToCreateListener = eventReadyToCreateListener;
    }


    public CreateCatchAppBody(@NonNull List<Integer> invitedUsers, @NonNull long startTime, @NonNull long endTime, String message,
                              @NonNull String eventName, @NonNull boolean isGroup, String googlePlacesID, int location) {
        this.invitedUsers = invitedUsers;
        this.times = new ArrayList<>();
        List<Long> time = new ArrayList<>();
        time.add(startTime);
        time.add(endTime);
        times.add(time);
        this.message = message;
        this.eventName = eventName;
        if (isGroup) {
            this.fundamentalType = TYPE_GROUP;
        } else {
            this.fundamentalType = TYPE_ONE;
        }
        this.googlePlacesID = googlePlacesID;
        this.location = location;
    }


    public List<Integer> getInvitedUsers() {
        return invitedUsers;
    }


    public void setInvitedUsers(List<Integer> invitedUsers) {
        this.invitedUsers = invitedUsers;
        isReadyToCreate();
    }


    public List<List<Long>> getTimes() {
        return times;
    }


    public void addTime(List<Long> times) {
        this.times.add(times);
    }


    public void setTimes(List<List<Long>> times) {
        this.times = times;
        isReadyToCreate();
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public String getEventName() {
        return eventName;
    }


    public void setEventName(String eventName) {
        this.eventName = eventName;
        isReadyToCreate();
    }


    public String getFundamentalType() {
        return fundamentalType;
    }


    public void setFundamentalType(boolean isGroup) {
        if (isGroup) {
            this.fundamentalType = TYPE_GROUP;
        } else {
            this.fundamentalType = TYPE_ONE;
        }
    }


    public String getGooglePlacesID() {
        return googlePlacesID;
    }


    public void setGooglePlacesID(String googlePlacesID) {
        this.googlePlacesID = googlePlacesID;
        isReadyToCreate();
    }


    public int getLocation() {
        return location;
    }


    public void setLocation(SimplePlace place) {
        this.location = place.getLocationId();
        this.googlePlacesID = place.getId();
        isReadyToCreate();
    }


    private boolean isReadyToCreate() {
        boolean isReady = false;

        isReady = eventName != null && !eventName.isEmpty();
        isReady = isReady && (invitedUsers != null && !invitedUsers.isEmpty());
        isReady = isReady && (times != null && !times.isEmpty());
        eventReadyToCreateListener.onEventReadyToCreate(isReady);

        return isReady;
    }
}
package com.tundramobile.catchapp.entity.response;

/**
 * Created by eugenetroyanskii on 31.03.17.
 */

public class BaseResponseModel {

    private String error = "";


    public String getError() {
        return error;
    }


    public void setError(String error) {
        this.error = error;
    }
}

package com.tundramobile.catchapp.entity.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListResponseModel<T> extends BaseResponseModel {

    @SerializedName("meta")
    Meta meta;

    @SerializedName("success")
    List<T> result = new ArrayList<>();


    public ListResponseModel() {
    }


    public Meta getMeta() {
        return meta;
    }


    public List<T> getResult() {
        return result;
    }


    public void setResult(List<T> result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "ListResponseModel{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

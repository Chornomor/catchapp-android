package com.tundramobile.catchapp.entity.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eugenetroyanskii on 03.04.17.
 */

public class CatchUpsCounters extends BaseResponseModel{

    @SerializedName("confirmed_count")
    private int confirmedCount;

    @SerializedName("history_count")
    private int historyCount;

    @SerializedName("ignored_count")
    private int ignoredCount;

    @SerializedName("incoming_count")
    private int incomingCount;

    @SerializedName("outgoing_count")
    private int outgoingCount;


    public int getConfirmedCount() {
        return confirmedCount;
    }


    public int getHistoryCount() {
        return historyCount;
    }


    public int getIgnoredCount() {
        return ignoredCount;
    }


    public int getIncomingCount() {
        return incomingCount;
    }


    public int getOutgoingCount() {
        return outgoingCount;
    }
}

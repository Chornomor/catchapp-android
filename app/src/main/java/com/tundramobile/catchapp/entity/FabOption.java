package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public class FabOption implements Parcelable {

    private int title;
    private int icon;


    public FabOption(@StringRes int title, @DrawableRes int icon) {
        this.title = title;
        this.icon = icon;
    }


    public FabOption() {
    }


    public int getTitle() {
        return title;
    }


    public void setTitle(@StringRes int title) {
        this.title = title;
    }


    public int getIcon() {
        return icon;
    }


    public void setIcon(@DrawableRes int icon) {
        this.icon = icon;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.title);
        dest.writeInt(this.icon);
    }


    protected FabOption(Parcel in) {
        this.title = in.readInt();
        this.icon = in.readInt();
    }


    public static final Parcelable.Creator<FabOption> CREATOR = new Parcelable.Creator<FabOption>() {
        @Override
        public FabOption createFromParcel(Parcel source) {
            return new FabOption(source);
        }


        @Override
        public FabOption[] newArray(int size) {
            return new FabOption[size];
        }
    };
}

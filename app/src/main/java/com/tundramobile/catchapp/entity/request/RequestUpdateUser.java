package com.tundramobile.catchapp.entity.request;

import com.tundramobile.catchapp.entity.User;

/**
 * Created by eugenetroyanskii on 03.04.17.
 */

public class RequestUpdateUser {

    private String token;
    private int userID;
    private int id;
    private UpdateUserModel user;


    public RequestUpdateUser(String token, User updateUserModel) {
        this.token = token;
        this.userID = updateUserModel.getId();
        this.id = updateUserModel.getId();

        UpdateUserModel user = new UpdateUserModel();
        user.homeEmail = updateUserModel.getHomeEmail();
        user.firstName = updateUserModel.getFirstName();
        user.lastName = updateUserModel.getLastName();
        user.deviceToken = updateUserModel.getDeviceToken();
        this.user = user;
    }


    public RequestUpdateUser(String token, User user, int profilePic) {
        this.token = token;
        this.userID = user.getId();
        this.id = user.getId();
        this.user = new UpdateUserModel(user, profilePic);
    }


    public String getToken() {
        return token;
    }


    public int getUserID() {
        return userID;
    }


    public int getId() {
        return id;
    }


    public UpdateUserModel getUser() {
        return user;
    }


    public class UpdateUserModel {
        private String firstName = "";
        private String lastName = "";
        private String deviceToken = "";
        private String homeEmail = "";
        private String workEmail = "";
        private String bio = "";
        private String jobTitle = "";
        private String skype = "";
        private int loginCount;
        private String address = "";
        private int permissionLocation;
        private int permissionNotification;
        private int permissionCalendarAny;
        private int permissionCalendarNative;
        private int permissionContacts;


        private UpdateUserModel() {
        }


        private UpdateUserModel(User user, int profilePic) {
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.deviceToken = user.getDeviceToken();
            this.homeEmail = user.getHomeEmail();
            this.workEmail = user.getWorkEmail();
            this.bio = user.getBio();
            this.jobTitle = user.getJobTitle();
            this.skype = user.getSkype();
            this.loginCount = user.getLoginCount();
            this.address = user.getAddress();
            this.permissionCalendarAny = user.getPermissionCalendarAny();
            this.permissionCalendarNative = user.getPermissionCalendarNative();
            this.permissionContacts = user.getPermissionContacts();
            this.permissionLocation = user.getPermissionLocation();
            this.permissionNotification = user.getPermissionNotification();
        }
    }
}

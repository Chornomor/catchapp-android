package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Event implements Parcelable {

    @SerializedName("event_id")
    private int id;

    @SerializedName("from")
    private long from;

    @SerializedName("to")
    private long to;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getFrom() {
        return from;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public long getTo() {
        return to;
    }

    public void setTo(long to) {
        this.to = to;
    }

    public Event() {
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (id != event.id) return false;
        if (from != event.from) return false;
        return to == event.to;

    }

    @Override
    public int hashCode() {
        long result = id;
        result = 31 * result + from;
        result = 31 * result + to;
        return (int) result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", from=" + from +
                ", to=" + to +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.from);
        dest.writeLong(this.to);
    }


    protected Event(Parcel in) {
        this.id = in.readInt();
        this.from = in.readLong();
        this.to = in.readLong();
    }


    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }


        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}

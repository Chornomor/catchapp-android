package com.tundramobile.catchapp.entity.response;

/**
 * Created by eugenetroyanskii on 03.04.17.
 */

public class LoginRespondOTP extends BaseResponseModel{

    private String success;
    private String token;
    private int id;


    public LoginRespondOTP(String token, int id) {
        this.token = token;
        this.id = id;
    }


    public String getSuccess() {
        return success;
    }


    public String getToken() {
        return token;
    }


    public int getId() {
        return id;
    }
}

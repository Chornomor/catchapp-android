package com.tundramobile.catchapp.entity.response;

/**
 * Created by eugenetroyanskii on 10.04.17.
 */

public class BooleanIsSuccessResponse extends BaseResponseModel{

    boolean success;


    public boolean isSuccess() {
        return success;
    }
}

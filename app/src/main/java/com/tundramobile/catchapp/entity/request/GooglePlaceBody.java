package com.tundramobile.catchapp.entity.request;

import com.google.gson.annotations.SerializedName;
import com.tundramobile.catchapp.entity.SimplePlace;

public class GooglePlaceBody {
    private String location;
    private String googlePlacesID;
    private int profilePic;
    private String lat;
    private String lon;
    private int priceRating;
    private String address;
    private String city;
    private String categoryCode;
    private String urlString;
    private String phoneNumber;
    @SerializedName("hours_isOpenNow")
    private int hoursIsOpenNow;
    @SerializedName("structuredHours")
    private String structuredHours;
    @SerializedName("hours_status")
    private String hoursStatus;

    public GooglePlaceBody(String location) {
        this.location = location;
    }

    public GooglePlaceBody(SimplePlace selectedPlace) {
        this.googlePlacesID = selectedPlace.getId();
        this.lat = ""+selectedPlace.getLatLng().latitude;
        this.lon = ""+selectedPlace.getLatLng().longitude;
        this.priceRating = selectedPlace.getPriceLevel();
        this.address = selectedPlace.getAddress();
        this.urlString = selectedPlace.getUrl();
        this.phoneNumber = selectedPlace.getPhone();
    }

    public String getLocation() {
    return location;
    }
    
    public void setLocation(String location) {
    this.location = location;
    }
    
    public String getGooglePlacesID() {
    return googlePlacesID;
    }
    
    public void setGooglePlacesID(String googlePlacesID) {
    this.googlePlacesID = googlePlacesID;
    }
    
    public int getProfilePic() {
    return profilePic;
    }
    
    public void setProfilePic(int profilePic) {
    this.profilePic = profilePic;
    }
    
    public String getLat() {
    return lat;
    }
    
    public void setLat(String lat) {
    this.lat = lat;
    }
    
    public String getLon() {
    return lon;
    }
    
    public void setLon(String lon) {
    this.lon = lon;
    }
    
    public int getPriceRating() {
    return priceRating;
    }
    
    public void setPriceRating(int priceRating) {
    this.priceRating = priceRating;
    }
    
    public String getAddress() {
    return address;
    }
    
    public void setAddress(String address) {
    this.address = address;
    }
    
    public String getCity() {
    return city;
    }
    
    public void setCity(String city) {
    this.city = city;
    }
    
    public String getCategoryCode() {
    return categoryCode;
    }
    
    public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
    }
    
    public String getUrlString() {
    return urlString;
    }
    
    public void setUrlString(String urlString) {
    this.urlString = urlString;
    }
    
    public String getPhoneNumber() {
    return phoneNumber;
    }
    
    public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    }
    
    public int getHoursIsOpenNow() {
    return hoursIsOpenNow;
    }
    
    public void setHoursIsOpenNow(int hoursIsOpenNow) {
    this.hoursIsOpenNow = hoursIsOpenNow;
    }
    
    public String getStructuredHours() {
    return structuredHours;
    }
    
    public void setStructuredHours(String structuredHours) {
    this.structuredHours = structuredHours;
    }
    
    public String getHoursStatus() {
    return hoursStatus;
    }
    
    public void setHoursStatus(String hoursStatus) {
    this.hoursStatus = hoursStatus;
    }

}

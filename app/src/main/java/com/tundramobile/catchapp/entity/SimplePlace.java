package com.tundramobile.catchapp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Sviatoslav Zaitsev on 19.04.17.
 */

public class SimplePlace implements Parcelable {
    private String primaryText;
    private String secondaryText;

    private int locationId;
    private String id;
    private String name;
    private String address;
    private String phone;
    private String url;
    private LatLng latLng;
    private float rating;
    private int priceLevel;


    public SimplePlace(int locationId, String locationName) {
        this.locationId = locationId;
        this.name = locationName;
    }

    public SimplePlace(String primaryText, String secondaryText, Place place) {
        this.primaryText = primaryText;
        this.secondaryText = secondaryText;

        this.id = place.getId();
        this.name = place.getName().toString();
        this.address = place.getAddress().toString();
        this.phone = place.getPhoneNumber().toString();
        this.url = place.getWebsiteUri() != null ? place.getWebsiteUri().toString() : "";
        this.latLng = place.getLatLng();
        this.rating = place.getRating();
        this.priceLevel = place.getPriceLevel();
    }


    protected SimplePlace(Parcel in) {
        primaryText = in.readString();
        secondaryText = in.readString();
        id = in.readString();
        name = in.readString();
        address = in.readString();
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        rating = in.readFloat();
    }

    public static final Creator<SimplePlace> CREATOR = new Creator<SimplePlace>() {
        @Override
        public SimplePlace createFromParcel(Parcel in) {
            return new SimplePlace(in);
        }


        @Override
        public SimplePlace[] newArray(int size) {
            return new SimplePlace[size];
        }
    };

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }


    public int getLocationId() {
        return locationId;
    }


    public String getPrimaryText() {
        return primaryText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public float getRating() {
        return rating;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public String getPhone() {
        return phone;
    }

    public String getUrl() {
        return url;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(primaryText);
        parcel.writeString(secondaryText);
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(address);
        parcel.writeParcelable(latLng, i);
        parcel.writeFloat(rating);
    }
}

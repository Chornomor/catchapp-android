package com.tundramobile.catchapp.entity.request;

/**
 * Created by eugenetroyanskii on 05.04.17.
 */

public class RequestContactBody {

    private String contacts = "";


    public String getContacts() {
        return contacts;
    }


    public void setContacts(String contacts) {
        this.contacts = contacts;
    }


    public void addContact(String contact) {
        if (contact == null) {
            return;
        }
        if (contacts.isEmpty()) {
            contacts = contact;
        } else {
            contacts = contacts + "," + contact;
        }
    }
}

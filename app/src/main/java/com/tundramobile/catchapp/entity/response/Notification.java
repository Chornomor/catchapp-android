package com.tundramobile.catchapp.entity.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("notificationType")
    @Expose
    private String notificationType;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("attachmentType")
    @Expose
    private String attachmentType;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("catch_up")
    @Expose
    private CatchUp catchUp;

    public int getId() {
        return id;
    }

    public String getSummary() {
        return summary;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public String getCreated() {
        return created;
    }

    public String getModified() {
        return modified;
    }

    public String getExpires() {
        return expires;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public User getUser() {
        return user;
    }

    public CatchUp getCatchUp() {
        return catchUp;
    }

    public class CatchUp {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("name")
        @Expose
        private String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public class User {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("profilePic_url")
        @Expose
        private String profilePicUrl;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getProfilePicUrl() {
            return profilePicUrl;
        }
    }
}
package com.tundramobile.catchapp.entity;

import com.google.gson.annotations.SerializedName;

public class CatchUpDetail {

    @SerializedName("google_place")
    private GooglePlace googlePlace;


    public GooglePlace getGooglePlace() {
        return googlePlace;
    }
}

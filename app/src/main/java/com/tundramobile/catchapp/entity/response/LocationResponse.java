package com.tundramobile.catchapp.entity.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sviatoslav Zaitsev on 20.04.17.
 */

public class LocationResponse extends BaseResponseModel {
    @SerializedName("success")
    @Expose
    private int success;

    public int getSuccess() {
        return success;
    }
}

package com.tundramobile.catchapp.entity.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 07.04.17.
 */

public class CreateGroupBody {

    private String name;

    @SerializedName("profilePic_url")
    private String groupIcon = "";

    /*list ids of members for this groups*/
    private ArrayList<Integer> members;


    public String getName() {
        return name;
    }


    public String getGroupIcon() {
        return groupIcon;
    }


    public ArrayList<Integer> getMembers() {
        return members;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }


    public void setMembers(ArrayList<Integer> members) {
        this.members = members;
    }


    @Override
    public String toString() {
        return "CreateGroupBody{" +
                "name='" + name + '\'' +
                ", groupIcon='" + groupIcon + '\'' +
                ", members=" + members +
                '}';
    }
}

package com.tundramobile.catchapp.entity.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eugenetroyanskii on 19.04.17.
 */

public class CreateCatchUpResponse extends BaseResponseModel{

    @SerializedName("success")
    private CreatedCatchUp result;



    public long getInitiatorId() {
        return result.initiatorId;
    }


    public long getLocationId() {
        return result.locationId;
    }


    public String getGooglePlaceId() {
        return result.googlePlaceId;
    }


    public String getInitialMessage() {
        return result.initialMessage;
    }


    public String getUpdatedAt() {
        return result.updatedAt;
    }


    public String getCreatedAt() {
        return result.createdAt;
    }


    public long getId() {
        return result.id;
    }


    public String getType() {
        return result.type;
    }


    public String getName() {
        return result.name;
    }

    private class CreatedCatchUp {

        @SerializedName("initiator_id")
        long initiatorId;
        @SerializedName("location_id")
        long locationId;
        @SerializedName("google_place_id")
        String googlePlaceId;
        @SerializedName("initial_message")
        String initialMessage;
        @SerializedName("updated_at")
        String updatedAt;
        @SerializedName("created_at")
        String createdAt;
        long id;
        String type;
        String name;
    }
}

package com.tundramobile.catchapp.entity;

public class Schedule {

    private int day;
    private String time;


    public int getDay() {
        return day;
    }


    public String getTime() {
        return time;
    }
}

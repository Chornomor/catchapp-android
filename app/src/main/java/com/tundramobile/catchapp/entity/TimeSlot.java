package com.tundramobile.catchapp.entity;

/**
 * Created by eugenetroyanskii on 10.04.17.
 */

public class TimeSlot {
    private CatchUp catchUp;
    private long slotId;
    private long from;
    private long to;


    public long getSlotId() {
        return slotId;
    }


    public void setSlotId(long slotId) {
        this.slotId = slotId;
    }


    public long getFrom() {
        return from;
    }


    public void setFrom(long from) {
        this.from = from;
    }


    public long getTo() {
        return to;
    }


    public void setTo(long to) {
        this.to = to;
    }

    public CatchUp getCatchUp() {
        return catchUp;
    }

    public void setCatchUp(CatchUp catchUp) {
        this.catchUp = catchUp;
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "catchUpId=" + catchUp.getId() +
                ", slotId=" + slotId +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}

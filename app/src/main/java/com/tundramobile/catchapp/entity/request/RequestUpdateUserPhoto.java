package com.tundramobile.catchapp.entity.request;

import com.tundramobile.catchapp.entity.User;

/**
 * Created by eugenetroyanskii on 28.04.17.
 */

public class RequestUpdateUserPhoto {

    private String token;
    private int userID;
    private int id;
    private UpdateUserPhotoModel user;


    public RequestUpdateUserPhoto(String token, User user, int profilePic) {
        this.token = token;
        this.userID = user.getId();
        this.id = user.getId();
        this.user = new UpdateUserPhotoModel(user, profilePic);
    }


    public String getToken() {
        return token;
    }


    public int getUserID() {
        return userID;
    }


    public int getId() {
        return id;
    }


    public UpdateUserPhotoModel getUser() {
        return user;
    }


    public class UpdateUserPhotoModel {
        private String firstName = "";
        private String lastName = "";
        private String deviceToken = "";
        private int profilePic;
        private String homeEmail = "";
        private String workEmail = "";
        private String bio = "";
        private String jobTitle = "";
        private String skype = "";
        private int loginCount;
        private String address = "";
        private int permissionLocation;
        private int permissionNotification;
        private int permissionCalendarAny;
        private int permissionCalendarNative;
        private int permissionContacts;


        private UpdateUserPhotoModel() {
        }


        private UpdateUserPhotoModel(User user, int profilePic) {
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.deviceToken = user.getDeviceToken();
            this.profilePic = profilePic;
            this.homeEmail = user.getHomeEmail();
            this.workEmail = user.getWorkEmail();
            this.bio = user.getBio();
            this.jobTitle = user.getJobTitle();
            this.skype = user.getSkype();
            this.loginCount = user.getLoginCount();
            this.address = user.getAddress();
            this.permissionCalendarAny = user.getPermissionCalendarAny();
            this.permissionCalendarNative = user.getPermissionCalendarNative();
            this.permissionContacts = user.getPermissionContacts();
            this.permissionLocation = user.getPermissionLocation();
            this.permissionNotification = user.getPermissionNotification();
        }
    }
}
package com.tundramobile.catchapp.entity.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.tundramobile.catchapp.entity.Contact;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by eugenetroyanskii on 05.04.17.
 */

public class ContactModel extends ListResponseModel<Contact> implements Parcelable {

    private ArrayList<Contact> unrecognised = new ArrayList<>();


    public ArrayList<Contact> getUnrecognised() {
        return unrecognised;
    }


    /*group both collection to one list for adapter*/
    public ArrayList<Contact> getAllContacts() {
        ArrayList<Contact> allContacts = new ArrayList<>(getResult());
        allContacts.addAll(unrecognised);
        return allContacts;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.unrecognised);
    }


    public ContactModel() {
    }


    protected ContactModel(Parcel in) {
        this.unrecognised = in.createTypedArrayList(Contact.CREATOR);
    }


    public static final Parcelable.Creator<ContactModel> CREATOR = new Parcelable.Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel source) {
            return new ContactModel(source);
        }


        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };
}

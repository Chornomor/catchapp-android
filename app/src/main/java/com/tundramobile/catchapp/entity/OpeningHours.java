package com.tundramobile.catchapp.entity;

public class OpeningHours {

    private Schedule open;
    private Schedule close;


    public Schedule getOpen() {
        return open;
    }


    public Schedule getClose() {
        return close;
    }
}
